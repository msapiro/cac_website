<?php
// Login not required for this page
require '../support/config.php';
require CLASSLOADER;

// if you want this page to have a custom title, set it here.
// leave it blank to use the default title for the site.
// this is the text that appears on the page tab in most browsers.
$title = '';
$description = 'California Alpine Club Membership Application';

require_once '../../support/functions.php';
require_once '../../support/formFunctions.php';
require 'includes/tools.php';

$badKey = false;
if (isset($_POST['key'])) {
	// the form was submitted.
	$key = trim($_POST['key']);
	$applicant = new Applicants();
	$applicant->getByKey($key);
	if (!$applicant->applicantID) {
		$badKey = true;
	} else {
		// redirect to the correct page
		$link = 'applyDetail.php?key=' . $key;
		header("Location: $link");
	}
}

$dues = array();
$entryFee = array();
foreach (['RG','JT','ST'] as $class) {
	$oClass = new Classes($class);
	$dues[$class] = $oClass->dues;
	$entryFee[$class] = $oClass->entryFee;
}
$dues['JTadd'] = $dues['RG'];
$entryFee['JTadd'] = $entryFee['RG'];

start_page($title, $description);
?>
<script>
"use strict";

$(function() {
    // do stuff when DOM is ready
    $("#email").focus();

	$("#btnSubmit").click(function() {
		 // check email; if possible create application and send code
		var email = $("#email").val();
		var reinstate =$('input[name="reinstate"]:checked').val();
    	$.post('ajax/createApply.php', {email: email, reinstate: reinstate}, function(data) {
			alert(data.msg);
    	}, "json");
	});

});

</script>
<?php
start_content_apply(); // in tools.php
?>

<div class="row">
	<div class="col-xl-10 offset-xl-1">

		<h2 class="mt-4">New Member Application</h2>

		<div class="card well">
			<div class="card-body">

				<ul>
					<li>When you first start the process, provide your email address and indicate the type. A
						code will be emailed to you.</li>
				</ul>

				<form autocomplete="off">
<?php
edit_field('Email', 'email', false, 100, false, false, 'email', 'col-md-3 text-md-end', 'col-md-7 col-lg-5');
?>

					<div class="row align-items-center mb-3">
						<label class="text-md-end col-md-3">
							<b>Type</b>
						</label>
						<div class="col-md-7">
							<div class="radio">
								<label>
									<input type="radio" name="reinstate" value="0" checked>&nbsp;Join as new member(s)
								</label>
							</div>

							<div class="radio">
								<label>
									<input type="radio" name="reinstate" value="1">&nbsp;Reinstate an expired or
										resigned membership
								</label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-5 offset-md-3">
							<button id="btnSubmit" type="button" class="btn btn-primary">Submit Email</button>
						</div>
					</div>
				</form>
			</div>
		</div>

		<div class="card well mt-3">
			<div class="card-body">

				<ul>
					<li>If you are returning to view or edit a previous application, provide the code that you
						received by email.</li>
				</ul>

				<form method="post" action="index.php" autocomplete="off">
					<!-- submit to this page -->

<?php
edit_field('Code', 'key', false, 100, false, false, false, 'col-md-3 text-md-end', 'col-md-7 col-lg-5');
?>

					<div class="row">
						<div class="col-md-5 offset-md-3">
							<button type="submit" class="btn btn-primary">Submit Code</button>
						</div>
					</div>
				</form>
<?php if ($badKey): ?>
	<div class="alert alert-danger mt-3">Application not found for the code you submitted!</div>
<?php endif; ?>
</div>
		</div>

		<h4 class="mt-3">Process Overview</h4>

		<p>You can start filling out your application form any time, save it, and continue it later.
			Your application will not be reviewed until you finalize and submit it.</p>

		<p>After you finalize and submit your application, it will be reviewed. The membership chair
			may contact you to clarify some items on your application. If it is approved, you will
			receive an email with a link to your invoice for entry fee and dues.</p>

		<p>Your membership will be activated after payment of fees online, or after receipt of a
			check if you choose to pay by mail.</p>

		<h4>Reinstatements</h4>

		<p>
			If your membership expired (or you resigned in good standing) less than 5 years ago, you can
			apply for reinstatement. There is no Entry Fee or Work/Social Activities requirement, but
			you will need two current members as sponsors. If your membership expired this year
			<a href="../account/renew.php">you can still renew</a> (login required). There is no
			need to apply for reinstatement.
		</p>

		<h4>Work and Social Activities</h4>

		<p>You need to attend two Club events, one of which must be a "work" activity as approved by
			the CAC leadership.</p>

		<h4>Sponsors</h4>

		<p>Requirements are to obtain two current members to sponsor your application. They may not
			be two parts of the same joint membership.</p>

		<h4>Fees</h4>

		<div class="row">
			<div class="col-lg-8">

				<table class="table table-striped">
					<thead>
						<tr>
							<th>Membership Type</th>
							<th class="text-end">Entry Fee</th>
							<th class="text-end">Annual Dues</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Regular Member</td>
							<td class="text-end">$<?= $entryFee['RG'] ?></td>
							<td class="text-end">$<?= $dues['RG'] ?></td>
						</tr>
						<tr>
							<td>Joint Members (two new members)</td>
							<td class="text-end">$<?= $entryFee['JT'] ?></td>
							<td class="text-end">$<?= $dues['JT'] ?></td>
						</tr>
						<tr>
							<td>Joint Member (add to current member)</td>
							<td class="text-end">$<?= $entryFee['JTadd'] ?></td>
							<td class="text-end">$<?= $dues['JTadd'] ?></td>
						</tr>
						<tr>
							<td>Student Member (age 18-26 on April 1)</td>
							<td class="text-end">$<?= $entryFee['ST'] ?></td>
							<td class="text-end">$<?= $dues['ST'] ?></td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="3">
								<ul>
									<li>Applications submitted Jan 1 - June 30 pay full annual dues.</li>
									<li>Applications submitted July 1 - Dec 31 pay one-half annual dues.</li>
									<li>Entry fee may be reduced by Associate Member Fees paid, for example as a
										guest at Alpine Lodge.</li>
									<li>Members who previously held Senior Regular or Senior Joint memberships can
										be reinstated with Senior status and reduced fees.</li>
								</ul>
							</td>
						</tr>
					</tfoot>
				</table>

			</div>
		</div>

		<h4>Questions?</h4>

		<p>
			If you have any questions, please contact <span class="emlink" data-user="membership"> the
				membership chair.</span>
		</p>

	</div>
</div>
<?php end_page(); ?>
