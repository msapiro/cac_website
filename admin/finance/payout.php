<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';
include 'includes/tools.php';

$payoutID = $_GET['id'];

// stripe credentials
require '/www/cac/data/credentials/stripe.php';

/**
 *
 * from config file:
 * @var string $stripeSecretKey
 * @var string $stripeApiVersion
 * */

$stripeClient = new \Stripe\StripeClient([
	'api_key' => $stripeSecretKey,
	'stripe_version' => $stripeApiVersion
]);

try {
	$transactions = $stripeClient->balanceTransactions->all( ["payout" => $payoutID] );
} catch (\Stripe\Exception\ApiErrorException $e) {
	$errMsg = 'Stripe returned the following error while retrieving the Balance Transactions:'
		. '<br><br>' . $e;
	error('Error', $errMsg);
	exit;
}

// hold our spreadsheet data in an array named table
// $table = [stripeID, Date, Total, Club Amount, FDN Amount, Stripe Fee, Net, Item]
$table = array();

$clubTotalGrand = 0;
$clubFeeGrand = 0;
$fdnTotalGrand = 0;
$fdnFeeGrand = 0;

foreach ($transactions->autoPagingIterator() as $transaction) {
	$type = $transaction->type;

	if ($type == 'payout') {
		// this balance transaction is from the payout
		$dateAvailable = date('Y-m-d', $transaction->available_on);
		$payoutAmount = abs($transaction->amount);

	} elseif ($type == 'refund') {
		// This balance transaction is a refund. There will
		// be no stripeID because it wasn't processed here.
		$stripeID = 'n/a';
		$clubTotal = $transaction->amount/100;
		$fdnTotal =0;
		$clubFee = $transaction->fee / 100;
		$fdnFee = 0;

		$clubTotalGrand += $clubTotal;
		$fdnTotalGrand += $fdnTotal;
		$clubFeeGrand += $clubFee;
		$fdnFeeGrand += $fdnFee;
		// [stripeID, Date, Total, Club Amount, FDN Amount, Stripe Fee, Club Net, Fdn Net, Item]
		$tRow = array();
		$tRow[] = $stripeID;
		$tRow[] = date('Y-m-d', $transaction->created);
		$tRow[] = currencyFormat($transaction->amount, true);
		$tRow[] = currencyFormat($transaction->amount, true);
		$tRow[] = '';
		$tRow[] = currencyFormat($transaction->fee, true);;
		$tRow[] = currencyFormat($transaction->amount - $transaction->fee, true);
		$tRow[] = '';
		$tRow[] = 'refund';
		$table[] = $tRow;
	} else {
		// these balance transactions are from charges
		$application = false;

		// the metadata is attached to the charge
		$chargeID = $transaction->source;
		$charge = $stripeClient->charges->retrieve($chargeID, []);

		if (isset($charge->metadata->stripeID)) {
			// newer charges have the stripeID in the metadata
			$stripeID = $charge->metadata->stripeID;

			// all applications say so in the metadata
			if (isset($charge->metadata->category)
				&& ($charge->metadata->category == 'application'
				|| $charge->metadata->category == 'reinstate')) {
				$application = true;
			}
		} else {
			// this may be an older charge. we need to get stripeID from the checkout session.
			$session = $stripeClient->checkout->sessions->all([
				'payment_intent' => $charge->payment_intent
			]);
			$stripeID = $session['data'][0]['client_reference_id'];
		}

		$clubTotal = 0;
		$fdnTotal = 0;
		$clubFee = 0;
		$fdnFee = 0;

		if ($application) {
			// simpler than the renewals. no CAC Foundation involvement
			$applicant = new Applicants();
			$applicant->getByStripeID($stripeID);
			$clubTotal = $applicant->entryFee + $applicant->dues;
			$clubFee = $transaction->fee / 100;
			if ($applicant->entryFee && $applicant->dues) {
				// apportion the stripe fee between the entryFee and the dues
				$applyFee = number_format(($applicant->entryFee / $clubTotal) * $clubFee, 2);
				$duesFee = $clubFee - $applyFee;
			} else {
				$applyFee = $clubFee; // only one will we used
				$duesFee = $clubFee;
			}

			$clubTotalGrand += $clubTotal;
			$clubFeeGrand += $clubFee;

			// [stripeID, Date, Total, Club Amount, FDN Amount, Stripe Fee, Club Net, Fdn Net, Item]
			$first = true;
			if (currencyFormat($applicant->entryFee)) {
				$tRow = array();
				$tRow[] = $stripeID;
				$tRow[] = date('Y-m-d', $transaction->created);
				$tRow[] = currencyFormat($transaction->amount, true);
				$tRow[] = currencyFormat($applicant->entryFee);
				$tRow[] = '';
				$tRow[] = currencyFormat($applyFee);
				$tRow[] = currencyFormat($applicant->entryFee - $applyFee);
				$tRow[] = '';
				$tRow[] = $applicant->applyType . ' entry fee';
				$table[] = $tRow;
				$first = false;
			}

			// [stripeID, Date, Total, Club Amount, FDN Amount, Stripe Fee, Club Net, Fdn Net, Item]
			if ($applicant->dues) {
				$tRow = array();
				if ($first) {
					$tRow[0] = $stripeID;
					$tRow[1] = date('Y-m-d', $transaction->created);
					$tRow[2] = currencyFormat($transaction->amount, true);
				} else {
					$tRow[0] = '';
					$tRow[1] = '';
					$tRow[2] = '';
				}
				$tRow[3] = currencyFormat($applicant->dues);
				$tRow[4] = '';
				$tRow[5] = currencyFormat($duesFee);
				$tRow[6] = currencyFormat($applicant->dues - $duesFee);
				$tRow[7] = '';
				$tRow[8] = $applicant->applyType . ' dues';
				$table[] = $tRow;
			}

		} else {
			// a renewal, which might include donations to the general fund and/or the Foundation
			$memberFee = 0;
			$gfFee = 0;

			$memberTrans = new MemberTrans;
			$memberTrans->getByStripeID($stripeID);
			if ($memberTrans->mTransID) {
				$clubTotal = $memberTrans->amount;
			}

			$donationGF = new DonationsGF();
			$donationGF->getByStripeID($stripeID);
			if ($donationGF->donationID) {
				$clubTotal += $donationGF->amount;
			}

			$donationFDN = new DonationsFDN();
			$donationFDN->getByStripeID($stripeID);
			if ($donationFDN->donationID) {
				$fdnTotal = $donationFDN->amount;
			}

			if ($clubTotal && $fdnTotal) {
				// apportion the stripe fee between the club and foundation
				// assign 2.9% stripe fee to Foundation donation
				$fdnFee = number_format(0.029 * $fdnTotal, 2);
				// the rest is assigned to the club
				$clubFee =  ($transaction->fee / 100) - $fdnFee;
			} elseif($clubTotal) {
				$clubFee = $transaction->fee / 100;
			} else {
				$fdnFee = $transaction->fee / 100;
			}

			if ($memberTrans->mTransID && $donationGF->donationID) {
				// apportion the club stripe fee between member dues and general fund donation
				// assign 2.9% stripe fee to Gen Fund donation
				$gfFee = number_format(0.029 * $donationGF->amount, 2);
				// the rest is assigned to the membership dues
				$memberFee = $clubFee - $gfFee;
			} elseif ($memberTrans->mTransID) {
				$memberFee = $clubFee;
			} else {
				$gfFee = $clubFee;
			}

			$clubTotalGrand += $clubTotal;
			$fdnTotalGrand += $fdnTotal;
			$clubFeeGrand += $clubFee;
			$fdnFeeGrand += $fdnFee;

			// [stripeID, Date, Total, Club Amount, FDN Amount, Stripe Fee, Club Net, Fdn Net, Item]
			$first = true;
			if ($memberFee) {
				$tRow = array();
				$tRow[] = $stripeID;
				$tRow[] = date('Y-m-d', $transaction->created);
				$tRow[] = currencyFormat($transaction->amount, true);
				$tRow[] = currencyFormat($memberTrans->amount);
				$tRow[] = '';
				$tRow[] = currencyFormat($memberFee);
				$tRow[] = currencyFormat($memberTrans->amount - $memberFee);
				$tRow[] = '';
				$tRow[] = $memberTrans->membershipType . ' ' . $memberTrans->action;
				$table[] = $tRow;
				$first = false;
			}

			// [stripeID, Date, Total, Club Amount, FDN Amount, Stripe Fee, Club Net, Fdn Net, Item]
			if ($gfFee) {
				$tRow = array();
				if ($first) {
					$tRow[0] = $stripeID;
					$tRow[1] = date('Y-m-d', $transaction->created);
					$tRow[2] = currencyFormat($transaction->amount, true);
				} else {
					$tRow[0] = '';
					$tRow[1] = '';
					$tRow[2] = '';
				}
				$tRow[3] = currencyFormat($donationGF->amount);
				$tRow[4] = '';
				$tRow[5] = currencyFormat($gfFee);
				$tRow[6] = currencyFormat($donationGF->amount - $gfFee);
				$tRow[7] = '';
				$tRow[8] = 'General Fund donation';
				$table[] = $tRow;
				$first = false;
			}

			// [stripeID, Date, Total, Club Amount, FDN Amount, Stripe Fee, Club Net, Fdn Net, Item]
			if ($fdnFee) {
				$tRow = array();

				if ($first) {
					$tRow[0] = $stripeID;
					$tRow[1] = date('Y-m-d', $transaction->created);
					$tRow[2] = currencyFormat($transaction->amount, true);
				} else {
					$tRow[0] = '';
					$tRow[1] = '';
					$tRow[2] = '';
				}
					$tRow[3] = '';
					$tRow[4] = currencyFormat($donationFDN->amount);
					$tRow[5] = currencyFormat($fdnFee);
					$tRow[6] = '';
					$tRow[7] =  currencyFormat($donationFDN->amount - $fdnFee);
					$tRow[8] = 'CAC Foundation donation';
					$table[] = $tRow;
			}

		}
	}
}

start_page('CAC Finance');
start_content();
toolbar();
?>

<div class="row justify-content-center">
	<div class="col-xl-11">

	<h3 class="pageheader">Payout Detail</h3>

	<table  class="table table-sm table-striped">
		<thead>
		<tr>
			<th class="text-center">stripeID</th>
			<th class="text-center">Date</th>
			<th class="text-end">Total</th>
			<th class="text-end">Club</th>
			<th class="text-end">Fdn</th>
			<th class="text-end">Stripe Fee</th>
			<th class="text-end">Club Net</th>
			<th class="text-end">Fdn Net</th>
			<th>Item</th>
		</tr>
		</thead>
		<tbody>
	<?php foreach ($table as $display): ?>
		<tr>
		<td class="text-center"><?= $display[0] ?></td>
		<td class="text-center"><?= $display[1] ?></td>
		<td class="text-end"><?= $display[2] ?></td>
		<td class="text-end"><?= $display[3] ?></td>
		<td class="text-end"><?= $display[4] ?></td>
		<td class="text-end"><?= $display[5] ?></td>
		<td class="text-end"><?= $display[6] ?></td>
		<td class="text-end"><?= $display[7] ?></td>
		<td><?= $display[8] ?></td>
		</tr>

	<?php endforeach; ?>
		</tbody>

		<tfoot>
		<tr>
		<td class="text-center"><b>Deposit</b></td>
		<td class="text-center"><?= $dateAvailable ?></td>
		<td class="text-end"><?= currencyFormat($clubTotalGrand + $fdnTotalGrand) ?></td>
		<td class="text-end"><?= currencyFormat($clubTotalGrand) ?></td>
		<td class="text-end"><?= currencyFormat($fdnTotalGrand) ?></td>
		<td class="text-end"><?= currencyFormat($clubFeeGrand + $fdnFeeGrand) ?></td>
		<td class="text-end"><?= currencyFormat($clubTotalGrand - $clubFeeGrand) ?></td>
		<td class="text-end"><?= currencyFormat($fdnTotalGrand - $fdnFeeGrand) ?></td>
		<td><?= currencyFormat($payoutAmount, true) ?> Net Deposit</td>
		</tr>
		</tfoot>

	</table>

	</div>
</div>

<?php end_page();

function currencyFormat ($num, $pennies = false) {
	if ($pennies) $num = $num / 100;
	if ($num == 0) {
		return '';
	} else {
		return '$' . number_format($num, 2);
	}
}
?>
