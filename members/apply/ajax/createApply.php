<?php
// login not required
require '../../support/config.php';
require CLASSLOADER;

require_once '../../../../wp-load.php';
use PHPMailer\PHPMailer\PHPMailer;

// called from index.php to test submitted email address and
// create the application if needed and send the code to the user.

$data = array();

if ( empty(trim($_POST['email'])) ) {
	$data['msg'] = 'No email address submitted.';
	echo json_encode($data);
	exit;
}

$email = trim($_POST['email']);

if (! filter_var($email, FILTER_VALIDATE_EMAIL)) {
	$data['msg'] = '"' . $email . '"' . " does not appear to be a valid email address.\n\n";
	$data['msg'] .= "If you disagree, please ask membership@californiaalpineclub.org for help.";
	echo json_encode($data);
	exit;
}

$applicant = new Applicants();

$member = new Members();

if (booly($_POST['reinstate'])) {
	// reinstatement application. see if the former member can be found and qualifies
	$memberArray = $member->getByEmail($email);

	switch (count($memberArray)) {
		case 0:
			$data['msg'] = '"' . $email . '"' . " is not associated with a CAC member.\n\n";
			$data['msg'] .= "Please ask membership@californiaalpineclub.org for help.";
			break;
		case 2:
			// Two members, but is this a joint membership.
			if ($memberArray[0]->membershipID != $memberArray[1]->membershipID) {
				// Different memberships. Treat as in default.
				$data['msg'] = '"' . $email . '"' . " is associated with multiple CAC members.\n\n";
				$data['msg'] .= "Please ask membership@californiaalpineclub.org for help.";
				break;
			} // Joint membership. Fall through.
		case 1:
			// exactly one member
			$member = $memberArray[0];
			$membership = new Memberships($member->membershipID);

			if (! $membership->isExpired()) {
				$data['msg'] = '"' . $email . '"' . " is associated with a current CAC member (not expired).\n\n";
				$data['msg'] .= "Please ask membership@californiaalpineclub.org for help.";
			} elseif (! $membership->isExpired(false, 'P9M')) {
				$data['msg'] = "Your membership is recently expired, but you can still log in and renew.";
				$data['msg'] .= " There is no need to submit an application.\n\n";
				$data['msg'] .= "If you need help, ask membership@californiaalpineclub.org.";
			} elseif ($membership->isExpired(false, 'P5Y')) {
				$data['msg'] = "Your membership expired more than 5 years ago.\n\n";
				$data['msg'] .= "Please ask membership@californiaalpineclub.org for help.";
			} else {
				// set up an application for reinstatement
				$applicant->addToMemberID = $member->memberID; // store the memberID here for use on other pages
				$applicant->applyType = $membership->class . 're';
				if ($membership->class == 'OR') $applicant->applyType = 'RGre'; // orphans default to RG
				foreach (['first', 'm_i', 'last', 'email', 'phone', 'mobile', 'dob'] as $field) $applicant->$field = $member->$field;
				foreach (['street', 'city', 'state', 'zip'] as $field) $applicant->$field = $membership->$field;
				if ($membership->class == 'JT' || $membership->class == 'JS') {
					// add the joint member to the application
					$jointMembers = $membership->getJointMembers();
					if (count($jointMembers) == 2) {
						foreach ($jointMembers as $jointMember) {
							if ($jointMember->memberID == $member->memberID) continue;
							foreach (['first', 'm_i', 'last', 'email', 'phone', 'mobile', 'dob'] as $field) {
								$appField = $field . '2';
								$applicant->$appField = $jointMember->$field;
							}
						}
					}
				}
				$applicant->dbSave(); // create the new application and set a code
				// combine applicantID with code for simplicity
				$key = $applicant->applicantID . ':' .  $applicant->code;
				$link = home_url() . '/cac/members/apply/reapply.php?key=' . $key;
				sendCode();
			}
			break;
		default:
			$data['msg'] = '"' . $email . '"' . " is associated with multiple CAC members.\n\n";
			$data['msg'] .= "Please ask membership@californiaalpineclub.org for help.";
			break;
	}

} else {
	// new member application. make sure this email is not already used
	$in_use_cac = $member->getByEmail($email);

	// also make sure it isn't in use by WordPress
	$in_use_wp = get_user_by('email', $email);

	if ($in_use_cac) {
		$data['msg'] = '"' . $email . '"' . " is already being used by a CAC member.\n\n";
		$data['msg'] .= "Please ask membership@californiaalpineclub.org for help.";

	} elseif ($in_use_wp) {
		$data['msg'] = '"' . $email . '"' . " is already being used by a WP user.\n\n";
		$data['msg'] .= "Please ask membership@californiaalpineclub.org for help.";

	} else {
		// good to go
		$applicant->email = $email;
		$applicant->dbSave(); // create the new application and set a code

		// combine applicantID with code for uniqueness
		$key = $applicant->applicantID . ':' .  $applicant->code;
		$link = home_url() . '/cac/members/apply/apply.php?key=' . $key;
		sendCode();
	}
}

echo json_encode($data);
exit;


function sendCode() {
	global $applicant, $link, $data, $key;

	// mail the code
	$mail = new PHPMailer();
	$mail->isSendmail();  // use system MTA (probably postfix)
	$mail->Sender = 'webmaster@californiaalpineclub.org';

	$mail->Subject = 'CAC Membership Application';
	$mail->setFrom('webmaster@californiaalpineclub.org', 'CAC webmaster');
	$mail->addReplyTo('membership@californiaalpineclub.org'); // default is to use fromAddress
	$mail->addAddress($applicant->email);

	$mail->CharSet = 'utf-8';
	$mail->XMailer = 'CAC Mail'; // default is phpMailer version

	$html = msgHTML($link, $key);
	$mail->msgHTML($html);

	$text = msgTEXT($link, $key);
	$mail->AltBody = $text;

	$mailSent = $mail->send();

	if ($mailSent) {
		$data['success'] = 1;
		$data['msg'] = 'An email message has been sent to ' . $applicant->email . ".\n\n";
		$data['msg'] .= 'Please follow the directions in that message to continue your application.';
	} else {
		$data['success'] = 0;
		$data['msg'] = 'The email message containing your code was not accepted for delivery.';
		$data['msg'] .= "Please ask membership@californiaalpineclub.org for help.";
	}
}

function msgHTML($link, $key) {
	//construct the html version of the code email

	$html = <<<HTML
<!DOCTYPE html>
<html lang="en">
<body style="font-family: sans-serif; font-size: 14px; margin-left: 10px">

<p>Most users can simply click the following button to continue the application process
or edit an application that has already been started.</p>

<a style="text-align: center; text-decoration: none; vertical-align: center;
		border: 1px solid; border-color: #0062cc; border-radius: 4px;
		color: white; background-color: #0069d9; padding: 7px;"
		href="[LINK]">Edit My Application</a>
		
<ul style="margin-top: 30px;">
	<li>Depending on your configuration, the link may open in a new tab or window,
	possibly even a different browser.</li>
	<li>If the button doesn't work for you, then copy the code below and paste it into the
	application start page.</li>
</ul>

<p>Your code is:<br>
<span style="font-family: monospace; font-size: 16px">[CODE]</span></p>
</body>
</html>
HTML;

	$html = str_replace(array('[LINK]', '[CODE]'), array($link, $key), $html);
	return $html;
}

function msgTEXT($link, $key) {
	//construct the text version of the code email

	$text = "Most users can simply click on the following link to continue the\n";
	$text .= "application process or edit an application that has already been started.\n\n";
	$text .= "$link \n\n";

	$text .= "Depending on your configuration, the link may open in a new tab or window, possibly even a different browser.\n";
	$text .= "If the link doesn't work for you, then copy the code below and paste it into the application start page.\n\n";

	$text .= "Your code is\n";
	$text .= "$key\n";
	return $text;
}
