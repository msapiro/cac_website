<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';

start_page('Events Report');

start_content();
?>

<h3 class="pageheader">Events Report</h3>

<div class="card well mt-3">
	<div class="card-body">

	<h4>Select Dates and Format</h4>
	<form action="report2.php" method="post" id="reportForm">
		<input type="hidden" name="reporttype" value="standard">

		<div class="row align-items-center mb-3">
			<label class="text-md-end col-md-2 col-lg-3">
				<b>Start Date</b>
			</label>
			<div class="col-6 col-md-4">
				<input class="form-control" type="text" name="rpt_start" maxlength="10" placeholder="YYYY-MM-DD">
			</div>
		</div>

		<div class="row align-items-center mb-3">
			<label class="text-md-end col-md-2 col-lg-3">
				<b>End Date</b>
			</label>
			<div class="col-6 col-md-4">
				<input class="form-control" type="text" name="rpt_end" maxlength="10" placeholder="YYYY-MM-DD">
			</div>
		</div>

		<div class="row align-items-center mb-3">
			<label class="text-lg-end col-lg-3">
				<b>Category (optional)</b>
			</label>
			<div class="col-sm-9">
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="category" id="rb_alpine" value="alpine">
					<label class="form-check-label" for="rb_alpine">Alpine</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="category" id="rb_echo" value="echo">
					<label class="form-check-label" for="rb_echo">Echo</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="category" id="rb_club" value="club">
					<label class="form-check-label" for="rb_club">Club</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="category" id="rb_sun" value="sun">
					<label class="form-check-label" for="rb_sun">Sundays</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="category" id="rb_fam" value="fam">
					<label class="form-check-label" for="rb_fam">Family</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="category" id="rb_cacf" value="cacf">
					<label class="form-check-label" for="rb_cacf">Foundation</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="category" id="rb_out" value="out">
					<label class="form-check-label" for="rb_out">Outings</label>
				</div>
			</div>
		</div>

		<div class="row align-items-center mb-3">
			<label class="text-md-end col-md-2 col-lg-3">
				<b>Format</b>
			</label>
			<div class="col-sm-8">
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="format" id="rb_xlsx" value="xlsx">
					<label class="form-check-label" for="rb_xlsx">Excel</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="format" id="rb_ods" value="ods">
					<label class="form-check-label" for="rb_ods">Open Document</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="format" id="rb_html" value="html">
					<label class="form-check-label" for="rb_html">HTML</label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-5 offset-md-2 offset-lg-3">
				<button type="submit" class="btn btn-primary">Get Report</button>
			</div>
		</div>

	</form>
	</div>
</div>

<div class="alert alert-info mt-3">
	<h3>Category</h3>
		<p>If you choose a category, only events of that category will
		be included. Otherwise, all events in the date range will be
		included.</p>
		<h4>Category descriptions:</h4>
		<ul>
			<li>Alpine: alpine-lodge-events</li>
			<li>Echo: echo-summit-lodge-events</li>
			<li>Club: official-club-events</li>
			<li>Sundays: sundays-at-alpine-lodge</li>
			<li>Family: youth-and-families</li>
			<li>Foundation: cac-foundation-events</li>
			<li>Outings: club-outings</li>
		</ul>
</div>

<?php end_page(); ?>
