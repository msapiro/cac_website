<?php
require '../../support/config.php';
require CLASSLOADER;

require '../includes/config.php'; // contains $allowGroups for this directory
//array_push($allowGroups, 'ROmembers');
require AUTH_AJAX;                // make sure user is logged in and a group member

require_once '../../../../wp-load.php';

use PHPMailer\PHPMailer\PHPMailer;

// test an application and send an invoice

$data = array();
inty($_POST['applicantID']);

$applicant = new Applicants($_POST['applicantID']);

if (empty($applicant->applicantID)) {
	stopNow('Did not find an applicant with applicantID = ' . $_POST['applicantID']);
}

if ($applicant->applyType == 'JTadd') {
	$member = new Members($applicant->addToMemberID, 'membership');
	if (! $member->memberID) stopNow('Existing Member (Joint) not found!');

	if ($member->membership->isExpired()) stopNow($member->fullname . ' membership is expired!');

	if ($member->membership->joint) {
		$membersArray = $member->membership->getJointMembers();
		if (count($membersArray) > 1) stopNow($member->fullname . ' joint membership already has two members!');
	}
}

$reinstate = false;
if (in_array($applicant->applyType, ['RGre', 'JTre', 'SRre', 'JSre'])) $reinstate = true;

if (! $reinstate) {
	// test the applicant email
	$member = new Members();
	$memberArray = $member->getByEmail($applicant->email);
	if ($memberArray) {
		stopNow($applicant->email . ' is already being used by a member!');
	}
}

if (in_array($applicant->applyType, ['JT', 'JTre', 'JSre'])) {
	// test the joint applicant email
	if (! filter_var($applicant->email2, FILTER_VALIDATE_EMAIL)) {
		stopNow($applicant->email2 . ' (joint applicant) is not a valid email address!');
	}
}

if ($applicant->applyType == 'JT') {
	// test the joint applicant email
	$member = new Members();
	$memberArray = $member->getByEmail($applicant->email2);
	if ($memberArray) {
		stopNow($applicant->email2 . ' (joint applicant) is already being used by a member!');
	}
}


//TODO: maybe test email against WordPress accounts?

$key =  $applicant->applicantID . ':' . $applicant->code;
$applicantPage = home_url() . '/cac/members/apply/apply.php?key=' . $key;
if (strpos($applicant->applyType, 're')) {
	$applicantPage = home_url() . '/cac/members/apply/reapply.php?key=' . $key;
}

$mailSent = sendInvoice();

if ($mailSent) {
	$data['success'] = 1;
	$data['msg'] = 'Sent an invoice for $' . number_format($applicant->entryFee + $applicant->dues , 2);
	$applicant->status('approved'); // add timestamp to the approved field
} else {
	$data['success'] = 0;
	$data['msg'] = 'There was a problem sending the message.';
}

echo json_encode($data);
exit;


function stopNow($msg) {
	global $data;
	$data['success'] = 0;
	$data['msg'] = $msg;
	echo json_encode($data);
	exit;
}

function sendInvoice() {
	global $applicant, $key;

	// mail the Invoice
	$mail = new PHPMailer();
	$mail->isSendmail();  // use system MTA (probably postfix)
	$mail->Sender = 'webmaster@californiaalpineclub.org';

	$mail->Subject = 'CAC Membership Invoice';
	$mail->setFrom('webmaster@californiaalpineclub.org', 'CAC webmaster');
	$mail->addReplyTo('membership@californiaalpineclub.org'); // default is to use fromAddress
	$mail->addAddress($applicant->email);

	$mail->CharSet = 'utf-8';
	$mail->XMailer = 'CAC Mail'; // default is phpMailer version

	$html = msgHTML();
	$mail->msgHTML($html);

	$text = msgTEXT();
	$mail->AltBody = $text;

	$mailresult = $mail->send();
	return $mailresult;
}

function msgHTML() {
	//construct the html version of the invoice email
	global $applicantPage, $key;

	$html = <<<HTML
<!DOCTYPE html>
<html lang="en">
<body style="font-family: sans-serif; font-size: 14px; margin-left: 10px">

<p>Congratulations! Your application for membership to the California Alpine Club has been 
approved. The next step in the process is for you to pay your entry fee and/or dues. Please 
return to your application webpage. It will now have a button that you can click to review 
your fees and options to make the payment.</p>

<p>Here again is the link to your application.</p>

<a style="text-align: center; text-decoration: none; vertical-align: center;
		border: 1px solid; border-color: #0062cc; border-radius: 4px;
		color: white; background-color: #0069d9; padding: 7px;"
		href="[APPLICANTPAGE]">See My Application</a>
	
<p>Your code is:<br>
<span style="font-family: monospace; font-size: 16px">[CODE]</span></p>
</body>
</html>
HTML;

	$html = str_replace(array('[APPLICANTPAGE]', '[CODE]'), array($applicantPage, $key), $html);
	return $html;
}

function msgTEXT() {
	//construct the text version of the invoice email
	global $applicantPage;

	$text = "Congratulations! Your application for membership to the California Alpine Club\n";
	$text .= "has been approved. The next step in the process is for you to pay your entry fee\n";
	$text .= "and/or dues. Please return to your application webpage. It will now have a button\n";
	$text .= "that you can click to see your fees and options to make the payment.\n\n";

	$text .= "Here again is the link to your application\n";
	$text .= "$applicantPage \n";

	return $text;
}
