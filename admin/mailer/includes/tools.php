<?php

//tools specific to this category

function getHelp($subject) {
	switch ($subject) {
		case 'htmlBody':
			$helpText = "<ul>
<li>This is a basic HTML editor. For complicated HTML, like that needed for responsive design, it is probably
easiest to create and test HTML content elsewhere and paste it here, using the <b>Source</b> button.</li>
<li>If this message is not using a template, you can put CSS in a style element inside the head element.</li>
<li>If this message is using a template, the head element and the html and body tags will be removed before
inserting into the template, but they will still appear in this editor.</li>
</ul>";
			break;

		case 'from':
			$helpText = "Here you MUST use an address <b>@californiaalpineclub.org</b> since the message will be
sent from the californiaalpineclub.org server. Otherwise, some recipient ISPs will reject it as spam.
If there is not an appropriate @californiaalpineclub.org address for replies, you can use
<b>eblasts@californiaalpineclub.org</b> and set <b>Reply To</b> to the address to which replies should go.";
			break;

		case 'replyTo':
			$helpText = "Optional. Leave blank to use <b>From Address</b> as <b>Reply To</b>.";
			break;

		case 'envelopeSender':
			$helpText = "<ul>
<li>The receiving mail server will set this as the <b>Return-Path</b>.</li>
<li>This is the address that bounce messages will be sent to.</li>
</ul>";
			break;

		case 'isTemplate':
			$helpText = "<ul>
<li>Check this box to make this message available as an HTML template.</li>
<li>The HTML body must contain a <b>[BODY]</b> string, which will be replaced with the HTML body of the sending message.</li>
<li>Only the HTML Body from this template matters for the sending message. The other fields will all be taken from
the sending message, not this template.</li>
<li>You cannot use a template in a message that is made available as a template (no nesting).</li>
</ul>";
			break;

		case 'useTemplate':
			$helpText = "<ul>
<li>Optionally, you can select another message to use as a template for the HTML in this message.</li>
<li>The template must contain a <b>[BODY]</b> string, which will be replaced with the Msg Content of this message
(omitting the head element and removing html and body tags).</li>
<li>If you want to use CSS in a style element, it must be in the head element of the template, not in this message.</li>
<li>Only the HTML from the template will be used to construct this message. All of the other fields (subject, addresses, etc)
will be taken from this message, not the template.</li>
<li>The template will not appear in the editor below, but will show on the detail page after you finish editing.</li>
<li>You cannot use a template in a message that is made available as a template (no nesting).</li>
</ul>";
			break;

		case 'textBody':
			$helpText = "<ul>
<li>For Plain Text Messages
	<ul><li>This text will be the body of the message.</li></ul>
</li>
<li>For HTML Messages
<ul>
<li>Message will be sent as multipart/alternative.</li>
<li>This plain text will be used for the alternative.</li>
<li>If you leave this blank, the program will extract plain text from your HTML (send yourself a test message to see it),
but you can probably do better, <b>especially if your message contains links</b>.</li>
<li>It might be best to start by writing the plain text version and then copy it into the HTML editor to add formatting.</li></ul>
</li></ul>";
			break;

		case 'queryString':
			$helpText = "<ul>
<li>A MySQL <b>SELECT</b> statement that returns the mandatory field <b>email</b> and optionally EITHER <b>fullname</b> OR
(<b>first</b> and <b>last</b>).</li>
<li>If an email address appears multiple times, only one message will be sent. The name will come from the first one processed.</li>
<li>For members, adding an <b>ORDER BY memberID</b> will give best results for joint members sharing email addresses.</li>
</ul>";
			break;

		case 'test':
			$helpText = "Sending one message is usually used for testing prior to sending the bulk message to a group. In that case
you should to add [TEST] to the subject line so the recipient knows this is a preliminary test and not the real bulk message.";
			break;

		case 'record':
			$helpText = "If you are sending the final message (not a test), then you should check this box to record that the message
was sent to this address. That prevents it from being sent to this address again later, maybe as part of a group. Leave this blank
when sending preliminary test messages.";
			break;

		default:
			$helpText = 'Help text not found. Is it defined?';
			break;
	}

	return $helpText;
}

function toolbar(){
	//create the toolbar for this page ?>

			<!-- Top Menu Bar -->
			<nav class="navbar navbar-expand cac_tool">
				<div class="navbar-nav">
					<a class="nav-link" href="index.php"><?php add_icon('mail-bulk') ?> Messages</a>
					<a class="nav-link" href="edit.php"><?php add_icon('plus') ?> New Message</a>
					<a class="nav-link" href="groups.php"><?php add_icon('user-friends') ?> Groups</a>
					<a class="nav-link" href="editGroup.php"><?php add_icon('user-plus') ?> New Group</a>
				</div>
			</nav>
<?php
}
?>
