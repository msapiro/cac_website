<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
array_push($allowGroups, 'ROmembers', 'trainee');
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';
include 'includes/tools.php';


$fromID = inty($_REQUEST['fromID']);

switch ($_REQUEST['searchtype']) {
	case 'where':
		$whereArray = $_SESSION['searchWhere'];
		break;

	default:
		if ($_POST['memberID']) {
			$whereArray = ['memberID' => $_POST['memberID']];
		} elseif ($_POST['membershipID']) {
			$whereArray = ['membershipID' => $_POST['membershipID']];
		} else {
			$whereArray = array();

			// we want to be able to search for wpID=0 and combine it with other criteria
			if ($_POST['wpID'] === '') {
				$wpID = false; // it was left blank
			} else {
				$wpID = $_POST['wpID']; // it might be set to zero
			}
			if ($wpID !== false) {
				$whereArray = ['wpID' => $wpID];
			}

			if ($_POST['first']) {
				$whereArray = array_merge($whereArray, ['first[~]' => wild($_POST['first'])]);
			}
			if ($_POST['last']) {
				$whereArray = array_merge($whereArray, ['last[~]' => wild($_POST['last'])]);
			}
			if ($_POST['email']) {
				$whereArray = array_merge($whereArray, ['email[~]' => wild($_POST['email'])]);
			}
			if ($_POST['class']) {
				$whereArray = array_merge($whereArray, ['class' => $_POST['class']]);
			}

			if ($_POST['expired'] <> -1) {
				$todayDate = date('Y-m-d');
				if ($_POST['expired'] == 1) {
					$comp = ['expiration[<]' => $todayDate];  // expired
				} else {
					$comp = ['expiration[>=]' => $todayDate]; // not expired
				}
				$whereArray = array_merge($whereArray, $comp);
			}
		}
		break;
}

if (! $whereArray) {
	error("Search Error","You must enter some kind of search criteria. Use your browser's <b>Back</b> button");
}

$db = (new Model(false))->db;
$data = $db->select(
		// table members JOIN memberships JOIN classes
		'members', ['[><]memberships' => 'membershipID', '[><]classes' => 'class'],
		'memberID',
		array_merge( $whereArray, ['ORDER' => ['first', 'last']] )
	);

$num = count($data);
if ($num == 1) {
	// we are done, just go to that detail page
	header("Location: detail.php?id=$data[0]");
}

$_SESSION['searchWhere'] = $whereArray;
$_SESSION['listPageMembers'] = $_SERVER["PHP_SELF"] . '?searchtype=where';

start_page("CAC - Search Results");
add_script('datatables'); ?>

<script>

$(function() {
	// do stuff when DOM is ready

	// make nice tooltips for the buttons
	$('.tip').tooltip({container: 'body', placement: 'bottom'});

	tableFeatures("itemTable", {
		"order": [ ],
		"stateSave": true,
		"drawCallback": function() {
			updateFoundArray();
		}
	});

	// clear any saved filter
	$('#itemTable').DataTable().search( '' ).columns().search( '' ).draw();

	// we don't load dataTables unless there are at least 3 items
	// so we force the update if there are just 2
	if($("#itemTable tbody tr").length == 2) {updateFoundArray();}

});

function updateFoundArray() {
	// read the data attribute of the rows to update the foundArray
	var foundArray = new Array();
	$("#itemTable tbody tr").each(function() {
		foundArray.push ( $(this).attr("data-id") );
	});
	var URL = '../../support/updateFound.php';
	$.post(URL, { arrayname: 'foundArrayMembers', order: foundArray.toString() });
}
</script>
<?php
start_content();
toolbar();
?>

<h3 class="pageheader">Found <?= $num ?> Members</h3>

<table id="itemTable" class="table table-striped table-sm">
<thead>
	<tr>
		<th class="text-center nosort">Action</th><th class="text-end">memberID&nbsp;</th>
		<th>Name</th><th class="text-end">membershipID&nbsp;</th><th class="text-end">WordPress ID&nbsp;</th>
		<th>Type</th><th>Expiration</th>
	</tr>
</thead>
<tbody>
<?php
foreach ($data as $memberID):
	$URLdetail = 'detail.php?id=' . $memberID . '&set=1';
	$URLedit = 'edit.php?id=' . $memberID . '&set=1';

	$member = new Members($memberID,'membership');
	$expirationDisplay = $member->membership->expiration;
	if ($member->membership->expired) $expirationDisplay = '<span style="color: red">' . $member->membership->expiration . '</span>';
?>
	<tr data-id="<?= $memberID ?>"<?php if ($memberID == $fromID) echo ' class="foundrow"'?>>
		<td class="text-center">
			<div class="btn-group btn-group-sm">
				<a href="<?= $URLdetail ?>" class="btn btn-secondary tip" title="Member Details"><?php add_icon('list-alt') ?></a>
				<a href="<?= $URLedit ?>" class="btn btn-secondary tip" title="Edit Member"><?php add_icon('edit') ?></a>
			</div>
		</td>
		<td class="text-end"><?= $memberID ?></td>
		<td><?= $member->fullname ?></td>
		<td class="text-end"><?= $member->membershipID ?></td>
		<td class="text-end"><?= $member->wpID ?></td>
		<td><?= $member->membership->memDescription ?></td>
		<td><?= $expirationDisplay ?></td>
	</tr>
<?php endforeach; ?>

</tbody>
</table>

<?php if ($whereArray): ?>
<div class="alert alert-info">
	Search criteria used:<br>
	<pre><?php print_r($whereArray) ?></pre>
</div>
<?php endif;

end_page();

function wild($string) {
	// add leading and trailing mysql wildcards and replace spaces with wildcards
	return '%' . preg_replace('/\s+/', '%', $string) . '%';
}
?>
