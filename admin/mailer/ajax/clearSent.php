<?php
require '../../support/config.php';
require CLASSLOADER;

require '../includes/config.php'; // contains $allowGroups for this directory
//array_push($allowGroups, 'ROmembers');
require AUTH_AJAX;                // make sure user is logged in and a group member

require '../../../support/functions.php';

requirePost();

$mailID = inty($_POST['mailID']);

if ($mailID) {
	$model = new Model();
	$model->db->delete('mailSent', ['mailID' => $mailID]);
}
