#!/usr/bin/python3

import datetime
import pymysql
import re

CUTOFF = datetime.datetime.now() - datetime.timedelta(days=365*3)

def format_ph(phone):
    if not phone:
        return 'None'
    if len(phone) == 10:
        return(f'({phone[:3]}){phone[3:6]}-{phone[6:]}')
    return phone

def main():
    db = pymysql.connect(user='CAC_query',
                         passwd='CAC_dbqry_1913',
                         db='alpine',
                         charset='utf8')
    c = db.cursor()
    c1 = db.cursor()
    c.execute("""SELECT first, last, email, phone, mobile, memberID FROM
              members JOIN memberships USING (membershipID)
              WHERE members.created >= %s""", CUTOFF)
    while True:
        rec = c.fetchone()
        if not rec:
            break
        first, last, email, phone, mobile, memberID = rec
        c1.execute("""SELECT notes from volNotes WHERE
                   memberID = %s""", memberID)
        vol_notes = c1.fetchall()
        notes = ''
        if len(vol_notes) == 0:
            pass
        elif len(vol_notes) == 1:
            if re.search('finance|insurance', vol_notes[0][0], re.I):
                notes = vol_notes[0][0]
        else:
            print(f'volNotes for member {memberID} has {len(vol_notes)} rows.')
        c1.execute("""SELECT oppID FROM volMembers WHERE memberID = %s AND
                   oppID = 10""", memberID)
        opps = c1.fetchall()
        if len(opps) > 0:
            opp = True
        else:
            opp = False
        if notes or opp:
            print(f'Member {first} {last} <{email}> phone {format_ph(phone)} '
                  f'cell {format_ph(mobile)}:')
            if notes:
                print(f'    Notes: {notes}')
            if opp:
                print('    Member indicated interest in Finance committee')
    c.close()
    c1.close()
    db.close()

if __name__ == '__main__':
    main()
