<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)

if (isset($allowGroups)) {
	require AUTH_PAGE; // make sure user is logged in and a group member
}

require '../../support/functions.php';


// copy members and memberships info from CAC database into new alpine database

start_page('CAC Migration');
start_content();
?>

<h3 class="pageheader">Setup and Migration</h3>

<div class="row">
	<div class="col-lg-10 offset-lg-1">

<h4>Setup</h4>

	<ol>
		<li>Copy <b>cac/setup/plugins/cac.php</b> to <b>wp-content/plugins/</b>.</li>
		<li>Manually create a submenu item for <b>CAC Account</b> in the <b>Members</b> menu
		using WordPress admin tools. The link can be <b>/cac/members</b>.</li>
	</ol>

<h4>Migration</h4>

	<ol>
		<li>Run the script to <a href="importMembers.php">Import Members</a> from the CAC database
		into the new <b>alpine</b> database.</li>

		<li>Run the script to <a href="importExpiration.php">Import Expiration and History</a> from the
		CAC database into the new <b>alpine</b> database.</li>

		 <li>Run the script to <a href="matchMembers.php">Match Members</a> that have WordPress accounts
		 to members in the <b>alpine</b> database.</li>

		<li> Run the script to <a href="makeWP.php">Make Wordpress</a> logins for current CAC members
		that don't have one.</li>

		<li> Run the script to <a href="deleteWP.php">delete WP logins</a>
		for expired CAC members.</li>

		 <li>Import member skill and interests from JoinIt:
		 <ul>
			 <li>Export JoinIt data to ods format.</li>
			 <li>Edit the ods file to remove the second row, which has text descriptions of the columns.</li>
			 <li>Also rename the worksheet tab to "joinit". This will be the name of the new mySQL table.</li>
			 <li>Use phpMyAdmin to import the ods file into the CAC database. Check the box
			 	<b>The first line of the file contains..."</b>.</li>
			 <li>Run the script to <a href="importSkills.php">Import Skills</a> from the new CAC table to the
			 	alpine database.</li>
		 </ul>
		 </li>
	</ol>

<div class="alert alert-info">
For initial setup you may not have an account with admin privileges yet. In order to run the scripts you
can temporarily edit <b>migration/includes/config.php</b> and comment out the <b>$allowGroups</b> line.
</div>

	</div>
</div>

<?php end_page(); ?>
