<?php

class Msgs extends Model {

	public $mailID;
	public $subject;
	public $fromName;
	public $fromAddress;
	public $replyTo;
	public $envelopeSender;
	public $html = 0;
	public $htmlBody = NULL;
	public $textBody = NULL;
	public $isTemplate = 0;
	public $useTemplate = 0;

	public function __construct($mailID = 0) {
		parent::__construct(); // set up our database
		if ($mailID) {
			$this->getByID($mailID);
		}
	}

	public function getByID($mailID) {
		// get the values for an existing message
		$row = $this->db->get('mail', '*', ['mailID' => $mailID]);
		if (! $row) return;
		foreach ($row as $key => $value) {
			$this->$key = $value;
		}
	}

	public function getAllSumm() {
		// return an array of msg summaries
		$msgArray = $this->db->select('mail', ['mailID', 'subject', 'html', 'isTemplate'], ['ORDER' => ['mailID' => "DESC"]]);
		foreach ($msgArray as $index=>$row) {
			// add data about messages already sent
			$sent = $this->db->count("mailSent", ['mailID' => $row['mailID']]);
			if ($sent) {
				$latest = $this->db->max("mailSent", "sentTime", ['mailID' => $row['mailID']]);
			} else {
				$latest = NULL;
			}
			$msgArray[$index]['sent'] = $sent;
			$msgArray[$index]['latest'] = $latest;
		}
		return $msgArray;
	}

	public function getTemplates() {
		// return an array of available html templates
		$templates = array();
		$rows = $this->db->select("mail", ["mailID", "subject"], ["html" => 1, "isTemplate" => 1]);
		foreach ($rows as $row) {
			$templates[$row['mailID']] = $row['subject'];
		}
		return $templates;
	}

	public function dbSave() {
		// save the message info into the database table
		$dataFields = array('subject', 'fromName', 'fromAddress', 'replyTo', 'envelopeSender',
			'html', 'htmlBody', 'textBody', 'isTemplate', 'useTemplate');

		nully($this->htmlBody);
		nully($this->textBody);

		if ($this->html) {
			// we use single quotes around our detail.php page iframe srcdoc, so sub them in the body
			$this->htmlBody = str_replace("'", '&apos;', $this->htmlBody);
			// enforce no nesting of templates
			if ($this->isTemplate) $this->useTemplate = 0;
		} else {
			$this->htmlBody = NULL;
			$this->isTemplate = 0;
			$this->useTemplate = 0;
		}

		$dataArray = array();
		foreach ($dataFields as $field) {
			$dataArray[$field] = $this->$field;
		}

		if ($this->mailID) {
			// update existing message
			$this->db->update('mail', $dataArray, ['mailID' => $this->mailID] );
		} else {
			// create new message
			$this->db->insert('mail', $dataArray);
			$this->mailID = $this->db->id();
		}
		return $this->mailID;
	}

	public function dbDelete() {
		if ($this->mailID) {
			// delete the log of sent mail
			$this->db->delete('mailSent', ['mailID' => $this->mailID]);

			// delete the message itself
			$obj = $this->db->delete('mail', ['mailID' => $this->mailID]);
			$ret = $obj->rowCount(); // number of affected rows
		} else {
			$ret = 0;
		}
		return $ret;
	}

} // end Msgs class

class MsgGroups extends Model {

	public $groupID;
	public $groupName;
	public $queryString;
	public $note = NULL;

	public function __construct($groupID = 0) {
		parent::__construct(); // set up our database
		if ($groupID) {
			$this->getByID($groupID);
		}
	}

	public function getByID($groupID) {
		// get the values for an existing group
		$row = $this->db->get('mailGroups', '*', ['groupID' => $groupID]);
		if (! $row) return;
		foreach ($row as $key => $value) {
			$this->$key = $value;
		}
	}

	public function getArray() {
		$groupsArray = array();
		$rows = $this->db->select('mailGroups', 'groupID', ['ORDER' => ["groupID" => "DESC"]]);
		foreach ($rows as $groupID) {
			$groupsArray[] = new MsgGroups($groupID);
		}
		return $groupsArray;
	}

	public function dbSave() {
		// save the msgGroup info into the database table
		$dataArray = [
				'groupName' => $this->groupName,
				'queryString' => trim($this->queryString),
				'note' => nully($this->note)
		];
		if ($this->groupID) {
			// update existing group
			$this->db->update('mailGroups', $dataArray, ['groupID' => $this->groupID] );
		} else {
			// create new group
			$this->db->insert('mailGroups', $dataArray);
			$this->groupID = $this->db->id();
		}
		return $this->groupID;
	}

	public function dbDelete() {
		if ($this->groupID) {
			$obj = $this->db->delete('mailGroups', ['groupID' => $this->groupID]);
			$ret = $obj->rowCount(); // number of affected rows
		} else {
			$ret = 0;
		}
		return $ret;
	}

} // end MsgGroups class
