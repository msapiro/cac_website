<?php

// privilege groups that are allowed to access pages in this category (Events)
$allowGroups = array('admin', 'events');
