<?php
/*
 *
 * Plugin Name: cac
 * Description: Plugin for California Alpine Club (CAC)
 * Version: 0.3
 *
 * The actual code is all in the included file.
 * place this file in wp-content/plugins/
 *
 */

$CAC_path = dirname(__FILE__, 3) . '/cac';

include $CAC_path . '/support/wp_plugin.php';
