<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';
require '../../support/formFunctions.php';
include 'includes/tools.php';

inty($_GET['id']);

$msg = new Msgs($_GET['id']);
if (empty($msg->mailID)) {
	error('Error', 'Did not find a message with mailID=' . $_GET['id']);
	exit;
}

$from = '';
if ($msg->fromName) $from = $msg->fromName . ' <' . $msg->fromAddress . '>';

if($msg->replyTo) {
	$reply = $msg->replyTo;
} else {
	$reply = $msg->fromAddress;
}

if ($msg->html) {
	$type = 'HTML';
	$body = $msg->htmlBody;
	if (is_null($body)) {
		$body = ''; // make it an empty string
	}

	if ($msg->isTemplate) {
		$type .= ' (this msg is an HTML template)';
	}

	if ($msg->useTemplate) {
		$template = new Msgs($msg->useTemplate);
		$templateLink = '<a href="detail.php?id=' . $msg->useTemplate . '">'
			. $template->subject . '</a>';

		// if our message has html, head, or body tags, remove them.
		// these should be only in the template.

		// from opening <html> to <body attrib="something">
		$body = preg_replace('/<html.*<body.*>/siU', '', $body);

		// from closing </body> to closing </html>
		$body = preg_replace('/<\/body.*html>/si', '', $body);

		// merge this body into the template
		$body = str_replace('[BODY]', $body, $template->htmlBody);
	}
} else {
	$type = 'Plain Text';
	if (is_null($msg->textBody)) {
		$msg->textBody = ''; // make it an empty string
	}
}

// make an array of the available groups
$groupSelect = array();
$groupSelect[0] = 'Select';
$groupsArray = (new MsgGroups())->getArray();
foreach ($groupsArray as $msgGroup) {
	$groupSelect[$msgGroup->groupID] = $msgGroup->groupName;
}

start_page('CAC Mailer');
?>
<script>
$(function() {
	// do stuff when DOM is ready

	 $(".pop").popover({trigger: 'hover'}); // info popups

});
</script>
<?php
start_content();
toolbar();
?>

<h3 class="pageheader">Message Detail</h3>

<div class="row">
	<div class="col-xl-10 offset-xl-1">


<div class="row align-items-center mb-2">
	<label class="col-2 col-lg-3 text-lg-end">
		<b>Mail ID</b>
	</label>
	<div class="col-1">
		<?= $msg->mailID ?>
	</div>
	<div class="col-5">
		<a href="edit.php?id=<?= $msg->mailID ?>"
			class="btn btn-secondary"><?php add_icon('edit')?> Edit Message</a>
	</div>
</div>

<?php
static_field('Subject', $msg->subject);
if (! $msg->isTemplate) {
	static_field('From', $from, array('From Address', 'from'));
	static_field('Reply To', $reply, array('Reply To Address', 'replyTo'));
	static_field('Envelope Sender', $msg->envelopeSender,
		array('Envelope Sender Address', 'envelopeSender'));
}
static_field('Msg Type', $type);
if ($msg->useTemplate):
?>
<div class="row align-items-center mb-1 mb-lg-3">
	<label class="text-lg-end col-lg-3">
		<b>Template</b>
	</label>
	<div class="col-lg-5">
		<?= $templateLink ?>
	</div>
</div>
<?php
endif;
?>

<div class="row mb-2">
	<label class="text-lg-end col-lg-3">
		<b>Preview</b>
	</label>
</div>

<div class="row mb-3">
<?php if ($msg->html): ?>
	<div class="col-lg-11 offset-lg-1">
		<iframe width="100%" height="700px" srcdoc='<?= $body ?>'></iframe>
	</div>
<?php else: ?>
	<div class="col-lg-11 offset-lg-1">
		<div style="border-width: 1px; border-style: solid; padding: 5px 10px 0 10px;">
			<pre><?= htmlspecialchars($msg->textBody) ?></pre>
		</div>
	</div>
<?php endif; ?>

</div>

<?php
if ($msg->html):
	if (! $msg->isTemplate):
		if (empty($msg->textBody)) {
			$txtDisplay = 'Not specified. Program will attempt to convert html to text.';
		} else {
			$txtDisplay = $msg->textBody;
		}
?>
<div class="row mb-3">
	<label class="text-lg-end col-lg-3">
		<b>Alt Plain Text</b>
	</label>
</div>

<div class="row mb-3">
	<div class="col-lg-11 offset-lg-1">
		<div style="border: 1px; border-style: solid; padding: 5px 10px 0 10px;">
			<pre><?= $txtDisplay ?></pre>
		</div>
	</div>
</div>

<?php
	endif;
endif; ?>

<div class="card well mt-3">
	<div class="card-body">
		<div class="col-lg-5 offset-lg-3">
			<h4>Send message to one person</h4>
		</div>
		<form action="sendOne.php" method="post" id="mailForm">
			<input type="hidden" name="mailID" value="<?= $msg->mailID ?>">
<?php
edit_field('To Name', 'toName', false, false, 'optional', false, 'toName',
	'col-lg-3', 'col-lg-8');
edit_field('To Address', 'toAddress', false, false, false, false, 'toAddress',
	'col-lg-3', 'col-lg-8');
?>

			<div class="row align-items-center mb-3">
				<label class="text-lg-end col-md-2 col-lg-3">
					<b>Subject</b> <?php Show_Info('Subject [TEST]', getHelp('test')) ?>
				</label>
				<div class="col-md-8 col-lg-5">
					<div class="form-check">
						<input class="form-check-input" type="checkbox" name="prepend"
							value="1" id="prepend" checked>
						<label class="form-check-label" for="prepend">
							Prepend <b>[TEST]</b> to subject
						</label>
					</div>
				</div>
			</div>

			<div class="row align-items-center mb-3">
				<label class="text-lg-end col-md-2 col-lg-3">
					<b>Record</b> <?php Show_Info('Record Sending', getHelp('record')) ?>
				</label>
				<div class="col-md-8 col-lg-5">
					<div class="form-check">
						<input class="form-check-input" type="checkbox" name="record"
							value="1" id="record">
						<label class="form-check-label" for="record">
							Record this email address as <b>Sent</b>
						</label>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-5 offset-lg-3">
					<button type="submit"
						class="btn btn-primary"><?php add_icon('envelope') ?> Send One Message
					</button>
				</div>
			</div>
		</form>
	</div>
</div> <!-- end card -->

<div class="card well mt-3 mb-3">
	<div class="card-body">
		<div class="col-lg-5 offset-lg-3">
			<h4>Send bulk message to a group</h4>
		</div>
		<form action="sendGroup.php" method="post" id="bulkForm">
			<input type="hidden" name="mailID" value="<?= $msg->mailID ?>">

<?php edit_select_field('Group', 'groupID', $groupSelect); ?>

			<div class="row">
				<div class="col-lg-5 offset-lg-3">
					<button type="submit"
						class="btn btn-primary"><?php add_icon('arrow-right') ?> Next
					</button>
				</div>
			</div>

		</form>
	</div>
</div> <!-- end card -->

	</div>
</div>

<?php end_page(); ?>
