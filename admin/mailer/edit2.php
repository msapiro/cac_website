<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';
// Ensure we came from a POST.
requirePOST();

$msg = new Msgs();

$msg->mailID = $_POST['mailID'];
$msg->subject = $_POST['subject'];
$msg->fromName = $_POST['fromName'];
$msg->fromAddress = $_POST['fromAddress'];
$msg->replyTo = $_POST['replyTo'];
$msg->envelopeSender = $_POST['envelopeSender'];
$msg->html = booly($_POST['html']); // checkbox
$msg->htmlBody = $_POST['htmlBody'];
$msg->textBody = $_POST['textBody'];
$msg->isTemplate = booly($_POST['isTemplate']); // checkbox
$msg->useTemplate = $_POST['useTemplate'];

$msg->dbSave();

header("Location: detail.php?id=$msg->mailID");
