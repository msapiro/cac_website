<?php
require '../support/config.php';
require CLASSLOADER;

require AUTH_PAGE; // enforce login

// if you want this page to have a custom title, set it here.
// leave it blank to use the default title for the site.
// this is the text that appears on the page tab in most browsers.
$title = '';
$pageDescription = 'California Alpine Club Member Services';

require '../../support/functions.php';
require '../../support/formFunctions.php';

// logged in user
$member = new Members('login', ['membership', 'wpData']);
if (empty($member->memberID)) {
	error('Error', 'Did not find the logged-in member.');
	exit;
}

$trailsFormat = 'Electronic only';
if ($member->membership->paperTrails) $trailsFormat = 'Electronic plus mailed paper';

$wpError = false;
if ($member->wpID) {
	if (! $member->wpData->wpID) {
		$wpError = true;
	}
}

$renewButton = false;
$curDateTime = new DateTime();
if (! $member->membership->expired) {
	// if membership is current, show renewal button after Jan 1
	$expireYear = explode('-', $member->membership->expiration)[0];
	$renewOpensDate = $expireYear . '-01-01';
	$renewOpensDateTime = new DateTime($renewOpensDate);

	if ($curDateTime >= $renewOpensDateTime) {
		$renewButton = true;
	} else {
		$btnText = 'Renewal opens ' . $renewOpensDate;
	}
} else {
	// if expired, show renewal button for 9 months after expiration
	$expDateTime = new DateTime($member->membership->expiration);
	$expDateTime->add(new DateInterval('P9M')); // add 9 months
	if ($curDateTime > $expDateTime) {
		// expired more than 9 months ago. member needs to be reinstated by admin
		$btnText = 'Renewal requires Reinstatement';
	} else {
		$renewButton = true;
	}
}

if ($member->membership->joint) {
	// get the other member
	$jointMembers = $member->membership->getJointMembers();
	$jointMember = NULL;
	foreach ($jointMembers as $thisMember) {
		if ($thisMember->memberID <> $member->memberID) {
			$jointMember = $thisMember;

			$wpErrorJoint = false;
			if ($jointMember->wpID) {
				$jointMember->wpData = new WP_data($jointMember->wpID);
				if (! $jointMember->wpData->wpID) {
					$wpErrorJoint = true;
				}
			}
		}
	}
}

$oDep = new Dependents();
$depsArray = $oDep->getAllDeps($member->membershipID);

start_page($title, $pageDescription);
?>
<script>
$(function() {
	// do stuff when DOM is ready

	// make nice tooltips for the buttons
	$('.tip').tooltip({container: 'body', placement: 'bottom'});

	$("#btnAddDep").click(function() {
		editDependent(0);
	});

	// focus on the first name input when adding a new dependent
	$("#depModal").on('shown.bs.modal', function() {
		if ($("#depID").val() == '0') {
        	$('#first').focus();
		}
    });

	$("#first").prop('required',true);

});


function removeMember(memberID) {
	var agree = confirm("Are you sure you want to delete this member from your Joint membership?");
	if (agree) {
		$.post('ajax/removeJointMember.php', {memberID: memberID}, function(data) {
			if (data.success) {
				location.reload();
			} else {
				alert("There was a problem removing the joint member. Please ask membership@californiaalpineclub.org for help.");
			}
		}, "json");
	}
}

function deleteDependent(elmt, depID) {
	var agree = confirm("Are you sure you want to delete this Dependent?");
	if (agree) {
		$.post('ajax/deleteDep.php', {depID: depID}, function() {
			$(elmt).closest("tr").remove();
		});
	}
	elmt.blur();
}

function editDependent(depID) {
	resetDependent(depID);
	$('#depModal').modal('show');
}

function resetDependent(depID) {
	if (depID == 0) {
		$("h4.modal-title").html("Add Dependent");
		$("#depID").val('0');
		$("#first").val('');
		$("#last").val('');
		$("#dob").val('');
	} else {
		$("h4.modal-title").html("Edit Dependent");
		// get the dependent data from database
		$.post('ajax/getDep.php', {depID: depID}, function(data) {
			if (data.success == 1) {
				$("#depID").val(depID);
				$("#first").val(data.first);
				$("#last").val(data.last);
				$("#dob").val(data.dob);
			}
		},'json');
	}
}
</script>
<?php start_content(); ?>

<h2 class="pageheader">Membership Details</h2>

<div class="row">
	<div class="col-xl-10 offset-xl-1">

		<div class="card well">
		<div class="card-body">

			<div class="row">
				<div class="col-lg">

					<a href="edit.php" class="btn btn-secondary mb-3"><?php add_icon('edit') ?> Edit</a>

					<table class="table table-sm table-striped">
					<thead>
						<tr class="table-dark">
							<th style="width: 180px">Personal Data</th>
							<th style="font-weight: normal;">memberID: <?= $member->memberID ?></th>
						</tr>
					</thead>
					<tbody>
						<tr><td>Name</td><td><?= $member->fullname ?></td></tr>
						<tr>
							<td>Email</td>
							<td><a href="mailto:<?= $member->email ?>"><?= $member->email ?></a></td>
						</tr>
						<tr><td>Main Phone</td><td><?= phone_number($member->phone) ?></td></tr>
						<tr><td>Mobile Phone</td><td><?= phone_number($member->mobile) ?></td></tr>
						<tr><td>Date of Birth</td><td><?= $member->dob ?></td></tr>
						<tr><td>Created</td><td><?= $member->created ?></td></tr>
					</tbody>
					</table>

					<table class="table table-sm table-striped">
					<thead>
						<tr class="table-dark">
							<th style="width: 180px">WordPress Data</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
<?php
if ($member->wpID):
	if ($wpError): ?>
						<tr><td colspan="2"><span style="color: red;">ERROR:</span> wordpressID does not exist</td></tr>
<?php
	else: ?>
						<tr><td>WP Username</td><td><?= $member->wpData->user_login ?></td></tr>
						<tr><td>WP Name</td><td><?= $member->wpData->wpFullname ?></td></tr>
						<tr>
							<td>WP Email</td>
							<td><a href="mailto:<?= $member->wpData->wpEmail ?>"><?= $member->wpData->wpEmail ?></a></td>
						</tr>
<?php
	endif;
endif; ?>
					</tbody>
					</table>
				</div>

				<div class="col-lg">

<?php if ($renewButton): ?>
					<a href="renew.php" class="btn btn-secondary mb-3"><?php add_icon('redo') ?> Renew</a>
<?php else: ?>
					<button type="button" class="btn btn-secondary mb-3" disabled><?php add_icon('redo') ?> <?= $btnText ?></button>
<?php endif; ?>
					<table class="table table-sm table-striped">
					<thead>
						<tr class="table-dark">
							<th style="width: 180px">Membership Data</th>
							<th style="font-weight: normal;">membershipID: <?= $member->membershipID ?></th>
						</tr>
					</thead>
					<tbody>
						<tr><td>Membership Type</td><td><?= $member->membership->memDescription ?></td></tr>
						<tr>
							<td>Expiration</td>
							<td><?php echo $member->membership->expiration; if ($member->membership->expired) echo ' <span style="color: red">EXPIRED</span>'; else if ($member->membership->resigned) echo ' <span style="color: red">RESIGNED</span>'; ?></td>
						</tr>
						<tr><td>Street Address</td><td><?= $member->membership->street ?></td></tr>
						<tr><td>City</td><td><?= $member->membership->city ?></td></tr>
						<tr><td>State</td><td><?= $member->membership->state ?></td></tr>
						<tr><td>Zip</td><td><?= $member->membership->zip ?></td></tr>
						<tr><td>Trails Newsletter</td><td><?= $trailsFormat?></td></tr>
						<tr><td>Sponsors</td><td><?= $member->membership->sponsors ?></td></tr>
						<tr><td>Created</td><td><?= $member->membership->created ?></td></tr>
					</tbody>
					</table>
				</div>

			</div> <!-- end row -->
		</div> <!--  end card body -->
		</div> <!-- end card -->

<?php
if ($member->membership->joint): ?>
		<h4 class="pageheader">Joint Member</h4>
		<div class="card well mt-4">
		<div class="card-body">

			<div class="row">
			<div class="col-lg-6">
<?php
	if (is_null($jointMember)): ?>
			<p>Please contact
				<a href="mailto:membership@californiaalpineclub.org">
					membership@californiaalpineclub.org
				</a> to add another member to your joint account.</p>
<?php
	else:
		$familyEmails = array();
		if ($member->email) $familyEmails[] = strtolower($member->email);
		$URLedit = 'edit.php?id=' . $jointMember->memberID;

		if (! empty($jointMember->email)) {
			if (! in_array(strtolower($jointMember->email), $familyEmails)) {
				$familyEmails[] = strtolower($jointMember->email);
			}
		}
?>

			<div class="btn-group">
				<a href="<?= $URLedit ?>" class="btn btn-secondary"><?php add_icon('edit') ?> Edit <?= $jointMember->first ?></a>
				<button type="button" class="btn btn-secondary"
					onclick="removeMember(<?= $jointMember->memberID ?>)"><?php add_icon('times')?> Remove</button>
			</div>

			<table class="table table-sm table-striped mt-3">
				<thead>
					<tr class="table-dark">
						<th style="width: 180px">Personal Data</th>
						<th style="font-weight: normal">memberID: <?= $jointMember->memberID ?></th>
					</tr>
				</thead>
				<tbody>
					<tr><td>Name</td><td><?= $jointMember->fullname  ?></td></tr>
					<tr><td>Email</td><td><a href="mailto:<?= $jointMember->email ?>"><?= $jointMember->email ?></a></td></tr>
					<tr><td>Main Phone</td><td><?= phone_number($jointMember->phone) ?></td></tr>
					<tr><td>Mobile Phone</td><td><?= phone_number($jointMember->mobile) ?></td></tr>
					<tr><td>Date of Birth</td><td><?= $jointMember->dob ?></td></tr>
					<tr><td>Created</td><td><?= $jointMember->created ?></td></tr>
				</tbody>
			</table>

			<table class="table table-sm table-striped">
				<thead>
					<tr class="table-dark">
						<th style="width: 180px">WordPress Data</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
<?php
		if ($jointMember->wpID):
			if ($wpErrorJoint): ?>
						<tr><td colspan="2"><span style="color: red;">ERROR:</span> wordpressID does not exist</td></tr>
<?php
			else: ?>
						<tr><td>WP Username</td><td><?= $jointMember->wpData->user_login ?></td></tr>
						<tr><td>WP Name</td><td><?= $jointMember->wpData->wpFullname ?></td></tr>
						<tr>
							<td>WP Email</td>
							<td><a href="mailto:<?= $jointMember->wpData->wpEmail ?>"><?= $jointMember->wpData->wpEmail ?></a></td>
						</tr>
<?php
			endif;
		endif; ?>
				</tbody>
			</table>
			</div> <!-- end left column -->
<?php
	if (count($familyEmails) <> 2):
?>
			<div class="col-lg-6"> <!-- right column -->
				<div class="alert alert-danger">
				<p>Every CAC member should have a valid and unique email address in order to be
					properly identified.</p>
				<p>Please edit your personal data or your joint member's data if you can fix
					this.</p>
				</div>
			</div>
<?php
	endif;
endif;
?>
				</div>  <!-- end row -->
			</div> <!--  end card body -->
			</div> <!-- end card -->
<?php
endif; // end if member->membership->joint
?>


		<h4 class="pageheader">Dependents</h4>
		<div class="card well mt-4">
		<div class="card-body">

			<p><button id="btnAddDep" class="btn btn-secondary" type="button"><?php add_icon('plus') ?> Add Dependent</button></p>
<?php if ($depsArray): ?>
			<table class="table table-striped table-sm col-lg-10">
				<thead>
					<tr><th class="text-center">Action</th><th>Dependent Name</th><th>Date Of Birth</th><th>Age</th></tr>
				</thead>
				<tbody>
<?php 	foreach ($depsArray as $thisDep): ?>

					<tr>
						<td class="text-center">
							<div class="btn-group btn-group-sm">
								<button type="button" class="btn btn-secondary tip"
									onclick="editDependent(<?= $thisDep->depID ?>)" title="Edit"><?php add_icon('edit') ?>
								</button>
								<button type="button" class="btn btn-secondary tip"
									onclick="deleteDependent(this,<?= $thisDep->depID ?>)" title="Delete"><?php add_icon('times') ?>
								</button>
							</div>
						</td>
						<td><?= $thisDep->fullname ?></td>
						<td><?= $thisDep->dob ?></td>
						<td><?= $thisDep->age ?></td>
					</tr>
<?php 	endforeach; ?>

				</tbody>
			</table>

<?php endif; ?>
		</div> <!-- end card-body -->
		</div> <!-- end card -->


	</div>
</div>


<!-- Modal for dependent add or edit -->
<div class="modal fade" id="depModal">
	<div class="modal-dialog">
		<div class="modal-content" id="depModalContent">
			<div class="modal-header">
				<h4 class="modal-title">Dependent</h4>
				<button type="button" class="btn-close" data-bs-dismiss="modal"></button>
			</div>

			<form id="depForm" method="post" action="editDep2.php">
				<input type="hidden" id="depID" name="depID" value="0">

				<div class="modal-body">
<?php
edit_field('First Name', 'first', false, 30, 'required', false, 'first');
edit_field('Last Name', 'last', false, 30, false, false, 'last');
edit_field('DOB', 'dob', false, 10, 'YYYY-MM-DD', false, 'dob');
?>
				</div> <!-- end modal-body -->

				<div class="modal-footer">
					<div class="btn-group">
						<button type="submit" class="btn btn-primary">Submit</button>
						<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
					</div>
				</div>
			</form>

        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal -->

<?php end_page(); ?>
