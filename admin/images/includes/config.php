<?php

// privilege groups that are allowed to access pages in this category (Mailer)
$allowGroups = array('admin', 'mailer');

// file extensions for image uploads
$allowed_extension = array("png","jpg","jpeg","gif", "webp", "svg");

// thumbnail jpeg size
$thumbWidth = 300;
$thumbHeight = 300;
$thumbRatio = $thumbWidth / $thumbHeight;
