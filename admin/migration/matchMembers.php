<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)

if (isset($allowGroups)) {
	require AUTH_PAGE; // make sure user is logged in and a group member
}

require_once '../../../wp-load.php';
require '../../support/functions.php';


// match WordPress users to CAC members using email

$proceed = booly($_GET['proceed']);

start_page('CAC Migration');
start_content();

if (! $proceed): ?>

<div class="alert alert-danger mt-4">
	<p><b>WARNING</b>: This will read WordPress user IDs and email addresses. For IDs that are not
	already set in the <b>alpine.members</b> table, it will try to match email address to a CAC member.
	If an email address matches multiple members (usually both members of a joint membership) a further
	attempt at matching first names will be made. For WP users without any email match, a final attempt
	using firstname and lastname will be made.
	</p>
	<p>You can run this script multiple times.</p>
</div>

<a class="btn btn-secondary" href="<?= $_SERVER['PHP_SELF'] . '?proceed=1' ?>">Continue</a>

<?php
	end_page();
	exit;
endif;


$numSet = 0;
$numMultimatch = 0;
$numFirst = 0;

$toMatch = array();
$unMatched = array();


$model = new Model();
$db = $model->db;

// get an aray of all WordPress user IDs (using a WordPress function)
$wpUsers = get_users( ['fields' => 'ID', 'orderby' => 'ID'] );

foreach($wpUsers as $wpID) {
	// see if this user is already matched to a CAC member
	if (! $db->has("members", ["wpID" => $wpID]) ) {
		$toMatch[] = $wpID;
	}
}

echo 'Total WordPress users = ' . count($wpUsers) . '<br>';
echo 'WordPress users not yet matched to CAC members = ' . count($toMatch) . '<br><br>';

foreach($toMatch as $wpID) {
	$userdata = get_userdata($wpID);

	$memberArray = $db->select("members", "memberID",
		[
		"email" => $userdata->user_email,
		"wpID" => 0
		]);
	$matches = count($memberArray);

	if ($matches == 0) {
		// nothing to do
		$unMatched[] =  $wpID;
	} elseif ($matches == 1) {
		// good, only one match
		$db->update("members", ["wpID" => $wpID], ["memberID" => $memberArray[0]]);
		$numSet++;
	} else {
		$numMultimatch++;
		// probably a joint membership using the same email.
		// maybe we can match on the first name
		$memberArray = $db->select("members", "memberID",
		[
		"email" => $userdata->user_email,
		"first" => $userdata->first_name,
		"wpID" => 0
		]);
		$matches = count($memberArray);
		if ($matches == 1) {
			// good, only one match when adding first name to criteria
			$db->update("members", ["wpID" => $wpID], ["memberID" => $memberArray[0]]);
			$numSet++;
			$numFirst++;
		} else {
			$unMatched[] = $wpID;
		}
	}
}

// all email matching is done. see if we can match anything left using first, last
$numFirstLast = 0;
$stillUnmatched = array();
foreach ($unMatched as $wpID) {
	$userdata = get_userdata($wpID);
	$memberArray = $db->select("members", "memberID",
			[
				"first" => $userdata->first_name,
				"last" => $userdata->last_name,
				"wpID" => 0
			]);
	$matches = count($memberArray);
	if ($matches == 1) {
		// good, only one match
		$db->update("members", ["wpID" => $wpID], ["memberID" => $memberArray[0]]);
		$numSet++;
		$numFirstLast++;
	} else {
		$stillUnmatched[] = $wpID;
	}
}
?>

<div class="row">
	<div class="col-xl-10 offset-xl-1">

<h4>Results</h4>
<p>
Set WordPress ID for <?= $numSet ?> CAC members<br>
<?= $numMultimatch ?> email addresses had multiple matches<br>
<?= $numFirst ?> multiple matches were resolved by (email, firstname)<br>
<?= $numFirstLast ?> members with no email match were matched using (firstname, lastname)
</p>

<p>The following WordPress users have not been matched to CAC members</p>

<table class="table table-sm table-striped">
	<thead>
		<tr><th class="text-center">wpID</th><th>email</th><th>First</th><th>Last</th></tr>
	</thead>
	<tbody>
<?php
foreach ($stillUnmatched as $wpID):
	$userdata = get_userdata($wpID);
?>
		<tr>
		<td class="text-center"><?= $wpID ?></td>
		<td><?= $userdata->user_email ?></td>
		<td><?= $userdata->first_name ?></td>
		<td><?= $userdata->last_name ?></td>
		</tr>
<?php
endforeach;
?>
	</tbody>
</table>
<p><a href="index.php">Setup and Migration</a></p>

	</div>
</div>
<?php end_page(); ?>
