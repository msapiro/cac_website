<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
array_push($allowGroups, 'ROmembers', 'trainee');
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';

include 'includes/tools.php';

start_page('CAC Reports');

start_content();
toolbar();
?>

<div class="row">
	<div class="col-xl-10 offset-xl-1">


<h3 class="pageheader">CAC Reports</h3>

<div class="card well mt-3">
	<div class="card-body">

	<h4>Select Report</h4>
	<form action="reports2.php" method="post" id="reportForm">
		<input type="hidden" name="reporttype" value="standard">

		<div class="row align-items-center mb-3">
			<label class="text-lg-end col-lg-3">
				<b>Report</b>
			</label>
			<div class="col-sm-9">
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="report" id="rb_roster" value="roster">
					<label class="form-check-label" for="rb_roster">Roster</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="report" id="rb_trails" value="trails">
					<label class="form-check-label" for="rb_trails">Trails</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="report" id="rb_since" value="since">
					<label class="form-check-label" for="rb_since">New Members</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="report" id="rb_host" value="host">
					<label class="form-check-label" for="rb_host">Host Training</label>
				</div>
			</div>
		</div>

		<div class="row align-items-center mb-3">
			<label class="text-lg-end col-lg-3">
				<b>Since</b>
			</label>
			<div class="col-6 col-md-5 col-lg-4">
				<input class="form-control" type="text" name="since_d" maxlength="10" placeholder="YYYY-MM-DD">
			</div>
		</div>

		<div class="row align-items-center mb-3">
			<label class="text-lg-end col-lg-3">
				<b>End</b>
			</label>
			<div class="col-6 col-md-5 col-lg-4">
				<input class="form-control" type="text" name="end_d" maxlength="10" placeholder="YYYY-MM-DD">
			</div>
		</div>

		<div class="row align-items-center mb-3">
			<label class="text-lg-end col-lg-3">
				<b>Expired</b>
			</label>
			<div class="col-sm-9">
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="expired" id="rb_include" value="include">
					<label class="form-check-label" for="rb_include">Include</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="expired" id="rb_omit" value="omit" checked>
					<label class="form-check-label" for="rb_omit">Omit</label>
				</div>
			</div>
		</div>
		<div class="row align-items-center mb-3">
			<label class="text-lg-end col-lg-3">
				<b>Lodge</b>
			</label>
			<div class="col-sm-9">
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="lodge" id="rb_alpine" value="alpine">
					<label class="form-check-label" for="rb_alpine">Alpine</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="lodge" id="rb_echo" value="echo">
					<label class="form-check-label" for="rb_echo">Echo</label>
				</div>
			</div>
		</div>

		<div class="row align-items-center mb-3">
			<label class="text-lg-end col-lg-3">
				<b>Format</b>
			</label>
			<div class="col-sm-9">
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="format" id="rb_xlsx" value="xlsx">
					<label class="form-check-label" for="rb_xlsx">Excel</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="format" id="rb_ods" value="ods">
					<label class="form-check-label" for="rb_ods">Open Document</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="format" id="rb_html" value="html">
					<label class="form-check-label" for="rb_html">HTML</label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-5 offset-lg-3">
				<button type="submit" class="btn btn-primary">Get Report</button>
			</div>
		</div>

	</form>
	</div>
</div>

<div class="alert alert-info mt-3">
	<h4>Since, End, Expired and Lodge</h4>
	<p>These entries apply only to the New Members and Host Training
	reports and are ignored for other reports. Details follow.

	<h4>New Members</h4>
	<ul>
		<li>For the New Members report you must specify the
		Since date which is the join date on or after which a
		member is considered new.</li>
		<li>For the New Members report you can optionally include an
		End date to exclude members who joined after that date.</li>
		<li>For the New Members report select Include or Omit for
		Expired to include or omit members whose membership is expired.</li>
		<li>For the New Members report, Lodge is ignored.</li>
	</ul>

	<h4>Host Training</h4>
	<ul>
		<li>For the Host training report you may optionally specify
		Since and/or End (only the year is significant) to limit the
		report to members trained within those years. The default for
		Since is the earliest year in the data and for End is the
		latest year in the data. Those who are trained in multiple
		years within the range will be included for each year.</li>
		<li>For the Host Training report select Include or Omit for
		Expired to include or omit members whose membership is expired.</li>
		<li>For the Host Training report you must select the lodge for
		the report.
	</ul>
</div>

<div class="card well mt-3">
	<div class="card-body">

        <h4>Membership Report</h4>

        <p>	This is a simple count of current members by class.</p>

        <form action="reports3.php" method="post" id="reportForm">

			<button type="submit" class="btn btn-primary">Get Report</button>

		</form>
	</div>
</div>


	</div>
</div>

<?php end_page(); ?>
