<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// array_push($allowGroups, 'ROmembers');
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';
require '../../support/formFunctions.php';
include 'includes/tools.php';


$catID = inty($_REQUEST['id']);

if ($catID) {
	$pageTitle = 'Edit Category';

	$volCat = new VolCats($catID);
	if (empty($volCat->catID)) {
		error('Error', 'Did not find a category with catID=' . $catID);
		exit;
	}
} else {
	$pageTitle = 'New Category';
	$volCat = new VolCats(); // blank object
}

start_page('CAC Volunteers');
?>
<script>
var catID = <?= $catID ?>;

function confirmDelete() {
    var agree=confirm("Deletion cannot be undone.\n\nAre you sure you want to delete this Opportunity Category?");
    if (agree) {
    	$.post('ajax/deleteCat.php', {catID: catID}, function() {
			location.href = "index.php";
		});
    }
}
</script>
<?php
start_content();
toolbar();
?>

<div class="row justify-content-center">
	<div class="col-xl-10">

	<div class="row">
		<div class="col-lg-5 offset-lg-3 mt-3"><h3><?= $pageTitle ?></h3></div>
	</div>

	<form action="edit2cat.php" method="post" id="oppForm">
		<input type="hidden" name="catID" value="<?= $volCat->catID ?>">
<?php
edit_field('Category Name', 'catName', $volCat->catName, 60, 'required');
static_submit();
?>

	</form>
	</div>
</div>

<?php end_page(); ?>
