<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
//array_push($allowGroups, 'ROmembers');
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';

require_once '../../../wp-load.php';
require_once ABSPATH . 'wp-admin/includes/user.php';
require 'includes/welcome.php';
include 'includes/tools.php';

// Ensure we came from a POST.
requirePost();

inty($_POST['applicantID']);

$title = 'CAC Membership';
$pageDescription = 'Membership Application';

$applicant = new Applicants($_POST['applicantID']);
if (!$applicant->applicantID) {
	error('Error', 'Did not find an application with applicantID ' . $_POST['applicantID'] . '.');
	exit();
}

$displayResult = '';

// create the payment
$oPmt = new Payments();
$oPmt->amount = $applicant->entryFee + $applicant->dues;
$oPmt->pmtType = $_POST['pmtType'];
$oPmt->item = $applicant->applyType . ' Application';
$oPmt->pmtAmount = $_POST['pmtAmount'];
$oPmt->transNote = $_POST['transNote'];
$oPmt->dbSave();

$applicant->pmtID = $oPmt->pmtID;
$applicant->adminID = $_SESSION['cac_login']['memberID']; // logged in user
$applicant->dbSave();

// new members and reinstatements are quite different
if (strpos($applicant->applyType, 're')) {
	$member = reinstate();
} else {
	$member = newmember();
}

start_page($title, $pageDescription);
start_content();
toolbar();
?>

<h2 class="pageheader">New Member from Application - Results</h2>

<div class="row">
	<div class="col-lg-10 offset-lg-1">

		<pre class="mt-3">
<?php echo $displayResult; ?>
		</pre>

<?php if ($member->memberID): ?>
	<a class="btn btn-primary" href="detail.php?id=<?= $member->memberID ?>">Member detail page</a>
<?php endif; ?>

	</div>
</div>

<?php
end_page();

function newMember() {
	global $applicant, $displayResult;

	$membership = new Memberships(); // blank Memberships object

	switch ($applicant->applyType) {
		case 'RG':
		case 'JT':
		case 'ST':
			// make a new membership
			$membership->class = $applicant->applyType;
			$membership->street = $applicant->street;
			$membership->city = $applicant->city;
			$membership->state = $applicant->state;
			$membership->zip = $applicant->zip;
			$membership->sponsors = $applicant->sponsor1 . ' & ' . $applicant->sponsor2;

			// expiration is always March 31 of next year
			$expYear = date('Y') + 1;
			$membership->expiration = $expYear . '-03-31';

			$membership->dbSave();
			$displayResult .= 'created a new ' . $membership->class . ' membership with membershipID = ' .
				$membership->membershipID . "\n";
			break;

		case 'JTadd':
			$addToMember = new Members($applicant->addToMemberID);
			$membership->getByID($addToMember->membershipID);

			switch ($membership->class) {
				case 'SR':
				case 'JS':
					// current member is Senior.
					$age = $applicant->getAge();
					if ($age >= 70) {
						// keep the current member's Senior status
						$membership->class = 'JS';
					} else {
						// might still be OK if combined age is high enough
						if ($addToMember->dob) {
							$memberAge = $applicant->getAge($addToMember->dob);
							$totalAge = $memberAge + $age;
							if ($totalAge >= 140) {
								$membership->class = 'JS';
							}
						}
					}
					break;
				default:
					// regular joint membership
					$membership->class = 'JT';
			}
			$membership->dbSave();
			break;
	}

	// make new member
	$member = new Members(); // blank Members object
	$member->membershipID = $membership->membershipID;

	$member->first = $applicant->first;
	$member->m_i = $applicant->m_i;
	$member->last = $applicant->last;
	$member->email = $applicant->email;
	$member->phone = $applicant->phone;
	$member->mobile = $applicant->mobile;
	$member->dob = $applicant->dob;

	// create a new WordPress user
	$_POST['first_name'] = $member->first;
	$_POST['last_name'] = $member->last;
	$_POST['email'] = $member->email;
	$login = strtolower($member->first . '_' . $member->last);
	$_POST['user_login'] = preg_replace('/\W/', '', $login); // remove non-alphanumeric

	$wpID = wpAddUser();
	if ($wpID) {
		$member->wpID = $wpID;
	} else {
		$member->wpID = 0;
	}
	$member->dbSave();
	$displayResult .= 'created a new member ' . $member->first . ' ' . $member->last . ' with memberID = ' .
		$member->memberID . "\n";

	if (sendWelcome($member)) {
		$displayResult .= 'Sent welcome email to ' . $member->email . "\n";
	} else {
		$displayResult .= 'Could not send welcome email to ' . $member->first . "\n";
	}

	if ($applicant->applyType == 'JT') {
		// make new joint member
		$member2 = new Members(); // blank Members object
		$member2->membershipID = $membership->membershipID;
		$member2->first = $applicant->first2;
		$member2->m_i = $applicant->m_i2;
		$member2->last = $applicant->last2;
		$member2->email = $applicant->email2;
		$member2->phone = $applicant->phone2;
		$member2->mobile = $applicant->mobile2;
		$member2->dob = $applicant->dob2;

		$_POST['first_name'] = $member2->first;
		$_POST['last_name'] = $member2->last;
		$_POST['email'] = $member2->email;
		$login = strtolower($member2->first . '_' . $member2->last);
		$_POST['user_login'] = preg_replace('/\W/', '', $login); // remove non-alphanumeric

		$wpID = wpAddUser();
		if ($wpID) {
			$member2->wpID = $wpID;
		} else {
			$member2->wpID = 0;
		}
		$member2->dbSave();
		$displayResult .= 'created a new member ' . $member2->first . ' ' . $member2->last . ' with memberID = ' .
			$member2->memberID . "\n";

		if (sendWelcome($member2)) {
			$displayResult .= 'Sent welcome email to ' . $member2->email . "\n";
		} else {
			$displayResult .= 'Could not send welcome email to ' . $member2->first . "\n";
		}
	}

	$applicant->status('complete'); // finished with this application
	$displayResult .= 'marked the application as complete';

	return $member;
}

function reinstate() {
	global $applicant, $displayResult;

	$member = new Members($applicant->addToMemberID, 'membership');

	$member->first = $applicant->first;
	$member->m_i = $applicant->m_i;
	$member->last = $applicant->last;
	$member->email = $applicant->email;
	$member->phone = $applicant->phone;
	$member->mobile = $applicant->mobile;
	$member->dob = $applicant->dob;

	if (!$member->wpID) {
		// create a new WordPress user
		$_POST['first_name'] = $member->first;
		$_POST['last_name'] = $member->last;
		$_POST['email'] = $member->email;
		$login = strtolower($member->first . '_' . $member->last);
		$_POST['user_login'] = preg_replace('/\W/', '', $login); // remove non-alphanumeric

		$wpID = wpAddUser();
		if ($wpID) {
			$member->wpID = $wpID;
		} else {
			$member->wpID = 0;
		}
	}
	$member->dbSave();
	$displayResult .= 'updated member ' . $member->first . ' ' . $member->last . "\n";

	if (sendWelcome($member)) {
		$displayResult .= 'Sent welcome email to ' . $member->email . "\n";
	} else {
		$displayResult .= 'Could not send welcome email to ' . $member->first . "\n";
	}

	$oldClass = new Classes($member->membership->class);
	$newClass = new Classes(str_replace('re', '', $applicant->applyType));

	// if oldClass is not joint then neither is newClass
	if ($oldClass->joint) {
		$jointMembers = $member->membership->getJointMembers();
		if (count($jointMembers) == 2) {
			foreach ($jointMembers as $jointMember) {
				if ($jointMember->memberID == $member->memberID) continue;
				$member2 = $jointMember;
			}
		} else {
			$member2 = false;
		}

		if ($newClass->joint) {
			$member2->first = $applicant->first2;
			$member2->m_i = $applicant->m_i2;
			$member2->last = $applicant->last2;
			$member2->email = $applicant->email2;
			$member2->phone = $applicant->phone2;
			$member2->mobile = $applicant->mobile2;
			$member2->dob = $applicant->dob2;

			if (!$member2->wpID) {
				// create a new WordPress user
				$_POST['first_name'] = $member2->first;
				$_POST['last_name'] = $member2->last;
				$_POST['email'] = $member2->email;
				$login = strtolower($member2->first . '_' . $member2->last);
				$_POST['user_login'] = preg_replace('/\W/', '', $login); // remove non-alphanumeric

				$wpID = wpAddUser();
				if ($wpID) {
					$member2->wpID = $wpID;
				} else {
					$member2->wpID = 0;
				}
			}
			$member2->dbSave();
			$displayResult .= 'updated member ' . $member2->first . ' ' . $member2->last . "\n";

			if (sendWelcome($member2)) {
				$displayResult .= 'Sent welcome email to ' . $member2->email . "\n";
			} else {
				$displayResult .= 'Could not send welcome email to ' . $member2->first . "\n";
			}
		} else {
			// oldClass was joint but newClass is not.
			if ($member2) {
				// orphan the old joint member
				$newMembership = new Memberships();
				$newMembership->class = 'OR';
				$newMembership->expiration = date('Y-m-d'); // today
				$newMembership->street = $member->membership->street;
				$newMembership->city = $member->membership->city;
				$newMembership->state = $member->membership->state;
				$newMembership->zip = $member->membership->zip;
				$newMembership->sponsors = $member->membership->sponsors;
				$newMembership->dbSave();
				$displayResult .= 'created a new orphan membership' . "\n";

				$member2->membershipID = $newMembership->membershipID;
				$member2->dbSave();
				$displayResult .= 'assigned orphan membership to ' . $member2->fullname . "\n";
			}
		}
	}

	$member->membership->street = $applicant->street;
	$member->membership->city = $applicant->city;
	$member->membership->state = $applicant->state;
	$member->membership->zip = $applicant->zip;
	$member->membership->sponsors = $applicant->sponsor1 . ' & ' . $applicant->sponsor2;

	$expYear = date('Y') + 1; // expiration is always March 31 of next year
	$member->membership->expiration = $expYear . '-03-31';
	$member->membership->class = $newClass->class;
	$member->membership->resigned = 0;
	$member->membership->resignedDate = null;
	$member->membership->dbSave();
	$displayResult .= 'updated expiration for ' . $newClass->class . " membership\n";

	$applicant->status('complete'); // finished with this application
	$displayResult .= 'marked the application as complete';
	return $member;
}
?>
