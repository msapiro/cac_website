<?php
require '../../support/config.php';
require CLASSLOADER;

require '../includes/config.php'; // contains $allowGroups for this directory
//array_push($allowGroups, 'ROmembers');
require AUTH_AJAX;                // make sure user is logged in and a group member

// get information about a member and membership


$data = array();
inty($_POST['applicantID']);
inty($_POST['memberID']);

$applicant = new Applicants($_POST['applicantID']);
$member = new Members($_POST['memberID'], 'membership'); // the addTo member

if (! $applicant->applicantID) {
	stopNow('Applicant not found!');
}

if (! $member->memberID) {
	$applicant->addToMemberID = NULL;
	$applicant->dbSave();
	stopNow('Member not found!');
}

if ($member->membership->isExpired()) stopNow($member->fullname . ' membership is expired!');

if ($member->membership->class == 'LM') {
	stopNow($member->fullname . ' is a Life Member. Cannot switch to joint membership.');
}

if ($member->membership->joint) {
	$membersArray = $member->membership->getJointMembers();
	if (count($membersArray) > 1) {
		stopNow($member->fullname . ' joint membership already has two members!');
	}
}

if ($member->membership->class == 'JL') {
	// this should not happen. JL should always be pairs, caught by test above
	stopNow($member->fullname . ' is a Joint Life Member. Cannot switch to regular joint membership.');
}

$data['success'] = 1;
$data['memberID'] = $member->memberID;

$applicant->addToMemberID = $member->memberID;
$applicant->dbSave();

echo json_encode($data);
exit();

function stopNow($msg) {
	global $data;
	$data['success'] = 0;
	$data['msg'] = $msg;
	echo json_encode($data);
	exit();
}
