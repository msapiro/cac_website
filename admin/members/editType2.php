<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';


requirePost();

if (empty ($_POST['class'])) {
	error('Error', 'The membership type ID was not provided.');
}

$mType = new Classes(); // empty object


$mType->class = $_POST['class'];
$mType->description = $_POST['description'];
$mType->joint = booly($_POST['joint']); // forces 0 if not checked
$mType->dues = $_POST['dues'];
$mType->entryFee = $_POST['entryFee'];
$mType->showUsers = booly($_POST['showUsers']);
$mType->showAdmin = booly($_POST['showAdmin']);
$mType->dbSave();

header("Location: types.php");
exit;
