<?php
require '../../support/config.php';
require CLASSLOADER;

require AUTH_AJAX; // enforce login

// save volunteer notes

$data = array();

// logged in user
$user = new Members('login', 'membership');
if (empty($user->memberID)) {
	$data['msg'] = 'Did not find the logged-in member.';
	echo json_encode($data);
	exit;
}

inty($_POST['memberID']);

$member = new Members($_POST['memberID']);
if (empty ($member->memberID)) {
	$data['msg'] = "Did not find a member with memberID=" . $_POST['memberID'];
	echo json_encode($data);
	exit;
}

if ($user->memberID != $member->memberID) {
	// users can only edit themselves and joint members
	if ($user->membershipID != $member->membershipID) {
		$data['msg'] = 'You can only edit yourself and your joint member.';
		echo json_encode($data);
		exit;
	}
}

$volNote = new VolNotes();
$volNote->memberID = $member->memberID;

if (isset($_POST['delete'])) {
	$volNote->dbDelete();
	$data['msg'] = "Successfully deleted comments for $member->first.";
} else {
	$volNote->notes =  $_POST['notes'];
	$volNote->dbSave();
	$data['msg'] = "Successfully saved comments for $member->first.";
}

echo json_encode($data);
