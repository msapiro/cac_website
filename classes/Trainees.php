<?php

class Trainees extends Model {

	public $traineeID;
	public $memberID;
	public $lodge;
	public $year;

	public function __construct($traineeID = 0) {
		parent::__construct(); // set up our database
		if ($traineeID) {
			$this->getByID($traineeID);
		}
	}

	public function getByID($traineeID) {
		// get the values for an existing trainee
		$row = $this->db->get('trainees', '*', ['traineeID' => $traineeID]);
		if (! $row) return;
		foreach ($row as $key => $value) {
			$this->$key = $value;
		}
	}

	public function getAllTrainings($member) {
		// get all the trainings for a member
		// return an array of trainee objects
		$trainings = array();
		$rows = $this->db->select('trainees', 'traineeID', ['memberID' => $member, 'ORDER' => ['lodge', 'year']]);
		foreach ($rows as $traineeID) {
			$trainings[] = new Trainees($traineeID);
		}
		return $trainings;
	}

	public function dbSave() {
		// save the trainee info into the database table
		$dataArray = [
			'memberID' => $this->memberID,
			'lodge' => $this->lodge,
			'year' => $this->year,
		];

		if ($this->traineeID) {
			// update existing trainee
			$this->db->update('trainees', $dataArray, ['traineeID' => $this->traineeID] );
		} else {
			// create new trainee
			$this->db->insert('trainees', $dataArray);
			$this->traineeID = $this->db->id();
		}
		return $this->traineeID;
	}

	public function dbDelete() {
		if ($this->traineeID) {
			$obj = $this->db->delete('trainees', ['traineeID' => $this->traineeID]);
			$ret = $obj->rowCount(); // number of affected rows
		} else {
			$ret = 0;
		}
		return $ret;
	}

} // end class
