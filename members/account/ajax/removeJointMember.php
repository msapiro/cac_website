<?php
require '../../support/config.php';
require CLASSLOADER;

require AUTH_AJAX; // enforce login


// remove a member from a joint membership.
// create a new "orphan" membership for the removed member rather than deleting the member.

inty($_POST['memberID']);
$orphanType = 'OR'; // this is the id for the "orphan" membership type
$data = array();

$orphan = new Members($_POST['memberID'],'membership');
if (empty($orphan->memberID) || $orphan->membership->joint != 1) {
	$data['success'] = 0;
	echo json_encode($data);
	exit;
}

// security check. make sure the logged in user and member are really in the same family
$user = new Members('login');
if ($user->membershipID <> $orphan->membershipID) {
	// do not allow user to remove a member from some other family membership
	$data['success'] = 0;
	echo json_encode($data);
	exit;
}

$data['success'] = 1;
$today = date('Y-m-d');

// make a new expired orphan membership, copying some of the data from the family
$newMembership = new Memberships();
$newMembership->class = $orphanType;
$newMembership->expiration = $today;
$newMembership->street = $orphan->membership->street;
$newMembership->city = $orphan->membership->city;
$newMembership->state = $orphan->membership->state;
$newMembership->zip = $orphan->membership->zip;
$membershipIDnew = $newMembership->dbSave();

// assign the new membership to the orphan
$orphan->membershipID = $membershipIDnew;

if (! is_null($orphan->email) && trim($orphan->email) != '') {
	// see if the orphan has a shared email address
	$memberArray = $orphan->getByEmail($orphan->email);
	if (count($memberArray) > 1) {
		// edit the email address for the orphaned member to avoid conflicts
		$orphan->email .= '.orphan';
	}
}

$orphan->dbSave();

echo json_encode($data);
