<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

$title = 'CAC Membership';
$pageDescription= 'California Alpine Club Renewal';

require '../../support/functions.php';
require '../../support/formFunctions.php';
include 'includes/tools.php';


inty($_GET['id']);

// flag for a reinstatement
if (isset($_GET['reinstate']) and $_GET['reinstate'] == 'yes') {
	$reinstate = true;
} else {
	$reinstate = false;
}

// get info on this member
$member = new Members($_GET['id'], ['membership']);
if (empty($member->memberID)) {
	error('Error', 'Did not find a member with memberID=' . $_GET['id']);
	exit;
}

// get a list of membership classes
$classArray = (new Classes())->getArray('admin');

// expiration is always March 31, renewal is delinquent (but allowed) after July 1.
$expireYear = explode('-', $member->membership->expiration)[0];
$thisYear = date('Y');

if (! $member->membership->expired) {
	// current member. Add one year to expiration.
	$newExpiration = ($expireYear + 1) . '-03-31';
} elseif ($reinstate) {
	// a reinstatement
        $newExpiration = ($thisYear + 1) . '-03-31';
} else {
	// expired member
	$thisMonth = date('n');
	if ($thisMonth >= 4) {
		// renewals from April 1 to Dec 31 must be for current term
		$newExpiration = ($thisYear + 1) . '-03-31';
	} else {
		// renewals from Jan 1 and April 1
		if ($expireYear == $thisYear - 1) {
			// this is probably a late renewal for the current term
			$newExpiration = ($thisYear) . '-03-31';
		} else {
			// there must have been a gap in membership and this renewal is for next term
			// this would work for reinstatement as renewal, but only after April 1
			$newExpiration = ($thisYear + 1) . '-03-31';
		}
	}
}

$memDescription = $member->membership->memDescription;
$family = $member->membership->joint;
if ($family) {
	$familyArray = $member->membership->getJointMembers();
	$family = count($familyArray);
	$memDescription .= ' (' . $family . ' members)';
}

start_page($title, $pageDescription);
?>
<script>
var classArray = <?php echo json_encode($classArray, JSON_NUMERIC_CHECK) ?>;

//is the current membership a family? If so, number of family members.
var currentIsFamily = parseInt(<?= $family ?>);
var dues = 0;

$(function() {
    // do stuff when DOM is ready

	updateDues();

    $("input[name='membershipType']").click(function() {
		updateDues();
		testMembershipChange();
    });

	$('#pmtType').change(function() {
		var pmtType = $(this).val();
		if (pmtType == 'cash' || pmtType == 'check') {
			updateAmount();
		} else {
			$("#pmtAmount").val('');
		}
    });

});

function updateAmount() {
	// update the amount in the transaction detail
	var amt = Number(dues) + Number($("#fdnAmount").val()) + Number($("#gfAmount").val());
	$("#pmtAmount").val(amt);
}

function updateDues() {
	// update the dues according to current settings of membership type and term
	var membershipType = $("input[name='membershipType']:checked").val();
	dues = ( classArray.find(x => x.class == membershipType).dues ).toFixed(2);
	$("#dues").html("$" + dues);
	$('#pmtType').change();
}

function testMembershipChange() {
	// warn about certain changes
	var membershipType = $("input[name='membershipType']:checked").val();
	var selectFamily =( classArray.find(x => x.class == membershipType) ).joint;

	if (selectFamily) {
		if (! currentIsFamily) {
			alert("You are changing from Individual membership to Joint."
				+ "\n\nYou can add the second member after completing renewal.");
		}
	} else {
		if (currentIsFamily > 1) {
			// only need to warn if there are other family members
			alert("You are changing from Joint membership to Individual."
				+ "\n\nThe second member will retain their current Joint membership and"
				+ " this member will get a new Individual membership.");
		}
	}
}
</script>
<?php
start_content();
toolbar();
?>

<?php if ($reinstate): ?>
<h2 class="pageheader">Membership Reinstatement</h2>
<?php else: ?>
<h2 class="pageheader">Membership Renewal</h2>
<?php endif; ?>

<div class="row">
	<div class="col-xl-10 offset-xl-1">

		<div class="row">

			<div class="col-lg"> <!-- left column -->
				<table class="table table-sm table-striped">
				<thead>
				<tr><th colspan="2">Personal Data</th></tr>
				</thead>
				<tbody>
				<tr>
					<td style="width: 120px">memberID</td>
					<td>
						<a href="detail.php?id=<?= $member->memberID ?>"><?= $member->memberID ?></a>
					</td>
				</tr>
				<tr><td>Name</td><td><?= $member->fullname ?></td></tr>
				<tr><td>Email</td><td><?= $member->email ?></td></tr>
				</tbody>
				</table>
			</div>	<!-- end left column -->

			<div class="col-lg"> <!-- right column -->
				<table class="table table-sm table-striped">
				<thead>
				<tr><th colspan="2">Membership Data</th></tr>
				</thead>
				<tbody>
				<tr>
					<td style="width: 120px">membershipID</td>
					<td><?= $member->membership->membershipID ?></td>
				</tr>
				<tr><td>Type</td><td><?= $memDescription ?></td></tr>
				<tr>
					<td>Expiration</td>
					<td>
<?php
	echo $member->membership->expiration;
	if ($member->membership->expired) {
		echo ' <span style="color: red">EXPIRED</span>';
	}
?>
					</td>
				</tr>
				</tbody>
				</table>
			</div> <!-- end right column -->

		</div> <!-- end row -->

		<form id="formRenew" action="renew2.php" method="post"
				onkeydown="return event.key != 'Enter';">
			<input type="hidden" name="memberID" value="<?= $member->memberID ?>">
			<input type="hidden" name="newExpiration" value="<?= $newExpiration ?>">
			<input type="hidden" name="reinstatement" value="<?= $reinstate ?>">
			<div class="row mb-2">
				<label class="col-lg-3 text-lg-end"><b>Membership Type</b></label>
				<div class="col-lg-5">
<?php
foreach ($classArray as $oType):
	if (strtolower($oType->class) == 'or') continue;
	$display = $oType->description . ' - $' . $oType->dues;
?>
					<div class="form-check">
<?php if (!$reinstate or $oType->class == $member->membership->class): ?>
						<input class="form-check-input" type="radio" name="membershipType"
							id="mem_<?= $oType->class ?>" value="<?= $oType->class ?>"
<?php if ($oType->class == $member->membership->class) echo ' checked' ?>>
						<label class="form-check-label" for="mem_<?= $oType->class ?>">
							<?= $display ?>
						</label>
<?php endif; ?>
					</div>
<?php
endforeach; ?>
				</div>
			</div>

			<div class="row mb-2">
				<label class="col-lg-3 text-lg-end"><b>Dues</b></label>
				<div class="col-lg-5">
					<span id="dues"><em style="color: #777">Select type above</em></span>
				</div>
			</div>

			<div class="row">
				<label class="col-lg-3 text-lg-end"><b>New Expiration</b></label>
				<div class="col-lg-5">
					<span id="expiration"><?= $newExpiration ?></span>
				</div>
			</div>

			<hr class="cac">

<?php
edit_field_prepend('CAC Foundation donation', 'fdn', '$', false, 10, false, 'fdnAmount');
edit_field_prepend('General Fund donation', 'gf', '$', false, 10, false, 'gfAmount');
?>

			<div class="card border-success">
		    	<h4 class="card-header text-white bg-success">Transaction Detail</h4>
				<div class="card-body">

					<div class="row align-items-center mb-3">
						<label class="col-lg-3 text-lg-end"><b>Payment type</b></label>
						<div class="col-lg-5">
							<select name="pmtType" id="pmtType" class="form-select">
								<option value="0">Select</option>
								<option value="check">Check</option>
								<option value="cash">Cash</option>
								<option value="none">No Transaction</option>
							</select>
						</div>
					</div>

					<div id="amountRow">
<?php edit_field_prepend('Amount', 'pmtAmount', '$', false, 10, false, 'pmtAmount'); ?>
					</div>

<?php edit_field('Transaction Note', 'transNote', false, 100, 'Check number, etc'); ?>
				</div> <!-- end card body -->
			</div> <!-- end card -->

				<div class="row mt-3 mb-3">
					<div class="col-lg-5 offset-lg-3">
						<div class="btn-group">
							<button type="submit" class="btn btn-primary">Submit</button>
							<a class="btn btn-secondary"
								href="detail.php?id=<?= $member->memberID ?>">
								Cancel
							</a>
						</div>
					</div>
				</div>
		</form>

	</div>
</div> <!-- end row -->

<?php end_page(); ?>
