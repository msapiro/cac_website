<?php
// ajax call to cancel a stripe transaction before it happens - no login required.
// this just cleans up the tables, since we know the transaction will never be processed.
require '../../support/config.php';
require CLASSLOADER;

session_name('CACSESSID');
session_start();

// we use a session variable rather than get or post so user cannot submit an arbitrary stripeID
if (empty($_SESSION['cancelStripe'])) exit;

$stripe = new Stripes($_SESSION['cancelStripe']);
if (empty ($stripe->stripeID)) exit;
if ($stripe->status != 'pending') exit;

$memberTrans = new MemberTrans();
$memberTrans->getByStripeID($stripe->stripeID);
$memberTrans->dbDelete();

$oDonations = new Donations();
$donationArray = $oDonations->getByStripeID($stripe->stripeID); // includes FDN and GF
foreach ($donationArray as $donation) {
	$donation->dbDelete();
}

$stripe->dbDelete();
