<?php

class Volunteers extends Model {

	public $volID;
	public $memberID;
	public $oppID;
	public $oppName;
	public $description;
	public $catID;
	public $catName;

	public function __construct($volID=0) {
		parent::__construct(); // set up our database
		if ($volID) {
			$this->getByID($volID);
		}
	}

	public function getByID($volID) {
		$row = $this->db->get('volMembers', [
			"[>]volOpps" => "oppID",
			"[>]volCategories" => "catID"
		],
		['volID', 'memberID', 'oppID', 'oppName', 'description', 'catID', 'catName'],
		['volID' => $volID]);

		if (! $row) return;
		foreach ($row as $key => $value) {
			$this->$key = $value;
		}
	}

	public function getByMemberID($memberID) {
		// return an array of volunteer opportunities assigned to this member
		$volArray = array();
		$rows = $this->db->select('volMembers', 'volID', ['memberID' => $memberID, 'ORDER' => 'volID']);
		foreach ($rows as $volID) {
			$volArray[] = new Volunteers($volID);
		}
		return $volArray;
	}

	public function dbSave() {
		// save the volunteer info into the database table
		$dataArray = [
			'memberID' => $this->memberID,
			'oppID' => $this->oppID
		];

		if ($this->volID) {
			// update existing
			$this->db->update('volMembers', $dataArray, ['volID' => $this->volID] );
		} else {
			// create new
			$this->db->insert('volMembers', $dataArray);
			$this->volID = $this->db->id();
		}
		return $this->volID;
	}

	public function dbDelete() {
		if (empty($this->volID)) {
			// find the volID from the member and opp
			$this->db->delete('volMembers', ['memberID' => $this->memberID, 'oppID' => $this->oppID]);
		} else {
			$this->db->delete('volMembers', ['volID' => $this->volID]);
		}
	}
} // end Volunteers class

class VolOpps extends Model {

	public $oppID;
	public $catID;
	public $oppName;
	public $catName;
	public $description = NULL;
	private $sortorder;

	public function __construct($oppID=0) {
		parent::__construct(); // set up our database
		if ($oppID) {
			$this->getByID($oppID);
		}
	}

	public function getByID($oppID) {
		$row = $this->db->get('volOpps', ["[>]volCategories" => "catID"],
			'*',
			['oppID' => $oppID]);

		if (! $row) return;
		foreach ($row as $key => $value) {
			$this->$key = $value;
		}
	}

	public function getAll() {
		// return an array of all VolOpps objects
		$oppArray = array();
		$rows = $this->db->select('volOpps', 'oppID', ['ORDER' => ['catID', 'sortorder']]);
		foreach ($rows as $oppID) {
			$oppArray[] = new VolOpps($oppID);
		}
		return $oppArray;
	}

	public function getByCategory($catID) {
		// return an array of VolOpps objects for one category
		$oppArray = array();
		$rows = $this->db->select('volOpps', 'oppID', ['catID' => $catID, 'ORDER' => 'sortorder']);
		foreach ($rows as $oppID) {
			$oppArray[] = new VolOpps($oppID);
		}
		return $oppArray;
	}

	public function getCount() {
		// count the number of non-expired volunteers assigned to this opportunity
		// include expired members until delinquent (June 30)
		$count = $this->db->count("volMembers",
			["[><]members" => "memberID", "[><]memberships" => "membershipID"], // joins
			"volID",
				[
					"oppID" => $this->oppID,
					"expiration[>]" => Medoo\Medoo::raw("DATE_SUB(CURDATE(), INTERVAL 3 MONTH)")
				]);
		return $count;
	}

	public function getVolunteers() {
		// similar to getCount() but return an array of arrays (volID, memberID)
		$volunteerArray = $this->db->select("volMembers",
			["[><]members" => "memberID", "[><]memberships" => "membershipID"], // joins
			["volID", "memberID"],
			[
				"oppID" => $this->oppID,
				"expiration[>]" => Medoo\Medoo::raw("DATE_SUB(CURDATE(), INTERVAL 3 MONTH)"),
				"ORDER" => "first"
			]);
		return $volunteerArray;
	}

	public function dbSave() {
		// save the info into the database table
		$dataArray = [
			'catID' => $this->catID,
			'oppName' => $this->oppName,
			'description' => nully($this->description)
		];

		if ($this->oppID) {
			// update existing
			$this->db->update('volOpps', $dataArray, ['oppID' => $this->oppID] );
		} else {
			// get current sort order
			$oSortOrder = new OppsOrder($this->catID);

			// create new opp
			$this->db->insert('volOpps', $dataArray);
			$this->oppID = $this->db->id();

			// put new opp at the front of the sort order
			array_unshift($oSortOrder->orderArray, $this->oppID);
			$oSortOrder->dbSave();
		}
		return $this->oppID;
	}

	public function dbDelete() {
		if ($this->oppID) {
			// delete all member assignments for this opportunity
			$this->db->delete('volMembers', ['oppID' => $this->oppID]);

			// delete the opportunity itself
			$obj = $this->db->delete('volOpps', ['oppID' => $this->oppID]);
			$ret = $obj->rowCount(); // number of affected rows
		} else {
			$ret = 0;
		}
		return $ret;
	}
} // end VolOpps class

class OppsOrder extends Model {

	public $orderArray = array();

	public function __construct($catID = 0) {
		parent::__construct(); // set up our database
		if ($catID) {
			$this->getOrder($catID);
		}
	}

	public function dbSave() {
		foreach ($this->orderArray as $order => $oppID) {
			$this->db->update('volOpps', ["sortorder" => $order], ['oppID' => $oppID] );
		}
	}

	public function getOrder($catID) {
		$this->orderArray = $this->db->select('volOpps', 'oppID', ['catID' => $catID, 'ORDER' => 'sortorder']);
	}
} // end OppsOrder class

class VolCats extends Model {

	public $catID;
	public $catName;
	private $sortorder;

	public function __construct($catID=0) {
		parent::__construct(); // set up our database
		if ($catID) {
			$this->getByID($catID);
		}
	}

	public function getByID($catID) {
		$row = $this->db->get('volCategories', ['catID', 'catName'], ['catID' => $catID, 'ORDER' => 'sortorder']);

		if (! $row) return;
		foreach ($row as $key => $value) {
			$this->$key = $value;
		}
	}

	public function getAll() {
		// return an array of all VolCats objects
		$catArray = array();
		$rows = $this->db->select('volCategories', 'catID', ['ORDER' => 'sortorder']);
		foreach ($rows as $catID) {
			$catArray[] = new VolCats($catID);
		}
		return $catArray;
	}

	public function getCount() {
		// count the number of opportunites assigned to this category
		$count = $this->db->count("volOpps", ["catID" => $this->catID]);
		return $count;
	}

	public function dbSave() {
		// save the info into the database table
		$dataArray = [
				'catID' => $this->catID,
				'catName' => $this->catName
		];

		if ($this->catID) {
			// update existing
			$this->db->update('volCategories', $dataArray, ['catID' => $this->catID] );
		} else {
			// get current sortorder
			$oSortOrder = new CatsOrder(true);

			// create new category
			$this->db->insert('volCategories', $dataArray);
			$this->catID = $this->db->id();

			// put new category at the front of the sort order
			array_unshift($oSortOrder->orderArray, $this->catID);
			$oSortOrder->dbSave();
		}
		return $this->catID;
	}

	public function dbDelete() {
		if ($this->catID) {
			$obj = $this->db->delete('volCategories', ['catID' => $this->catID]);
			$ret = $obj->rowCount(); // number of affected rows
		} else {
			$ret = 0;
		}
		return $ret;
	}
} // end VolCats class

class CatsOrder extends Model {

	public $orderArray = array();

	public function __construct($get = false) {
		parent::__construct(); // set up our database
		if ($get) {
			$this->getOrder();
		}
	}

	public function dbSave() {
		foreach ($this->orderArray as $order => $catID) {
			$this->db->update('volCategories', ["sortorder" => $order], ['catID' => $catID] );
		}
	}

	public function getOrder() {
		$this->orderArray = $this->db->select('volCategories', 'catID', ['ORDER' => 'sortorder']);
	}
} // end CatsOrder class

class VolNotes extends Model {

	public $memberID;
	public $notes = NULL;

	public function __construct($memberID=0) {
		parent::__construct(); // set up our database
		if ($memberID) {
			$this->getByID($memberID);
		}
	}

	public function getByID($memberID) {
		$this->memberID = $memberID;
		$this->notes = $this->db->get('volNotes', 'notes', ['memberID' => $memberID]);
	}

	public function dbSave() {
		// save the info into the database table (replace if exists)
		// medoo doesn't seem to have REPLACE INTO function, so delete old version first
		$this->dbDelete();
		if (! empty($this->notes)) $this->db->insert('volNotes', ['memberID' => $this->memberID, 'notes' => $this->notes]);
	}

	public function dbDelete() {
		$this->db->delete('volNotes', ['memberID' => $this->memberID]);
	}

}