<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
array_push($allowGroups, 'ROmembers');
require AUTH_PAGE; // make sure user is logged in and a group member

$title = 'CAC Membership';
$pageDescription = 'Application Details';

require '../../support/functions.php';
require '../../support/formFunctions.php';
include 'includes/tools.php';

inty($_GET['id']);

$applicant = new Applicants($_GET['id']);
if (!$applicant->applicantID) {
	error('Error', 'Did not find an application with applicantID ' . $_GET['id'] . '.');
	exit();
}

$reinstate = false;
switch ($applicant->applyType) {
	case 'RG':
		$appType = 'Individual Membership';
		break;
	case 'JT':
		$appType = 'Joint Membership (two new members)';
		break;
	case 'JTadd':
		$addToWarning = false;
		$addToDanger = false;
		$appType = 'Joint Membership<br>add to current member ' . $applicant->addToMember;
		if ($applicant->addToMemberID) {
			$oAddTo = new Members($applicant->addToMemberID, 'membership');
			$addToDisplay = '<a href="detail.php?id=' . $oAddTo->memberID . '">' . $oAddTo->fullname . '</a>';
			$addToDisplay .= ' memberID=' . $oAddTo->memberID . ' ' . $oAddTo->membership->memDescription;

			if (in_array($oAddTo->membership->class, ['SR','JS'])) {
				// adding to senior or joint senior. can we verify ages?
				if (empty($applicant->dob)) {
					// cannot verify, all we can do is switch to regular joint membership
					$addToDanger = 'Trying to join with Senior member, but applicant DOB is unknown.<br>';
					$addToDanger .= 'Please fix by editing the application before proceeding!';
				} else {
					// calculate age
					$age = $applicant->getAge();
					if ($age >= 70) {
						$addToWarning = "Applicant (age $age) will be added as Senior Joint Member.";
					} else {
						// might still be OK if current member is old enough
						if ($oAddTo->dob) {
							$memberAge = $applicant->getAge($oAddTo->dob);
							$totalAge = $memberAge + $age;
							if ($totalAge >= 140) {
								$addToWarning = "Applicant (age $age) will be added as Senior Joint Member.<br>
									Combined age is $totalAge.";
							} else {
								$addToWarning = "Applicant (age $age) and combined age ($totalAge) do not qualify
									as Senior.<br>Membership will be converted to Regular Joint.";
							}
						} else {
							$addToWarning = "Applicant is age $age. Current senior member DOB unknown. If you can
								update member's DOB record the Membership might qualify as Senior Joint. Otherwise
								it will be converted to Regular Joint.";
						}
					}
				}
			}
		} else {
			$addToDisplay = '<span style="color: red">Please edit using the <b>Edit Application</b> button below.</span>';
		}
		break;
	case 'ST':
		$appType = 'Student Membership';
		break;
	case 'RGre':
		$appType = 'Reinstate Individual Membership';
		$reinstate = true;
		break;
	case 'JTre':
		$appType = 'Reinstate Joint Membership';
		$reinstate = true;
		break;
	case 'SRre':
		$appType = 'Reinstate Senior Membership';
		$reinstate = true;
		break;
	case 'JSre':
		$appType = 'Reinstate Joint Senior Membership';
		$reinstate = true;
}

if ($reinstate) {
	$member = new Members($applicant->addToMemberID, 'membership');
	$oldText = $member->fullname . '<br>';
	$oldText .= $member->membership->memDescription . ', expired ' . $member->membership->expiration;

	$jointMembers = $member->membership->getJointMembers();
	if (count($jointMembers) == 2) {
		foreach ($jointMembers as $jointMember) {
			if ($jointMember->memberID == $member->memberID) continue;
			$oldText .= '<br>Joint Member: ' . $jointMember->fullname;
			if ($jointMember->email) $oldText .= ' &lt;' . $jointMember->email . '&gt;';
		}
	}
}

$approvedDisplay = '';
if ($applicant->submitted) {
	if ($applicant->approved) {
		$approvedDisplay = "$applicant->approved (invoice sent)";
	} else {
		$approvedDisplay = '<button id="btnApprove" type="button" class="btn btn-sm btn-primary">' .
			add_icon_text('thumbs-up') . ' Approve Application</button>';
		$approvedDisplay .= ' <br class="d-md-none">(sends an invoice)';
	}
}

$completeDisplay = '';
if ($applicant->approved) {
	if ($applicant->complete) {
		$completeDisplay = $applicant->complete;
	} else {
		$completeDisplay = ' (waiting for payment)';
		$completeDisplay .= ' <a href="applyPayment.php?id=' . $applicant->applicantID .
			'" class="btn btn-sm btn-secondary">';
		$completeDisplay .= add_icon_text('money-check-alt') . ' Received a check</a>';
	}
}

$feeTotal = number_format($applicant->entryFee + $applicant->dues, 2);
$warnFee = 0;
if ($feeTotal == 0) $warnFee = 1;

$editPage = 'applyEdit.php?id=' . $applicant->applicantID;

start_page($title, $pageDescription);
add_script('jqueryUI'); // for auto-complete
?>
<script>
"use strict";

var applicantID = "<?= $applicant->applicantID ?>";
var warnFee = parseInt("<?= $warnFee ?>");

$(function() {
    // do stuff when DOM is ready

    $(".pop").popover({trigger: 'hover'}); // info popups

    $("#btnApprove").click(function() {
        if (warnFee) {
            var agree = confirm("The total fee is zero!\nAre you sure you want to approve this application?");
            if (! agree) {return false};
        }
    	$.post('ajax/applyApprove.php', {applicantID: applicantID}, function(data) {
    		alert(data.msg);
    		if (data.success) {
    			location.reload();
    		}
    	}, 'json');
    });

    $("#btnDelete").click(function() {
    	 var agree = confirm("Are you sure you want to delete this application?");
         if (! agree) {return false};
         $.post('ajax/applyDelete.php', {applicantID: applicantID}, function() {
        	 window.location = "applications.php";
     	});
    });

});

</script>
<style>
td.first {
	width: 200px
}

td.fee {
	width: 75px
}
</style>
<?php
start_content();
toolbar();
?>

<h2 class="pageheader">Membership Application Details</h2>

<div class="row">
	<div class="col-lg-10 col-xl-8 offset-lg-1 offset-xl-2">

		<table class="table table-sm table-striped">
			<thead>
				<tr class="table-dark">
					<th colspan="2">Applicant</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="first">Name</td>
					<td><?= $applicant->fullname ?></td>
				</tr>
				<tr>
					<td>Email</td>
					<td><a href="mailto:<?= $applicant->email ?>?subject=California Alpine Club application">
							<?= $applicant->email ?></a></td>
				</tr>
				<tr>
					<td>Main Phone</td>
					<td><?= phone_number($applicant->phone) ?></td>
				</tr>
				<tr>
					<td>Mobile Phone</td>
					<td><?= phone_number($applicant->mobile) ?></td>
				</tr>
				<tr>
					<td>Date of Birth</td>
					<td><?= $applicant->dob ?></td>
				</tr>
			</tbody>
		</table>

<?php if ( in_array($applicant->applyType, ['JT', 'JTre', 'JSre']) ): ?>
		<table class="table table-sm table-striped">
			<thead>
				<tr class="table-dark">
					<th colspan="2">Joint Applicant</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="first">Name</td>
					<td><?= $applicant->fullname2 ?></td>
				</tr>
				<tr>
					<td>Email</td>
					<td><a href="mailto:<?= $applicant->email2 ?>?subject=California Alpine Club application">
						<?= $applicant->email2 ?></a></td>
				</tr>
				<tr>
					<td>Main Phone</td>
					<td><?= phone_number($applicant->phone2) ?></td>
				</tr>
				<tr>
					<td>Mobile Phone</td>
					<td><?= phone_number($applicant->mobile2) ?></td>
				</tr>
				<tr>
					<td>Date of Birth</td>
					<td><?= $applicant->dob2 ?></td>
				</tr>
			</tbody>
		</table>
<?php endif; ?>

		<table class="table table-sm table-striped">
			<thead>
				<tr class="table-dark">
					<th colspan="2">Address</th>
				</tr>
			</thead>
			<tbody>
<?php if ($applicant->applyType != 'JTadd'): ?>
			<tr>
					<td class="first">Street Address</td>
					<td><?= $applicant->street ?></td>
				</tr>
				<tr>
					<td>City</td>
					<td><?= $applicant->city ?></td>
				</tr>
				<tr>
					<td>State</td>
					<td><?= $applicant->state ?></td>
				</tr>
				<tr>
					<td>Zip</td>
					<td><?= $applicant->zip ?></td>
				</tr>
<?php else: ?>
			<tr>
					<td class="first">Street Address</td>
					<td>from current member</td>
				</tr>
<?php endif; ?>
		</tbody>
		</table>

		<table class="table table-sm table-striped">
			<thead>
				<tr class="table-dark">
					<th colspan="2">Application</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="first">Type</td>
					<td><?= $appType ?></td>
				</tr>
<?php if ($applicant->applyType == 'JTadd'): ?>
				<tr>
					<td>Join Current Member</td>
					<td><?= $addToDisplay ?></td>
				</tr>
<?php if ($addToDanger): ?>
				<tr>
					<td colspan="2"><div class="alert alert-danger"><?= $addToDanger ?></div></td>
				</tr>
<?php endif;

	if ($addToWarning) :
		?>
				<tr>
					<td colspan="2"><div class="alert alert-warning"><?= $addToWarning ?><br>
						(age as of April 1, <?= date('Y') ?>)</div></td>
				</tr>
<?php endif;
endif;

if ($reinstate) :
	?>
			<tr>
					<td>Old Membership</td>
					<td><?= $oldText ?></td>
				</tr>
<?php endif; ?>
			<tr>
					<td>Sponsor #1</td>
					<td><?= $applicant->sponsor1 ?></td>
				</tr>
				<tr>
					<td>Sponsor #2</td>
					<td><?= $applicant->sponsor2 ?></td>
				</tr>
<?php if (! $reinstate): ?>
			<tr>
					<td>Event #1</td>
					<td><?= $applicant->event1 ?></td>
				</tr>
				<tr>
					<td>Event #2</td>
					<td><?= $applicant->event2 ?></td>
				</tr>
<?php endif; ?>
			<tr>
					<td>Comments</td>
					<td>
<?php
if (! is_null($applicant->notes)) {
	echo nl2br($applicant->notes);
}
?></td>
				</tr>
				<tr>
					<td>Code</td>
					<td><?= $applicant->applicantID ?>:<?= $applicant->code ?></td>
				</tr>
			</tbody>
		</table>

		<table class="table table-sm table-striped">
			<thead>
				<tr class="table-dark">
					<th>Fees</th>
					<th colspan="2"><a class="btn btn-sm btn-warning"
						href="applyFees.php?id=<?= $applicant->applicantID ?>">Edit Fees</a></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="first">Entry Fee</td>
					<td class="fee text-end">$<?= $applicant->entryFee ?></td>
					<td></td>
				</tr>
				<tr>
					<td>Annual Dues</td>
					<td class="fee text-end">$<?= $applicant->dues ?></td>
					<td></td>
				</tr>
				<tr>
					<td><b>Total</b></td>
					<td class="fee text-end"><b>$<?= $feeTotal ?></b></td>
					<td></td>
				</tr>
<?php if ($feeTotal == 0): ?>
				<tr>
					<td colspan="3" style="color: red">Administrator - please edit fees.</td>
				</tr>
<?php endif; ?>
		</tbody>
		</table>

		<table class="table table-sm table-striped">
			<thead>
				<tr class="table-dark">
					<th colspan="2">Status</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="first">
						<?php Show_Info('Application Created', getHelp('appCreated'))?> Application Created
					</td>
					<td><?= $applicant->created ?></td>
				</tr>
				<tr>
					<td><?php Show_Info('Application Submitted', getHelp('appSubmitted'))?> Application Submitted</td>
					<td><?= $applicant->submitted ?></td>
				</tr>
				<tr>
					<td><?php Show_Info('Application Approved', getHelp('appApproved'))?> Application Approved</td>
					<td><?= $approvedDisplay ?></td>
				</tr>
				<tr>
					<td><?php Show_Info('Application Complete', getHelp('appComplete'))?> Application Complete</td>
					<td><?= $completeDisplay ?></td>
				</tr>
			</tbody>
		</table>

		<div class="btn-group mb-3">
			<a class="btn btn-secondary" href="<?= $editPage ?>"><?= add_icon('edit') ?> Edit Application</a>
<?php if (! $applicant->complete): ?>
			<button class="btn btn-secondary" id="btnDelete" type="button">
				<?= add_icon('trash') ?> Delete Application
			</button>
<?php endif; ?>
		</div>

	</div>
</div>

<?php end_page(); ?>
