// some custom javascript functions that may be used on multiple pages

// define globally
var defaultSite = 'californiaalpineclub.org';


$(function() {
	// do stuff when DOM is ready
	
	// replace text email addresses with clickable links after the page loads
	// to avoid exposing the addresses to email address harvesting robots
	convertToLinks();
	
});	


// create clickable email links for every tag on the page that has class="emlink"
// or class="emlink-icon".
// for emlink-icon the link is only an icon, not the text.
function convertToLinks() {

	$(".emlink").each(function() {
		var user = $(this).data('user');
		var site = $(this).data('site');
		if (typeof site == 'undefined') { site = defaultSite }
		var assembly = user + '@' + site;
		$(this).html('<a href=\"mailto:' + assembly + '\">' + assembly + '<\/a>');
		
	});
	
	$(".emlink-icon").each(function() {
		var user = $(this).data('user');
		var site = $(this).data('site');
		if (typeof site == 'undefined') { site = defaultSite }
		var assembly = user + '@' + site;
		$(this).html('<a href=\"mailto:' + assembly + '\"><span class=\"fas fa-envelope" aria-hidden="true\"></span><\/a>');
	});
	
}
