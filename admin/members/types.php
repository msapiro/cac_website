<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// array_push($allowGroups, 'someGroup');
require AUTH_PAGE; // make sure user is logged in and a group member


$title = 'CAC Membership';
$pageDescription = 'Membership Types';

require '../../support/functions.php';
require '../../support/formFunctions.php';
include 'includes/tools.php';


$mTypes = new Classes();
$mTypesArray = $mTypes->getArray();

start_page($title, $pageDescription);
add_script('jqueryui');  // for sortable
?>
<script>
$(function() {
	// make nice tooltips for the buttons
	$('.tip').tooltip({container: 'body', placement: 'bottom'});

	$("#typeTable tbody").sortable( {
			handle: '.handle',
			axis: "y",
			cursor: "ns-resize",
			containment: "#typeTable",
			opacity: .50,
			update: function() {
				var order = $("#typeTable tbody").sortable('serialize');
				$.post("ajax/classSorter.php", {input: order});
			}
		});

	$('.btnEdit').click(function() {
		var id = $(this).closest('tr').data('typeid');
		var URL = 'editType.php?id=' + id;
		window.open(URL, "_self");
	});

	$('.btnDelete').click(function() {
	    var agree=confirm("Deletion cannot be undone.\n\n"
	    	    + "Are you sure you want to delete this membership type?");
	    if (agree) {
	    		var id = $(this).closest('tr').data('typeid');
			$.post('ajax/deleteType.php', {class: id}, function(data) {
				location.reload();
			});
	    } else {
		    $(this).blur();
	    }
	});

	$('#btnAdd').click(function() {
		window.open('editType.php', "_self");
	});

});
</script>
<?php
start_content();
toolbar();
?>

<h2 class="pageheader">Membership Types</h2>

<div class="row  justify-content-center">
	<div class="col-xl-10 col-xxl-8">

		<table id="typeTable" class="table table-striped">
		<thead>
			<tr>
				<th>Order</th>
				<th class="text-center">Action</th>
				<th class="text-center">Type</th>
				<th>Description</th>
				<th class="text-center">Joint</th>
				<th class="text-end">Dues</th>
				<th class="text-end">Entry Fee</th>
				<th class="text-center">Show Members</th>
				<th class="text-center">Show Admins</th>
				<th class="text-end">Usage</th>
			</tr>
		</thead>
		<tbody>
<?php
foreach ($mTypesArray as $mType):
	$mType->getUsage();
	if ($mType->joint) {
		$displayJoint = add_icon_text('check');
	} else {
		$displayJoint = '';
	}
	if ($mType->entryFee > 0) {
		$displayEntry = '$' . $mType->entryFee;
	} else {
		$displayEntry = '';
	}
?>
			<tr data-typeid="<?= $mType->class ?>" id="class_<?= $mType->class ?>">
				<td class="handle"><?php add_icon('arrows-alt-v') ?></td>
				<td class="text-center">
					<div class="btn-group btn-group-sm">
					<button type="button" class="btn btn-secondary tip btnEdit" title="Edit">
						<?php add_icon('edit') ?>
					</button>
					<button type="button"
						class="btn btn-secondary tip
							btnDelete<?php if ($mType->usage) echo ' disabled' ?>"
						title="Delete">
						<?php add_icon('times') ?>
					</button>
					</div>
				</td>
				<td class="text-center"><?= $mType->class ?></td>
				<td><?= $mType->description ?></td>
				<td class="text-center"><?= $displayJoint ?></td>
				<td class="text-end">$<?= $mType->dues ?></td>
				<td class="text-end"><?= $displayEntry ?></td>
				<td class="text-center"><?= $mType->showUsers ?></td>
				<td class="text-center"><?= $mType->showAdmin ?></td>
				<td class="text-end"><?= $mType->usage ?></td>
			</tr>
<?php
endforeach;
?>
			<tr>
				<td colspan="10">
					<button id="btnAdd" type="button" class="btn btn-secondary tip" title="New">
						<?php add_icon('plus') ?>&nbsp;Add New Type
					</button>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="7">
				<ul>
				<li>Types will be presented to members and admins in the order above. You can
					drag-and-drop the handle in the first column to change the order.</li>
				<li><b>Joint</b> column indicates that membership can have two members</li>
				<li><b>Dues</b> - annual membership dues</li>
				<li><b>Entry Fee</b> - one-time entry fee</li>
				<li><b>Show</b> - allow Members / Admins respectively to see and select this
					type</li>
				<li><b>Usage</b> - how many memberships (current + expired) are assigned to this
					type</li>
				<li>Orphan type is assigned to members who are removed from a joint membership</li>
				<li>Types with Usage > 0 cannot be deleted</li>
				</ul>
				</td>
		</tfoot>
		</table>

	</div>
</div>

<?php end_page(); ?>
