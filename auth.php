<?php
// REQUIRE this NEAR THE TOP of any regular (not ajax) CAC pages to enforce login,
// but AFTER the classLoader is required.

// If user is not logged in, she will be redirected to the login page.
// After login, the user will be redirected back to the initial page (which REQUIREd this script).

// start our CAC session
if (session_status() !== PHP_SESSION_ACTIVE) {
	session_name('CACSESSID');
	session_start();
}

// we need access to some wordpress functions
require __DIR__ . '/../wp-load.php';

require_once '../support/config.php'; // this will be either admin/support or members/support

require __DIR__ . '/support/restoreGlobals.php';

/**
 * from include:
 * @var array $allowGroups
 */

$wpID = get_current_user_id();

if ($wpID) {
	// a WordPress user is logged in
	$member = new Members();

	if (isset($_SESSION['cac_login']['wpID']) && $_SESSION['cac_login']['wpID'] == $wpID && $_SESSION['cac_login']['memberID']) {
		// our session is already set up and CAC user is logged in
	} else {
		// clear the login array, in case it exists
		$_SESSION['cac_login'] = array();

		// query the alpine database for CAC user data
		$member->getByWpID($wpID, 'membership');

		if (empty($member->memberID)) {
			// a WP user is logged in but has no associated member account
			authlog("WP user_id $wpID not linked to CAC member");
			$location = home_url() . '/cac/members/home/noacct.php';
			header('Location: ' . $location);
			exit;
		}

		if (empty($member->membership->expiration)) {
			// it is possible that the expiration is null. if not then it is a valid date.
			authlog('empty expiration date', $member->memberID);
			$location = home_url() . '/cac/members/home/expired.php?empty=1';
			header('Location: ' . $location);
			exit;
		}

		// expiration is always March 31, renewal is delinquent after July 1.
		if ($member->membership->expired) {
			// allow login up to 9 months following the expiration.
			$expiredGrace = $member->membership->isExpired(false, 'P9M');
			if ($expiredGrace) {
				// expired more than 9 months ago. member needs to be reinstated by admin
				authlog('expired > 9mo ago', $member->memberID);
				$location = home_url() . '/cac/members/home/expired.php?exp=' . $member->membership->expiration;
				header('Location: ' . $location);
				exit;
			}
		}

		// valid login (might be expired within last year)
		$_SESSION['cac_login']['memberID'] = $member->memberID;
		$_SESSION['cac_login']['membershipID'] = $member->membershipID;
		$_SESSION['cac_login']['wpID'] = $wpID;
		$_SESSION['cac_login']['name'] = $member->fullname;
		$_SESSION['cac_login']['last_access'] = time();
		$_SESSION['cac_login']['privGroups'] = array();

		authlog('authenticated');

		// see if this member is also in any privilege groups
		$groupArray = $member->db->select("authGroups", "privGroup", ["memberID" => $member->memberID]);
		foreach ($groupArray as $group) {
			$_SESSION['cac_login']['privGroups'][] = $group;
		}
	}

	// see if the page that included this script has group restrictions
	if (! empty($allowGroups)) {
		// if it is not an array, make it so
		if (! is_array($allowGroups)) $allowGroups = array($allowGroups);

		$allowed = false;
		if (! empty($_SESSION['cac_login']['privGroups'])) {
			// if there are any elements in common, then we allow access
			$allowed = count(array_intersect($_SESSION['cac_login']['privGroups'], $allowGroups));
		}

		if (! $allowed) {
			// User is not in a group that has privileges to the page.
			authlog('not in authorized group');
			$location = home_url() . '/cac/members/home/nogroup.php';
			header('Location: ' . $location);
			exit;
		}
	}

	// If we get to here, the user is allowed to access the page that included this.
	// Undo the stupid changes that WordPress made to some Global Variables.
	restoreGlobals();

} else {
	// clear the login array, in case it exists
	$_SESSION['cac_login'] = array();

	//authlog('not logged in to WP'); // no db access for this

	// go to the WP login page. redirect back to the page that included this after login
	$location = wp_login_url($_SERVER['REQUEST_URI']);
	header('Location: ' . $location);
	exit;
}


function authlog($event, $id=0) {
	global $member; // for db access

	$toPage = $_SERVER['REQUEST_URI'];

	$page = parse_url($toPage, PHP_URL_PATH);
	$urlQuery = false;
	if (! empty(parse_url($toPage, PHP_URL_QUERY))) $urlQuery = '?' . parse_url($toPage, PHP_URL_QUERY);
	if ($urlQuery) $page .= $urlQuery;

	$memberID = 0;
	if (! empty($_SESSION['cac_login']['memberID'])) {
		$memberID = $_SESSION['cac_login']['memberID'];
	} elseif ($id) {
		$memberID = $id;
	}

	$member->db->insert("authLog", ["memberID" => $memberID, "event" => $event, "page" => $page]);
}
