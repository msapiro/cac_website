<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';
require '../../support/formFunctions.php';
include 'includes/tools.php';

inty($_GET['id']);

$msgGroup = new MsgGroups($_GET['id']);
if (empty($msgGroup->groupID)) {
	error('Error', 'Did not find a message group with groupID=' . $_GET['id']);
	exit;
}

$db = $msgGroup->db; // database
$resultArray = array();
$num = 0;

// not great security to execute a user-entered mySQL query but these are
// created by admin users. Let's just make sure the first word is SELECT.
if (stripos($msgGroup->queryString, 'select') !== 0) {
	$badQuery = true;
	$db_error = 'Only SELECT statements will be executed.';
} else {
	$pdoStmt = $db->query($msgGroup->queryString);
	if ($db->error) {
		$db_error = $db->error;
		$badQuery = true;
	} else {
		$badQuery = false;
		$num = $pdoStmt->rowCount();
		if ($num) {
			// get first 10 results as a sample
			$limit = min($num, 10);
			for ($i=1; $i<=$limit; $i++) {
				$result = $pdoStmt->fetch();
				$resultArray[] = $result;
			}
		}
	}
}

start_page('CAC Mailer');
?>
<script>
$(function() {
	// do stuff when DOM is ready

	 $(".pop").popover({trigger: 'hover'}); // info popups

});
</script>
<?php
start_content();
toolbar();
?>

<h3 class="pageheader">Email Group</h3>

<div class="row">
	<div class="col-xl-10 offset-xl-1">

<?php
static_field('groupID', $msgGroup->groupID, false, false, 'col-3', 'col-5');
static_field('Group Name', $msgGroup->groupName, false, false, 'col-3', 'col-8');
?>

<div class="row mb-3">
	<label class="text-lg-end col-lg-3">
		<b>Query String</b>
	</label>
	<div class="col-lg-7">
		<?= nl2br($msgGroup->queryString) ?>
	</div>
</div>

<?php
if (! empty($msgGroup->note)):
?>
<div class="row mb-3">
	<label class="text-lg-end col-lg-3">
		<b>Note</b>
	</label>
	<div class="col-lg-7">
		<?= nl2br($msgGroup->note) ?>
	</div>
</div>
<?php
endif;
static_field('Addresses', $num, false, false, 'col-3', 'col-3');
?>

<div class="row mb-3">
	<label class="text-lg-end col-lg-3">
		<b>First Ten <br class="d-none d-lg-inline">Addresses</b>
	</label>

	<div class="col-lg-7">
<?php
if ($badQuery):
	echo '<b>ERROR:</b> ' . $db_error;
else: ?>
		<table class="table table-sm table-striped">
		<thead>
			<tr><th>Name</th><th>Email</th></tr>
		</thead>
		<tbody>
<?php
foreach ($resultArray as $row):

	if (isset($row['email'])) {
		$email = $row['email'];
	} else {
		$email = 'email field not set';
	}

	$name = false;
	if (isset($row['fullname'])) {
		$name = $row['fullname'];
	} elseif ( isset($row['first']) && isset($row['last']) ) {
		$name = $row['first'] . ' ' . $row['last'];
	}
?>
			<tr>
				<td><?php if($name) echo $name ?></td>
				<td><?= $email ?></td>
			</tr>
<?php
endforeach; ?>
		</tbody>
		</table>
<?php
endif;
?>
	</div>
</div>

<div class="row mb-3">
	<div class="col-lg-5 offset-lg-3">
		<a href="editGroup.php?id=<?= $msgGroup->groupID ?>" class="btn btn-secondary"><?php add_icon('edit')?> Edit Group</a>
	</div>
</div>

	</div>
</div>
<?php end_page(); ?>
