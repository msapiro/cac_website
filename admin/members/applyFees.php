<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
//array_push($allowGroups, 'ROmembers');
require AUTH_PAGE; // make sure user is logged in and a group member

$title = 'CAC Membership';
$pageDescription = 'Application Details';

require '../../support/functions.php';
require '../../support/formFunctions.php';
include 'includes/tools.php';

inty($_GET['id']);

$applicant = new Applicants($_GET['id']);
if (! $applicant->applicantID) {
	error('Error', 'Did not find an application with applicantID ' . $_GET['id'] . '.');
	exit;
}

switch ($applicant->applyType) {
	case 'RG':
		$appType = 'Individual Membership';
		break;
	case 'JT':
		$appType = 'Joint Membership (two new members)';
		break;
	case 'JTadd':
		$appType = 'Joint Membership (add to current member)<br>';
		if ($applicant->addToMemberID) {
			$oAddTo = new Members($applicant->addToMemberID, 'membership');
			$appType .= '<a href="detail.php?id=' . $oAddTo->memberID . '">' . $oAddTo->fullname . '</a>';
			$appType .= ' memberID=' . $oAddTo->memberID . ' ' . $oAddTo->membership->memDescription;
		} else {
			$appType .= '<span style="color: red">Current Member is undefined. Fix this before assigning fees!</span>';
		}
		break;
	case 'ST':
		$appType = 'Student Membership';
		break;
	case 'RGre':
		$appType = 'Reinstate Individual Membership';
		break;
	case 'JTre':
		$appType = 'Reinstate Joint Membership';
		break;
	case 'SRre':
		$appType = 'Reinstate Senior Membership';
		break;
	case 'JSre':
		$appType = 'Reinstate Joint Senior Membership';
}

start_page($title, $pageDescription);
?>
<script>
"use strict";
var applicantID = "<?= $applicant->applicantID ?>";

$(function() {
    // do stuff when DOM is ready

    $(".pop").popover({trigger: 'hover'}); // info popups

    $("#btnSuggest").click(function() {
    	$.post('ajax/suggestFees.php', {applicantID: applicantID}, function(data) {
    		if (data.success) {
				$("#entryFee").val(data.entryFee.toFixed(2));
				$("#dues").val(data.dues.toFixed(2));
    		} else {
				alert(data.msg);
    		}
    	}, 'json');
    });

});

</script>
<style>
	td.first {width: 150px}
</style>
<?php
start_content();
toolbar();
?>

<h2 class="pageheader">Membership Application Fees</h2>

<div class="row">
	<div class="col-lg-10 col-xl-8 offset-lg-1 offset-xl-2">

		<table class="table table-sm table-striped">
		<thead>
			<tr class="table-dark"><th colspan="2">Applicant</th></tr>
		</thead>
		<tbody>
			<tr><td class="first">Name</td><td><?= $applicant->fullname ?></td></tr>
			<tr>
				<td>Email</td>
				<td><a href="mailto:<?= $applicant->email ?>?subject=California Alpine Club application"><?= $applicant->email ?></a></td>
			</tr>
		</tbody>
		</table>

<?php if ($applicant->applyType == 'JT'): ?>
		<table class="table table-sm table-striped">
		<thead>
			<tr class="table-dark"><th colspan="2">Joint Applicant</th></tr>
		</thead>
		<tbody>
			<tr><td class="first">Name</td><td><?= $applicant->fullname2 ?></td></tr>
			<tr>
				<td>Email</td>
				<td><a href="mailto:<?= $applicant->email2 ?>?subject=California Alpine Club application"><?= $applicant->email2 ?></a></td>
			</tr>
		</tbody>
		</table>
<?php endif; ?>


		<table class="table table-sm table-striped">
		<thead>
			<tr class="table-dark"><th colspan="2">Application</th></tr>
		</thead>
		<tbody>
			<tr><td class="first">Type</td><td><?= $appType ?></td></tr>
		</tbody>
		</table>


<div class="card mb-3">
	<h4 class="card-header">Administrator - please edit fees</h4>
 	<div class="card-body">

		<div class="row align-items-center mb-3">
			<div class="col-6 offset-sm-4 offset-md-3">
<?php Show_Info('Suggest Fees', getHelp('suggestFees')) ?>&nbsp;
				<button id="btnSuggest" class="btn btn-secondary" type="button">
					Suggest
				</button>
			</div>
		</div>

		<form id="formApply" action="applyFees2.php" method="post" autocomplete="off">
			<input type="hidden" name="applicantID" value="<?= $applicant->applicantID ?>">

	 		<div class="row mb-3">
				<label class="text-sm-end col-sm-4 col-md-3"><b>Entry Fee</b></label>
				<div class="col-6 col-sm-4 col-lg-3">
					<div class="input-group">
						<span class="input-group-text">$</span>
						<input type="text" class="form-control" id="entryFee" name="entryFee" maxlength="7" value="<?= $applicant->entryFee ?>">
					</div>
				</div>
			</div>

			<div class="row mb-3">
				<label class="text-sm-end col-sm-4 col-md-3"><b>Annual Dues</b></label>
				<div class="col-6 col-sm-4 col-lg-3">
					<div class="input-group">
						<span class="input-group-text">$</span>
						<input type="text" class="form-control" id="dues" name="dues" maxlength="7" value="<?= $applicant->dues ?>">
					</div>
				</div>
			</div>

			<div class="row mb-3">
				<div class="col-6 offset-sm-4 offset-md-3">
					<div class="btn-group">
						<button type="submit" class="btn btn-primary">Save</button>
						<a class="btn btn-secondary" href="applyDetail.php?id=<?= $applicant->applicantID ?>">Cancel</a>
					</div>
				</div>
			</div>
		</form>

<?php if ($applicant->approved): ?>
		<div class="alert alert-danger">
			Invoice has already been sent. Do not edit fees at this point without good reason!
		</div>
<?php endif; ?>

	</div>
</div> <!-- end card -->


	</div>
</div>

<?php end_page(); ?>
