<?php
// this is the text that appears on the page tab in most browsers.
$title = 'Not Authorized';

require '../support/config.php';
require CLASSLOADER;

require '../../support/functions.php';

// we need access to some wordpress functions
require_once( __DIR__ . '/../../../wp-load.php' );

start_page($title);
start_content();

$empty = booly($_GET['empty']);
?>

<div class="row">

	<div class="alert alert-danger col-lg-8 offset-lg-2 mt-4">

<?php if ($empty): ?>
		<p>You are logged in as a WordPress user, but your CAC account is missing an
		expiration date.</p>

		<p>This is unusual. Please contact
		<span class="emlink" data-user="webmaster">the webmaster</span>.</p>

<?php else: ?>
		<p>You are logged in as a WordPress user, but your CAC account
		expired <?= $_GET['exp'] ?>. You will need to apply for reinstatement as a member.
		</p>

		<p>Please contact
		<span class="emlink" data-user="webmaster">the webmaster</span>.</p>
<?php endif; ?>

		<p><a href="<?= home_url() ?>">Go to CAC public website</a></p>

	</div>

</div> <!-- end row -->

<?php end_page(); ?>
