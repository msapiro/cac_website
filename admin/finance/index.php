<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';
include 'includes/tools.php';

require '/www/cac/data/credentials/stripe.php'; // stripe credentials

/**
 *
 * from config file:
 * @var string $stripeSecretKey
 * @var string $stripeApiVersion
 * */

$stripeClient = new \Stripe\StripeClient([
	'api_key' => $stripeSecretKey,
	'stripe_version' => $stripeApiVersion
]);

$weeks = 4; // show 4 weeks worth of payouts
$offset = inty($_GET['offset']);

if ($offset) {
	$string = '-' . $offset * $weeks . ' weeks';
	$periodEnd = strtotime($string);
} else {
	$periodEnd = time();
}
$string = '-' . $weeks . ' weeks';
$periodStart = strtotime($string, $periodEnd);

$thisPage = $_SERVER["PHP_SELF"];
$prevURL = false;
$prev = $offset + 1;
$prevURL = $thisPage . '?offset=' . $prev;

$next = $offset - 1;
if ($next > 0) {
	$nextURL = $thisPage . '?offset=' . $next;
} elseif ($next == 0) {
	$nextURL = $thisPage;
} else {
	$nextURL = false;
}

// the arrival date is not the same as the deposit date, but pretty close.
// in the detail view we can get the transaction available_on date, which is better.

$payouts = $stripeClient->payouts->all([
	"status" => "paid",
	"arrival_date" => ["gte" => $periodStart, "lte" => $periodEnd]
]);

start_page('CAC Finance');
?>
<script>

$(function() {
    // do stuff when DOM is ready

    $('.tip').tooltip({placement: 'bottom'});
});

</script>
<?php
start_content();
toolbar();
?>

<div class="row justify-content-center">
	<div class="col-lg-11 col-xl-10">

<div class="row">
	<div class="col-sm-6 col-md-5 col-lg-4 col-xl-3 mt-2">
		<div class="btn-group">
<?php if ($prevURL): ?>
			<a class="btn btn-secondary" href="<?= $prevURL ?>">
				<?php add_icon('chevron-left') ?> Previous
			</a>
<?php else : ?>
			<a class="btn btn-secondary disabled" href="#">
				<?php add_icon('chevron-left') ?> Previous
			</a>
<?php endif;
if ($nextURL): ?>
			<a class="btn btn-secondary" href="<?= $nextURL ?>">
				Next <?php add_icon('chevron-right') ?>
			</a>
			<a class="btn btn-secondary" href="<?= $thisPage ?>">
				<?php add_icon('step-forward') ?>
			</a>
<?php else : ?>
			<a class="btn btn-secondary disabled" href="#">
				Next <?php add_icon('chevron-right') ?>
			</a>
<?php endif; ?>
		</div>
	</div>

	<div class="col-sm-6">
		<h3 class="pageheader">Stripe Payouts</h3>
	</div>
</div> <!-- end row -->


<table class="table table-sm table-striped">
	<thead>
		<tr>
			<th class="text-center">Detail</th>
			<th class="text-center">Approx. Date</th>
			<th class="text-end">Amount</th>
			<th>Payout ID</th>
		</tr>
	</thead>
	<tbody>

<?php
foreach ($payouts->autoPagingIterator() as $payout):
	$arrival_date =  date('Y-m-d', $payout->arrival_date);
	$amount = number_format($payout->amount / 100, 2);
	$payoutID = $payout->id;
?>
		<tr>
		<td class="text-center">
			<a class="btn btn-secondary btn-sm tip" href="payout.php?id=<?= $payoutID ?>"
				title="Payout Detail"><?php add_icon('list-alt') ?>
			</a>
		</td>
		<td class="text-center"><?= $arrival_date ?></td>
		<td class="text-end">$<?= $amount ?></td>
		<td>&nbsp;<?= $payoutID ?></td>
		</tr>

<?php endforeach; ?>
	</tbody>
</table>


</div>
</div>

<!-- Modal for displaying details of one Payout -->
<div class="modal fade" id="payoutModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        	<div class="modal-header">
	        	<h4 class="modal-title">Payout Details</h4>
				<button type="button" class="btn-close" data-bs-dismiss="modal"></button>
			</div>

			<div class="modal-body">
				<table id="stripeTable" class="table table-sm table-striped">
					<thead>
						<tr>
							<th class="text-center">Date</th>
							<th>Description</th>
							<th class="text-end">Amount</th>
							<th class="text-end">Club</th>
							<th class="text-end">Fdn</th>
							<th class="text-end">Stripe Fee</th>
							<th class="text-end">Net</th>
						</tr>
					</thead>
					<tbody id="payoutDetail">
						<!-- ajax will fill -->
					</tbody>
				</table>
			</div>

        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal -->

<?php end_page(); ?>
