<?php

// privilege groups that are allowed to access pages in this category (Mailer)
$allowGroups = array('admin', 'mailer');

// seconds to allow mailer batch to run before returning progress report
$batchTime = 15;
