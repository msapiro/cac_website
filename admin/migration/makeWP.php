<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)

if (isset($allowGroups)) {
	require AUTH_PAGE; // make sure user is logged in and a group member
}

require_once '../../../wp-load.php';
require '../../support/functions.php';
// WP admin functions for user management
require ABSPATH . 'wp-admin/includes/user.php';


// make and link a WordPress user for all current members with email and
// without a wpID.

$proceed = booly($_GET['proceed']);

start_page('CAC Migration');
start_content();

if (! $proceed): ?>

<div class="alert alert-danger mt-4">
	<p><b>WARNING</b>: This will create a wordpress user first_last for all current <b>alpine.members</b>
	who don't have a wpID and link it to the member.  For duplicate emails, generally joint members,
	only the first email is used and the second is skipped as WP doesn't allow two users with the same
	email.
	</p>
	<p>You can run this script multiple times.</p>
</div>

<a class="btn btn-secondary" href="<?= $_SERVER['PHP_SELF'] . '?proceed=1' ?>">Continue</a>

<?php
	end_page();
	exit;
endif;

global $displayResult;

$model = new Model();
$db = $model->db;

// Process current users with wpID = 0
$cutoff = date('Y-m-d', time() - 3600*24*91); // now - 91 days grace
$rows = $db->select("members", ["[>]memberships"=>"membershipID"],
	['memberID', 'first', 'last', 'email'],
	["AND"=>["expiration[>=]"=>$cutoff, "resigned"=>0, "email[!]"=> null,
	"wpID"=>0, "class[!]"=>['OR', 'CH']],
	"ORDER"=>"email"]
	);
$lastemail = '';
echo '<pre>';
foreach ($rows as $row) {
	if ($row['email'] == $lastemail) {
		// Duplicate email, skip.
		continue;
	}
	$lastemail = $row['email'];
	// Get the member object.
	$member = new Members($row['memberID']);
	// Create a WP user
	$_POST['user_login'] = strtolower($row['first'] . '_' . $row['last']);
	// Cleanse the name.
	$clean = preg_replace('/[^a-z_]/', '', $_POST['user_login']);
	if ($_POST['user_login'] != $clean) {
		$_POST['user_login'] = $clean;
	}
	$_POST['first_name'] = $row['first'];
	$_POST['last_name'] = $row['last'];
	$_POST['email'] = $row['email'];
	$displayResult = '';
        $wpID = wpAddUser();
        if (! $wpID) {
		$email = $row['email'];
		print("$displayResult  Email=$email\n");
		continue;
	}
	$displayResult = '';
        if (! wpAssign($member, $wpID)) {
		print("$displayResult  wpID=$wpID; memberID=$member->memberID\n");
                continue;
	}
	print("Processed memberID=$member->memberID; wpID=$member->wpID\n");
}
?>

</pre>

<p><a href="index.php">Setup and Migration</a></p>

<?php end_page(); ?>
