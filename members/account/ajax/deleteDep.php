<?php
require '../../support/config.php';
require CLASSLOADER;

require AUTH_AJAX; // enforce login


inty($_POST['depID']);

$dep = new Dependents($_POST['depID']);

if ($dep->depID) {
	// security check  - make sure this is a dependent in the user's membership
	$user = new Members('login');
	if ($user->membershipID == $dep->membershipID) {
		$dep->dbDelete();
	}
}
