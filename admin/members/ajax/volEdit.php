<?php
require '../../support/config.php';
require CLASSLOADER;

require '../includes/config.php'; // contains $allowGroups for this directory
//array_push($allowGroups, 'ROmembers');
require AUTH_AJAX;                // make sure user is logged in and a group member

// add or delete a member volunteer for a particular opportunity


$data = array();
inty($_POST['memberID']);
inty($_POST['oppID']);
$check = booly($_POST['check']); // is the checkbox checked?

$member = new Members($_POST['memberID']);
if (empty ($member->memberID)) {
	$data['success'] = 0;
	$data['msg'] = "Did not find a member with memberID=" . $_POST['memberID'];
} else {
	$vol = new Volunteers();
	$vol->memberID = $member->memberID;

	$volOpp = new VolOpps($_POST['oppID']);
	if (empty ($volOpp->oppID)) {
		$data['success'] = 0;
		$data['msg'] = "Did not find an opportunity with oppID=" . $_POST['oppID'];
	} else {
		$data['success'] = 1;
		$vol->oppID = $volOpp->oppID;
		if ($check) {
			$vol->dbSave();
			$data['msg'] = "Successfully assigned $member->first to $volOpp->oppName.";
		} else {
			$vol->dbDelete();
			$data['msg'] = "Successfully cleared $member->first from $volOpp->oppName.";
		}
	}
}
echo json_encode($data);
