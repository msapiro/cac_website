<?php
// login not required for this page
require '../support/config.php';
require CLASSLOADER;

$title = 'CAC Membership';
$pageDescription = 'Application Details';

require '../../support/functions.php';
require '../../support/formFunctions.php';
require 'includes/tools.php';

if (empty($_GET['key'])) {
	error('Error', 'You must submit a code to identify the application.');
	exit();
}

$applicant = new Applicants();
$applicant->getByKey($_GET['key']);
if (!$applicant->applicantID) {
	error('Error', 'Did not find an application for code ' . $_GET['key']);
	exit();
}

$warn = false;
$typeWarn = false;
$emailWarn = false;
$eventWarn = false;
$sponsorWarn = false;
$dobWarn = false;
$dob2Warn = false;
$addressWarn = false;


$reinstate = false;
switch ($applicant->applyType) {
	case 'RG':
		$appType = 'Individual Membership';
		break;
	case 'JT':
		$appType = 'Joint Membership (two new members)';
		break;
	case 'JTadd':
		$appType = 'Joint Membership<br>add to current member ' . $applicant->addToMember;
		break;
	case 'ST':
		$appType = 'Student Membership';
		break;
	case 'RGre':
		$appType = 'Reinstate Individual Membership';
		$reinstate = true;
		break;
	case 'JTre':
		$appType = 'Reinstate Joint Membership';
		$reinstate = true;
		break;
	case 'SRre':
		$appType = 'Reinstate Senior Membership';
		$reinstate = true;
		break;
	case 'JSre':
		$appType = 'Reinstate Joint Senior Membership';
		$reinstate = true;
		break;
	default:
		$appType = '<span style="color: red">Unknown - please edit!</span>';
		$typeWarn = 'Application Type (Regular, Joint, Student, etc) has not been selected.';
}

$key = $applicant->applicantID . ':' . $applicant->code;

$editPage = 'apply.php?key=' . $key;
$feesPage = 'fees.php?key=' . $key;

if ($reinstate) {
	$editPage = 're' . $editPage;
	$member = new Members($applicant->addToMemberID, 'membership');
	$oldText = $member->fullname . '<br>';
	$oldText .= $member->membership->memDescription
		. ', expired ' . $member->membership->expiration;

	$jointMembers = $member->membership->getJointMembers();
	if (count($jointMembers) == 2) {
		foreach ($jointMembers as $jointMember) {
			if ($jointMember->memberID == $member->memberID) continue;
			$oldText .= '<br>Joint Member: ' . $jointMember->fullname;
			if ($jointMember->email) $oldText .= ' &lt;' . $jointMember->email . '&gt;';
		}
	}

	if ($applicant->applyType == 'SRre' || $applicant->applyType == 'JSre') {
		if (empty($applicant->dob)) {
			$dobWarn = 'Applicant Date of Birth required for Senior Membership.';
		}
	}
}

if ($applicant->submitted) {
	$submitted = $applicant->submitted;
} else {
	$submitted = 'Not yet';

	if (empty($applicant->sponsor1) || empty($applicant->sponsor2)) {
		$sponsorWarn = 'Two current members must be listed as sponsors.
			They may not be two parts of the same joint membership.';
	}

	if (is_null($applicant->street) || is_null($applicant->city)
		|| is_null($applicant->state) || is_null($applicant->zip)) {
		$addressWarn = 'All four address fields are required.';
	}
}

start_page($title, $pageDescription);
?>
<script>
"use strict";
var key = "<?= $applicant->applicantID ?>" + ':' +  "<?= $applicant->code ?>";

$(function() {
	// do stuff when DOM is ready

	 $(".pop").popover({trigger: 'hover'}); // info popups

	$("#btnSubmit").click(function() {
		var msg = "You will not be able to edit your application after submitting.\n\n";
		msg += "Are you sure you are ready to submit?";
		var agree = confirm(msg);
		if (agree) {
			$.post("ajax/submitApply.php", {key: key}, function(data) {
				if (data.success) {
					location.reload();
	    		} else {
					alert(data.msg);
	    		}
			}, 'json');
		}
	});

});
</script>
<style>
td.first {
	width: 200px
}

td.fee {
	width: 75px
}
</style>
<?php
start_content_apply(); // in tools.php
?>

<h2 class="pageheader">Membership Application Details</h2>

<div class="row">
	<div class="col-lg-10 col-xl-8 offset-lg-1 offset-xl-2">



		<table class="table table-sm table-striped">
			<thead>
				<tr class="table-dark">
					<th colspan="2">Applicant</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="first">Name</td>
					<td><?= $applicant->fullname ?></td>
				</tr>
				<tr>
					<td>Email</td>
					<td><?= $applicant->email ?></td>
				</tr>
				<tr>
					<td>Main Phone</td>
					<td><?= phone_number($applicant->phone) ?></td>
				</tr>
				<tr>
					<td>Mobile Phone</td>
					<td><?= phone_number($applicant->mobile) ?></td>
				</tr>
				<tr>
					<td>Date of Birth</td>
					<td><?= $applicant->dob ?></td>
				</tr>
			</tbody>
		</table>

<?php
if (in_array($applicant->applyType, ['JT','JTre','JSre'])):

	if (is_null($applicant->email2)) {
		$emailWarn = 'The joint applicant must have a valid and unique email address.';
	} elseif (strtolower($applicant->email2) == strtolower($applicant->email)) {
		$emailWarn = 'Email for the joint applicant cannot match applicant.';
	} elseif (! filter_var($applicant->email2 , FILTER_VALIDATE_EMAIL)) {
		$emailWarn = 'The joint applicant must have a valid and unique email address.';
	}

	if ($applicant->applyType == 'JSre') {
		if (empty($applicant->dob2)) {
			$dob2Warn = 'Joint Applicant Date of Birth required for Senior Membership.';
		}
	}
	?>
		<table id="jtApplicant" class="table table-sm table-striped">
			<thead>
				<tr class="table-dark">
					<th colspan="2">Joint Applicant</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="first">Name</td>
					<td><?= $applicant->fullname2 ?></td>
				</tr>
				<tr>
					<td>
						Email
					</td>
					<td<?php if ($emailWarn) echo ' class="is-invalid"' ?>>
						<?= $applicant->email2 ?>
					</td>
				</tr>
				<tr>
					<td>Main Phone</td>
					<td><?= phone_number($applicant->phone2) ?></td>
				</tr>
				<tr>
					<td>Mobile Phone</td>
					<td><?= phone_number($applicant->mobile2) ?></td>
				</tr>
				<tr>
					<td>Date of Birth</td>
					<td><?= $applicant->dob2 ?></td>
				</tr>
			</tbody>
		</table>
<?php endif; ?>

		<table class="table table-sm table-striped">
			<thead>
				<tr class="table-dark">
					<th colspan="2">Address</th>
				</tr>
			</thead>
			<tbody>
<?php if ($applicant->applyType != 'JTadd'): ?>
			<tr>
					<td class="first">Street Address</td>
					<td><?= $applicant->street ?></td>
				</tr>
				<tr>
					<td>City</td>
					<td><?= $applicant->city ?></td>
				</tr>
				<tr>
					<td>State</td>
					<td><?= $applicant->state ?></td>
				</tr>
				<tr>
					<td>Zip</td>
					<td><?= $applicant->zip ?></td>
				</tr>
<?php else: ?>
			<tr>
					<td style="width: 200px">Street Address</td>
					<td>from current member</td>
				</tr>
<?php endif; ?>
		</tbody>
		</table>

		<table class="table table-sm table-striped">
			<thead>
				<tr class="table-dark">
					<th colspan="2">Application</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="first">Type</td>
					<td><?= $appType ?></td>
				</tr>
<?php if ($reinstate): ?>
			<tr>
					<td>Old Membership</td>
					<td><?= $oldText ?></td>
				</tr>
<?php endif; ?>
			<tr>
					<td>Sponsor #1</td>
					<td><?= $applicant->sponsor1 ?></td>
				</tr>
				<tr>
					<td>Sponsor #2</td>
					<td><?= $applicant->sponsor2 ?></td>
				</tr>
<?php if (! $reinstate):
	if (empty($applicant->event1) || empty($applicant->event2)) {
		$eventWarn = 'Two Club events must be listed. One must be a work activity.';
	}
?>
			<tr>
					<td>Event #1</td>
					<td><?= $applicant->event1 ?></td>
				</tr>
				<tr>
					<td>Event #2</td>
					<td><?= $applicant->event2 ?></td>
				</tr>
<?php endif; ?>
			<tr>
					<td>Comments</td>
					<td>
<?php if (! is_null($applicant->notes)) echo nl2br($applicant->notes); ?>
					</td>
				</tr>
				<tr>
					<td>Code</td>
					<td><?= $key ?></td>
				</tr>
			</tbody>
		</table>

		<table class="table table-sm table-striped">
			<thead>
				<tr class="table-dark">
					<th colspan="2">Status</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="first">
						<?php Show_Info('Application Created', getHelp('appCreated'))?>
						Application Created
					</td>
					<td><?= $applicant->created ?></td>
				</tr>
				<tr>
					<td>
						<?php Show_Info('Application Submitted', getHelp('appSubmitted'))?>
						Application Submitted
					</td>
					<td><?= $submitted ?></td>
				</tr>
				<tr>
					<td><?php Show_Info('Application Approved', getHelp('appApproved'))?>
						Application Approved
					</td>
					<td><?= $applicant->approved ?></td>
				</tr>
				<tr>
					<td><?php Show_Info('Application Complete', getHelp('appComplete'))?>
						Application Complete
					</td>
					<td><?= $applicant->complete ?></td>
				</tr>
			</tbody>
		</table>

<?php if ($applicant->complete): ?>
		<div class="alert alert-success">
			Your membership application process is complete. If you need help, please contact
				<span class="emlink" data-user="membership">the membership chair</span>.
		</div>

<?php elseif ($applicant->approved): ?>
		<div class="alert alert-success">
			<p>
				Your application has been approved, pending payment of fees.<br>If you need
				help, please contact
				<span class="emlink" data-user="membership">the membership chair</span>.
			</p>
			<a href="<?= $feesPage ?>" class="btn btn-primary">
				<?= add_icon('money-check-alt') ?> Review and pay fees
			</a>
		</div>

<?php elseif ($applicant->submitted): ?>
		<div class="alert alert-success">
			Your application has been submitted. You cannot edit it again. If your application
			is approved you will receive an invoice by email. If you need help, please contact
			<span class="emlink" data-user="membership">the membership chair</span>.
		</div>
<?php else:
	if ($typeWarn || $emailWarn || $addressWarn || $eventWarn || $sponsorWarn
		|| $dobWarn || $dob2Warn) {
		$warn = '';
		if ($typeWarn) $warn .= '<br>' . $typeWarn;
		if ($emailWarn) $warn .= '<br>' . $emailWarn;
		if ($addressWarn) $warn .= '<br>' . $addressWarn;
		if ($eventWarn) $warn .= '<br>' . $eventWarn;
		if ($sponsorWarn) $warn .= '<br>' . $sponsorWarn;
		if ($dobWarn) $warn .= '<br>' . $dobWarn;
		if ($dob2Warn) $warn .= '<br>' . $dob2Warn;
	}
?>

		<div class="btn-group mb-2">
			<button id="btnSubmit" type="button"
				class="btn btn-primary"<?php if ($warn) echo ' disabled' ?>>
				<?= add_icon('share-square') ?> Submit Application
			</button>
			<a class="btn btn-secondary" href="<?= $editPage ?>"><?= add_icon('edit') ?> Edit</a>
		</div>

<?php if ($warn): ?>
		<div class="alert alert-danger">
			<b>Please edit to correct the following:</b><?= $warn ?>
		</div>

<?php endif;
endif;
?>

	</div>
</div>

<?php end_page(); ?>
