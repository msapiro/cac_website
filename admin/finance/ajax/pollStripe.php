<?php
require '../../support/config.php';
require CLASSLOADER;

require '../includes/config.php';         // contains $allowGroups for this directory
//array_push($allowGroups, 'ROmembers');  // override config for this page (optional)
require AUTH_AJAX;                        // make sure user is logged in and a group member


// It is possible, though unlikely, that a user will close browser after paying
// for something on stripe site, before being re-directed to our success page.
// If that happens, the user will not be credited for payment on our site, even
// though a receipt was sent from Stripe to the user.

// This ajax call tests a particular pending stripe entry to see if it was, in fact,
// completed. If so, we release the pending changes.

require '../../../support/functions.php';

include '../../../members/payment/includes/functions.php';


$data = array();
$data['polled'] = 0;   // did not poll Stripe
$data['released'] = 0; // did not release pending changes

$stripe = new Stripes($_POST['stripeID']);

if (empty($stripe->stripeID)) {
	$data['success'] = 0;
	$data['msg'] = 'Could not find a transaction for stripeID=' . $_POST['stripeID'];
	echo json_encode($data);
	exit;
}

/**
 * from config file:
 * @var string $stripeSecretKey
 * @var string $stripeApiVersion
 * */

if ($stripe->status == 'pending') {
	// we need to call Stripe
	require '/www/cac/data/credentials/stripe.php';
	$stripeClient = new \Stripe\StripeClient([
		'api_key' => $stripeSecretKey,
		'stripe_version' => $stripeApiVersion
	]);

	$data['polled'] = 1;
	$found = false;

	if (!is_null($stripe->chargeID) && preg_match('/^cs_/', $stripe->chargeID)) {
		// we have a checkout session ID; use it directly.
		try {
			$session = $stripeClient->checkout->sessions->retrieve(
				$stripe->chargeID, ['expand' => ['payment_intent']]
			);
		} catch (\Stripe\Exception\ApiErrorException $e) {
			$data['msg'] = "Stripe returned the following error while retrieving"
				. " the checkout session.\n\n" . $e;
			echo json_encode($data);
			exit;
		}

		$found = true;
		$paymentIntent = $session['payment_intent']; // might be NULL

	} else {
		// search on Stripe for the completed session
		$createdTime = strtotime($stripe->created);
		$endTime = $createdTime + (24 * 60 * 60); // search a reasonable period 24 hours

		$events = $stripeClient->events->all([
			'type' => 'checkout.session.completed',
			'created' => [
				// Search for event objects created after our stripe table entry was created
				'gte' => $createdTime,
				'lt' => $endTime
			]
		]);

		foreach ($events->autoPagingIterator() as $event) {
			$session = $event->data->object;
			$refID = $session->client_reference_id;

			if ($refID == $stripe->stripeID) {
				$found = true;

				// this is a completed session, so there must be a payment_intent
				$paymentIntentID = $session->payment_intent;
				$paymentIntent = $stripeClient->paymentIntents->retrieve($paymentIntentID);
				break; // no need to test more events
			}
		}
	}

	if ($found) {
		// we have a session object to work with

		if (is_null($paymentIntent)) {
			// user didn't submit payment info
			$data['msg'] = 'Checkout session not completed for stripeID=' . $stripe->stripeID;

		} elseif ($paymentIntent->status == 'succeeded') {
			// we have a payment_intent to work with

			// new in api version 2022-11-15
			$chargeID = $paymentIntent->latest_charge;
			$charge = $stripeClient->charges->retrieve($chargeID,
				['expand' => ['balance_transaction']]);

			$stripe->status = 'ok';
			$stripe->chargeID = $charge->id;
			$stripe->stripeAmount = $charge->amount / 100; // maybe test this against $amount
			$stripe->stripeFee = $charge->balance_transaction->fee / 100;
			$stripe->amountNet = $charge->balance_transaction->net / 100;
			$stripe->dbSave();

			switch ($paymentIntent->metadata->category) {
				case 'dues':
					$transType = $paymentIntent->metadata->transType;
					switch ($transType) {
						case 'new':
							releaseNewMember($stripe->stripeID);
							$data['released'] = 1;
							break;
						case 'renew':
							releaseRenewal($stripe->stripeID);
							$data['released'] = 1;
							break;
						case 'reactivate':
							releaseRenewal($stripe->stripeID);
							$data['released'] = 1;
							break;
					}
					break;

				case 'application':
					releaseApplication($stripe->stripeID);
					$data['released'] = 1;
					break;

				case 'reinstate':
					releaseReinstate($stripe->stripeID);
					$data['released'] = 1;
					break;

				default:
					// unmatched transaction type
					break;
			}

			$data['msg'] = 'Found a completed checkout session for stripeID=' . $stripe->stripeID;

		} else {
			$data['msg'] = 'Payment_intent not completed for stripeID=' . $stripe->stripeID;
		}

	} else {
		$data['msg'] = 'Could not find a checkout session for stripeID='
			. $stripe->stripeID . '.';
	}

} elseif ($stripe->status == 'ok') {
	// already done with this item
	$data['msg'] = 'Checkout session for stripeID=' . $stripe->stripeID
		. ' was already processed successfully.';
} else {
	// something else, maybe in the future
	$data['msg'] = 'Checkout session for stripeID=' . $stripe->stripeID
		. ' was already processed with status = ' . $stripe->status;
}

echo json_encode($data);
