<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';
include 'includes/tools.php';

$msgArray = (new Msgs())->getAllSumm();

start_page('CAC Mailer');
?>
<script>
$(function() {
	// do stuff when DOM is ready

	// make nice tooltips for the buttons
	$('.tip').tooltip({container: 'body', placement: 'bottom'});

});

function confirmDelete(mailID) {
	var agree = confirm("Are you sure you want to delete this Message?");
	if (agree) {
		$("#row-" + mailID + " button.tip").tooltip("dispose");
		$("#row-" + mailID).remove();
		$.post('ajax/deleteMail.php', {mailID: mailID});
	} else {
		$("button.tip").blur();
	}
}

function copy(mailID) {
	var msg = "This will make a complete copy of the message, including any images.\n";
	msg += "The new message will have [COPY] prepended to the Subject.\n\n";
	msg += "Are you sure you want to copy this Message?";
	var agree = confirm(msg);
	if (agree) {
		$.post('ajax/copyMail.php', {mailID: mailID}, function() {
			location.reload();
		});
	}
}
</script>
<?php
start_content();
toolbar();
?>

<h3 class="pageheader">Message List</h3>

<div class="row">
	<div class="col-xl-10 offset-xl-1">

	<table class="table table-sm table-striped">
	<thead>
		<tr><th class="text-center">Action</th><th class="text-center">Template</th><th>Subject</th>
		<th class="text-center">Type</th><th class="text-center">Sent</th><th>Most Recent</th></tr>
	</thead>
	<tbody>
<?php
foreach ($msgArray as $row):
	$URLedit = 'edit.php?id=' . $row['mailID'];
	$URLdetail = 'detail.php?id=' . $row['mailID'];
	$type = 'text';
	$template = '';
	if($row['html']) {
		$type = 'html';
		if ($row['isTemplate']) $template = add_icon_text('clone', 'far');
	}
?>
		<tr id="row-<?= $row['mailID'] ?>">
		<td class="text-center">
			<div class="btn-group btn-group-sm">
				<button type="button" class="btn btn-secondary tip" onclick="window.open('<?= $URLdetail ?>','_self')" title="Detail">
					<?php add_icon('list-alt') ?>
				</button>
				<button type="button" class="btn btn-secondary tip" onclick="window.open('<?= $URLedit ?>','_self')" title="Edit">
					<?php add_icon('edit') ?>
				</button>
				<button type="button" class="btn btn-secondary tip" onclick="copy(<?= $row['mailID'] ?>)" title="Copy">
					<?php add_icon('copy') ?>
				</button>
				<button type="button" class="btn btn-secondary tip" onclick="confirmDelete(<?= $row['mailID'] ?>)" title="Delete">
					<?php add_icon('times') ?>
				</button>
			</div>
		</td>
		<td class="text-center"><?= $template ?></td>
		<td><?= $row['subject'] ?></td>
		<td class="text-center"><?= $type ?></td>
		<td class="text-center"><?= $row['sent'] ?></td>
		<td><?= $row['latest'] ?></td>
		</tr>
<?php endforeach; ?>
	</tbody>
	</table>

	</div>
</div>

<?php end_page(); ?>
