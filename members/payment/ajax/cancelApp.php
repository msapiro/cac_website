<?php
// no login required.
// ajax call to cancel a stripe transaction related to a membership application before it happens.
// this just cleans up the tables, since we know the transaction will never be processed.
require '../../support/config.php';
require CLASSLOADER;

session_name('CACSESSID');
session_start();

// we use a session variable rather than get or post so user cannot submit an arbitrary stripeID
if (empty($_SESSION['cancelStripe'])) exit;

$stripe = new Stripes($_SESSION['cancelStripe']);
if (empty ($stripe->stripeID)) exit;
if ($stripe->status != 'pending') exit;

$applicant = new Applicants();
$applicant->getByStripeID($stripe->stripeID);
$applicant->stripeID = 0;
$applicant->dbSave();

$stripe->dbDelete();
