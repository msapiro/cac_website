<?php
require '../../support/config.php';
require CLASSLOADER;

$allowGroups = array('admin', 'trainee');
require AUTH_AJAX;  // make sure user is logged in and a group member


$training = new Trainees($_POST['trainingID']);
$training->dbDelete();
