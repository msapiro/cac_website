<?php
require '../support/config.php';
require CLASSLOADER;

require AUTH_PAGE; // enforce login

require '../../support/functions.php';
require '../../support/formFunctions.php';

// stripe credentials
require '/www/cac/data/credentials/stripe.php';


// this is the text that appears on the page tab in most browsers.
$title = 'CAC Renewal';
$pageDescription = 'California Alpine Club Member Services';


$memberID = $_SESSION['cac_login']['memberID']; // logged in user
$cancelID = inty($_GET['cancelID']); // callback from Stripe

/**
 * from include:
 * @var string $stripePublicKey
 */

if ($cancelID) {
	// callback - payment was canceled by user on the stripe page.
	$displayCancel = true;

	$stripe = new Stripes($cancelID);
	if (empty($stripe->stripeID)) {
		error('Error', 'Did not find a Stripe transaction matching stripeID=' . $cancelID);
		exit;
	}

	$memberTrans = new MemberTrans();
	$memberTrans->getByStripeID($stripe->stripeID);
	if (empty($memberTrans->mTransID)) {
		error('Error', 'Did not find a Member transaction matching stripeID=' . $cancelID);
		exit;
	}

	$email = $_GET['email'];  // add this second check to prevent leaking info
	if ($memberTrans->email != $email) {
		error('Error', 'Email check failed for stripeID=' . $cancelID . ' with email=' . $email);
		exit;
	}

	if ($stripe->status != 'pending') {
		// this is not a pending transaction, so we shouldn't be on this page
		header("Location: ../payment/success.php?id=$stripe->stripeID");
		exit;
	}

	$member = new Members($memberTrans->memberID, 'membership');
	$dues = $memberTrans->amount;
	$newExpiration = $memberTrans->expiration;

	$donationFDN = new DonationsFDN();
	$donationFDN->getByStripeID($stripe->stripeID);
	$fdnAmount = $donationFDN->amount;
	if (in_array($fdnAmount, [25, 50, 100])) {
		$fdnSelect = '#fdn' . number_format($fdnAmount, 0);
	} else {
		$fdnSelect = '#fdnOther';
	}

	$donationGF = new DonationsGF();
	$donationGF->getByStripeID($stripe->stripeID);
	$gfAmount = $donationGF->amount;
	if (in_array($gfAmount, [25, 50, 100])) {
		$gfSelect = '#gf' .  number_format($gfAmount, 0);
	} else {
		$gfSelect = '#gfOther';
	}

} else {
	$displayCancel = false;
	$member = new Members($memberID, 'membership');

	// expiration is always March 31, renewal is delinquent (but allowed) after July 1.
	$expireYear = explode('-', $member->membership->expiration)[0];

	if (! $member->membership->expired) {
		// current member. Add one year to expiration.
		$newExpiration = ($expireYear + 1) . '-03-31';
	} else {
		// expired member. is expiration date less than 1 year ago?

		if ($member->membership->isExpired(false, 'P9M')) {
			// expired more than 9 months ago. member needs to be reinstated by admin
			$newExpiration = false;
		} else {
			// renew for the current period
			$thisMonth = date('n');
			$thisYear = date('Y');
			if ($thisMonth >= 4) {
				// renewals from April 1 to Dec 31
				$newExpiration = ($thisYear + 1) . '-03-31';
			} else {
				// renewals from Jan 1 and April 1
				// this is probably a late renewal for the current term
				// This shouldn't happen any more, but treat as reinstatement.
				$newExpiration = ($thisYear+ 1) . '-03-31';
			}
		}
	}

	$dues = $member->membership->dues;
	$membershipType = $member->membership->class;
	$item = $member->membership->memDescription . ' Renewal';

	// make a 'pending' entry for this transaction. it may or may not ever be completed, depending on payment.
	$stripe = new Stripes();
	$stripe->amount = $dues; // this might be increased later if donations are added to the stripe transaction
	$stripe->item = $item;
	$stripe->dbSave();

	$memberTrans = new MemberTrans();
	$memberTrans->stripeID = $stripe->stripeID;
	$memberTrans->amount = $dues;
	$memberTrans->memberID = $memberID;
	$memberTrans->email = $member->email;
	$memberTrans->membershipID = $member->membershipID;
	$memberTrans->action = 'renew';
	$memberTrans->membershipType = $membershipType;
	$memberTrans->expiration = $newExpiration;
	$memberTrans->dbSave();

	$fdnSelect = '#fdnOther';
	$fdnAmount = '0.00';
	$gfSelect = '#gfOther';
	$gfAmount = '0.00';
}

$_SESSION['cancelStripe'] = $stripe->stripeID; // used if the user clicks the cancel button

start_page($title, $pageDescription);
?>
<script>
"use strict";
var stripeID = "<?= $stripe->stripeID ?>";
var fdnSelect = "<?= $fdnSelect ?>";
var fdnAmount = "<?= $fdnAmount ?>";
var gfSelect = "<?= $gfSelect ?>";
var gfAmount = "<?= $gfAmount ?>";

$(function() {
    // do stuff when DOM is ready

    $("input[name='fdn']").click(function() {
		if (this.id == "fdnOther") {
			$("#fdnAmount").prop("disabled", false).focus();
		} else {
			$("#fdnAmount").val("").prop("disabled", true);
		}
    });

    $("input[name='gf']").click(function() {
		if (this.id == "gfOther") {
			$("#gfAmount").prop("disabled", false).focus();
		} else {
			$("#gfAmount").val("").prop("disabled", true);
		}
    });

    $("#btnCancel").click( function() {
		$.post("../payment/ajax/cancel.php", function() {
			window.open('index.php', "_self");
		});
	});

    $("#btnCheckout").click(function() {
    	// prevent double submit or cancel
    	$(this).prop("disabled",true).html("Please wait ...");
    	$("#btnCancel").prop("disabled",true);

        // post the donations
        var fdnAmount = $("input[name='fdn']:checked").val();
        if (fdnAmount == 'other') fdnAmount = $("#fdnAmount").val();
		var gfAmount = $("input[name='gf']:checked").val();
		if (gfAmount == 'other') gfAmount = $("#gfAmount").val();

    	// create a stripe session, then redirect to stripe checkout using the new sessionURL
    	// we pass the fromPage explicitly since php $_SERVER['HTTP_REFERER'] is unreliable.
    	var fromPage = window.location.href;
    	$.post('ajax/membershipSession.php', {stripeID: stripeID, fdn: fdnAmount, gf: gfAmount, fromPage: fromPage}, function(data) {
    		if (data.success == 1) {
    			window.open(data.sessionURL, "_self");
    		} else {
    			var err = 'Could not create a Stripe checkout session.<br>';
    			err += data.msg + '<br>';
    			err += 'Please ask membership@californiaalpineclub.org for help.';
   				$('#error-msg').html(err).show();
   			}
    	}, 'json');
    });

	$(fdnSelect).click();
	if (fdnSelect == "#fdnOther") {$("#fdnAmount").val(fdnAmount).blur()};
	$(gfSelect).click();
	if (gfSelect == "#gfOther") {$("#gfAmount").val(gfAmount).blur()};

});
</script>

<style>
	#renewData div.form-group {margin-bottom: 0px;}
</style>

<?php start_content(); ?>

<h2 class="pageheader">Membership Renewal</h2>

<div class="row">
	<div class="col-xl-10 offset-xl-1 col-lg-12 col-md-10">

		<div class="row">

			<div class="col-lg"> <!-- left column -->
				<table class="table table-sm table-striped">
				<thead>
					<tr class="table-dark">
						<th style="width: 180px">Personal Data</th><th style="font-weight: normal;">memberID: <?= $memberID ?></th>
					</tr>
				</thead>
				<tbody>
					<tr><td>Name</td><td><?= $member->fullname ?></td></tr>
					<tr><td>Email</td><td><?= $member->email ?></td></tr>
				</tbody>
				</table>

				<table class="table table-sm table-striped">
				<thead>
						<tr class="table-dark">
							<th style="width: 180px">Membership Data</th><th style="font-weight: normal;">membershipID: <?= $member->membershipID ?></th>
						</tr>
					</thead>
				<tbody>
					<tr><td>Type</td><td><?= $member->membership->memDescription ?></td></tr>
					<tr>
						<td>Expiration</td>
						<td><?php echo $member->membership->expiration; if ($member->membership->expired) echo ' <span style="color: red">EXPIRED</span>'; ?></td>
					</tr>
				</tbody>
				</table>
			</div> <!-- end left column -->

			<div class="col-lg"> <!-- right column -->

				<div class="card well">
				<div class="card-body">

<?php if ($newExpiration): ?>
				<form id="formRenew" action="memberRenewPay.php" method="post" autocomplete="off">
					<div id="renewData">

<?php
static_field('Membership Type', $member->membership->memDescription, false, false, 'col-sm col-md-4 col-lg', 'col-sm col-md-8 col-lg');
static_field('Renewal Term', '1 year', false, false, 'col-sm col-md-4 col-lg', 'col-sm col-md-8 col-lg');
static_field('Dues', '$' . $dues, false, false, 'col-sm col-md-4 col-lg', 'col-sm col-md-8 col-lg');
static_field('New Expiration', $newExpiration, false, false, 'col-sm col-md-4 col-lg', 'col-sm col-md-8 col-lg');
?>
					</div> <!--  end renewData -->

					<hr class="cac">

					<p>Your generous additional donations to the CAC Foundation and the General Fund are greatly appreciated.</p>

					<div><b>CAC Foundation Donation</b> (Tax Deductible)</div>

					<div class="form-check">
						<input class="form-check-input" type="radio" name="fdn" id="fdn25" value="25">
						<label class="form-check-label" for="fdn25">$25</label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="fdn" id="fdn50" value="50">
						<label class="form-check-label" for="fdn50">$50</label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="fdn" id="fdn100" value="100">
						<label class="form-check-label" for="fdn100">$100</label>
					</div>
					<div class="form-check form-check-inline col-4">
						<input class="form-check-input" type="radio" name="fdn" id="fdnOther" value="other">
						<label class="form-check-label mb-1" for="fdnOther">Other</label>

						<div class="input-group input-group-sm">
							<span class="input-group-text">$</span>
							<input type="text" class="form-control text-end" name="fdnAmount" id="fdnAmount">
						</div>
					</div>

					<div class="mt-3"><b>General Fund Donation</b> (Not deductible)</div>

					<div class="form-check">
						<input class="form-check-input" type="radio" name="gf" id="gf25" value="25">
						<label class="form-check-label" for="gf25">$25</label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="gf" id="gf50" value="50">
						<label class="form-check-label" for="gf50">$50</label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="gf" id="gf100" value="100">
						<label class="form-check-label" for="gf100">$100</label>
					</div>
					<div class="form-check form-check-inline col-4">
						<input class="form-check-input" type="radio" name="gf" id="gfOther" value="other">
						<label class="form-check-label mb-1" for="gfOther">Other</label>

						<div class="input-group input-group-sm">
							<span class="input-group-text">$</span>
							<input type="text" class="form-control text-end" name="gfAmount" id="gfAmount">
						</div>
					</div>

					<div class="col-lg-10 mt-3">
						<div class="btn-group">
							<button id="btnCheckout" type="button" class="btn btn-primary"><?php add_icon('cc-stripe', 'fab') ?> &nbsp;Pay online</button>
							<button id="btnCancel" type="button" class="btn btn-secondary">Cancel</button>
						</div>
					</div>
				</form>

<?php
	if ($displayCancel): ?>
			<div class="alert alert-warning mt-3">
				<?php add_icon('exclamation-triangle')?> You did not complete the payment page.<br>
				You can try again using the <b>Pay online</b> button above.<br>
				If you do not want to pay online, please click the <b>Cancel</b> button above.
			</div>
<?php
	else: ?>
			<div class="alert alert-success mt-3">
				<ul>
					<li>If you do not want to proceed with payment, please click the <b>Cancel</b> button above.</li>
					<li>You can cancel on the next screen by clicking on the Alpine Club logo.</li>
				</ul>
			</div>
<?php
	endif;
else: ?>

				<div class="alert alert-danger">
					Your membership expired more than one year ago. You cannot renew. You must apply for reinstatement.
				</div>
<?php endif; ?>

				</div>
				</div> <!-- end card -->

			</div> <!-- end right column -->
		</div> <!-- end row -->

	</div>
</div> <!-- end row -->

<?php end_page(); ?>
