<?php
// create a stripe session for a pending membership application
// login not required for this page
require '../../support/config.php';
require CLASSLOADER;

/**
 * from included files:
 *
 * @var string $stripeSecretKey
 * @var string $stripeApiVersion
 */

$data = array();
$stripe = new Stripes($_POST['stripeID']);

if (empty($stripe->stripeID)) stopNow('stripeID invalid or not provided');

$applicant = new Applicants();
$applicant->getByStripeID($stripe->stripeID);

if (empty($applicant->applicantID)) stopNow('application not found');
if ($stripe->status != 'pending') stopNow('stripeID=' . $stripe->stripeID . ' not pending');
if ($stripe->amount == 0) stopNow('total fees = 0');

// create the stripe client
require '/www/cac/data/credentials/stripe.php'; // stripe credentials
$stripeClient = new \Stripe\StripeClient([
	'api_key' => $stripeSecretKey,
	'stripe_version' => $stripeApiVersion
]);

$entryFee_item = false;
if ($applicant->entryFee > 0) {
	$entryFee_item = [
		'quantity' => 1,
		'price_data' => [
			'currency' => 'usd',
			'unit_amount' => $applicant->entryFee * 100,
			'product_data' => [
				'name' => 'Entry Fee'
			]
		]
	];
}

$dues_item = false;
if ($applicant->dues > 0) {
	$dues_item = [
		'quantity' => 1,
		'price_data' => [
			'currency' => 'usd',
			'unit_amount' => $applicant->dues * 100,
			'product_data' => [
				'name' => 'Membership Dues'
			]
		]
	];
}

$line_items = array();
if ($entryFee_item) $line_items[] = $entryFee_item;
if ($dues_item) $line_items[] = $dues_item;

// we need access to wordpress home_url() function
require __DIR__ . '/../../../../wp-load.php';

$success_url = home_url() . '/cac/members/payment/success.php?sessionID={CHECKOUT_SESSION_ID}&id='
	. $stripe->stripeID;

// return to the calling page with cancelID set. include email as a second check.
// user might cancel multiple times, so construct the URL from parts rather than adding on.
$parts = parse_url($_POST['fromPage']);
$cancel_url = $parts['scheme'] . '://' . $parts['host'] . $parts['path'] . '?cancelID='
	. $stripe->stripeID . '&email=' . $applicant->email;

$category = 'application';
if (in_array($applicant->applyType, ['RGre','JTre','SRre','JSre'])) {
	$category = 'reinstate';
}

// the description attached to the payment intent gets transferred to the payment,
// which is helpful to extract on our Admin financial pages.
$stripeSession = $stripeClient->checkout->sessions->create(
	[
	"client_reference_id" => $stripe->stripeID,
	"customer_email" => $applicant->email,
	"success_url" => $success_url,
	"cancel_url" => $cancel_url,
	"mode" => "payment",
	"line_items" => $line_items,
	"payment_intent_data" => [
		"description" => "Application Fees",
		"metadata" => [
			"stripeID" => $stripe->stripeID,
			"category" => $category,
			"entryFee" => $applicant->entryFee,
			"dues" => $applicant->dues
			]
		]
	]);

// we store the sessionID in the chargeID field temporarily.
// replace it with the chargeID after successful payment.
$stripe->chargeID = $stripeSession->id;
$stripe->dbSave();

$data['success'] = 1;
$data['sessionURL'] = $stripeSession->url;
echo json_encode($data);
exit();

function stopNow($msg) {
	$data = array();
	$data['success'] = 0;
	$data['msg'] = $msg;
	echo json_encode($data);
	exit();
}
