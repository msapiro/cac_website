<?php
// functions to send a welcome message to new and reinstated members

use PHPMailer\PHPMailer\PHPMailer;

require_once __DIR__ . '/../../../../wp-load.php';

$volunteerPage = home_url() . '/cac/members/volunteer';

function sendWelcome($member) {
	if (!$member->email) return false;

	$mail = new PHPMailer();
	$mail->isSendmail(); // use system MTA (probably postfix)
	$mail->Sender = 'webmaster@californiaalpineclub.org';

	$mail->Subject = 'CAC Membership';
	$mail->setFrom('webmaster@californiaalpineclub.org', 'CAC webmaster');
	$mail->addReplyTo('membership@californiaalpineclub.org'); // default is to use fromAddress
	$mail->addAddress($member->email);

	$mail->CharSet = 'utf-8';
	$mail->XMailer = 'CAC Mail'; // default is phpMailer version

	$html = msgHTML();
	$mail->msgHTML($html);

	$text = msgTEXT();
	$mail->AltBody = $text;

	$mailresult = $mail->send();
	return $mailresult;
}

function msgHTML() {
	// construct the html version of the email
	global $volunteerPage;

	$html = <<<HTML
	<!DOCTYPE html>
	<html lang="en">
	<body style="font-family: sans-serif; font-size: 14px; margin-left: 10px">
	
	<p>Welcome to the California Alpine Club!</p>
	
	<p>You probably received another message with instructions to set up your login password.
	Once you are able to log in, please visit our Volunteer page by clicking on the button below
	to indicate your skills and interests. All members are expected to participate in one or more 
	volunteer opportunities per year. For example, you may coordinate a Club event, host at Alpine
	or Echo Lodge, cook or clean-up at CAC Club events at Alpine Lodge, edit our TRAILS newsletter, 
	proofread CAC documents, fill a leadership position, join a committee, or lead a hike or 
	outing.</p>
	
	<a style="text-align: center; text-decoration: none; vertical-align: center;
		border: 1px solid; border-color: #0062cc; border-radius: 4px;
		color: white; background-color: #0069d9; padding: 7px;"
		href="[VOLUNTEERPAGE]">Volunteer Skills and Interests</a>
			
	</body>
	</html>
	HTML;

	$html = str_replace('[VOLUNTEERPAGE]', $volunteerPage, $html);
	return $html;
}

function msgTEXT() {
	// construct the text version of the email
	global $volunteerPage;

	$text = "Welcome to the California Alpine Club!\n\n";
	$text .= "You probably received another message with instructions to set up your\n";
	$text .= "login password. Once you are able to log in, please visit our Volunteer\n";
	$text .= "page to indicate your skills and interests. All members are expected to\n";
	$text .= "participate in one or more volunteer opportunities per year. For example,\n";
	$text .= "you may coordinate a Club event, host at Alpine or Echo Lodge, cook or\n";
	$text .= "clean-up at CAC Club events at Alpine Lodge, edit our TRAILS newsletter,\n";
	$text .= "proofread CAC documents, fill a leadership position, join a committee,\n";
	$text .= "or lead a hike or outing.\n\n";

	$text .= "Here is the link to the Volunteer page:\n";
	$text .= "$volunteerPage \n";

	return $text;
}