<?php
require '../support/config.php';
require CLASSLOADER;

$allowGroups = array('admin', 'trainee');
require AUTH_PAGE; // make sure user is logged in and a group member


$title = 'CAC Host Trainees';
$pageDescription = 'Add member training';

require '../../support/functions.php';
require '../../support/formFunctions.php';

include 'includes/tools.php';

$mid = inty($_GET['id']);
if (! ($mid)) {
	error('Error', 'This script requires a member id.');
	exit;
}
$member = new Members($mid);
$traineeClass = new Trainees();
$trainings = $traineeClass->getAllTrainings($mid);

$lodgeSelect = ['0' => 'Select', 'A' => 'Alpine', 'E' => 'Echo'];

start_page($title, $pageDescription);
?>
<script>
$(function() {
	// do stuff when DOM is ready

	// make nice tooltips for the buttons
	$('.tip').tooltip({container: 'body', placement: 'bottom'});

	$("#btnAddTraining").click(function() {
		$('#trainModal').modal('show');
	});
});

function deleteTraining(elmt, trainingID) {
	var agree = confirm("Are you sure you want to delete this training?");
	if (agree) {
		$.post('ajax/deleteTraining.php', {trainingID: trainingID}, function() {
			$(elmt).closest("tr").remove();
		});
	}
	elmt.blur();
}
</script>

<?php
start_content();
?>

<h2 class="pageheader">Training Details for
	<a href="detail.php?id=<?= $member->memberID?>"><?= $member->fullname ?></a>
</h2>

<div class="row justify-content-center">
	<div class="col-xl-7 col-lg-9">
		<div class="card well mt-4">
		<div class="card-body">

			<button id="btnAddTraining" class="btn btn-secondary" type="button">
				<?php add_icon('plus') ?>&nbsp;Add Training
			</button>
<?php
if ($trainings): ?>
			<table class="table table-striped table-sm col-lg-10">
				<thead>
					<tr>
						<th class="text-center">Delete</th>
						<th>trainingID</th>
						<th>Lodge</th>
						<th>Year</th>
					</tr>
				</thead>
				<tbody>
<?php
	foreach ($trainings as $training):
		if ($training->lodge == 'A') {
			$lodge = 'Alpine';
		} elseif ($training->lodge == 'E') {
			$lodge = 'Echo';
		} else {
			$lodge = 'Unknown';
		}
?>

					<tr>
						<td class="text-center">
							<button type="button" class="btn btn-secondary btn-sm tip"
								onclick="deleteTraining(this,<?= $training->traineeID ?>)"
								title="Delete"><?php add_icon('times') ?>
							</button>
						</td>
						<td><?= $training->traineeID ?></td>
						<td><?= $lodge ?></td>
						<td><?= $training->year ?></td>
					</tr>
<?php   endforeach; ?>

				</tbody>
			</table>
<?php endif; ?>

		</div>
		</div>
	</div>
</div>


<!-- Modal for training add -->
<div class="modal fade" id="trainModal">
	<div class="modal-dialog">
		<div class="modal-content" id="trainModalContent">
			<div class="modal-header">
				<h4 class="modal-title">Add Training for <?= $member->fullname ?></h4>
				<button type="button" class="btn-close" data-bs-dismiss="modal"></button>
			</div>

			<form id="trainForm" method="post" action="addTrain2.php">
				<input type="hidden" name="memberID" value="<?= $member->memberID ?>">
			<div class="modal-body">

<?php
edit_select_field('Lodge', 'lodge', $lodgeSelect, false, false, 'lodge');
edit_field('Year', 'year', false, 4, 'YYYY', false, 'year');
?>

			</div> <!-- end modal-body -->

			<div class="modal-footer">
				<div class="btn-group">
					<button type="submit" class="btn btn-primary">Submit</button>
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
				</div>
			</div>
			</form>

        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal -->
<?php end_page(); ?>
