<?php
// if you want this page to have a custom title, set it here.
// leave it blank to use the default title for the site.
// this is the text that appears on the page tab in most browsers.
$title = 'Not Authorized';

require '../support/config.php';
require '../../support/functions.php';

// we need access to some wordpress functions
require_once( __DIR__ . '/../../../wp-load.php' );

start_page($title);
start_content();

// +++++ Edit the page content below +++++ ?>

<div class="row">

	<div class="alert alert-danger col-lg-8 offset-lg-2 mt-4">

		<p>You are logged in as a CAC member, but you do not have the additional privileges needed
		to access the page you requested.</p>

		<p>If you think this is an error, please contact
		<span class="emlink" data-user="webmaster">the webmaster</span>.</p>

		<p><a href="<?= home_url() ?>">Go to CAC public website</a></p>

	</div>

</div> <!-- end row -->

<?php // +++++ Edit the page content above +++++
end_page(); ?>
