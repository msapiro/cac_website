<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member


$img = new Images();

$imageArray = $img->getAll(true);

// construct the json expected by image browser. we are not using folders.
// https://github.com/spantaleev/ckeditor-imagebrowser

$first = true;
echo '[';
foreach ($imageArray as $image) {
	if (! $first) echo ',';
	$obj = array("image" =>$image-> fileURL, "thumb" => $image->thumbURL);
	echo  json_encode($obj, JSON_UNESCAPED_SLASHES);
	$first = false;
}
echo ']';
