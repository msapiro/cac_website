<?php
require '../../support/config.php';
require CLASSLOADER;

require '../includes/config.php'; // contains $allowGroups for this directory
//array_push($allowGroups, 'ROmembers');
require AUTH_AJAX;                // make sure user is logged in and a group member


/**
 * from Config
 * @var int $batchTime
 */

$data = array();

if (empty($_SESSION['addresses'])) {
	$data['more'] = 0;
	$data['progress'] = 'ERROR: zero email addresses';
	echo json_encode($data);
	exit;
}

$returnTime = time() + $batchTime;  // batchTime is in config.php

//make sure we don't time out if someone sets a large batchTime
$limitTime = $batchTime + 5;
if ($limitTime > ini_get('max_execution_time')) set_time_limit($limitTime);


$mailID = inty($_POST['mailID']);
$nextIndex = inty($_POST['nextIndex']);

$startIndex = $nextIndex;
$data['more'] = 1; // indicates more addresses to process after this batch
$data['error'] = '';

if ($mailID) {
	$msg = new Msgs($mailID);
} else {
	$data['more'] = 0;
	$data['progress'] = 'ERROR: mail ID not specified.';
	echo json_encode($data);
	exit;
}

// construct the message
use PHPMailer\PHPMailer\PHPMailer;

$mail = new PHPMailer();
$mail->isSendmail();  // use system MTA (probably postfix)
$mail->CharSet = 'utf-8';
$mail->XMailer = 'CAC Mail'; // default is phpMailer version
$mail->AddCustomHeader("List-Id: CAC Mailer <mailer.californiaalpineclub.org>"); // for Gmail users to filter
$mail->AddCustomHeader("X-List-Id: CAC Mailer <mailer@californiaalpineclub.org>");  // for opendkim signing

if ($mail->validateAddress($msg->envelopeSender)) {
	$mail->Sender = $msg->envelopeSender;
}

$mail->setFrom($msg->fromAddress, $msg->fromName);

if (! empty($msg->replyTo)) {
	if ($mail->validateAddress($msg->replyTo)) {
		$mail->addReplyTo($msg->replyTo); // default is to use fromAddress
	}
}

$mail->Subject = $msg->subject;

if ($msg->html) {
	$body = $msg->htmlBody;
	if (is_null($body)) {
		$body = ''; // make it an empty string
	}

	if ($msg->useTemplate) {
		// if our message has html, head, or body tags, remove them.
		// these should be only in the template
		$body = preg_replace('/<html.*<body.*>/siU', '', $body);  // from opening <html> to <body attrib="something">
		$body = preg_replace('/<\/body.*html>/si', '', $body);    // from closing </body> to closing </html>

		// perform the merge
		$template = new Msgs($msg->useTemplate);
		$body = str_replace('[BODY]', $body, $template->htmlBody);
	}

	// where to find image files
	$prependDir = realpath(__DIR__ . '/../../../uploads');

	$relativeDir = '/cac/uploads/';
	if ($_SERVER['CONTEXT_PREFIX'] != '/') $relativeDir = $_SERVER['CONTEXT_PREFIX'] . $relativeDir;

	// remove the relativeDir from relative image tags
	$body = preg_replace_callback ('/<img.* src="(.*)".*>/isU',
		function ($matches) use($relativeDir) {
			if (strpos($matches[1], '://') !== false) {
				// contains a protocol, do not modify
				return $matches[0];
			}
			// else remove the relative path
			return str_replace($relativeDir, '', $matches[0]);
		}, $body);

	$mail->msgHTML($body, $prependDir);
	if (! empty($msg->textBody)) $mail->AltBody = $msg->textBody;
} else {
	$mail->Body = $msg->textBody;
}

// start sending the batch
while (time() <= $returnTime) {
	if (! isset($_SESSION['addresses'][$nextIndex])) {
		// finished
		$data['more'] = 0;
		break;
	}
	$toAddress = $_SESSION['addresses'][$nextIndex];
	$toName = '';
	if (! empty($_SESSION['names'][$nextIndex])) $toName = $_SESSION['names'][$nextIndex];
	$mail->addAddress($toAddress, $toName);

	if ($mail->send()) {
		$msg->db->insert("mailSent", ["mailID" => $mailID, "email" => $toAddress]);
	} else {
		$data['error'] .= "ERROR sending to $toAddress " . $mail->ErrorInfo;
	}
	$mail->clearAddresses();
	$nextIndex++;
}
$processed = $nextIndex - $startIndex;

$data['nextIndex'] = $nextIndex;
$data['progress'] = "Processed $processed addresses";

echo json_encode($data);
