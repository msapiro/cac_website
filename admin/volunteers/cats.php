<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// array_push($allowGroups, 'ROmembers');
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';
include 'includes/tools.php';


$volCat = new VolCats();
$catArray = $volCat->getAll();

start_page('CAC Volunteers');
add_script('jqueryui');  // for sortable
?>
<script>
$(function() {
	// do stuff when DOM is ready

	 $("#catTable tbody").sortable({
			handle: '.handle',
			axis: "y",
			cursor: "ns-resize",
			containment: "#catTable",
			opacity: .50,
			update: function() {
		        var order = $("#catTable tbody").sortable('serialize');
				$.post("ajax/catSorter.php", {input: order});
		    }
		});

	$("button.remove").click(function() {
		var catID = $(this).closest("tr").data("id");
		var hasOpps = parseInt($(this).data("opps"));
		if (hasOpps) {
			alert("Cannot delete a Category with Opportunities assigned to it.\n" +
				"Re-assign or delete them first.");
		} else {
			var agree = confirm("Are you sure you want to delete this Category?");
			if (agree) {
				$.post('ajax/deleteCat.php', {catID: catID}, function() {
					location.reload(); //reload this page
				});
			}
		}
	});

});
</script>
<?php
start_content();
toolbar();
?>

<h3 class="pageheader">Opportunity Categories</h3>

<div class="row justify-content-center mt-3">
	<div class=" col-md-8 col-lg-7 col-xl-6">

	<table id="catTable" class="table table-striped table-sm">
		<thead>
			<tr><th class="text-center">Order</th><th class="text-center">Action</th><th>Category</th><th class="text-center">Opps</th></tr>
		</thead>
		<tbody>
<?php
foreach ($catArray as $oCat):
	$URLedit = 'editCat.php?id=' . $oCat->catID;
?>
			<tr id="row_<?= $oCat->catID ?>" data-id="<?= $oCat->catID ?>">
				<td class="handle"><?php add_icon('arrows-alt-v') ?></td>
				<td class="text-center">
					<div class="btn-group btn-group-sm">
					<a href="<?= $URLedit ?>" class="btn btn-secondary tip" title="Edit"><?php add_icon('edit') ?></a>
					<button type="button" class="btn btn-secondary tip remove" title="Delete" data-opps="<?= $oCat->getCount() ?>">
						<?php add_icon('trash') ?>
					</button>
					</div>
				</td>
				<td><?= $oCat->catName ?></td>
				<td class="text-center"><span class="badge bg-primary bg-pill"><?= $oCat->getCount() ?></span></td>
			</tr>
<?php
endforeach;
?>
	</tbody>
	</table>

	</div>
</div>

<?php end_page(); ?>
