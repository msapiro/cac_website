#!/usr/bin/python3

import sys
import pymysql

from datetime import date, timedelta

AGE_65 = timedelta(65 * 365.25)
AGE_55 = timedelta(55 * 365.25)
AGE_45 = timedelta(45 * 365.25)
AGE_35 = timedelta(35 * 365.25)
AGE_20 = timedelta(20 * 365.25)
now = date.today()
    
def get_cat(dob):
    if not dob or isinstance(dob, str):
        return 'Missing'
    if dob <= now - AGE_65:
        return '65 and over'
    if dob <= now - AGE_55:
        return '55 to 65'
    if dob <= now - AGE_45:
        return '45 to 55'
    if dob <= now - AGE_35:
        return '35 to 45'
    if dob <= now - AGE_20:
        return '20 to 35'
    return 'Under 20'

def main():
    db = pymysql.connect(user='CAC_query',
                         passwd='CAC_dbqry_1913',
                         db='alpine',
                         charset='utf8')
    c = db.cursor()
    c.execute("""SELECT dob, members.created, memberID FROM memberships
              JOIN members USING (membershipID)
              WHERE class NOT IN ('', '0', 'OR', 'NM') AND expiration > %s AND resigned = 0""",
              (now - timedelta(91)))
    last_year = dict()
    last_five = dict()
    year_ago = date(now.year-1, now.month, now.day)
    five_ago = date(now.year-5, now.month, now.day)
    while True:
        rec = c.fetchone()
        if not rec:
            break
        dob, created, mid = rec
        cat = get_cat(dob)
        if not created or isinstance(created, str):
            continue
        created = date(created.year, created.month, created.day)
        if created >= year_ago:
            last_year[cat] = last_year.setdefault(cat, 0) + 1
        if created >= five_ago:
            last_five[cat] = last_five.setdefault(cat, 0) + 1
    print('Ages of new members in the last year')
    for cat in ('Missing', '65 and over', '55 to 65', '45 to 55', '35 to 45',
                '20 to 35', 'Under 20'):
        try:
            print(f'    {cat}\t{last_year[cat]}')
        except KeyError:
            print(f'    {cat}\t0')
    print('Ages of new members in the last five years')
    for cat in ('Missing', '65 and over', '55 to 65', '45 to 55', '35 to 45',
                '20 to 35', 'Under 20'):
        try:
            print(f'    {cat}\t{last_five[cat]}')
        except KeyError:
            print(f'    {cat}\t0')
    c.close()
    db.close()

if __name__ == '__main__':
    main()
