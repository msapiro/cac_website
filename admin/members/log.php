<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
array_push($allowGroups, 'ROmembers');
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';
include 'includes/tools.php';


$title = 'CAC Members';
$pageDescription = 'Recent Membership Activity';

$offset = inty($_REQUEST['offset']);

$thisPage = $_SERVER["PHP_SELF"];

$monthStartUnixTime = mktime(0, 0, 0, date("n") - $offset, 1);    // first day of this month
$monthDisplay = date('F Y', $monthStartUnixTime);
$monthEndUnixTime = mktime(0, 0, 0, date("n") + 1 - $offset, 1);  // first day of next month

$cutoff1 = date('Y-m-d', $monthStartUnixTime);
$cutoff2 = date('Y-m-d', $monthEndUnixTime);

$prev = $offset + 1;
$prevURL = $thisPage . '?offset=' . $prev;

$nextURL = false;
$next = $offset - 1;
if ($next >= 0) {
	$nextURL = $thisPage . '?offset=' . $next;
}

$model = new Model();
$db = $model->db;

$query = "CREATE TEMPORARY TABLE tempTrans (
	adminName varchar(120) DEFAULT NULL,
	date datetime DEFAULT NULL,
	memberName varchar(120) DEFAULT NULL,
	memberID int(11) DEFAULT NULL,
	membershipID int(11) DEFAULT NULL,
	item varchar(250) DEFAULT NULL,
	fee decimal(8,2) DEFAULT 0,
	pmtType varchar(40) DEFAULT NULL,
	pmtAmount decimal(8,2) DEFAULT NULL,
	stripeFee decimal(8,2) DEFAULT NULL,
	amountNet decimal(8,2) DEFAULT NULL,
	status varchar(40) DEFAULT NULL,
	stripeID int(11) DEFAULT 0,
	transNote varchar(100) DEFAULT NULL)";
$db->query($query);


// membership transactions using stripe
$stripeRows = $db->select("memberTrans", ["[><]stripe" => "stripeID", "[>]members" => "memberID"], [
	"members.first",
	"members.last",
	"memberTrans.memberID",
	"memberTrans.membershipID",
	"stripe.stripeID",
	"stripe.status",
	"stripe.amount",
	"stripe.stripeAmount",
	"stripe.amountNet",
	"stripe.stripeFee",
	"stripe.created",
	"stripe.item"
	],[
	"stripe.created[<>]" => [$cutoff1, $cutoff2]
	]);

foreach ($stripeRows as $row) {
	$db->insert("tempTrans", [
		"date" => $row['created'],
		"memberName" => $row['first'] . ' ' . $row['last'],
		"memberID" => $row['memberID'],
		"membershipID" => $row['membershipID'],
		"item" => $row['item'],
		"fee" => $row['amount'],
		"pmtType" => 'stripe',
		"stripeID" => $row['stripeID'],
		"pmtAmount" => $row['stripeAmount'],
		"stripeFee" => $row['stripeFee'],
		"amountNet" => $row['amountNet'],
		"status" => $row['status']
	]);
}

// membership transactions not using stripe
$pmtRows = $db->select("memberTrans",
	["[><]payments" => "pmtID", "[>]members" => "memberID", "[>]members(adm)" => ["adminID" => "memberID"]],[
	"members.first",
	"members.last",
	"adm.last(adminName)",
	"memberTrans.memberID",
	"memberTrans.membershipID",
	"payments.pmtType",
	"payments.amount",
	"payments.pmtAmount",
	"payments.transNote",
	"payments.created",
	"payments.item"
	],[
	"payments.created[<>]" => [$cutoff1, $cutoff2]
	]);

foreach ($pmtRows as $row) {
	$db->insert("tempTrans", [
		"date" => $row['created'],
		"memberName" => $row['first'] . ' ' . $row['last'],
		"adminName" => $row['adminName'],
		"memberID" => $row['memberID'],
		"membershipID" => $row['membershipID'],
		"item" => $row['item'],
		"fee" => $row['amount'],
		"pmtType" => $row['pmtType'],
		"pmtAmount" => $row['pmtAmount'],
		"transNote" => $row['transNote']
	]);
}

// applications using stripe
$stripeRows = $db->select("applicants", ["[><]stripe" => "stripeID"], [
	"applicants.first",
	"applicants.last",
	"applicants.email",
	"stripe.stripeID",
	"stripe.status",
	"stripe.amount",
	"stripe.stripeAmount",
	"stripe.amountNet",
	"stripe.stripeFee",
	"stripe.created",
	"stripe.item"
	],[
	"stripe.created[<>]" => [$cutoff1, $cutoff2]
	]);

foreach ($stripeRows as $row) {
	// see if we can associate the applicant with a new member using email
	$member = new Members();
	$memberArray = $member->getByEmail($row['email']);
	if (count($memberArray) == 1) {
		$member = $memberArray[0];
		$memberID = $member->memberID;
		$membershipID = $member->membershipID;
	} else {
		$memberID = 0;
		$membershipID = 0;
	}

	$db->insert("tempTrans", [
		"date" => $row['created'],
		"memberName" => $row['first'] . ' ' . $row['last'],
		"memberID" => $memberID,
		"membershipID" => $membershipID,
		"item" => $row['item'],
		"fee" => $row['amount'],
		"pmtType" => 'stripe',
		"stripeID" => $row['stripeID'],
		"pmtAmount" => $row['stripeAmount'],
		"stripeFee" => $row['stripeFee'],
		"amountNet" => $row['amountNet'],
		"status" => $row['status']
	]);
}

// applications not using stripe
$pmtRows = $db->select("applicants",
	["[><]payments" => "pmtID", "[>]members(adm)" => ["adminID" => "memberID"]],[
		"applicants.first",
		"applicants.last",
		"applicants.email",
		"adm.last(adminName)",
		"payments.pmtType",
		"payments.amount",
		"payments.pmtAmount",
		"payments.transNote",
		"payments.created",
		"payments.item"
		],[
		"payments.created[<>]" => [$cutoff1, $cutoff2]
		]);

foreach ($pmtRows as $row) {
	// see if we can associate the applicant with a new member using email
	$member = new Members();
	$memberArray = $member->getByEmail($row['email']);
	if (count($memberArray) == 1) {
		$member = $memberArray[0];
		$memberID = $member->memberID;
		$membershipID = $member->membershipID;
	} else {
		$memberID = 0;
		$membershipID = 0;
	}

	$db->insert("tempTrans", [
		"date" => $row['created'],
		"memberName" => $row['first'] . ' ' . $row['last'],
		"adminName" => $row['adminName'],
		"memberID" => $memberID,
		"membershipID" =>$membershipID,
		"item" => $row['item'],
		"fee" => $row['amount'],
		"pmtType" => $row['pmtType'],
		"pmtAmount" => $row['pmtAmount'],
		"transNote" => $row['transNote']
	]);
}

// get all the rows in calendar order
$rows = $db->select("tempTrans", "*", ["ORDER" => ["date" => "DESC"]]);

start_page($title, $pageDescription);
?>
<script>
$(function() {
	// do stuff when DOM is ready

	// make nice tooltips for the buttons
	$('.tip').tooltip({container: 'body', placement: 'bottom'});

});

function pollStripe(stripeID, btn) {
	btn.html('Wait...');
	$.post("../finance/ajax/pollStripe.php", {stripeID: stripeID}, function(data) {
		var message = data.msg + "\n\nPolled Stripe: ";
		if (data.polled) {
			message += "Yes";
				message += "\nReleased pending changes: ";
				if (data.released) {
					message += "Yes";
				} else {
					message += "No";
				}
		} else {
			message += "No";
		}
		alert(message);
		if (data.released) {
			location.reload(true); // reload this page to display changed data
		} else {
			btn.html('Verify').blur();
		}
	}, 'json');
}
</script>
<?php
start_content();
toolbar();
?>

<div class="row">
	<div class="col-md-5 col-lg-4 col-xl-3 mt-3">
		<div class="btn-group">

<?php if ($prevURL): ?>

			<a class="btn btn-secondary" href="<?= $prevURL ?>"><?php add_icon('chevron-left') ?> Previous</a>
<?php else : ?>
			<a class="btn btn-secondary disabled" href="#"><?php add_icon('chevron-left') ?> Previous</a>
<?php endif;

if ($nextURL): ?>
			<a class="btn btn-secondary" href="<?= $nextURL ?>">Next <?php add_icon('chevron-right') ?></a>
			<a class="btn btn-secondary" href="<?= $thisPage ?>"><?php add_icon('step-forward') ?> </a>
<?php else : ?>
			<a class="btn btn-secondary disabled" href="#">Next <?php add_icon('chevron-right') ?></a>
<?php endif; ?>
		</div>
	</div>

	<div class="col-md-5">
		<h3 class="pageheader">Membership Activity</h3>
	</div>
</div> <!-- end row -->


<h4>Transactions: <?= $monthDisplay ?></h4>

<table class="table table-sm table-striped">
	<thead>
	<tr><th>Admin</th><th>Date</th><th>Member</th><th class="text-end">mshipID</th><th>Item</th>
		<th class="text-end">Fee</th><th class="text-end">Payment</th><th>Type</th><th class="text-end">StripeFee</th><th class="text-end">Net</th>
	</tr>
	</thead>
	<tbody>
<?php

foreach ($rows as $row):
	switch ($row['pmtType']) {
		case 'stripe':
			($row['stripeFee'] > 0) ? $stripeFee = '$' . $row['stripeFee'] : $stripeFee = '';
			($row['amountNet'] > 0) ? $amountNet = '$' . $row['amountNet'] : $amountNet = '';
			($row['fee'] > 0) ? $fee = '$' . $row['fee'] : $fee = '';
			($row['pmtAmount'] > 0) ? $pmtAmount = '$' . $row['pmtAmount'] : $pmtAmount = '';
			if ($row['status'] == 'pending') {
				$pmtAmount = 'incomplete';
				$stripeFee = '<button class="btn btn-sm btn-secondary tip" title="Call Stripe to verify"
					onclick="pollStripe(' . $row['stripeID'] . ',$(this))">Verify</button>';
			}
			break;
		case 'check':
		case 'cash':
			($row['fee'] > 0) ? $fee = '$' . $row['fee'] : $fee = '';
			($row['pmtAmount'] > 0) ? $pmtAmount = '$' . $row['pmtAmount'] : $pmtAmount = '';
			break;
		case 'none':
			($row['fee'] > 0) ? $fee = '$' . $row['fee'] : $fee = '';
			$pmtAmount = '';
			break;
	}
?>
	<tr>
	<td><?= $row['adminName'] ?></td>
	<td><?= $row['date'] ?></td>
	<td><a href="detail.php?id=<?= $row['memberID'] ?>"><?= $row['memberName'] ?></a></td>
	<td class="text-end"><?= $row['membershipID'] ?></td>
	<td><?= str_replace(' + ', ' +<br>', $row['item']) ?></td>
	<td class="text-end"><?= $fee ?></td>
	<td class="text-end"><?= $pmtAmount ?></td>
	<td><?= $row['pmtType'] ?></td>
<?php if ($row['pmtType'] == 'stripe'): ?>
	<td class="text-end"><?= $stripeFee ?></td>
	<td class="text-end"><?= $amountNet ?></td>
<?php else: ?>
	<td colspan="2"><?= $row['transNote'] ?></td>
<?php endif; ?>
	</tr>
<?php
endforeach;
?>
	</tbody>
</table>

<div class="row">
	<div class="col-md-5 col-lg-4 col-xl-3">
		<div class="btn-group">

<?php if ($prevURL): ?>
			<a class="btn btn-secondary" href="<?= $prevURL ?>"><?php add_icon('chevron-left') ?> Previous</a>
<?php else : ?>
			<a class="btn btn-secondary disabled" href="#"><?php add_icon('chevron-left') ?> Previous</a>
<?php endif;

if ($nextURL): ?>
			<a class="btn btn-secondary" href="<?= $nextURL ?>">Next <?php add_icon('chevron-right') ?></a>
			<a class="btn btn-secondary" href="<?= $thisPage ?>"><?php add_icon('step-forward') ?> </a>
<?php else : ?>
			<a class="btn btn-secondary disabled" href="#">Next <?php add_icon('chevron-right') ?></a>
<?php endif; ?>
		</div>
	</div>
</div> <!-- end row -->

<?php end_page(); ?>
