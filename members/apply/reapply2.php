<?php
// Login not required for this page
require '../support/config.php';
require CLASSLOADER;

require '../../support/functions.php';
require_once '../../../wp-load.php';
require '../../support/restoreGlobals.php';
restoreGlobals(); //stupid WordPress

requirePost();

if (empty($_POST['key'])) {
	error('Error', 'You must submit a code to identify the application.');
	exit;
}

$applicant = new Applicants();
$applicant->getByKey($_POST['key']);
if (! $applicant->applicantID) {
	error('Error', 'Did not find an application for code ' . $_POST['key']);
	exit;
}

$member = new Members($applicant->addToMemberID);

$applicant->applyType = $_POST['rbApplyType'];

// omit email from stdFields. it is set when application is created
$stdFields = ['first', 'm_i', 'last', 'phone', 'mobile', 'dob',  'sponsor1', 'sponsor2', 'notes'];
$addressFields = ['street', 'city', 'state', 'zip'];
$jointFields = ['first2', 'm_i2', 'last2', 'email2', 'phone2', 'mobile2', 'dob2'];

foreach ($stdFields as $field) $applicant->$field = $_POST[$field];
foreach ($addressFields as $field) $applicant->$field = $_POST[$field];


if ($applicant->applyType == 'JTre' || $applicant->applyType == 'JSre') {
	// this will be a joint membership
	foreach ($jointFields as $field) $applicant->$field = $_POST[$field];

	// check email for joint applicant
	if (! filter_var($applicant->email2, FILTER_VALIDATE_EMAIL)) {
		error('Error', "email for joint applicant is not a valid address. Use your <b>Back</b> button.");
		exit;
	}

	if (strtolower($applicant->email2) == strtolower($applicant->email)) {
		error('Error', "email for joint applicant cannot match applicant. Use your <b>Back</b> button.");
		exit;
	}

	// get the current joint member, if any
	$membership = new Memberships($member->membershipID);
	$jointMembers = $membership->getJointMembers();
	if (count($jointMembers) == 2) {
		foreach ($jointMembers as $jointMember) {
			if ($jointMember->memberID == $member->memberID) continue;
			$jointEmail = strtolower($jointMember->email);
		}
	}

	if (strtolower($applicant->email2) == $jointEmail) {
		// ok, nothing is being changed
	} else {
		$member2 = new Members();
		$in_use_cac = $member2->getByEmail($applicant->email2);
		$in_use_wp = get_user_by('email', $applicant->email2);

		if ($in_use_cac) {
			error('Error', '"' . $applicant->email2 . '"' . " is already being used by a CAC member.<br>Use your <b>Back</b> button.");
			exit;
		}

		if ($in_use_wp) {
			error('Error', '"' . $applicant->email2 . '"' . " is already being used by a WordPress user.<br>Use your <b>Back</b> button.");
			exit;
		}
	}

} else {
	// clear the joint applicant fields
	foreach ($jointFields as $field) $applicant->$field = NULL;

	if ($applicant->applyType == 'SRre') {
		$membership = new Memberships($member->membershipID);
		if ($membership->class == 'JS') {
			// a Joint Senior membership will be converted to Individual Senior.
			// check that the applicant qualifies for Senior status on her own.
			// the expired joint membership might have been based on aggregate age.
			if ($applicant->dob) {
				$age = $applicant->getAge();
				if ($age < 70) error('Error', "Applicant (age $age) does not qualify for Senior Membership based on DOB.<br>
					Use your <b>Back button</b>.");
			} else {
				error('Error', 'DOB must be entered to verify Senior status. Use your <b>Back</b> button.');
			}
		}
	}
}

$applicant->dbSave();

header("Location: applyDetail.php?key=$applicant->applicantID:$applicant->code");
exit;
