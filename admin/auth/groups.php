<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';
require '../../support/formFunctions.php';

include 'includes/tools.php';

/**
 * @var array $privGroups
 */

$db = (new Model(false))->db;

$rows = $db->select("authGroups", ["[>]members" => "memberID"], [
		"privID", "memberID", "privGroup",
		"fullname" => Medoo\Medoo::raw("CONCAT(<first>, ' ', <last>)")
],[
		"ORDER" => ["privGroup", "first", "last"]
]);

start_page('CAC Authentication');
add_script('datatables');
?>
<script>
$(function() {
	// do stuff when DOM is ready

	tableFeatures('privTable', {
		"order": [3, 'asc'],
		"stateSave": false,
	});

	// make nice tooltips for the buttons
	$('.tip').tooltip({container: 'body', placement: 'bottom'});

	// some basic form validation
	$("#addForm").submit(function(event) {
		if ( $("#memberID").val() == 0 ) {
			alert ("You must select a valid member.");
			event.preventDefault();
		} else if ( $("#privGroup").val() == 0 ) {
			alert ("You must select a privilege group.");
			event.preventDefault();
		}
	});
});

function confirmDelete(privID) {
	$('.tip').tooltip('hide');
	var agree = confirm("Are you sure you want to delete this Member from the Group?");
	if (agree) {
		$("#row-" + privID).remove();
		$.post('ajax/deletePriv.php', {privID: privID});
	}
}

// open the member search modal after clearing fields
function memberSearch() {
	resetSearch();
	$('#searchModal').modal('show');
}

function submitSearch() {
	var first = $("#first").val();
	var last = $("#last").val();
	var email = $("#email").val();
	if (first.length == 0 && last.length == 0 && email.length == 0) {
		$("#searchResults").html("You did not enter any search criteria!");
		return false;
	}
	$("#searchHelp").hide();
	$("#searchResults").load('ajax/memberSearch.php', {first: first, last: last, email: email});
}

//clear the member search form and results
function resetSearch() {
	$('#searchForm')[0].reset();
	$("#searchResults").html('');
	$("#searchHelp").show();
}

// user clicks on member name in the search modal
function addMember(memberID) {
	$('#searchModal').modal('hide');
	$.post('ajax/addPriv.php', {memberID: memberID}, function(data) {
		$("#memberID").val(data.memberID);
		$("#memberDisplay").html(data.text);
	}, 'json');

}
</script>
<?php
start_content();
toolbar();
?>

<div class="row justify-content-center">
<div class="col-lg-9 col-xl-8">

<h3 class="pageheader">Privilege Groups</h3>

<table id="privTable" class="table table-sm table-striped">
<thead>
	<tr><th class="text-center nosort">Delete</th><th class="nosort">memberID</th><th>Member</th><th>Group</th></tr>
</thead>
<tbody>
<?php
foreach ($rows as $row):
?>

<tr id="row-<?= $row['privID'] ?>">
	<td class="text-center">
		<button type="button" class="btn btn-secondary btn-sm tip" onclick="confirmDelete(<?= $row['privID'] ?>)" title="Delete">
			<?php add_icon('times') ?>
		</button>
	</td>
	<td><?= $row['memberID'] ?></td>
	<td><?= $row['fullname'] ?></td>
	<td><?= $row['privGroup'] ?></td>
</tr>
<?php  endforeach; ?>
</tbody>
</table>

<div class="card well mt-4">
<div class="card-body">

<form id="addForm" action="add2.php" method="post">
	<input type="hidden" id="memberID" name="memberID" value="0">

	<h4 class="text-center">Add a Member to a Privilege Group</h4>

	<div class="row mb-3">
		<div class="col-lg-3">
			<div class="text-end">
				<button class="btn btn-secondary" type="button" onclick="memberSearch('editor')">Member lookup</button>
			</div>
		</div>

		<div id="memberDisplay" class="col-lg-8 mt-2">
			<!-- ajax will fill -->
		</div>
	</div>

	<div class="row align-items-center mb-3">
		<label class="text-lg-end col-lg-3"><b>Group</b></label>
		<div class="col-lg-5">
			<select name="privGroup" id="privGroup" class="form-select">
				<option value="0">Select</option>
<?php foreach ($privGroups as $mygroup): ?>
				<option value="<?= $mygroup ?>"><?= $mygroup ?></option>
<?php endforeach; ?>
			</select>
		</div>
	</div>

<?php static_submit(); ?>
</form>

</div>
</div>

<div class="alert alert-info mt-3">
<ul>
<li>Members of the <b>admin</b> group have privileges to all CAC Administration pages. You do not need to put them in
additional groups.</li>
<li>Members of the <b>ROmembers</b> group have Read-Only access to the Members section.</li>
</ul>
</div>

</div>
</div>

<!-- Modal for member search -->
<div class="modal fade" id="searchModal">
	<div class="modal-dialog">
		<div class="modal-content" id="searchModalContent">
			<div class="modal-header">
				<h4 class="modal-title">Member Search</h4>
				<button type="button" class="btn-close" data-bs-dismiss="modal"></button>
			</div>

			<div class="modal-body">
				<form id="searchForm">

<?php
edit_field('First Name', 'first', false, 40, false, false, 'first');
edit_field('Last Name', 'last', false, 40, false, false, 'last');
edit_field('Email', 'email', false, 60, false, false, 'email');
?>
				</form>

				<div id="searchResults">
					<!-- ajax will fill this -->
				</div>

				<div id="searchHelp" class="alert alert-info">
				<ul>
					<li>Search will only find current members.</li>
					<li>You can use partial words in the search fields.</li>
					<li>Example: "john mit" will find "Johnny Smith"</li>
					<li>When the results display, click on the member you want.</li>
				</ul>
				</div>

			</div>

			<div class="modal-footer">
				<div class="btn-group">
					<button type="button" id="submitModal" class="btn btn-primary" onclick="submitSearch()">Search</button>
					<button type="button" class="btn btn-secondary" onclick="resetSearch()">Clear</button>
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
				</div>

			</div>
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal -->

<?php end_page(); ?>
