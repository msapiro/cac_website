<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';

// Ensure we came from a POST.
requirePOST();

inty($_POST['memberID']);

$member = new Members($_POST['memberID']);
if (empty($member->memberID)) {
	error('Error', 'Did not find a member with memberID=' . $_POST['memberID']);
	exit;
}

$member->db->insert("authGroups", [
	"memberID" => $member->memberID,
	"privGroup" => $_POST['privGroup'] ]);

header("Location: groups.php");
exit;
