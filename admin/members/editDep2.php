<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';


// Ensure we came from a POST.
requirePOST();

$dep = new Dependents();
$dep->depID = $_POST['depID'];
$dep->membershipID = $_POST['membershipID'];
$dep->first = $_POST['first'];
$dep->last = $_POST['last'];
$dep->dob = $_POST['dob'];

$dep->dbSave();

$memberID = $_POST['memberID'];
header("Location: detail.php?id=$memberID");
