<?php

class Payments extends Model {

	public $pmtID;
	public $amount = 0;
	public $pmtType = 'unknown';
	public $item;
	public $pmtAmount = 0;
	public $transNote = NULL;
	public $created;
	public $modified;

	public function __construct($pmtID = 0) {
		parent::__construct(); // set up our database
		if ($pmtID) {
			$this->getByID($pmtID);
		}
	}

	public function getByID($pmtID) {
		// get the values for an existing payment
		$row = $this->db->get('payments', '*', ['pmtID' => $pmtID]);
		if (! $row) return;
		foreach ($row as $key => $value) {
			$this->$key = $value;
		}
	}

	public function dbSave() {
		// save the payment info into the database table
		$dataArray = [
			'amount' => $this->amount,
			'pmtType' => $this->pmtType,
			'item' => $this->item,
			'pmtAmount' => $this->pmtAmount,
			'transNote' => nully($this->transNote)
		];

		if ($this->pmtID) {
			// update existing payment
			$this->db->update('payments', $dataArray, ['pmtID' => $this->pmtID] );
		} else {
			// create new payment
			$this->db->insert('payments', $dataArray);
			$this->pmtID = $this->db->id();
		}
		return $this->pmtID;
	}

	public function dbDelete() {
		if ($this->pmtID) {
			$obj = $this->db->delete('payments', ['pmtID' => $this->pmtID]);
			$ret = $obj->rowCount(); // number of affected rows
		} else {
			$ret = 0;
		}
		return $ret;
	}
} // end Payments class

class MemberTrans extends Model {

	public $mTransID;
	public $adminID = 0;
	public $memberID = 0;
	public $membershipID = 0;
	public $stripeID = 0;
	public $pmtID = 0;
	public $amount = 0;
	public $action;
	public $membershipType = NULL;
	public $expiration = NULL;
	public $first = NULL;
	public $m_i = NULL;
	public $last = NULL;
	public $email = NULL;
	public $phone = NULL;
	public $mobile = NULL;
	public $street = NULL;
	public $city = NULL;
	public $state = NULL;
	public $zip = NULL;

	public function __construct($mTransID = 0) {
		parent::__construct(); // set up our database
		if ($mTransID) {
			$this->getByID($mTransID);
		}
	}

	public function getByID($mTransID) {
		// get the values for an existing payment
		$row = $this->db->get('memberTrans', '*', ['mTransID' => $mTransID]);
		if (! $row) return;
		foreach ($row as $key => $value) {
			$this->$key = $value;
		}
	}

	public function getByStripeID($stripeID) {
		// get the values for an existing payment
		$row = $this->db->get('memberTrans', '*', ['stripeID' => $stripeID]);
		if (! $row) return;
		foreach ($row as $key => $value) {
			$this->$key = $value;
		}
	}

	public function getByPaymentID($pmtID) {
		// get the values for an existing payment
		$row = $this->db->get('memberTrans', '*', ['pmtID' => $pmtID]);
		if (! $row) return;
		foreach ($row as $key => $value) {
			$this->$key = $value;
		}
	}

	public function dbSave() {
		// save the payment info into the database table
		$dataFields = array('adminID', 'memberID', 'membershipID', 'stripeID', 'pmtID', 'amount', 'action', 'membershipType',
			'expiration', 'first', 'last', 'email', 'phone', 'mobile', 'street', 'city', 'state', 'zip');

		$dataArray = array();
		foreach ($dataFields as $field) {
			$dataArray[$field] = $this->$field;
		}

		if ($this->mTransID) {
			// update existing transaction
			$this->db->update('memberTrans', $dataArray, ['pmtID' => $this->mTransID] );
		} else {
			// create new transaction
			$this->db->insert('memberTrans', $dataArray);
			$this->mTransID = $this->db->id();
		}
		return $this->mTransID;
	}

	public function dbDelete() {
		if ($this->mTransID) {
			$obj = $this->db->delete('memberTrans', ['mTransID' => $this->mTransID]);
			$ret = $obj->rowCount(); // number of affected rows
		} else {
			$ret = 0;
		}
		return $ret;
	}
} // end MemberTrans class

class Donations extends Model {

	public $donationID;
	public $adminID = 0;
	public $memberID = 0;
	public $stripeID = 0;
	public $pmtID = 0;
	public $type;
	public $amount;

	public function __construct($donationID = 0) {
		parent::__construct(); // set up our database
		if ($donationID) {
			$this->getByID($donationID);
		}
	}

	public function getByID($donationID) {
		// get the values for an existing payment
		$row = $this->db->get('donations', '*', ['donationID' => $donationID]);
		if (! $row) return;
		foreach ($row as $key => $value) {
			$this->$key = $value;
		}
	}

	public function getByStripeID($stripeID) {
		// return an array of donation objects or an empty array
		$donationsArray = array();
		$rows = $this->db->select('donations', 'donationID', ['stripeID' => $stripeID, 'ORDER' => ['donationID']]);
		foreach ($rows as $donationID) {
			$donationsArray[] = new Donations($donationID);
		}
		return $donationsArray;
	}

	public function dbSave() {
		// save the donation info into the database table
		$dataFields = array('adminID', 'memberID', 'stripeID', 'pmtID', 'type', 'amount');

		$dataArray = array();
		foreach ($dataFields as $field) {
			$dataArray[$field] = $this->$field;
		}

		if ($this->donationID) {
			// update existing
			$this->db->update('donations', $dataArray, ['donationID' => $this->donationID] );
		} else {
			// create new
			$this->db->insert('donations', $dataArray);
			$this->donationID = $this->db->id();
		}
		return $this->donationID;
	}

	public function dbDelete() {
		if ($this->donationID) {
			$obj = $this->db->delete('donations', ['donationID' => $this->donationID]);
			$ret = $obj->rowCount(); // number of affected rows
		} else {
			$ret = 0;
		}
		return $ret;
	}
} // end Donations class

// extend the general Donations class to the specific General Fund and Foundation
class DonationsGF extends Donations {
	public $type = 'gf';

	public function getByStripeID($stripeID) {
		$row = $this->db->get('donations', '*', ['stripeID' => $stripeID, 'type' => $this->type]);
		if (! $row) return;
		foreach ($row as $key => $value) {
			$this->$key = $value;
		}
	}

	public function getByPaymentID($pmtID) {
		$row = $this->db->get('donations', '*', ['pmtID' => $pmtID, 'type' => $this->type]);
		if (! $row) return;
		foreach ($row as $key => $value) {
			$this->$key = $value;
		}
	}
} // end DonationsGF class

class DonationsFDN extends Donations {
	public $type = 'fdn';

	public function getByStripeID($stripeID) {
		$row = $this->db->get('donations', '*', ['stripeID' => $stripeID, 'type' => $this->type]);
		if (! $row) return;
		foreach ($row as $key => $value) {
			$this->$key = $value;
		}
	}

	public function getByPaymentID($pmtID) {
		$row = $this->db->get('donations', '*', ['pmtID' => $pmtID, 'type' => $this->type]);
		if (! $row) return;
		foreach ($row as $key => $value) {
			$this->$key = $value;
		}
	}
}  // end DonationsFDN class

class Stripes extends Model {

	public $stripeID;
	public $status = 'pending';
	public $amount;
	public $stripeAmount = NULL;
	public $stripeFee = NULL;
	public $amountNet = NULL;
	public $item;
	public $chargeID = NULL;
	public $created;
	public $modified;

	public function __construct($stripeID = 0) {
		parent::__construct(); // set up our database
		if ($stripeID) {
			$this->getByID($stripeID);
		}
	}

	public function getByID($stripeID) {
		// get the values for an existing payment
		$row = $this->db->get('stripe', '*', ['stripeID' => $stripeID]);
		if (! $row) return;
		foreach ($row as $key => $value) {
			$this->$key = $value;
		}
	}

	public function dbSave() {
		// save the payment info into the database table
		$dataFields = array('status', 'amount', 'stripeAmount', 'stripeFee', 'amountNet', 'item', 'chargeID');

		$dataArray = array();
		foreach ($dataFields as $field) {
			$dataArray[$field] = $this->$field;
		}

		if ($this->stripeID) {
			// update existing transaction
			$this->db->update('stripe', $dataArray, ['stripeID' => $this->stripeID] );
		} else {
			// create new transaction
			$this->db->insert('stripe', $dataArray);
			$this->stripeID = $this->db->id();
		}
		return $this->stripeID;
	}

	public function dbDelete() {
		if ($this->stripeID) {
			$obj = $this->db->delete('stripe', ['stripeID' => $this->stripeID]);
			$ret = $obj->rowCount(); // number of affected rows
		} else {
			$ret = 0;
		}
		return $ret;
	}
} // end Stripes class
