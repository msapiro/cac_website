<?php
// Login not required for this page
require '../support/config.php';
require CLASSLOADER;

// if you want this page to have a custom title, set it here.
// leave it blank to use the default title for the site.
// this is the text that appears on the page tab in most browsers.
$title = '';
$description = 'California Alpine Club Member Reinstatement';

require_once '../../support/functions.php';
require_once '../../support/formFunctions.php';
require 'includes/tools.php';


if (empty($_GET['key'])) {
	error('Error', 'You must submit a code to identify the application.');
	exit;
}

$applicant = new Applicants();
$applicant->getByKey($_GET['key']);
if (! $applicant->applicantID) {
	error('Error', 'Did not find an application for code ' . $_GET['key']);
	exit;
}

if ($applicant->submitted) {
	// cannot edit anymore. go to detail page
	header("Location: applyDetail.php?key=$applicant->applicantID:$applicant->code");
	exit;
}

$member = new Members($applicant->addToMemberID, 'membership');
$oldText = $member->fullname . '<br>';
$oldText .= $member->membership->memDescription . ', expired ' . $member->membership->expiration;

$jointMembers = $member->membership->getJointMembers();
if (count($jointMembers) == 2) {
	foreach ($jointMembers as $jointMember) {
		if ($jointMember->memberID == $member->memberID) continue;
		$oldText .= '<br>Joint Member: ' . $jointMember->fullname;
		if ($jointMember->email) $oldText .= ' &lt;' . $jointMember->email . '&gt;';
	}
}

$key = $applicant->applicantID . ':' . $applicant->code;

start_page($title, $description);
add_script('moment.min');
?>
<script>
"use strict";
var applyType = "<?= $applicant->applyType ?>";

$(function() {
    // do stuff when DOM is ready

	$("input[name='rbApplyType']").click(function() {
		var appType = $(this).val();
		if (appType == 'JTre' || appType == 'JSre') {
			$("#jtApplicant").show();
		} else {
			$("#jtApplicant").hide();
		}
	});

	$("#" + applyType).click();

	// prevent the enter key from submitting the form
	$('#formApply').on("keydown", ":input:not(textarea)", function(event) {
    	if (event.key == "Enter") {
        	event.preventDefault();
    	}
	});

    // some form validation, but most validation is done on applyDetail.php page
    $("#formApply").submit(function(event) {
        //DOB must be blank or a valid date
		if (isValidDate('#dob', false)) {
			$('#dob').removeClass("is-invalid");
		} else {
			event.preventDefault();
			alert("Applicant Date of Birth is invalid. Format as YYYY-MM-DD");
    		$('#dob').addClass("is-invalid");
		}
		if (isValidDate('#dob2', false)) {
			$('#dob2').removeClass("is-invalid");
		} else {
			event.preventDefault();
			alert("Joint applicant Date of Birth is invalid. Format as YYYY-MM-DD");
    		$('#dob2').addClass("is-invalid");
		}
	});
});

//check validity of user-entered date
function isValidDate(dateField, required) {
	var myDate = $(dateField).val().trim();
	var dateOK = false;
	if (required == false && myDate == '') {
		return true; // accept empty field
	} else {
		// strict parsing, accept several formats
		var dateTest = moment(myDate,["YYYY-MM-DD", "YYYY/MM/DD", "MM-DD-YYYY",  "MM/DD/YYYY"], true);
		if (dateTest.isValid()) {
			$(dateField).val(dateTest.format("YYYY-MM-DD")); // set the input field
			return true;
		} else {
			return false;
		}
	}
}
</script>
<?php
start_content_apply(); // in tools.php
?>

<div class="row">
	<div class="col-xl-10 offset-xl-1">

	<h2 class="mt-4">Membership Reinstatement</h2>

	<form id="formApply" action="reapply2.php" method="post">
		<input type="hidden" name="key" value="<?= $key ?>">
<?php
static_field('Code', $key);
?>

<div class="row mb-3">
	<label class="text-lg-end col-lg-3">
		<b>Old Membership</b>
	</label>
	<div class="col-lg-5">
		<?= $oldText ?>
	</div>
</div>

<div class="row">
	<label class="text-lg-end col-lg-3">
		<b>Reapplying for</b>
	</label>

	<div class="col-lg-7">
		<div class="form-check">
			<input class="form-check-input" type="radio" name="rbApplyType" value="RGre" id="RGre">
			<label class="form-check-label" for="RGre">
				Individual Membership
			</label>
		</div>

<?php if ($member->membership->class == 'SR' || $member->membership->class == 'JS'):
	// allow senior member to reinstate as senior
	// also allow joint senior to reinstate as individual senior ?>
		<div class="form-check">
			<input class="form-check-input" type="radio" name="rbApplyType" value="SRre" id="SRre">
			<label class="form-check-label" for="SRre">
				Individual Senior Membership
			</label>
		</div>
<?php
endif;
if ($member->membership->joint):
	// Joint and Joint Senior can reinstate as Joint JT
?>

		<div class="form-check">
			<input class="form-check-input" type="radio" name="rbApplyType" value="JTre" id="JTre">
			<label class="form-check-label" for="JTre">
				Joint Membership
			</label>
		</div>
<?php
endif;
if ($member->membership->class == 'JS'):
	// only Joint Senior can reinstate as Joint Senior JS
?>

		<div class="form-check">
			<input class="form-check-input" type="radio" name="rbApplyType" value="JSre" id="JSre">
			<label class="form-check-label" for="JSre">
				Joint Senior Membership
			</label>
		</div>
<?php endif;
if ($member->membership->class == 'ST'):
	// only Student can reinstate as Student ?>

		<div class="form-check">
			<input class="form-check-input" type="radio" name="rbApplyType" value="STre" id="STre">
			<label class="form-check-label" for="STre">
				Student Membership (age 18 to 26, on April 1)
			</label>
		</div>
<?php
endif;
?>

		<div class="alert alert-info mt-3">
<?php if ($member->membership->joint): ?>
			Joint memberships can be reinstated as Joint or Individual members.
<?php else: ?>
			If you want to switch from an individual membership to a joint membership, first reinstate your
			individual membership. Then your partner can apply for a new membership joined to yours.
<?php endif; ?>
	</div>

	</div>
</div>

<h4>Applicant</h4>
<?php
edit_field('First Name', 'first', $applicant->first, 30);
edit_field('Middle Initial', 'm_i', $applicant->m_i, 20);
edit_field('Last Name', 'last', $applicant->last, 30);
static_field('Email', $applicant->email);
edit_field('Phone', 'phone', phone_number($applicant->phone), 20, 'no need to format');
edit_field('Mobile Phone', 'mobile', phone_number($applicant->mobile), 20, 'no need to format');
edit_field('Date of Birth', 'dob', $applicant->dob, 10, 'YYYY-MM-DD', false, 'dob');
?>

<div id="jtApplicant">
<h4>Joint Applicant</h4>

	<div class="row">
		<div class="col-lg-3"></div>
		<div class="col-lg-7 alert alert-info">
			<p>Please make corrections to the joint applicant below, but do not use this to replace
			a former joint member with a different person.</p>
			<p>Make sure the joint applicant has a valid email that is different from yours.</p>
		</div>
	</div>
<?php
edit_field('First Name', 'first2', $applicant->first2, 30);
edit_field('Middle Initial', 'm_i2', $applicant->m_i2, 20);
edit_field('Last Name', 'last2', $applicant->last2, 30);
edit_field('Email', 'email2', $applicant->email2, 100, 'Required - must be unique');
edit_field('Phone', 'phone2', phone_number($applicant->phone2), 20, 'no need to format');
edit_field('Mobile Phone', 'mobile2', phone_number($applicant->mobile2), 20, 'no need to format');
edit_field('Date of Birth', 'dob2', $applicant->dob2, 10, 'YYYY-MM-DD', false, 'dob2');
?>
</div>

<h4>Address</h4>
<?php
	edit_field('Street Address', 'street', $applicant->street, 120);
	edit_field('City', 'city', $applicant->city, 50);
	edit_field('State', 'state', $applicant->state, 2);
	edit_field('Zip', 'zip', $applicant->zip, 10);
?>

<h4>Sponsors</h4>
<div class="row">
	<div class="col-lg-8">
		<p>List two current Alpine Club members who have agreed to sponsor your application.
		They may not be two parts of the same joint membership.</p>
	</div>
</div>
<?php
edit_field('Sponsor #1', 'sponsor1', $applicant->sponsor1, 100);
edit_field('Sponsor #2', 'sponsor2', $applicant->sponsor2, 100);
?>

<h4>Additional Comments</h4>
<div class="row">
	<div class="col-lg-8">
		<p>Provide any other information that might be helpful in processing your application.</p>
	</div>
</div>
<?php
edit_textfield('Notes', 'notes', $applicant->notes, 4);

static_submit('Save and Review');
?>
	</form>

	</div>
</div>
<?php end_page(); ?>
