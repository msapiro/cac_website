// function and defaults for DataTables

function tableFeatures(id, extras) {
	// provide a simpler way to apply DataTable enhancements to our tables
	// making some features dependent on the number of data rows in the table.
	id = "#" + id;
	var rowCount = $(id + " tbody tr").length;
	if (rowCount > 2) {
		// we add DataTable features to the table only if more than 2 body rows
		CACdataTableDefaults();
		if (rowCount > 50) {
			// also add pagination
			extras["paging"] = true;

		}
		$(id).dataTable( extras );
	}
}

function CACdataTableDefaults() {
	// set some systemwide defaults for our DataTables enhancements.
	// by default we do not enable pagination.
	$.extend($.fn.dataTable.defaults, {
		"columnDefs": [ {"orderable": false, "targets": 'nosort'},
		                {"visible": false, "targets": 'noshow'},
		                {"searchable": false, "targets": 'noshow'}],
		"pagingType": "simple",
		"paging": false,
		"pageLength": 50,
		"lengthMenu": [[50, 100, -1], [50, 100, "All"]],
		"language" : {
			"lengthMenu" : "_MENU_ records per page",
			"search" : "Filter records: ",
			"info": "Showing _START_ to _END_ of _TOTAL_ records",
			"infoFiltered": "(filtered from _MAX_ total)"
			}
	});
}
