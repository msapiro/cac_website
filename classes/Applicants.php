<?php

class Applicants extends Model {
	public $applicantID;
	public $code;
	public $applyType = 'RG';
	public $stripeID = 0;
	public $pmtID = 0;
	public $adminID = 0;
	public $addToMember = NULL;
	public $addToMemberID = NULL;

	public $first = NULL;
	public $m_i = NULL;
	public $last = NULL;
	public $email = NULL;
	public $phone = NULL;
	public $mobile = NULL;
	public $dob = NULL;

	public $first2 = NULL;
	public $m_i2 = NULL;
	public $last2 = NULL;
	public $email2 = NULL;
	public $phone2 = NULL;
	public $mobile2 = NULL;
	public $dob2 = NULL;

	public $street = NULL;
	public $city = NULL;
	public $state = NULL;
	public $zip = NULL;

	public $sponsor1 = NULL;
	public $sponsor2 = NULL;
	public $event1 = NULL;
	public $event2 = NULL;
	public $notes = NULL;
	public $entryFee = 0;
	public $dues = 0;

	public $submitted = NULL;
	public $approved = NULL; // approved pending payment
	public $complete = NULL; // membership changes made

	public $created;
	public $modified;

	public $fullname;
	public $fullname2 = NULL;

	public function __construct($applicantID=0) {
		parent::__construct(); // set up our database
		if ($applicantID) {
			$this->getByID($applicantID);
		}
	}

	public function getByID($applicantID) {
		// get the data for an existing applicant.
		$row = $this->db->get('applicants', '*', ['applicantID' => $applicantID]);
		if (! $row) return;
		foreach ($row as $key => $value) {
			$this->$key = $value;
		}
		$this->getFullnames();
	}

	// when getting for non-admins, make sure the code matches
	public function getByKey($key) {
		if (! strpos($key, ':')) return;
		list($applicantID, $code) = explode(':', trim($key));
		$row = $this->db->get('applicants', '*', ['applicantID' => $applicantID, 'code' => $code]);
		if (! $row) return;
		foreach ($row as $key => $value) {
			$this->$key = $value;
		}
		$this->getFullnames();
	}

	public function getByStripeID($stripeID) {
		// get the values for an existing payment
		$row = $this->db->get('applicants', '*', ['stripeID' => $stripeID]);
		if (! $row) return;
		foreach ($row as $key => $value) {
			$this->$key = $value;
		}
		$this->getFullnames();
	}

	public function getFullnames() {
		$this->fullname = $this->first;
		if ($this->m_i) $this->fullname .= ' ' . $this->m_i . '.';
		$this->fullname .= ' ' . $this->last;

		if ($this->first2) {
			$this->fullname2 = $this->first2;
			if ($this->m_i2) $this->fullname2 .= ' ' . $this->m_i2 . '.';
			$this->fullname2 .= ' ' . $this->last2;
		}
	}

	public function getAll($limit = false) {
		// return an array of application objects, or an empty array
		$applyArray = array();
		$where = ['ORDER' => ['applicantID' => 'DESC']];
		if ($limit) $where = array_merge($where, ['LIMIT' => $limit]);
		$rows = $this->db->select('applicants', 'applicantID', $where);
		foreach ($rows as $applicantID) {
			$applyArray[] = new Applicants($applicantID);
		}
		return $applyArray;
	}

	public function status($field) {
		// put the current timestamp in the submitted, approved, or complete field, which is probably NULL
		$tz = new DateTimeZone('America/Los_Angeles');
		$now = new DateTime('now', $tz);
		$timestamp = $now->format('Y-m-d H:i:s');
		$this->db->update('applicants', [$field => $timestamp], ['applicantID' => $this->applicantID] );
		$this->$field = $timestamp;
	}

	public function getAge($dob = 0) {
		// return age in years as of April 1 of this year
		// or return NULL if dob is not set
		if (! $dob) $dob = $this->dob;
		if (empty($dob)) return NULL;
		$tz = new DateTimeZone('America/Los_Angeles');
		$birth = new DateTime($dob, $tz);
		$april1 = new DateTime(date('Y') . '-04-01', $tz);
		$interval = $april1->diff($birth);
		return $interval->y;
	}

	public function dbSave() {
		// save the applicant info into the database table
		$dataArray = [
			'applyType' => $this->applyType,
			'stripeID' => inty($this->stripeID),
			'pmtID' => inty($this->pmtID),
			'adminID' => inty($this->adminID),
			'addToMember' => nully($this->addToMember),
			'addToMemberID' => nully($this->addToMemberID),
			'first' => nully($this->first),
			'm_i' => nully($this->m_i),
			'last' => nully($this->last),
			'email' => nully($this->email),
			'phone' => nully($this->phone),
			'mobile' => nully($this->mobile),
			'dob' => nully($this->dob),
			'first2' => nully($this->first2),
			'm_i2' => nully($this->m_i2),
			'last2' => nully($this->last2),
			'email2' => nully($this->email2),
			'phone2' => nully($this->phone2),
			'mobile2' => nully($this->mobile2),
			'dob2' => nully($this->dob2),
			'street' => nully($this->street),
			'city' => nully($this->city),
			'state' => nully($this->state),
			'zip' => nully($this->zip),
			'sponsor1' => nully($this->sponsor1),
			'sponsor2' => nully($this->sponsor2),
			'event1' => nully($this->event1),
			'event2' => nully($this->event2),
			'notes' => nully($this->notes),
			'entryFee' => $this->entryFee,
			'dues' => $this->dues
		];

		if (! is_null($dataArray['state'])) {
			$dataArray['state'] = strtoupper($dataArray['state']);
		}

		if (! is_null($dataArray['m_i'])) {
			$dataArray['m_i'] = preg_replace('/(.*)\.$/', '$1', $dataArray['m_i']); // remove trailing .
		}

		if ($this->applicantID) {
			// update existing applicant
			$this->db->update('applicants', $dataArray, ['applicantID' => $this->applicantID] );
		} else {
			// create new applicant

			// generate and set a pass code.
			// note that ultimtely the MySQL comparison is not case sensitive,
			// so this is not as strong as it appears
			$charPool = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNPQRSTUWXYZ123456789";
			$pass = array();
			$length = strlen($charPool) - 1;
			$pwlength = random_int(20,32);
			for ($i = 0; $i < $pwlength; $i++) {
				$n = random_int(0, $length);
				$pass[] = $charPool[$n];
			}
			$this->code = implode($pass);
			$dataArray = array_merge($dataArray, ['code' => $this->code]);
			$this->db->insert('applicants', $dataArray);
			$this->applicantID = $this->db->id();
		}
		return $this->applicantID;
	}

	public function dbDelete() {
		if ($this->applicantID) {
			if ($this->stripeID) {
				require_once __DIR__ . '/Payments.php';
				$stripe = new Stripes($this->stripeID);
				if ($stripe->status == 'pending') $stripe->dbDelete();
			}
			$obj = $this->db->delete('applicants', ['applicantID' => $this->applicantID]);
			$ret = $obj->rowCount(); // number of affected rows
		} else {
			$ret = 0;
		}
		return $ret;
	}
} // end Applicants class
