<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';
require '../../support/formFunctions.php';

include 'includes/tools.php';

/**
 * @var array  $allowed_extension
 */

start_page('CAC Images');
?>
<script>
$(function() {

	$("#uploadFile").change(function() {
		resetUpload();
	});
});

function resetUpload() {
	$("#testResults").hide().html('');
}

function submitUpload() {
	resetUpload();

	var docFilename = $("#uploadFile").val();

	$.post('ajax/testFile.php', {docFilename: docFilename}, function( data ) {
		if (data.badFilename) {
			$("#testResults").html('Invalid filename - see below for requirements');
			$("#testResults").show();
		} else if(data.badExtension) {
			$("#testResults").html('Invalid extension - see below for requirements');
			$("#testResults").show();
		} else {
			// OK, good to go with the upload
			$("#uploadForm").submit();
		}
	}, "json");
}

function cancelUpload() {
	$("#uploadFile").val('');
	resetUpload();
}
</script>
<?php
start_content();
toolbar();
?>

<h3 class="pageheader">Upload Image File</h3>

<div class="row">
	<div class="col-xl-10 offset-xl-1">


<form id="uploadForm" enctype="multipart/form-data" action="upload.php" method="post">
	<input type="hidden" name="MAX_FILE_SIZE" value="2048000"> <!-- 2 MB -->

	<div class="row align-items-center mb-3">
		<label class="text-md-end col-md-3"><b>File to upload</b></label>
		<div class="col-md-8">
			<input type="file" class="form-control" name="upload" id="uploadFile">
		</div>
	</div>
</form>

<div class="row mb-3">
	<div class="btn-group col-md-3 offset-md-3">
		<button type="button" class="btn btn-primary" onclick="submitUpload()">Submit</button>
		<button type="button" class="btn btn-secondary" onclick="cancelUpload()">Cancel</button>
	</div>
</div>


<div id="testResults" class="alert alert-danger" style="display: none">
</div>

<div class="alert alert-info">
	<ul>
		<li>Maximum file size: 2 MB</li>
		<li>File name may contain only letters, numbers, spaces, _ (underscore) - (dash) . (dot)</li>
		<li>File name maximum length: 200 characters</li>
		<li>Allowed extensions: <?= implode(', ' , $allowed_extension) ?></li>
	</ul>
</div>

	</div>
</div>

<?php end_page(); ?>