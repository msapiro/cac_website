<?php
require '../../support/config.php';
require CLASSLOADER;

require AUTH_AJAX; // enforce login

// add or delete a member volunteer for a particular opportunity

$data = array();

inty($_POST['memberID']);
inty($_POST['oppID']);
$check = booly($_POST['check']); // is the checkbox checked?

// logged in user
$user = new Members('login', 'membership');
if (empty($user->memberID)) {
	$data['success'] = 0;
	$data['msg'] = 'Did not find the logged-in member.';
	echo json_encode($data);
	exit;
}

$member = new Members($_POST['memberID']);
if (empty ($member->memberID)) {
	$data['success'] = 0;
	$data['msg'] = "Did not find a member with memberID=" . $_POST['memberID'];
	echo json_encode($data);
	exit;
}

if ($user->memberID != $member->memberID) {
	// users can only edit themselves and joint members
	if ($user->membershipID != $member->membershipID) {
		$data['success'] = 0;
		$data['msg'] = 'You can only edit yourself and your joint member.';
		echo json_encode($data);
		exit;
	}
}

$vol = new Volunteers();
$vol->memberID = $member->memberID;

$volOpp = new VolOpps($_POST['oppID']);
if (empty ($volOpp->oppID)) {
	$data['success'] = 0;
	$data['msg'] = "Did not find an opportunity with oppID=" . $_POST['oppID'];
} else {
	$data['success'] = 1;
	$vol->oppID = $volOpp->oppID;
	if ($check) {
		$vol->dbSave();
		$data['msg'] = "Successfully assigned $member->first to $volOpp->oppName.";
	} else {
		$vol->dbDelete();
		$data['msg'] = "Successfully cleared $member->first from $volOpp->oppName.";
	}
}

echo json_encode($data);
