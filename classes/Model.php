<?php
require_once '/www/cac/data/credentials/db.php';

// most of our models (classes) require database access so we
// configure it once in this Model class and then extend it to the others.

class Model {

	public $db;

	public function __construct($update = true) {
		if ($update) {
			$config = $GLOBALS['db_update'];
		} else {
			$config = $GLOBALS['db_query'];
		}
		$this->db = new Medoo\Medoo($config);
	}
}
