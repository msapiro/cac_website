#!/usr/bin/python3

import pymysql

from datetime import date

now = date.today()
BUCKET_SIZE = 5
START = 2014
if now.month > 6:
    END = now.year + 1
else:
    END = now.year
ranges = dict()

for x in range(START, END):
    ranges[x] = [0 for x in range(BUCKET_SIZE + 1)]

def duration(expiration, created):
    if created:
        x = expiration.year - created.year
    else:
        x = 10
    if x == 0:
        return 1
    elif x >= BUCKET_SIZE:
        return BUCKET_SIZE
    else:
        return x

def main():
    db = pymysql.connect(user='CAC_query',
                         passwd='CAC_dbqry_1913',
                         db='alpine',
                         charset='utf8')
    c = db.cursor()
    c.execute("""SELECT created, expiration FROM memberships
              WHERE class NOT IN ('', '0', 'OR', 'NM')""")
    while True:
        rec = c.fetchone()
        if not rec:
            break
        if not rec[1].year in ranges.keys():
            continue
        ranges[rec[1].year][0] += 1
        ranges[rec[1].year][duration(rec[1], rec[0])] += 1
    c.close()
    db.close()

    print('Year\tTotal Expired\tDurations')
    print('\t\t1 year\t2 years\t3 years\t4 years\t5 or more years')
    for year, totals in ranges.items():
        print(f'{year}\t{totals[0]}\t{totals[1]}\t{totals[2]}\t{totals[3]}\t'
              f'{totals[4]}\t{totals[5]}')

if __name__ == '__main__':
    main()
