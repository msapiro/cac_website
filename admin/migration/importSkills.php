<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)

if (isset($allowGroups)) {
	require AUTH_PAGE; // make sure user is logged in and a group member
}

require '../../support/functions.php';


//import skills and interests from JoinIt to the alpine database

$proceed = booly($_GET['proceed']);

start_page('CAC Migration');
start_content();

if (! $proceed): ?>

<div class="alert alert-danger mt-4">
	<p><b>WARNING</b>: This will truncate the table <b>alpine.volMembers</b>, then read member skills
	and interests from the table <b>CAC.joinit</b> and add them to the <b>alpine.volMembers</b> table.
	The joinit table must be created using the instructions on the <a href="index.php">Setup and Migration</a> page.</p>
	<p>Also, the <b>skills</b> text field from <b>CAC.member</b> will be copied to <b>alpine.volMembers</b> after
	truncating that table.</p>
</div>

<a class="btn btn-secondary" href="<?= $_SERVER['PHP_SELF'] . '?proceed=1' ?>">Continue</a>

<?php
	end_page();
	exit;
endif;

// set up a read-only connection to the CAC database
require_once '/www/cac/data/credentials/migrate.php';
$db_m = new Medoo\Medoo($db_migrate);

// and a separate connection to our alpine database
$model = new Model();
$db = $model->db;

$db->query("TRUNCATE TABLE <volMembers>");

// map some JoinIt column names
define("LODGE", "DNayyjHKezXK94vRi");
define("OUTDOOR", "GXP5gpCJCR9kv8YYh");
define("LEADERSHIP", "Hw52vQjNMDSREnPNv");
define("SOCIAL", "NckiBWzdtrS3hACno");

$unMatched = array();
$matched = 0;
$numMultimatch = 0;
$numFirst = 0;
$numEmail = 0;

$rows = $db_m->select("joinit",
	["first_name(first)",
	"external_id(membershipID)",
	"email",
	LODGE,
	OUTDOOR,
	LEADERSHIP,
	SOCIAL],
	["ORDER" => "external_id"]);

foreach($rows as $row) {
	// identify this member
	$memberArray = $db->select("members", "memberID", ["membershipID" => $row['membershipID']]);
	$matches = count($memberArray);
	if ($matches == 0) {
		// nothing to do
		$unMatched[] = array($row['first'], $row['membershipID'], $row['email']);
	} elseif ($matches == 1) {
		// good, only one match
		$matched++;
		process($memberArray[0], $row[LODGE], $row[OUTDOOR], $row[LEADERSHIP], $row[SOCIAL]);
	} else {
		$numMultimatch++;
		// try adding the first name to the criteria
		$memberArray = $db->select("members",
		"memberID",
		[
		"membershipID" => $row['membershipID'],
		"first" => $row['first']
		]);
		$matches = count($memberArray);
		if ($matches == 1) {
			// good, only one match
			$matched++;
			$numFirst++;
			process($memberArray[0], $row[LODGE], $row[OUTDOOR], $row[LEADERSHIP], $row[SOCIAL]);
		} else {
			// last attempt with email
			$memberArray = $db->select("members",
			"memberID",
			[
			"membershipID" => $row['membershipID'],
			"email" => $row['email']
			]);
			$matches = count($memberArray);
			if ($matches == 1) {
				// good, only one match
				$matched++;
				$numEmail++;
				process($memberArray[0], $row[LODGE], $row[OUTDOOR], $row[LEADERSHIP], $row[SOCIAL]);
			} else {
				$unMatched[] = array($row['first'], $row['membershipID'], $row['email']);
			}
		}
	}
}

// the skills text field can be copied from the CAC.members table
$db->query("TRUNCATE TABLE <volNotes>");

$rows = $db_m->select("member",
	["membID(memberID)", "skills"],
	["skills[!]" => '', "ORDER" => "membID"]
	);

foreach($rows as $row) {
	$db->insert("volNotes", ["memberID" => $row['memberID'], "notes" => $row['skills']]);
}
?>

<div class="row">
	<div class="col-xl-10 offset-xl-1">

<h4>Results</h4>
<p>
<?= count($rows) ?> Total JoinIt users<br>
<?= $matched ?> JoinIt users matched to CAC members<br>
<?= count($unMatched) ?> JoinIt users not matched to CAC members<br><br>

<?= $numFirst ?> matches needed first name in addition to membershipID<br>
<?= $numEmail ?> matches needed email in addition to membershipID
</p>

<?php if ($unMatched): ?>
<p>The following JoinIt users could not be matched to CAC members</p>

<table class="table table-sm table-striped">
	<thead>
		<tr><th>First</th><th>email</th><th>membershipID</th></tr>
	</thead>
	<tbody>
<?php
	foreach ($unMatched as $arr):
?>
		<tr>
		<td><?= $arr[0] ?></td>
		<td><?= $arr[2] ?></td>
		<td><?= $arr[1] ?></td>
		</tr>
<?php
	endforeach;
?>
	</tbody>
</table>
<?php endif; ?>

	</div>
</div>
<?php end_page();


function process($memberID, $lodge, $outdoor, $leadership, $social) {
	if ($lodge && $lodge != 'N/A') assignOpps($memberID, $lodge);
	if ($outdoor && $outdoor != 'N/A') assignOpps($memberID, $outdoor);
	if ($leadership && $leadership != 'N/A') assignOpps($memberID, $leadership);
	if ($social && $social != 'N/A') assignOpps($memberID, $social);
}

function assignOpps($memberID, $string) {
	global $db;
	$stringArray = explode(' | ', $string);
	foreach ($stringArray as $opp) {
		// correct misspelling in JoinIt
		$opp = str_replace('memeber', 'member', $opp);

		$oppID = $db->get("volOpps", "oppID", ["oppName" => $opp]);
		if ($oppID) {
			$db->insert("volMembers", ["memberID" => $memberID, "oppID" => $oppID]);
		} else {
			echo 'Did not find a match for - ' . $opp . '<br>';
		}
	}
}
?>
