<?php

//tools specific to this category

function getHelp($subject) {
	switch ($subject) {
		case 'memberAuto':
			$helpText = "<ul>
<li>Start typing a member name or email address.</li>
<li>You can use partial words.</li>
<li>Select a member from the matches, or keep typing to reduce the set.</li>
<li>You can use your arrow keys to scroll and the Return key to select.</li>
</ul>";
			break;

		case 'appCreated':
			$helpText = "When you saved the first draft of the application.";
			break;

		case 'appSubmitted':
			$helpText = "When you finished the application and submitted it.";
			break;

		case 'appApproved':
			$helpText = "When the administrator finished reviewing the application, approved it, and sent an invoice.";
			break;

		case 'appComplete':
			$helpText = "When you paid the invoice online or the administrator received a physical check from you.";
			break;

		default:
			$helpText = 'Help text not found. Is it defined?';
			break;
	}

	return $helpText;
}

// replace the regular start_content() function with a simpler version
// used only for the application pages, which do not require login

function start_content_apply() {
	// insert our custom css last so it overrides anything defined before it.
	// then close the head and create the top navbar. then the content.
	?>
	<link rel="stylesheet" type="text/css" href="<?= fileVersion('../../support/cac.css') ?>">
</head>

<body>
<div class="container">

	<!-- Banner content just above the main menu bar -->
	<div class="container-fluid" id="topBanner">
		<!-- visible on xs, sm, md -->
		<img class="img-fluid d-lg-none" src="../../support/graphics/cac-logo.png" alt="CAC Banner">

		<!-- visible on lg, xl - set width to 730 so there is room for the seal -->
		<img class="d-none d-lg-block" src="../../support/graphics/cac-logo.png" style="width: 730px" alt="CAC Banner">
	</div>

	<!--  Navbar collapses when going from md to sm -->
	<nav id="topMenu" class="navbar navbar-expand-md navbar-light">
		<ul class="navbar-nav me-auto">
			<li class="nav-item"><a class="nav-link" href="../apply/">APPLICATION</a></li>
		</ul>
	</nav> <!-- end topMenu -->

	<div class="d-none d-lg-block" style="position: relative; top: -150px; float: right; height: 0 !important; margin-right: 15px; z-index: 2;">
		<img src="../../support/graphics/cac-seal.png">
	</div>

	<div class="mainarea">

<?php
}
