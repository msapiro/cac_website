<?php
require '../../support/config.php';
require CLASSLOADER;

require '../includes/config.php'; // contains $allowGroups for this directory
//array_push($allowGroups, 'ROmembers');
require AUTH_AJAX;                // make sure user is logged in and a group member

// this script re-orders the categories
// when the items are dragged and dropped.

$output = array();
parse_str($_POST['input'], $output);

$oOrder = new CatsOrder();
$oOrder->orderArray = $output['row'];
$oOrder->dbSave();
