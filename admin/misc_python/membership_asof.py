#!/usr/bin/python3

import re
import sys
import pymysql

def usage():
    print(f'Usage: {sys.argv[0]} yyyy-mm-dd', file=sys.stderr)
    sys.exit(1)

def main():
    if len(sys.argv) != 2:
        usage()
    mo = re.match('(\d{4})-(\d{1,2})-(\d{1,2})$', sys.argv[1])
    if not mo:
        usage()
    
    db = pymysql.connect(user='CAC_query',
                         passwd='CAC_dbqry_1913',
                         db='alpine',
                         charset='utf8')
    c = db.cursor()
    c.execute("""SELECT class FROM memberships
              WHERE class NOT IN ('', '0', 'OR', 'NM') AND
              expiration > %s AND resigned = 0 AND
              (created is NULL OR created < %s)""",
              (sys.argv[1], sys.argv[1]))
    st = rg = jt = sr = js = lm = jl = total_1 = total_2 = 0
    while True:
        rec = c.fetchone()
        if not rec:
            break
        if rec[0] in ('ST', 'RG', 'JT', 'SR', 'JS', 'LM', 'JL'):
            total_1 += 1
            total_2 += 1
        if rec[0] == 'ST':
            st += 1
        elif rec[0] == 'RG':
            rg += 1
        elif rec[0] == 'JT':
            jt += 1
        elif rec[0] == 'SR':
            sr += 1
        elif rec[0] == 'JS':
            js += 1
        elif rec[0] == 'LM':
            lm += 1
        elif rec[0] == 'JL':
            jl += 1
        if rec[0] in ('JT', 'JS', 'JL'):
            total_2 += 1
    print(f"""As of {sys.argv[1]}
    Student Memberships:       {st}
    Regular Memberships:       {rg}
    Joint Memberships:         {jt}
    Senior Memberships:        {sr}
    Joint Senior Memberships:  {js}
    Life Memberships:          {lm}
    Joint Life Memberships:    {jl}
    -----------------------------
    Total Memberships:        {total_1}
    Total Members:            {total_2}""")
    c.close()
    db.close()

if __name__ == '__main__':
    main()
