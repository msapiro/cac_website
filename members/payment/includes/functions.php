<?php
// some common functions for the payments

// after successful charge, release the pending change
require_once '../../../admin/members/includes/welcome.php';

use PHPMailer\PHPMailer\PHPMailer;

function releaseRenewal($stripeID) {
	// TODO: this is overly complicated - we don't currently allow members
	// to choose membership class at renewal
	$stripe = new Stripes($stripeID);
	if (empty($stripe->stripeID)) exit();
	if ($stripe->status != 'ok') exit();

	$trans = new MemberTrans();
	$trans->getByStripeID($stripeID);
	if (empty($trans->mTransID)) exit();

	// can we just update the membership or do we need to make a new one?
	$updateMembership = true;

	$member = new Members($trans->memberID, 'membership');
	$oClass = new Classes($trans->membershipType);

	if ($oClass->class == $member->membership->class) {
		// no change in membershipType, update
	} else {
		if ($oClass->joint == $member->membership->joint) {
			// no change in joint status, update
		} else {
			if ($oClass->joint) {
				// changing from Individual to Joint, update
			} else {
				// changing from Joint to Individual. see if there is a joint member
				$jointMembers = $member->membership->getJointMembers();
				if (count($jointMembers) == 1) {
					// change does not affect other members, update
				} else {
					// make a new membership for this user. other member will retain the old joint membership
					$updateMembership = false;
					$newMembership = new Memberships();
					$newMembership->class = $oClass->class;
					$newMembership->expiration = $trans->expiration;
					$newMembership->paperTrails = $member->membership->paperTrails;
					$newMembership->street = $member->membership->street;
					$newMembership->city = $member->membership->city;
					$newMembership->state = $member->membership->state;
					$newMembership->zip = $member->membership->zip;
					$newMembership->sponsors = $member->membership->sponsors;
					$newMembershipID = $newMembership->dbSave();

					// assign new membership to the member
					$member->membershipID = $newMembershipID;
					$member->dbSave();
				}
			}
		}
	}

	if ($updateMembership) {
		$member->membership->class = $oClass->class;
		$member->membership->expiration = $trans->expiration;
		if ($member->membership->resigned) {
			$member->membership->resigned = 0;
			$member->membership->resignedDate = null;
		}
		$member->membership->dbSave();
	}
}

function releaseApplication($stripeID) {
	$stripe = new Stripes($stripeID);
	if (empty($stripe->stripeID)) exit();
	if ($stripe->status != 'ok') exit();

	$applicant = new Applicants();
	$applicant->getByStripeID($stripeID);

	$membership = new Memberships(); // blank Memberships object

	switch ($applicant->applyType) {
		case 'RG':
		case 'JT':
		case 'ST':
			// make a new membership
			$membership->class = $applicant->applyType;
			$membership->street = $applicant->street;
			$membership->city = $applicant->city;
			$membership->state = $applicant->state;
			$membership->zip = $applicant->zip;
			$membership->sponsors = $applicant->sponsor1 . ' & ' . $applicant->sponsor2;

			// expiration is always March 31 of next year
			$expYear = date('Y') + 1;
			$membership->expiration = $expYear . '-03-31';

			$membership->dbSave();
			break;

		case 'JTadd':
			$addToMember = new Members($applicant->addToMemberID);
			$membership->getByID($addToMember->membershipID);

			switch ($membership->class) {
				case 'SR':
				case 'JS':
					// current member is Senior.
					$age = $applicant->getAge();
					if ($age >= 70) {
						// keep the current member's Senior status
						$membership->class = 'JS';
					} else {
						// might still be OK if current member is old enough
						if ($addToMember->dob) {
							$memberAge = $applicant->getAge($addToMember->dob);
							$totalAge = $memberAge + $age;
							if ($totalAge >= 140) {
								// combined age qualifies
								$membership->class = 'JS';
							}
						}
					}
					break;
				default:
					// regular joint membership
					$membership->class = 'JT';
			}
			$membership->dbSave();
	}

	require_once __DIR__ . '/../../../support/functions.php'; // need wpAddUser() function
	require_once __DIR__ . '/../../../../wp-load.php';
	require_once ABSPATH . 'wp-admin/includes/user.php';

	// make new member
	$member = new Members(); // blank Members object
	$member->membershipID = $membership->membershipID;

	$member->first = $applicant->first;
	$member->m_i = $applicant->m_i;
	$member->last = $applicant->last;
	$member->email = $applicant->email;
	$member->phone = $applicant->phone;
	$member->mobile = $applicant->mobile;
	$member->dob = $applicant->dob;

	// create a new WordPress user
	$_POST['first_name'] = $member->first;
	$_POST['last_name'] = $member->last;
	$_POST['email'] = $member->email;
	$login = strtolower($member->first . '_' . $member->last);
	$_POST['user_login'] = preg_replace('/\W/', '', $login); // remove non-alphanumeric

	$wpID = wpAddUser();
	if ($wpID) {
		$member->wpID = $wpID; // success adding WP user
	} else {
		$member->wpID = 0;
	}
	$member->dbSave();
	sendWelcome($member);

	if ($applicant->applyType == 'JT') {
		// make new joint member
		$member2 = new Members(); // blank Members object
		$member2->membershipID = $membership->membershipID;
		$member2->first = $applicant->first2;
		$member2->m_i = $applicant->m_i2;
		$member2->last = $applicant->last2;
		$member2->email = $applicant->email2;
		$member2->phone = $applicant->phone2;
		$member2->mobile = $applicant->mobile2;
		$member2->dob = $applicant->dob2;

		$_POST['first_name'] = $member2->first;
		$_POST['last_name'] = $member2->last;
		$_POST['email'] = $member2->email;
		$login = strtolower($member2->first . '_' . $member2->last);
		$_POST['user_login'] = preg_replace('/\W/', '', $login); // remove non-alphanumeric

		$wpID = wpAddUser();
		if ($wpID) {
			$member2->wpID = $wpID; // success adding WP user
		} else {
			$member2->wpID = 0;
		}
		$member2->dbSave();
		sendWelcome($member2);
	}

	$applicant->status('complete'); // finished with this application
	notifyMembership($applicant);
}

function releaseReinstate($stripeID) {
	$stripe = new Stripes($stripeID);
	if (empty($stripe->stripeID)) exit();
	if ($stripe->status != 'ok') exit();

	$applicant = new Applicants();
	$applicant->getByStripeID($stripeID);

	$member = new Members($applicant->addToMemberID, 'membership');

	$member->first = $applicant->first;
	$member->m_i = $applicant->m_i;
	$member->last = $applicant->last;
	$member->email = $applicant->email;
	$member->phone = $applicant->phone;
	$member->mobile = $applicant->mobile;
	$member->dob = $applicant->dob;

	if (!$member->wpID) {
		// create a new WordPress user
		$_POST['first_name'] = $member->first;
		$_POST['last_name'] = $member->last;
		$_POST['email'] = $member->email;
		$login = strtolower($member->first . '_' . $member->last);
		$_POST['user_login'] = preg_replace('/\W/', '', $login); // remove non-alphanumeric

		require_once __DIR__ . '/../../../support/functions.php'; // need wpAddUser() function
		require_once __DIR__ . '/../../../../wp-load.php';
		require_once ABSPATH . 'wp-admin/includes/user.php';

		$wpID = wpAddUser();
		if ($wpID) {
			$member->wpID = $wpID; // success adding WP user
		} else {
			$member->wpID = 0;
		}
	}
	$member->dbSave();
	sendWelcome($member);

	$oldClass = new Classes($member->membership->class);
	$newClass = new Classes(str_replace('re', '', $applicant->applyType));

	// if oldClass is not joint then neither is newClass
	if ($oldClass->joint) {
		$jointMembers = $member->membership->getJointMembers();
		if (count($jointMembers) == 2) {
			foreach ($jointMembers as $jointMember) {
				if ($jointMember->memberID == $member->memberID) continue;
				$member2 = $jointMember;
			}
		} else {
			$member2 = false;
		}

		if ($newClass->joint) {
			$member2->first = $applicant->first2;
			$member2->m_i = $applicant->m_i2;
			$member2->last = $applicant->last2;
			$member2->email = $applicant->email2;
			$member2->phone = $applicant->phone2;
			$member2->mobile = $applicant->mobile2;
			$member2->dob = $applicant->dob2;

			if (!$member2->wpID) {
				// create a new WordPress user
				$_POST['first_name'] = $member2->first;
				$_POST['last_name'] = $member2->last;
				$_POST['email'] = $member2->email;
				$login = strtolower($member2->first . '_' . $member2->last);
				$_POST['user_login'] = preg_replace('/\W/', '', $login); // remove non-alphanumeric

				require_once __DIR__ . '/../../../support/functions.php'; // need wpAddUser() function
				require_once __DIR__ . '/../../../../wp-load.php';
				require_once ABSPATH . 'wp-admin/includes/user.php';

				$wpID = wpAddUser();
				if ($wpID) {
					$member2->wpID = $wpID; // success adding WP user
				} else {
					$member2->wpID = 0;
				}
			}
			$member2->dbSave();
			sendWelcome($member2);
		} else {
			// oldClass was joint but newClass is not.
			if ($member2) {
				// orphan the old joint member
				$newMembership = new Memberships();
				$newMembership->class = 'OR';
				$newMembership->expiration = date('Y-m-d'); // today
				$newMembership->street = $member->membership->street;
				$newMembership->city = $member->membership->city;
				$newMembership->state = $member->membership->state;
				$newMembership->zip = $member->membership->zip;
				$newMembership->sponsors = $member->membership->sponsors;
				$newMembership->dbSave();
				$member2->membershipID = $newMembership->membershipID;
				$member2->dbSave();
			}
		}
	}

	$member->membership->street = $applicant->street;
	$member->membership->city = $applicant->city;
	$member->membership->state = $applicant->state;
	$member->membership->zip = $applicant->zip;
	$member->membership->sponsors = $applicant->sponsor1 . ' & ' . $applicant->sponsor2;

	$expYear = date('Y') + 1; // expiration is always March 31 of next year
	$member->membership->expiration = $expYear . '-03-31';
	$member->membership->class = $newClass->class;
	$member->membership->resigned = 0;
	$member->membership->resignedDate = null;
	$member->membership->dbSave();

	$applicant->status('complete'); // finished with this application
	notifyMembership($applicant);
}

function notifyMembership($applicant) {
	$link = home_url() . '/cac/admin/members/applyDetail.php?id=' . $applicant->applicantID;

	$text = "An approved CAC membership application has been paid online.\n";
	$text .= $applicant->fullname;
	if ($applicant->applyType == 'JT') $text .= ' and ' . $applicant->fullname2;

	$text .= "\n\n";
	$text .= "Started: $applicant->created \n";
	$text .= "Submitted: $applicant->submitted \n";
	$text .= "Approved: $applicant->approved \n";
	$text .= "Completed: $applicant->complete \n\n";

	$text .= "Link to review the accepted application: \n";
	$text .= "$link \n";

	$mail = new PHPMailer();
	$mail->isSendmail(); // use system MTA (probably postfix)
	$mail->Sender = 'webmaster@californiaalpineclub.org';

	$mail->Subject = "CAC Application paid: " . $applicant->fullname;
	if ($applicant->applyType == 'JT') $mail->Subject .= ' and ' . $applicant->fullname2;

	$mail->setFrom('webmaster@californiaalpineclub.org', 'CAC webmaster');
	$mail->addAddress('membership@californiaalpineclub.org');

	$mail->CharSet = 'utf-8';
	$mail->XMailer = 'CAC Mail'; // default is phpMailer version

	$mail->Body = $text;

	$mailresult = $mail->send();
	return $mailresult;
}
