<?php

// privilege groups that are allowed to access pages in this category (Migration)

// uncomment the following to restrict access to the migration scripts
// after the initial run and after admin privileges have been set up for
// at least one member

$allowGroups = array('admin');
