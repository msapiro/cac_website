// javascript functions common to pages related to payments

function cancelButton(URL) {
	$("#btnCancel").click( function() {
		$.post("../payment/ajax/cancel.php", function() {
			window.open(URL, "_self");
		});
	});
}

function checkoutButton(URL) {
	$("#btnCheckout").click(function() {
		
		// prevent double submit or cancel
		$(this).prop("disabled",true).html("Please wait ...");
		$("#btnCancel").prop("disabled",true);
		
		// create a stripe session, then redirect to stripe checkout using the new sessionURL
		// we pass the fromPage explicitly since php $_SERVER['HTTP_REFERER'] is unreliable.
		var fromPage = window.location.href;
		$.post(URL, {stripeID: stripeID, fromPage: fromPage}, function(data) {
			if (data.success == 1) {
				window.open(data.sessionURL, "_self");
			} else {
				var err = 'Could not create a Stripe checkout session.<br>';
				err += data.msg + '<br>';
				err += 'Please ask membership@californiaalpineclub.org for help.';
				$('#error-msg').html(err).show();
			}
		}, 'json');
	});	
}
