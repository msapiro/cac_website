<?php
require '../support/config.php';
require CLASSLOADER;

require AUTH_PAGE; // enforce login

require '../../support/functions.php';


// Ensure we came from a POST.
requirePOST();

inty($_POST['memberID']);

// logged in user
$user = new Members('login');
if (empty($user->memberID)) {
	error('Error', 'Did not find the logged-in member.');
	exit;
}

// member record to edit
$member = new Members($_POST['memberID']);
if (empty($member->memberID)) {
	error('Error', 'Did not find the member to edit.');
	exit;
}

if ($user->memberID == $member->memberID) {
	// user self-updating
	$membershipEdit = true;   // accept input for personal and membership data
	$membership = new Memberships($member->membershipID);
} else {
	// user trying to edit another member. see if the other member is the joint member
	if($user->membershipID != $member->membershipID) {
		error('Not Allowed',
			'The member you are trying to edit is not a part of your Joint membership!');
		exit;
	}
	$membershipEdit = false;  // only accept input for the personal data
}

// CAC member data
$member->first = $_POST['first'];
$member->m_i = $_POST['m_i'];
$member->last = $_POST['last'];
$member->email = $_POST['email'];
$member->phone = $_POST['phone'];
$member->mobile = $_POST['mobile'];
$member->dob = $_POST['dob'];
$member->dbSave();

if ($membershipEdit) {
	// CAC membership data
	$membership->street= $_POST['street'];
	$membership->city= $_POST['city'];
	$membership->state= $_POST['state'];
	$membership->zip= $_POST['zip'];
	$membership->paperTrails = booly($_POST['paperTrails']); // checkbox
	$membership->dbSave();
}

header("Location: index.php");
exit;
