#!/usr/bin/python3

import pymysql

def main():
    db = pymysql.connect(user='CAC_query',
                         passwd='CAC_dbqry_1913',
                         db='alpine',
                         charset='utf8')
    c = db.cursor()
    c1 = db.cursor()
    c.execute('SELECT membershipID FROM memberships')
    while True:
        rec = c.fetchone()
        if not rec:
            break
        c1.execute("""SELECT memberID FROM members WHERE
                   membershipID = %s""", rec[0])
        membs = c1.fetchall()
        if len(membs) in (1, 2):
            continue
        if len(membs) == 0:
            print(f'Membership ID {rec[0]} has no members')
        else:
            print(f'Membership ID {rec[0]} has {len(membs)} members. '
                  f'Menber IDs: {membs}')
    c.close()
    c1.close()
    db.close()

if __name__ == '__main__':
    main()
