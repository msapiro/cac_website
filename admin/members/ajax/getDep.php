<?php
require '../../support/config.php';
require CLASSLOADER;

require '../includes/config.php'; // contains $allowGroups for this directory
//array_push($allowGroups, 'ROmembers');
require AUTH_AJAX;                // make sure user is logged in and a group member


$data = array();
$dep = new Dependents($_POST['depID']);

if ($dep->depID) {
	$data['depID'] = $dep->depID;
	$data['first'] = $dep->first;
	$data['last'] = $dep->last;
	$data['dob'] = $dep->dob;
	$data['success'] = 1;
} else {
	$data['success'] = 0;
}

echo json_encode($data);
