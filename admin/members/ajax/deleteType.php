<?php
require '../../support/config.php';
require CLASSLOADER;

require '../includes/config.php'; // contains $allowGroups for this directory
require AUTH_AJAX;                // make sure user is logged in and a group member


// called by ajax to delete a Membership Type

nully($_POST['class']);

$oType = new Classes($_POST['class']);

if ($oType->class) {
	$oType->dbDelete();
}
