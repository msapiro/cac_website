<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
//array_push($allowGroups, 'ROmembers');
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';

$volCat = new VolCats();

$volCat->catID = $_POST['catID'];
$volCat->catName = $_POST['catName'];

$volCat->dbSave();

header("Location: cats.php");
