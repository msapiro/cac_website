<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';
include 'includes/tools.php';

use PHPMailer\PHPMailer\PHPMailer;

/**
 * from config:
 * @var int $batchTime
 */

$msg = new Msgs($_POST['mailID']);
if (empty($msg->mailID)) {
	error('Error', 'Did not find a message with mailID=' . $_POST['mailID']);
	exit;
}

$db = $msg->db;

$msgGroup = new MsgGroups($_POST['groupID']);
if (empty($msgGroup->groupID)) {
	error('Error', 'Did not find a message group with groupID=' . $_POST['groupID']);
	exit;
}

$type = 'Plain Text';
if ($msg->html) $type = 'HTML';

$mail = new PHPMailer();
$messageErrors = false;
$msgErrorFlag = 0;

if (! empty($msg->envelopeSender)) {
	if (! $mail->validateAddress($msg->envelopeSender)) add_error('invalid <b>Envelope Sender</b> address: ' . $msg->envelopeSender);
}
if (! $mail->validateAddress($msg->fromAddress)) add_error('invalid <b>From</b> address: ' . $msg->fromAddress);
if (! empty($msg->replyTo)) {
	if (! $mail->validateAddress($msg->replyTo)) add_error('invalid <b>Reply To</b> address: ' . $msg->replyTo);
}
if (empty($msg->subject)) add_error('Subject field is empty');
if ($msg->html) {
	if (empty($msg->htmlBody)) add_error('message body is empty');
} else {
	if (empty($msg->textBody)) add_error('message body is empty');
}

if ($messageErrors) $msgErrorFlag = 1;

// make an array of all email addresses to which this msg already sent
$alreadySent = array();
$alreadySent = $db->select("mailSent", "email", ["mailID" => $msg->mailID]);
$numSent = count($alreadySent);

// make session arrays of email addresses and names still to be sent
$_SESSION['addresses'] = array();
$_SESSION['names'] = array();
$blank = 0;
$invalid = 0;
$duplicate = 0;
$numSentGroup = 0;

if (stripos($msgGroup->queryString, 'select') !== 0) {
	$numGroup = 0;
} else {
	$pdoStmt = $db->query($msgGroup->queryString);
	if ($db->error) {
		$numGroup = 0;
	} else {
		$numGroup = $pdoStmt->rowCount();
		while ($row = $pdoStmt->fetch()) {
			if ($row['email'] == '') {
				$blank++;
				continue;
			}
			if (! $mail->validateAddress($row['email'])) {
				$invalid++;
				continue;
			}
			if (in_array($row['email'], $alreadySent)) {
				$numSentGroup++;
				continue;
			}
			if (in_array($row['email'], $_SESSION['addresses'])) {
				$duplicate++;
				continue;
			}

			$_SESSION['addresses'][] = $row['email'];

			if (isset($row['fullname'])) {
				$name = $row['fullname'];
			} elseif ( isset($row['first']) && isset($row['last']) ) {
				$name = $row['first'] . ' ' . $row['last'];
			} else {
				$name = '';
			}
			$_SESSION['names'][] = $name;
		}
	}
}

$toSend = count($_SESSION['addresses']);

start_page('CAC Mailer');
add_script('moment.min');
?>
<script>
var mailID = "<?= $msg->mailID ?>";
var groupID = "<?= $msgGroup->groupID ?>";
var toSend = "<?= $toSend ?>";
var batchTime = "<?= $batchTime ?>"; // in config.php
var msgErrorFlag = <?= $msgErrorFlag ?>;
var stopFlag = false;

$(function() {
	// do stuff when DOM is ready

	$("#btnSend").click(function() {
		var agree = confirm("Are you sure you want to send this message to the Group?");
		if (agree) {
			$("#btnSend").addClass("disabled").prop("disabled", true);
			$("#btnStop").removeClass("disabled").prop("disabled", false);
			showProgress("Starting batch send. Do not close this page!");
			showProgress("Progress will be reported approximately every " + batchTime + " seconds.<br>");
			sendBatch(0);
		}
	});

	$("#btnStop").click(function() {
		stopFlag = true;
		$("#btnStop").addClass("disabled").prop("disabled", true);
		showProgress("STOP after the current batch!");
	});

	$("#btnClear").click(function() {
		var msg = "This will remove all addresses from the list, so some might receive another copy of the message.\n\n";
		msg += "This will not send the message.\n\n";
		msg +=  "Are you sure you want to clear the list?";
		if ( confirm(msg) ) {
			$.post('ajax/clearSent.php', {mailID: mailID}, function() {
				$("#reloadForm").submit(); // reload this page (with POST variables)
			});
		}
	});

	if (msgErrorFlag || toSend == 0) {$("#btnSend").addClass("disabled").prop("disabled", true)};

});


function sendBatch(nextIndex) {
	if (stopFlag) {
		showProgress("Stopped at your request.")
		return false;
	}
	showProgress("Starting next batch at index " + nextIndex + " ...");
	$.post('ajax/sendBatch.php', {mailID: mailID, nextIndex: nextIndex}, function(data) {
		showProgress(data.progress);
		$("#divError").append(data.error + "<br>");
		if (data.more == 1) {
			sendBatch(data.nextIndex);
		} else {
			showProgress("Finished!");
		}
	}, 'json');
}

function showProgress(text) {
	var timestamp = moment().format("YYYY-MM-DD hh:mm:ss");
	$("#divProgress").append(timestamp + " " + text + "<br>");
}
</script>
<?php
start_content();
toolbar();
?>
<h3 class="pageheader">Send Bulk Mail to a Group</h3>

<div class="row">
	<div class="col-xl-10 offset-xl-1">


<div class="row">
	<div class="col-lg">
		<h4>Message</h4>

		<a class="btn btn-secondary" href="detail.php?id=<?= $msg->mailID ?>"><?php add_icon('envelope') ?>
			mail ID: <?= $msg->mailID ?>
		</a>

		<table class="table table-sm table-bordered mt-3">
		<tr><td colspan="2">Subject: <?= $msg->subject ?></td></tr>
		<tr><td>Type</td><td><?= $type ?><?php if ($msg->useTemplate) echo ' using a template'?></td></tr>
		<tr><td>Total Already Sent (any group)</td><td><?= $numSent ?></td></tr>
<?php if ($messageErrors): ?>
		<tr><td colspan="2"><?= $messageErrors ?></td></tr>
<?php endif; ?>
		</table>

		<button id="btnClear" type="button" class="btn btn-secondary"><?php add_icon('eraser') ?> Clear Already Sent List</button>
	</div>

	<div class="col-lg">
		<h4>Group</h4>

		<a class="btn btn-secondary" href="detailGroup.php?id=<?= $msgGroup->groupID ?>"><?php add_icon('user-friends') ?>
			groupID: <?= $msgGroup->groupID ?>
		</a>

		<table class="table table-sm table-bordered mt-3">
		<tr><td colspan="2"><?= $msgGroup->groupName ?></td></tr>
		<tr><td>Addresses in Group</td><td class="text-center"><?= $numGroup ?></td></tr>
		<tr><td>Already Sent from Group</td><td class="text-center"><?= $numSentGroup ?></td></tr>
<?php if($blank): ?>
		<tr><td>Blank addresses</td><td class="text-center"><?= $blank ?></td></tr>
<?php
endif;
if ($invalid): ?>
		<tr><td>Invalid addresses</td><td class="text-center"><?= $invalid ?></td></tr>
<?php
endif;
if ($duplicate): ?>
		<tr><td>Duplicate addresses</td><td class="text-center"><?= $duplicate ?></td></tr>
<?php
endif; ?>
		<tr><td>Remaining to Send from Group</td><td class="text-center"><?= $toSend ?></td></tr>
		</table>

		<button id="btnSend" type="button" class="btn btn-primary"><?php add_icon('share-square') ?> Send to Group</button>
		<button id="btnStop" type="button" class="btn btn-secondary disabled" disabled><?php add_icon('hand-paper') ?> Stop</button>

	</div>
</div>

<div class="card mt-3">
	<div class="card-header">
		Progress Log
	</div>
	<div id="divProgress" class="card-body">
		<!-- ajax will fill -->
	</div>
</div>

<div class="card mt-3">
	<div class="card-header">
		Error Log
	</div>
	<div id="divError" class="card-body">
		<!-- ajax will fill -->
	</div>
</div>

	</div>
</div>

<!-- form to reload this page with post variables -->
<form id="reloadForm" action="sendGroup.php" method="POST">
	<input type="hidden" name="mailID" value="<?= $msg->mailID ?>">
	<input type="hidden" name="groupID" value="<?= $msgGroup->groupID ?>">
</form>

<?php
end_page();

function add_error($text) {
	global $messageErrors;
	if ($messageErrors) $messageErrors .= '<br>';
	$messageErrors .=  'ERROR: ' . $text;
}
?>
