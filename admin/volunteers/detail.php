<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
array_push($allowGroups, 'ROmembers');
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';
include 'includes/tools.php';


inty($_GET['id']);
$fromID = inty($_GET['fromID']);

$volOpp = new VolOpps($_GET['id']);
if (empty($volOpp->oppID)) {
	error('Error', 'Did not find an opportunity with oppID=' . $_GET['id']);
	exit;
}

$memberArray = $volOpp->getVolunteers();

$numMembers = count($memberArray);
if (! $numMembers) $numMembers = 'none';

$_SESSION['listPageMembers'] = $_SERVER["PHP_SELF"] . '?id=' . $_GET['id'];

start_page('CAC Volunteers');
add_script('datatables');
?>
<script>
"use strict";
var oppID = <?= $volOpp->oppID ?>;

$(function() {
	// do stuff when DOM is ready

	// make nice tooltips for the buttons
	$('.tip').tooltip({container: 'body', placement: 'bottom'});

	tableFeatures("itemTable", {
		"order": [[1,"asc"]],
		"stateSave": true,
		"drawCallback": function() {
			updateFoundArray();
		}
	});

	// clear any saved filter
	$('#itemTable').DataTable().search( '' ).columns().search( '' ).draw();

	// we don't load dataTables unless there are at least 3 items
	// so we force the update if there are just 2
	if($("#itemTable tbody tr").length == 2) {updateFoundArray();}

	$("button.remove").click(function() {
		var volID = $(this).closest("tr").data("vol_id");
		var agree = confirm("Are you sure you want to remove the Member from this Volunteer Opportunity?");
		if (agree) {
			$.post('ajax/deleteVol.php', {volID: volID}, function(){
				location.reload(); //reload this page
			});
		}
	});

});

function confirmDelete() {
    var agree = confirm("Deletion cannot be undone.\n\n" +
    	    "Deletion will also remove all member assignments to this opportunity.\n\n" +
    	    "Are you sure you want to delete this Volunteer Opportunity?");
    if (agree) {
    	$.post('ajax/delete.php', {oppID: oppID}, function() {
			location.href = "index.php";
		});
    }
}

function updateFoundArray() {
	// read the data attribute of the rows to update the foundArray
	var foundArray = new Array();
	$("#itemTable tbody tr").each(function() {
		foundArray.push (  $(this).attr("data-id") );
	});
	var URL = '../../support/updateFound.php';
	$.post(URL, { arrayname: 'foundArrayMembers', order: foundArray.toString() });
}
</script>
<?php
start_content();
toolbar($volOpp->oppID);
?>

<h3 class="pageheader"><?= $volOpp->oppName ?></h3>

<div class="row justify-content-center mb-4">
	<table class="table table-sm table-bordered col-sm-11 col-md-8 col-lg-6 col-xl-5">
<?php
makeRow('Members', $numMembers);
makeRow('Category', $volOpp->catName);
if (is_null($volOpp->description)) {
	$volOpp->description = '';
}
makeRow('Description', nl2br($volOpp->description));
?>
	</table>
</div>


<table id="itemTable" class="table table-striped table-sm">
<thead>
	<tr>
		<th class="text-center nosort">&nbsp;Action</th><th>Name</th><th>Email</th><th class="nosort">Phone</th><th class="nosort">Mobile</th>
		<th>City</th>
	</tr>
</thead>
<tbody>
<?php
foreach ($memberArray as $data):
	$member = new Members($data['memberID'], 'membership');

	$URLdetail = '../members/detail.php?id=' . $member->memberID . '&set=1';
	$emailDisplay = '';
	if (! empty($member->email)) $emailDisplay = '<a href="mailto:' . $member->email . '">' . $member->email . '</a>';

?>
	<tr data-id="<?= $member->memberID ?>" data-vol_id="<?= $data['volID'] ?>"<?php if ($member->memberID == $fromID) echo ' class="foundrow"'?>>
		<td class="text-center nosort">
			<div class="btn-group btn-group-sm">
				<a href="<?= $URLdetail ?>" class="btn btn-secondary tip" title="Member Details"><?php add_icon('list-alt') ?></a>
				<button type="button" class="btn btn-secondary tip remove" title="Remove from Opportunity"><?php add_icon('minus-circle') ?></button>
			</div>
		</td>
		<td><?= $member->fullname ?></td>
		<td><?= $emailDisplay ?></td>
		<td><?= phone_number($member->phone) ?></td>
		<td><?= phone_number($member->mobile) ?></td>
		<td><?= $member->membership->city ?></td>
	</tr>
<?php endforeach; ?>

</tbody>
</table>

<?php end_page(); ?>
