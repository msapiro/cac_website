<?php
require '../../support/config.php';
require CLASSLOADER;

require '../includes/config.php'; // contains $allowGroups for this directory
//array_push($allowGroups, 'ROmembers');
require AUTH_AJAX;                // make sure user is logged in and a group member


$msg = new Msgs($_POST['mailID']);
if (empty($msg->mailID)) {
	error('Error', 'Did not find a message with mailID=' . $_POST['mailID']);
	exit;
}

$msg->mailID = NULL;  // force to save as new
$msg->isTemplate = 0; // don't make this a template, even if copying a template
$msg->subject = '[COPY] ' . $msg->subject;

$msg->dbSave();
