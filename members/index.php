<?php
// This file just redirects the entering user to the home page
// which we keep in a directory called home in order to simplify
// the navigation menu.  Do not copy this to any other directory.

header("Location: home/index.php");
