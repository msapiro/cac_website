<?php
require '../support/config.php';
require CLASSLOADER;

//require 'includes/config.php'; // contains $allowGroups for this directory
//array_push($allowGroups, 'trainee');
$allowGroups = array('admin', 'trainee');
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';

// Ensure we came from a POST.
requirePOST();

$memberID = inty($_POST['memberID']);

if (empty($_POST['lodge'])) {
	error('Error', 'Please select a lodge.');
	exit;
}
if (! preg_match('/^\d{4}$/', $_POST['year'])) {
	error('Error', 'Please enter a 4-digit year.');
	exit;
}
if ($memberID) {
	$train = new Trainees();
	$train->memberID = $memberID;
	$train->lodge = $_POST['lodge'];
	$train->year = $_POST['year'];
	$train->dbSave();
}

header("Location: traineeEdit.php?id=$memberID");
