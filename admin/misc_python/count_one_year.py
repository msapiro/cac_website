#!/usr/bin/python3

import pymysql

from datetime import date

now = date.today()
START = 2014
if now.month > 6:
    END = now.year + 1
else:
    END = now.year

def main():
    db = pymysql.connect(user='CAC_query',
                         passwd='CAC_dbqry_1913',
                         db='alpine',
                         charset='utf8')
    c = db.cursor()
    print('Year\tTotal One year members')
    for year in range(START, END):
        c.execute("""SELECT count(created) FROM memberships
                  WHERE class NOT IN ('', '0', 'OR', 'NM')
                  AND created BETWEEN %s AND %s""",
                  (f'{year-1}-03-31', f'{year}-03-31'))
        rec = c.fetchone()
        print(f'{year}\t{rec[0]}')
    c.close()
    db.close()

if __name__ == '__main__':
    main()
