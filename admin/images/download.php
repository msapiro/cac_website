<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

// this page forces the user browser to save the file rather than display it

$image = new Images($_GET['id']);
if (empty($image->imageID)) {
	error('ERROR', 'Did not find an image with imageID=' . $_GET['id']);
	exit;
}

// a lot of this is to discourage caching in IE
header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename="' . $image->imageID . '.' . $image->ext . '"');
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');
header('Content-Length: ' . filesize($image->filePath));
ob_clean();
flush();
readfile($image->filePath);
exit;
