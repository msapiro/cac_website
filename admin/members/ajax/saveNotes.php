<?php
require '../../support/config.php';
require CLASSLOADER;

require '../includes/config.php'; // contains $allowGroups for this directory
//array_push($allowGroups, 'ROmembers');
require AUTH_AJAX;                // make sure user is logged in and a group member

// save volunteer notes


$data = array();
inty($_POST['memberID']);

$member = new Members($_POST['memberID']);
if (empty ($member->memberID)) {
	$data['msg'] = "Did not find a member with memberID=" . $_POST['memberID'];
	echo json_encode($data);
	exit;
}

$volNote = new VolNotes();
$volNote->memberID = $member->memberID;

if (isset($_POST['delete'])) {
	$volNote->dbDelete();
	$data['msg'] = "Successfully deleted notes for $member->first.";
} else {
	$volNote->notes =  $_POST['notes'];
	$volNote->dbSave();
	$data['msg'] = "Successfully saved notes for $member->first.";
}

echo json_encode($data);
