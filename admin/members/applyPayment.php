<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
//array_push($allowGroups, 'ROmembers');
require AUTH_PAGE; // make sure user is logged in and a group member

$title = 'CAC Application';
$pageDescription= 'California Alpine Club Application';

require '../../support/functions.php';
require '../../support/formFunctions.php';
include 'includes/tools.php';

inty($_GET['id']);

$applicant = new Applicants($_GET['id']);
if (! $applicant->applicantID) {
	error('Error', 'Did not find an application with applicantID ' . $_GET['id'] . '.');
	exit;
}

switch ($applicant->applyType) {
	case 'RG':
		$appType = 'Individual Membership';
		break;
	case 'JT':
		$appType = 'Joint Membership (two new members)';
		break;
	case 'JTadd':
		$oAddTo = new Members($applicant->addToMemberID);
		$appType = 'Joint Membership<br>add to member <a href="detail.php?id=' . $oAddTo->memberID
			. '">' . $oAddTo->fullname . '</a>';
		break;
	case 'ST':
		$appType = 'Student Membership';
		break;
	case 'RGre':
		$appType = 'Reinstate Individual Membership';
		break;
	case 'JTre':
		$appType = 'Reinstate Joint Membership';
		break;
	case 'SRre':
		$appType = 'Reinstate Senior Membership';
		break;
	case 'JSre':
		$appType = 'Reinstate Joint Senior Membership';
}

$feeTotal = number_format($applicant->entryFee + $applicant->dues, 2);


start_page($title, $pageDescription);
?>
<script>

$(function() {
    // do stuff when DOM is ready


	$('#pmtType').change(function() {
		var pmtType = $(this).val();
		if (pmtType == 'cash' || pmtType == 'check') {
			updateAmount();
		} else {
			$("#pmtAmount").val('');
		}
    });

});

</script>
<style>
	td.first {width: 200px}
	td.fee {width: 75px}
</style>
<?php
start_content();
toolbar();
?>

<h2 class="pageheader">Application Payment</h2>

<div class="row">
	<div class="col-xl-10 offset-xl-1">

		<div class="alert alert-warning">
			Most applicants pay online and their membership is activated automatically.<br>
			This page is only needed for those applicants who mail in a paper check.
		</div>


		<table class="table table-sm table-striped">
		<thead>
			<tr class="table-dark"><th colspan="2">Applicant</th></tr>
		</thead>
		<tbody>
			<tr><td class="first">Name</td><td><?= $applicant->fullname ?></td></tr>
			<tr>
				<td>Email</td>
				<td>
					<a href="mailto:<?= $applicant->email ?>?subject=California Alpine Club application">
						<?= $applicant->email ?>
					</a>
				</td>
			</tr>
			<tr><td>Main Phone</td><td><?= phone_number($applicant->phone) ?></td></tr>
			<tr><td>Mobile Phone</td><td><?= phone_number($applicant->mobile) ?></td></tr>
		</tbody>
		</table>

		<table class="table table-sm table-striped">
		<thead>
			<tr class="table-dark"><th colspan="3">Application and Fees</th></tr>
		</thead>
		<tbody>
			<tr><td class="first">Type</td><td colspan="2"><?= $appType ?></td></tr>
			<tr>
				<td class="first">Entry Fee</td>
				<td class="fee text-end">$<?= $applicant->entryFee ?></td>
				<td></td>
			</tr>
			<tr>
				<td>Annual Dues</td>
				<td class="fee text-end">$<?= $applicant->dues ?></td>
				<td></td>
			</tr>
			<tr>
				<td><b>Total</b></td>
				<td class="fee text-end"><b>$<?= $feeTotal ?></b></td>
				<td></td>
			</tr>
		</tbody>
		</table>


		<form id="formPayment" action="applyPay2.php" method="post"
			onkeydown="return event.key != 'Enter';">
			<input type="hidden" name="applicantID" value="<?= $applicant->applicantID ?>">

			<div class="card border-success">
		    	<h4 class="card-header text-white bg-success ">Transaction Detail</h4>
				<div class="card-body">

					<div class="row align-items-center mb-3">
						<label class="col-lg-3 text-lg-end">
							<b>Payment type</b>
						</label>
						<div class="col-lg-5">
							<select name="pmtType" id="pmtType" class="form-select">
								<option value="0">Select</option>
								<option value="check">Check</option>
								<option value="cash">Cash</option>
								<option value="none">No Transaction</option>
							</select>
						</div>
					</div>

					<div id="amountRow">
<?php edit_field_prepend('Amount', 'pmtAmount', '$', $feeTotal, 10, false, 'pmtAmount'); ?>
					</div> <!-- end amountRow -->

<?php edit_field('Transaction Note', 'transNote', false, 100, 'Check number, etc'); ?>
				</div> <!-- end card body -->
			</div>  <!-- end card -->

				<div class="row mt-3 mb-3">
					<div class="col-lg-5 offset-lg-3">
						<div class="btn-group">
							<button type="submit" class="btn btn-primary">
								Submit
							</button>
							<a class="btn btn-secondary"
								href="applyDetail.php?id=<?= $applicant->applicantID ?>">
								Cancel
							</a>
						</div>
					</div>
				</div>
		</form>

	</div>
</div> <!-- end row -->

<?php end_page(); ?>
