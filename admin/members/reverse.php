<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// array_push($allowGroups, 'ROmembers', 'trainee');
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';


inty($_GET['id']);
$proceed = booly($_GET['proceed']);

$membership = new Memberships($_GET['id']);
if (empty($membership)) {
	error('Error', 'Did not find a membership with membershipID=' . $_GET['id']);
 	exit;
}

$title = 'Reverse an erroneous membership';
$pageDescription = 'Reverse an erroneous membership';

start_page($title, $pageDescription);
start_content();
?>

<h2 class="pageheader">Membership Reversal</h2>

<?php if (! $proceed): ?>

<div class="alert alert-danger mt-4">
	<p><b>WARNING</b>: This will delete the membership with ID <?= $_GET['id'] ?>,
	but only if this is within 2 hours of creating it. Only continue if creating
	this membership was an error.</p>
</div>

<a class="btn btn-secondary" href="<?= $_SERVER['PHP_SELF'] . '?proceed=1&id=' . $_GET['id'] ?>">
	Continue
</a>

<?php
	end_page();
	exit;
endif;
?>

<div class="row">
	<div class="col-xl-10 offset-xl-1">
		<p><?= $membership->delete_all($_GET['id']) ?></p>
	</div>
</div>

<?php end_page(); ?>
