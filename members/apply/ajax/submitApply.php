<?php
// login not required
require '../../support/config.php';
require CLASSLOADER;

require_once '../../../../wp-load.php';
use PHPMailer\PHPMailer\PHPMailer;

// user submits application for approval
$data = array();

if (empty($_POST['key'])) {
	error('Error', 'You must submit a code to identify the application.');
	exit();
}

$applicant = new Applicants();
$applicant->getByKey($_POST['key']);
if (!$applicant->applicantID) {
	$data['success'] = 0;
	$data['msg'] = 'No application found for that code.';
	echo json_encode($data);
	exit();
}

$applicant->status('submitted');
notifyMembership();
$data['success'] = 1;

echo json_encode($data);
exit();

function notifyMembership() {
	global $applicant;

	$link = home_url() . '/cac/admin/members/applyDetail.php?id=' . $applicant->applicantID;

	$text = "A new CAC membership application has been received from\n";
	$text .= $applicant->fullname;
	if ($applicant->applyType == 'JT') $text .= ' and ' . $applicant->fullname2;

	$text .= "\n\n";
	$text .= "Started: $applicant->created \n";
	$text .= "Submitted: $applicant->submitted \n\n";

	$text .= "Link to review and approve the application: \n";
	$text .= "$link \n";

	$mail = new PHPMailer();
	$mail->isSendmail(); // use system MTA (probably postfix)
	$mail->Sender = 'webmaster@californiaalpineclub.org';

	$mail->Subject = "Rec'd CAC Application from " . $applicant->fullname;
	if ($applicant->applyType == 'JT') $mail->Subject .= ' and ' . $applicant->fullname2;

	$mail->setFrom('webmaster@californiaalpineclub.org', 'CAC webmaster');
	$mail->addAddress('membership@californiaalpineclub.org');

	$mail->CharSet = 'utf-8';
	$mail->XMailer = 'CAC Mail'; // default is phpMailer version

	$mail->Body = $text;

	$mailresult = $mail->send();
	return $mailresult;
}
