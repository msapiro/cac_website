<?php
/** Prevents an invalid user from calling an ajax page directly.
 *
 * REQUIRE this near the top (but after the config.php file, which contains $allowGroups)
 * of simple pages like ajax calls, where it would not work to redirect invalid user to a login page.
 *
 * @var array $allowGroups
 */

if (session_status() !== PHP_SESSION_ACTIVE) {
	session_name('CACSESSID');
	session_start();
}

if ( empty($_SESSION['cac_login']['memberID']) ) {
	// User is NOT logged in. Do not proceed.
	http_response_code(403);
	echo 'Invalid user. Not logged in.';
	exit;
} else {
	// User is already logged in. Update the access time so our session file will be modified.
	// This helps prevent premature garbage collection from expiring our session data.
	$_SESSION['cac_login']['last_access'] = time();

	// see if the page that included this script has group restrictions
	if (! empty($allowGroups) ) {
		// if it is not an array, make it so
		if (! is_array($allowGroups)) $allowGroups = array($allowGroups);

		$allowed = false;
		if (! empty($_SESSION['cac_login']['privGroups'])) {
			// if there are any elements in common, then we allow access
			$allowed = count(array_intersect($_SESSION['cac_login']['privGroups'], $allowGroups));
		}

		if (! $allowed) {
			// User is not in a group that has privileges to the page.
			http_response_code(403);
			echo 'Invalid user. Not in authorized group.';
			exit;
		}
	}
}
