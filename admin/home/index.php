<?php
require '../support/config.php';
require CLASSLOADER;

require AUTH_PAGE; // make sure user is logged in

// if you want this page to have a custom title, set it here.
// leave it blank to use the default title for the site.
// this is the text that appears on the page tab in most browsers.
$title = '';
$description = 'California Alpine Club Administration';

require '../../support/functions.php';

start_page($title, $description);
start_content();
// +++++ Edit the page content below +++++ ?>

<div class="row">
	<div class="col-lg-10 offset-lg-1">

<h2 class="mt-4">Admin home page</h2>

<p>Welcome, you are logged in as <b><?= $_SESSION['cac_login']['name'] ?></b>.</p>

<?php
if ($_SESSION['cac_login']['privGroups']): ?>
<p>You are a member of the following privilege groups:
<b><?php echo implode(', ', $_SESSION['cac_login']['privGroups']); ?></b></p>
<?php endif; ?>

<h4>Useful Links</h4>

<p><a href="<?= home_url() ?>" target="_blank">Public CAC website</a> (in a new tab)</p>
<p><a href="<?= admin_url() ?>" target="_blank">WordPress Admin website</a> (in a new tab)</p>
<p><a href="<?= home_url() . '/cac/members'?>">CAC Members pages</a></p>

<h4>Useful Dates</h4>
<div class="row">
	<div class=" col-md-10 col-lg-7">
		<table class="table table-sm">
			<thead>
				<tr><th>Event</th><th>Date</th></tr>
			</thead>
			<tbody>
				<tr><td>Memberships expire</td><td>March 31</td></tr>
				<tr><td>Renewals delinquent after</td><td>June 30</td></tr>
				<tr><td>Login &amp; self-renewal allowed until</td><td>Dec 31</td></tr>
			</tbody>
		</table>
	</div>
</div>

	</div>
</div>

<?php end_page(); ?>
