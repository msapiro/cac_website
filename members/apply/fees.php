<?php
// Login not required for this page
require '../support/config.php';
require CLASSLOADER;

// if you want this page to have a custom title, set it here.
// leave it blank to use the default title for the site.
// this is the text that appears on the page tab in most browsers.
$title = '';
$description = 'California Alpine Club Membership Application';

require_once '../../support/functions.php';
require_once '../../support/formFunctions.php';
require 'includes/tools.php';

session_name('CACSESSID');
session_start();

$cancelID = inty($_GET['cancelID']); // callback from Stripe

if ($cancelID) {
	// callback - payment was canceled by user on the stripe page.
	$stripe = new Stripes($cancelID);
	if (empty($stripe->stripeID)) {
		error('Error', 'Did not find a Stripe transaction with stripeID=' . $cancelID);
		exit();
	}

	$applicant = new Applicants();
	$applicant->getByStripeID($stripe->stripeID);

	if (!$applicant->applicantID) {
		error('Error', 'Did not find an application with stripeID=' . $stripe->stripeID);
		exit();
	}

	$email = $_GET['email']; // add this second check to prevent leaking info
	if ($applicant->email != $email) {
		error('Error', 'Email check failed for stripeID=' . $cancelID . ' with email=' . $email);
		exit();
	}
} else {
	if (empty($_GET['key'])) {
		error('Error', 'You must submit a code to identify the application.');
		exit();
	}

	$applicant = new Applicants();
	$applicant->getByKey($_GET['key']);
	if (!$applicant->applicantID) {
		error('Error', 'Did not find an application for code ' . $_GET['key']);
		exit();
	}
}

if (!$applicant->approved) {
	error('Error', 'Application has not been approved yet. Cannot create an invoice.');
	exit();
}

if ($applicant->complete) {
	error('Error', 'Application has already been completed.');
	exit();
}

switch ($applicant->applyType) {
	case 'RG':
		$appType = 'Individual Membership';
		$stripeItem = 'Regular Application';
		break;
	case 'JT':
		$appType = 'Joint Membership (two new members)';
		$stripeItem = 'Joint Application';
		break;
	case 'JTadd':
		$oAddTo = new Members($applicant->addToMemberID);
		$appType = 'Joint Membership<br>add to current member ' . $oAddTo->fullname;
		$stripeItem = 'Joint Add-on Application';
		break;
	case 'ST':
		$appType = 'Student Membership';
		$stripeItem = 'Student Application';
		break;
	case 'RGre':
		$appType = 'Reinstate Individual Membership';
		$stripeItem = 'Reinstate RG Application';
		break;
	case 'JTre':
		$appType = 'Reinstate Joint Membership';
		$stripeItem = 'Reinstate JT Application';
		break;
	case 'SRre':
		$appType = 'Reinstate Senior Membership';
		$stripeItem = 'Reinstate SRApplication';
		break;
	case 'JSre':
		$appType = 'Reinstate Joint Senior Membership';
		$stripeItem = 'Reinstate JS Application';
}

$feeTotal = number_format($applicant->entryFee + $applicant->dues, 2);

if ($applicant->stripeID) {
	$stripe = new Stripes($applicant->stripeID);
	// make sure our stripe object is up to date with any edits
	$stripe->amount = $feeTotal;
	$stripe->item = $stripeItem;
	$stripe->dbSave();
} else {
	// create a 'pending' stripe object
	$stripe = new Stripes();
	$stripe->amount = $feeTotal;
	$stripe->item = $stripeItem;
	$stripe->dbSave();
	$applicant->stripeID = $stripe->stripeID;
	$applicant->dbSave();
}

$_SESSION['cancelStripe'] = $stripe->stripeID; // used if the user clicks the cancel button

$key = $applicant->applicantID . ':' . $applicant->code;
$cancelLink = 'applyDetail.php?key=' . $key;

start_page($title, $description);
?>
<script>
"use strict";
var stripeID = "<?= $applicant->stripeID ?>";

$(function() {
    // do stuff when DOM is ready

	$("#btnCancel").click( function() {
		$.post("../payment/ajax/cancelApp.php", function() {
			window.open('<?= $cancelLink ?>', "_self");
		});
	});

	 $("#btnCheckout").click(function() {
		// prevent double submit or cancel
		$(this).prop("disabled",true).html("Please wait ...");
		$("#btnCancel").prop("disabled",true);
	    	// create a stripe session, then redirect to stripe checkout using the new sessionURL
	    	// we pass the fromPage explicitly since php $_SERVER['HTTP_REFERER'] is unreliable.
	    	var fromPage = window.location.href;
	    	$.post('ajax/applySession.php', {stripeID: stripeID, fromPage: fromPage}, function(data) {
	    		if (data.success == 1) {
	    			window.open(data.sessionURL, "_self");
	    		} else {
	    			var err = 'Could not create a Stripe checkout session.<br>';
	    			err += data.msg + '<br>';
	    			err += 'Please ask membership@californiaalpineclub.org for help.';
	   				$('#error-msg').html(err).show();
	   			}
	    	}, 'json');
	    });

});
</script>
<style>
td.first {
	width: 150px
}

td.fee {
	width: 75px
}
</style>
<?php
start_content_apply(); // in tools.php
?>

<div class="row">
	<div class="col-lg-10 col-xl-8 offset-lg-1 offset-xl-2">

		<h2 class="mt-4">Membership Application</h2>

		<table class="table table-sm table-striped">
			<thead>
				<tr class="table-dark">
					<th colspan="2">Applicant</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="first">Name</td>
					<td><?= $applicant->fullname ?></td>
				</tr>
				<tr>
					<td>Email</td>
					<td><?= $applicant->email ?></td>
				</tr>
			</tbody>
		</table>

<?php if ($applicant->applyType == 'JT'): ?>
		<table id="jtApplicant" class="table table-sm table-striped">
			<thead>
				<tr class="table-dark">
					<th colspan="2">Joint Applicant</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="first">Name</td>
					<td><?= $applicant->fullname2 ?></td>
				</tr>
				<tr>
					<td>Email</td>
					<td><?= $applicant->email2 ?></td>
				</tr>
			</tbody>
		</table>
<?php endif; ?>

		<table class="table table-sm table-striped">
			<thead>
				<tr class="table-dark">
					<th colspan="3">Application Fees</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="first">Type</td>
					<td colspan="2"><?= $appType ?></td>
				</tr>
				<tr>
					<td>Entry Fee</td>
					<td class="fee text-end">$<?= $applicant->entryFee ?></td>
					<td></td>
				</tr>
				<tr>
					<td>Annual Dues</td>
					<td class="fee text-end">$<?= $applicant->dues ?></td>
					<td></td>
				</tr>
				<tr>
					<td><b>Total</b></td>
					<td class="fee text-end"><b>$<?= $feeTotal ?></b></td>
					<td></td>
				</tr>
			</tbody>
		</table>

		<p>
			If you have questions about the fees, please contact <span class="emlink"
				data-user="membership">the membership chair</span>.
		</p>

		<div class="col-lg-10 mt-3">
			<div class="btn-group">
				<button id="btnCheckout" type="button" class="btn btn-primary"><?php add_icon('cc-stripe', 'fab') ?>
					&nbsp;Pay online
				</button>
				<button id="btnCancel" type="button" class="btn btn-secondary">Cancel</button>
			</div>
		</div>

		<div class="card well mt-3">
			<div class="card-body">
				<p>
					If you don't want to pay online you can mail a check made out to <b>California Alpine Club</b>
					to:
				</p>

				<address>
					Candy Barnhill, Membership Chair<br>P.O. Box 753<br>Ashland, OR 97520-0026
				</address>

				<ul>
					<li>If you pay online your membership will be activated immediately.</li>
					<li>If you pay by check your membership will be activated when the check is received.</li>
					<li>In either case you will receive an email with further information and a request to
						edit your Volunteer Skills and Interests on our website.</li>
				</ul>
			</div>
		</div>

<?php if ($cancelID): ?>
		<div class="alert alert-warning mt-3">
			<?php add_icon('exclamation-triangle')?> You did not complete the payment page.<br> You can
			try again using the <b>Pay online</b> button above.<br> If you do not want to pay online,
			please click the <b>Cancel</b> button above.
		</div>
<?php endif; ?>

	</div>
</div>

<?php end_page(); ?>
