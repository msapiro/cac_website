<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
//array_push($allowGroups, 'ROmembers');
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';

requirePost();

inty($_POST['applicantID']);

$applicant = new Applicants($_POST['applicantID']);
if (! $applicant->applicantID) {
	error('Error', 'Did not find an application with applicantID ' . $_POST['applicantID'] . '.');
	exit;
}

$applicant->applyType = $_POST['rbApplyType'];

// omit email from stdFields. it is set when application is created.
// omit addToMemberID from stdFields. it is set by an ajax call.
// omit entryFee and dues from stdFields. they are set in applyFees.php
$stdFields = ['first', 'm_i', 'last', 'phone', 'mobile', 'dob', 'sponsor1', 'sponsor2', 'event1', 'event2', 'notes'];
$addressFields = ['street', 'city', 'state', 'zip'];
$jointFields = ['first2', 'm_i2', 'last2', 'email2', 'phone2', 'mobile2', 'dob2'];

if (strpos($applicant->applyType, 're')) {
	$_POST['event1'] = NULL;
	$_POST['event2'] = NULL;
}

foreach ($stdFields as $field) $applicant->$field = $_POST[$field];

if ($applicant->applyType == 'JTadd') {
	// we already have a membership from the existing member
	foreach ($addressFields as $field) $applicant->$field = NULL;
} else {
	foreach ($addressFields as $field) $applicant->$field = $_POST[$field];
}

if (in_array($applicant->applyType, ['JT', 'JTre', 'JSre'])) {
	foreach ($jointFields as $field) $applicant->$field = $_POST[$field];
} else {
	foreach ($jointFields as $field) $applicant->$field = NULL;
}

$applicant->dbSave();

header("Location: applyDetail.php?id=$applicant->applicantID");
exit;
