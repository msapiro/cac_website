<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';

// WP admin functions for user management
require_once ABSPATH . 'wp-admin/includes/user.php';

include 'includes/tools.php';

// Ensure we came from a POST.
requirePOST();

$title = 'CAC Membership';
$pageDescription = 'New/Edit member';

// member record to edit (0 means new member)
$memberID = inty($_POST['memberID']); // CAC memberID, not WP user_id

$addJointMember = booly($_POST['addJointMember']);

if (strtolower($_POST['wpAssign']) === 'clear') {
	$wpDetach = true;
} else {
	$wpDetach = false;
}

$wpAssign = inty($_POST['wpAssign']);

if ($memberID) {
	$member = new Members($memberID);
	if (empty($member->memberID)) {
		error('Error', 'Did not find a member with memberID=' . $memberID);
		exit;
	}
	$membership = new Memberships($member->membershipID);
	if ($addJointMember) {
		// adding a joint member
		if (! $membership->joint) {
			error('Not Allowed', 'You are trying to add a Joint member to an Individual membership!');
			exit;
		}
		$member = new Members(); // replace  with a blank Members object
		$member->membershipID = $membership->membershipID;
		$member->wpID = 0;
	} else {
		// editing an existing member
	}
} else {
	// adding a new member
	$member = new Members(); // blank Members object
	$member->wpID = 0;
	$membership = new Memberships(); // blank Memberships object
}

// CAC member data
$member->first = $_POST['first'];
$member->m_i = $_POST['m_i'];
$member->last = $_POST['last'];
$member->email = $_POST['email'];
$member->phone = $_POST['phone'];
$member->mobile = $_POST['mobile'];
$member->dob = $_POST['dob'];

if (! $addJointMember) {
	// CAC membership data
	$membership->class = $_POST['membershipType'];
	$membership->street= $_POST['street'];
	$membership->city= $_POST['city'];
	$membership->state= strtoupper($_POST['state']);
	$membership->zip= $_POST['zip'];
	$membership->sponsors = $_POST['sponsors'];
	$membership->paperTrails = booly($_POST['paperTrails']); // checkbox
	if (! $membership->resigned and booly($_POST['resigned'])) {
		$membership->resignedDate = date('Y-m-d');
	} else if (! booly($_POST['resigned'])) {
		$membership->resignedDate = null;
	}
	$membership->resigned = booly($_POST['resigned']); // checkbox
}

if ($memberID) {
	if ($addJointMember) {
		$pageTitle = 'Adding a Joint Member - results';

		// create the new joint member
		$member->dbSave();
		$displayResult = 'created a new family member with memberID = ' . $member->memberID . "\n";

	} else {
		$pageTitle = 'Editing a Member - results';

		$membership->dbSave();
		$displayResult = "updated $membership->class membership with membershipID = $membership->membershipID \n";

		$member->dbSave();
		$displayResult .= "updated member with memberID = $member->memberID \n";
	}

	if ($wpAssign || $wpDetach) {
		// assign member to an existing WP account, or detach from an account
		wpAssign($member, $wpAssign, $wpDetach);
	} elseif ($member->wpID) {
		// already have a WP account, edit it
		wpEditUser($member->wpID);
	} else {
		// create a new WP user
		// We didn't ask for WP name. Use the same as CAC account.
		$_POST['first_name'] = $_POST['first'];
		$_POST['last_name'] = $_POST['last'];
		$wpID = wpAddUser();
		if ($wpID) {
			// success adding WP user
			$member->wpID = $wpID;
			$member->dbSave();
		}
	}
} else {
	$pageTitle = 'Creating a New Member - results';

	// expiration is always March 31 of next year.
	// joining after July 1 pay half dues
	$expYear = date('Y') + 1;
	$membership->expiration = $expYear . '-03-31';

	$membershipID = $membership->dbSave();

	if ($membershipID) $displayResult = "created a new $membership->class membership with membershipID = $membershipID \n";

	// create the new member
	$member->membershipID = $membershipID;
	$memberID = $member->dbSave();
	if ($memberID) $displayResult .= 'created a new member with memberID = ' . $memberID . "\n";

	if ($wpAssign) {
		// assign the new member to an existing WP account
		wpAssign($member, $wpAssign);
	} else {
		// create a new WP user
		// We didn't ask for WP name. Use the same as CAC account.
		$_POST['first_name'] = $_POST['first'];
		$_POST['last_name'] = $_POST['last'];
		$wpID = wpAddUser();
		if ($wpID) {
			// success adding WP user
			$member->wpID = $wpID;
			$member->dbSave();
		}
	}
}

start_page($title, $pageDescription);
start_content();
toolbar();
?>

<h2 class="pageheader"><?= $pageTitle ?></h2>

<div class="row">
	<div class="col-lg-10 offset-lg-1">

	<pre class="mt-3">
<?php echo $displayResult; ?>
	</pre>

<?php if ($member->memberID): ?>
	<a class="btn btn-primary" href="detail.php?id=<?= $member->memberID ?>">Member detail page</a>
<?php endif; ?>

	</div>
</div>

<?php
end_page();
?>
