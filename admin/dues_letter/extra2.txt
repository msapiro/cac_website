If you renew now you will save the Registrar the time and effort of having to mail a reminder and most importantly you save the environment by using less paper.
You may renew online on our web site at www.californiaalpineclub.org at no extra cost. Online renewal is preferred if at all possible.
