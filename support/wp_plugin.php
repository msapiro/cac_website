<?php
/*
 *
 * This file is "included" into wp-content/plugins/cac.php
 *
 */


// add the CAC Admin menu item to the admin page sidebar.
// position 80 is "Settings" so use 85 to go after that.
// only display for users with the 'edit_users' capability, ie admins.
function linked_url() {
	global $menu;
	add_menu_page( 'page_title_unused', 'CAC Admin', 'edit_users', 'slug_unused', '', 'dashicons-media-spreadsheet', 85);

	// assign a link to the new menu item
	$link =  get_home_url() . '/cac/admin';
	$menu[85][2] = $link;
}
add_action( 'admin_menu', 'linked_url' );

// We may need to set some phpMailer config items later. This is how to do it.
// commented out for now.
function set_phpmailer_details( $phpmailer ) {
	$phpmailer->isSendmail();  // use system MTA
}
//add_action( 'phpmailer_init', 'set_phpmailer_details' );
