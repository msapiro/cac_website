<?php

// privilege groups that are allowed to access pages in this category (Volunteers)
$allowGroups = array('admin', 'members', 'volunteer');
