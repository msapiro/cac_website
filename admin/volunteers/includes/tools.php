<?php

//tools specific to this category

function getHelp($subject) {
	switch ($subject) {
		case 'htmlBody':
			$helpText = "<ul>
<li>This is a basic HTML editor. For complicated HTML, like that needed for responsive design, it is probably
easiest to create and test HTML content elsewhere and paste it here, using the <b>Source</b> button.</li>
<li>If this message is not using a template, you can put CSS in a style element inside the head element.</li>
<li>If this message is using a template, the head element and the html and body tags will be removed before
inserting into the template, but they will still appear in this editor.</li>
</ul>";
			break;


		default:
			$helpText = 'Help text not found. Is it defined?';
			break;
	}

	return $helpText;
}

function toolbar($id=0) {
	//create the toolbar for this page ?>

			<!-- Top Menu Bar -->
			<nav class="navbar navbar-expand cac_tool">
				<div class="navbar-nav">
					<a class="nav-link" href="index.php"><?php add_icon('list') ?> Opp. List</a>
					<a class="nav-link" href="edit.php"><?php add_icon('plus') ?> Opportunity</a>
					<a class="nav-link" href="cats.php"><?php add_icon('list-alt') ?> Cat. List</a>
					<a class="nav-link" href="editCat.php"><?php add_icon('plus') ?> Category</a>

<?php if ($id): ?>
					<a class="nav-link" href="edit.php?id=<?= $id ?>"><?php add_icon('edit') ?> Edit</a>
					<a class="nav-link" href="javascript:confirmDelete()"><?php add_icon('trash') ?> Delete</a>
<?php endif; ?>
				</div>
			</nav>
<?php
}
?>
