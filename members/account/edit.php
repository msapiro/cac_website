<?php
require '../support/config.php';
require CLASSLOADER;

require AUTH_PAGE; // enforce login

require '../../support/functions.php';
require '../../support/formFunctions.php';


$title = 'CAC Membership';
$pageDescription = 'Edit member';

// logged in user
$member = new Members('login', ['membership', 'wpData']);
if (empty($member->memberID)) {
	error('Error', 'Did not find the logged-in member.');
	exit;
}

// optional requested member to edit
$editMember = inty($_REQUEST['id']);

if ($member->membership->joint && $editMember && ($editMember != $member->memberID)) {
	// editing an existing member, probably a joint member
	$oMember = new Members($editMember, ['membership', 'wpData']);
	if (empty($oMember->memberID)) {
		error('Error', 'Did not find the member you want to edit.');
		exit;
	}
	if ($member->membershipID <> $oMember->membershipID) {
		error('Error', 'The member you requested to edit is not in your Joint Membership');
		exit;
	}
	$pageTitle = "Edit Joint Member";
	$member = $oMember;
} else {
	$pageTitle = "Edit my Account";
}

start_page($title, $pageDescription);
?>
<script>
$(function() {
    // do stuff when DOM is ready

	$(".pop").popover({trigger: 'hover'}); // info popups

    $("#paperTrails").click(function() {
		if ( $(this).prop("checked") ) {
			var msg = "Every member with valid email receives an electronic version of Trails.\n";
			msg += "Printing and mailing the newsletter is costly.\n";
			msg +=	"Please don't request paper if the electronic version is sufficient. Thanks!";
			msg += "\n\nAre you sure you need the paper Trails?";
			if (! confirm(msg)) {;
				$(this).prop("checked", false);
			}
		}
    });

});

</script>
<?php
start_content();
?>

<h2 class="pageheader"><?= $pageTitle ?></h2>

<div class="row">
	<div class="col-lg-10 offset-lg-1">

	<form id="memberForm" method="post" action="edit2.php">
		<input type="hidden" name="memberID" value="<?= $member->memberID ?>">
<?php
static_field('CAC memberID', $member->memberID);
edit_field('First Name', 'first', $member->first, 30, false, false, 'first');
edit_field('Middle Initial', 'm_i', $member->m_i, 20);
edit_field('Last Name', 'last', $member->last, 30, false, false, 'last');
edit_field('Email', 'email', $member->email, 100, false, false, 'email');
edit_field('Phone', 'phone', phone_number($member->phone), 20);
edit_field('Mobile Phone', 'mobile', phone_number($member->mobile), 20);
edit_field('Date of Birth', 'dob', $member->dob, 10, 'YYYY-MM-DD');

if (! $editMember) :
	static_field('CAC membershipID', $member->membershipID);
	static_field('Membership Type', $member->membership->memDescription);
	edit_field('Street Address', 'street', $member->membership->street, 120);
	edit_field('City', 'city', $member->membership->city, 50);
	edit_field('State', 'state', $member->membership->state, 2);
	edit_field('Zip', 'zip', $member->membership->zip, 10);
?>
	<div class="row align-items-center mb-2">
		<label class="col-lg-3 text-lg-end"><b>Trails Newsletter</b></label>
		<div class="col-lg-7 mt-2">
			<div class="form-check">
				<input class="form-check-input" type="checkbox" name="paperTrails" id="paperTrails"
					value="1"<?php if ($member->membership->paperTrails) echo ' checked' ?>>
				<label class="form-check-label" for="paperTrails">I need a mailed paper copy of Trails</label>
			</div>
		</div>
	</div>

<?php
endif;

static_submit();
?>

	</form>

<hr class="cac">

<?php
if ($member->wpID):
	// already have a WP account
	static_field('WP Username', $member->wpData->user_login);
	static_field('WP Name', $member->wpData->wpFullname);
	static_field('WP Email', $member->wpData->wpEmail);
	if (! $editMember):
?>

<div class="row mb-3">
	<div class="col-lg-5 offset-lg-3">
		<a class="btn btn-secondary" href="<?= get_edit_profile_url() ?>" target="_blank">
			Edit my WordPress Profile
		</a>
	</div>
</div>

<?php
	endif;
endif; ?>

	</div>
</div>

<?php end_page(); ?>
