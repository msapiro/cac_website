<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';
require '../../support/formFunctions.php';

include 'includes/tools.php';

inty($_GET['id']);
$image = new Images($_GET['id']);
if (empty($image->imageID)) {
	error('ERROR', 'Did not find an image with imageID=' . $_GET['id']);
	exit;
}

$mailerDisplay = 'No';
if ($image->hide) $mailerDisplay = 'Yes';

$member = new Members($image->memberID);
if (empty($member->memberID)) $member->first = 'unknown';

start_page('CAC Images');
?>
<script>
var imageID = <?= $image->imageID ?>;

$(function() {
    // do stuff when DOM is ready

    $('.tip').tooltip({placement: 'bottom', container: 'body'});
    $(".pop").popover({trigger: 'hover'}); // info popups

    var copySupported = document.queryCommandSupported("copy");
    if (! copySupported) { $(".btnCopy").hide(); }

});

function confirmDelete() {
    var agree=confirm("Deletion cannot be undone.\n\nAre you sure you want to delete this image?");
    if (agree) {
    	$.post('ajax/delete.php', {imageID: imageID}, function() {
			location.href = "index.php";
		});
    }
}

function toClipboard(text) {
	$("#clipper").val(text).show().select();
	document.execCommand("cut"); // only works on input element
	$("#clipper").blur().hide();
	alert("Copied path to clipboard:\n\n " + text);
}
</script>
<?php
start_content();
toolbar($image->imageID);
?>

<h3 class="pageheader">Image Details &nbsp; <small>imageID=<?= $image->imageID ?></small></h3>

<div class="row">
	<div class="col-xl-10 offset-xl-1">

<?php if (! empty($_GET['duplicate'])): ?>
		<div class="alert alert-danger">
			<p><b>Important - Your file was not stored on the server!</b></p>
			<p>The file you uploaded appears to be identical to the file described below,
				which is already in our Images database.</p>
			<p>Feel free to edit the information below if you can improve it.</p>
		</div>

<?php endif; ?>
		<div class="row mb-3">
			<div class="col-lg-6 offset-lg-3">
				<img class="img-thumbnail" src="<?= $image->fileURL ?>" alt="<?= $image->name ?>">
			</div>
		</div>

<?php
static_field('Image Name', $image->name, false, false, 'col-md-3', 'col-md-9');
static_field('Dimensions', $image->width. 'w x ' . $image->height . 'h', false, false,
	'col-3 col-md-3', 'col-3 col-md-9');
static_field('File Size', byteSize($image->size), false, false,
	'col-3 col-md-3', 'col-3 col-md-5');
static_field('Original Filename', $image->origFilename, false, false,
	'col-md-3', 'col-md-9');
static_field('Uploaded', $image->created . ' by ' . $member->fullname, false, false,
	'col-md-3', 'col-md-9');
static_field('Credit', $image->credit, false, false, 'col-md-3', 'col-md-9');
static_field('Hide in Mailer', $mailerDisplay, array('Hide in Mailer', 'hide'), false,
	'col-md-3', 'col-md-5');
?>

<div class="row align-items-center mb-3">
	<label class="text-md-end col-md-3">
		<b>Relative Link</b>&nbsp;<?php Show_Info('Relative Link', getHelp('relativeLink')) ?>
	</label>
	<div class="col-md-9">
		<button class="btn btn-secondary btn-sm btnCopy"
			onclick="toClipboard('<?= $image->fileURL ?>')"><?php add_icon('copy') ?> Copy
		</button>
		&nbsp;<a href="<?= $image->fileURL ?>"><?= $image->fileURL ?></a>
	</div>
</div>

<div class="row align-items-center mb-3">
	<label class="text-md-end col-md-3">
		<b>Absolute Link</b>&nbsp;<?php Show_Info('Absolute Link', getHelp('relativeLink')) ?>
	</label>
	<div class="col-md-9">
		<button class="btn btn-secondary btn-sm btnCopy"
			onclick="toClipboard('<?= $image->absoluteURL ?>')"><?php add_icon('copy') ?> Copy
		</button>
		&nbsp;<a href="<?= $image->absoluteURL ?>"><?= $image->absoluteURL ?></a>
	</div>
</div>

<?php
static_field('File Path',  $image->filePath, false, false, 'col-md-3', 'col-md-9');
static_field('Notes', $image->notes, false, false, 'col-md-3', 'col-md-6');
?>

<!-- used for clipboard copy -->
<input type="text" id="clipper" style="display: none">

	</div>
</div>

<?php end_page(); ?>
