<?php
require '../../support/config.php';
require CLASSLOADER;

require '../includes/config.php'; // contains $allowGroups for this directory
require AUTH_AJAX;                // make sure user is logged in and a group member


// this script re-orders the membership types (classes)
// when the items are dragged and dropped.

if (empty($_POST['input'])) {
	echo 'Missing POST data';
	exit;
}

$output = array();
parse_str($_POST['input'], $output);

$oOrder = new classOrder();
$oOrder->orderArray = $output['class'];
$oOrder->dbSave();
