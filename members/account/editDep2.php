<?php
require '../support/config.php';
require CLASSLOADER;

require AUTH_PAGE; // enforce login

// logged in user
$user = new Members('login');
if (empty($user->memberID)) {
	error('Error', 'Did not find the logged-in member.');
	exit;
}


if (empty($_POST['depID'])) {
	// new dependent
	$dep = new Dependents();
	$dep->membershipID = $user->membershipID;
	$dep->first = $_POST['first'];
	$dep->last = $_POST['last'];
	$dep->dob = $_POST['dob'];
	$dep->dbSave();
} else {
	// edit existing dependent
	$dep = new Dependents($_POST['depID']);
	// security check
	if ($user->membershipID == $dep->membershipID) {
		$dep->first = $_POST['first'];
		$dep->last = $_POST['last'];
		$dep->dob = $_POST['dob'];
		$dep->dbSave();
	}
}

header("Location: index.php");
exit;
