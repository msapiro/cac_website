<?php
// Login not required for this page
require '../support/config.php';
require CLASSLOADER;

// if you want this page to have a custom title, set it here.
// leave it blank to use the default title for the site.
// this is the text that appears on the page tab in most browsers.
$title = '';
$description = 'California Alpine Club Membership Application';

require_once '../../support/functions.php';
require_once '../../support/formFunctions.php';
require 'includes/tools.php';

if (empty($_GET['key'])) {
	error('Error', 'You must submit a code to identify the application.');
	exit();
}

$applicant = new Applicants();
$applicant->getByKey($_GET['key']);
if (!$applicant->applicantID) {
	error('Error', 'Did not find an application for code ' . $_GET['key']);
	exit();
}

$key = $applicant->applicantID . ':' . $applicant->code;

if ($applicant->submitted) {
	// cannot edit anymore. go to detail page
	header("Location: applyDetail.php?key=$key");
	exit();
}

start_page($title, $description);
add_script('moment.min');
?>
<script>
"use strict";
var applyType = "<?= $applicant->applyType ?>";

$(function() {
    // do stuff when DOM is ready

	$("input[name='rbApplyType']").click(function() {
		var appType = $(this).val();
		if (appType== 'JT') {
			$("#jtApplicant").show();
		} else {
			$("#jtApplicant").hide();
		}
		if (appType== 'JTadd') {
			$("#jtMember").show();
			$("#divAddress").hide();
			$("#divNoAddress").show();
		} else {
			$("#jtMember").hide();
			$("#divAddress").show();
			$("#divNoAddress").hide();
		}
	});

	$("#" + applyType).click();

	// prevent the enter key from submitting the form
	$('#formApply').on("keydown", ":input:not(textarea)", function(event) {
		if (event.key == "Enter") {
			event.preventDefault();
		}
	});

    // some form validation, but most validation is done on applyDetail.php page
    $("#formApply").submit(function(event) {
        //DOB must be blank or a valid date
		if (isValidDate('#dob', false)) {
			$('#dob').removeClass("is-invalid");
		} else {
			event.preventDefault();
    		alert("Applicant Date of Birth is invalid. Format as YYYY-MM-DD");
    		$('#dob').addClass("is-invalid");
		}
		if (isValidDate('#dob2', false)) {
			$('#dob2').removeClass("is-invalid");
		} else {
			event.preventDefault();
    		alert("Joint applicant Date of Birth is invalid. Format as YYYY-MM-DD");
    		$('#dob2').addClass("is-invalid");
		}
	});

});

//check validity of user-entered date
function isValidDate(dateField, required) {
	var myDate = $(dateField).val().trim();
	var dateOK = false;
	if (required == false && myDate == '') {
		return true; // accept empty field
	} else {
		// strict parsing, accept several formats
		var dateTest = moment(myDate,["YYYY-MM-DD", "YYYY/MM/DD", "MM-DD-YYYY",  "MM/DD/YYYY"], true);
		if (dateTest.isValid()) {
			$(dateField).val(dateTest.format("YYYY-MM-DD")); // set the input field
			return true;
		} else {
			return false;
		}
	}
}
</script>
<?php
start_content_apply(); // in tools.php
?>

<div class="row">
	<div class="col-xl-10 offset-xl-1">

		<h2 class="mt-4">Membership Application</h2>

		<form id="formApply" action="apply2.php" method="post">
			<input type="hidden" name="key" value="<?= $key ?>">
<?php
static_field('Code', $key);
?>

			<div class="row">
				<label class="text-lg-end col-lg-3">
					<b>Applying for</b>
				</label>

				<div class="col-lg-5">
					<div class="form-check">
						<input class="form-check-input" type="radio" name="rbApplyType" value="RG" id="RG">
						<label class="form-check-label" for="RG"> Individual Membership</label>
					</div>

					<div class="form-check">
						<input class="form-check-input" type="radio" name="rbApplyType" value="JT" id="JT">
						<label class="form-check-label" for="JT"> Joint Membership (two new members)</label>
					</div>

					<div class="form-check">
						<input class="form-check-input" type="radio" name="rbApplyType" value="JTadd" id="JTadd">
						<label class="form-check-label" for="JTadd"> Joint Membership (add to existing member)</label>
					</div>

					<div class="form-check">
						<input class="form-check-input" type="radio" name="rbApplyType" value="ST" id="ST">
						<label class="form-check-label" for="ST"> Student Membership (age 18 to 26, on April 1)</label>
					</div>
				</div>
			</div>

			<div id="jtMember" style="display:none">
<?php
edit_field('Existing Member Name', 'addToMember', $applicant->addToMember, 100);
?>
			</div>

			<h4>Applicant</h4>
<?php
edit_field('First Name', 'first', $applicant->first, 30);
edit_field('Middle Initial', 'm_i', $applicant->m_i, 20);
edit_field('Last Name', 'last', $applicant->last, 30);
static_field('Email', $applicant->email);
edit_field('Phone', 'phone', phone_number($applicant->phone), 20, 'no need to format');
edit_field('Mobile Phone', 'mobile', phone_number($applicant->mobile), 20, 'no need to format');
edit_field('Date of Birth', 'dob', $applicant->dob, 10, 'YYYY-MM-DD', false, 'dob');
?>

			<div id="jtApplicant">
				<h4>Joint Applicant</h4>
<?php
edit_field('First Name', 'first2', $applicant->first2, 30);
edit_field('Middle Initial', 'm_i2', $applicant->m_i2, 20);
edit_field('Last Name', 'last2', $applicant->last2, 30);
edit_field('Email', 'email2', $applicant->email2, 100, 'Required - must be unique');
edit_field('Phone', 'phone2', phone_number($applicant->phone2), 20, 'no need to format');
edit_field('Mobile Phone', 'mobile2', phone_number($applicant->mobile2), 20, 'no need to format');
edit_field('Date of Birth', 'dob2', $applicant->dob2, 10, 'YYYY-MM-DD', false, 'dob2');
?>
			</div>

			<h4>Address</h4>
			<div id="divNoAddress" class="row">
				<div class="col-lg-8">
					<p>Address will be taken from the existing member in your joint membership. After your
						application is accepted, either one of you may edit the address.</p>
				</div>
			</div>
			<div id="divAddress">
<?php
edit_field('Street Address', 'street', $applicant->street, 120);
edit_field('City', 'city', $applicant->city, 50);
edit_field('State', 'state', $applicant->state, 2);
edit_field('Zip', 'zip', $applicant->zip, 10);
?>
			</div>

			<h4>Sponsors</h4>
			<div class="row">
				<div class="col-lg-8">
					<p>List two current Alpine Club members who have agreed to sponsor your application. They
						may not be two parts of the same joint membership.</p>
				</div>
			</div>
<?php
edit_field('Sponsor #1', 'sponsor1', $applicant->sponsor1, 100);
edit_field('Sponsor #2', 'sponsor2', $applicant->sponsor2, 100);
?>

			<h4>Work and Social Activities</h4>
			<div class="row">
				<div class="col-lg-8">
					<p>List two Alpine Club events (with dates) that you have attended. One event must be a
						&quot;work&quot; activity.</p>
				</div>
			</div>
<?php
edit_field('Event #1', 'event1', $applicant->event1, 100);
edit_field('Event #2', 'event2', $applicant->event2, 100);
?>

			<h4>Credits, Additional Comments</h4>
			<div class="row">
				<div class="col-lg-8">
					<p>
						If you have paid fees as an Associate Member, as a guest at Echo Lodge for example, you
						can receive a credit toward your entry fee (but not your dues). Indicate that here and
						submit your receipts to <span class="emlink" data-user="membership">the membership chair</span>.
					</p>

					<p>Also provide any other information that might be helpful in processing your
						application.</p>
				</div>
			</div>
			<?php
			edit_textfield('Comments', 'notes', $applicant->notes, 4);

static_submit('Save and Review');
?>
	</form>

	</div>
</div>
<?php end_page(); ?>
