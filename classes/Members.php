<?php

class Members extends Model {
	public $memberID;
	public $membershipID;
	public $wpID = 0;
	public $first;
	public $m_i = NULL;
	public $last;
	public $email = NULL;
	public $phone = NULL;
	public $mobile = NULL;
	public $dob = NULL;
	public $created;
	public $modified;

	public $fullname;
	public $membership = NULL;
	public $wpData = NULL;


	public function __construct($memberID=0, $expand=false) {
		// memberID can be an integer, or the string 'login', which finds the logged-in user
		parent::__construct(); // set up our database
		if ($memberID) {
			if ($memberID == 'login') $memberID = $_SESSION['cac_login']['memberID'];
			$this->getByID($memberID, $expand);
		}
	}

	public function getByID($memberID, $expand=false) {
		// get the data for an existing member.
		// $expand can be a string or an array of strings.
		$row = $this->db->get('members', '*', ['memberID' => $memberID]);
		if (! $row) return;
		foreach ($row as $key => $value) {
			$this->$key = $value;
		}
		$this->fullname = $this->first;
		if ($this->m_i) $this->fullname .= ' ' . $this->m_i . '.';
		$this->fullname .= ' ' . $this->last;

		// see if we should expand to include more classes
		if ($expand) {
			if (! is_array($expand)) $expand = array($expand);
			foreach ($expand as $type) {
				switch ($type) {
					case 'membership':
						$this->membership = new Memberships($this->membershipID);
						break;
					case 'wpData':
						$this->wpData = new WP_data($this->wpID);
						break;
				}
			}
		}
	}

	public function getByWpID($wpID, $expand=false) {
		// get the data for an existing member.
		// $expand can be a string or an array of strings.
		$row = $this->db->get('members', '*', ['wpID' => $wpID]);
		if (! $row) return;
		foreach ($row as $key => $value) {
			$this->$key = $value;
		}
		$this->fullname = $this->first;
		if ($this->m_i) $this->fullname .= ' ' . $this->m_i . '.';
		$this->fullname .= ' ' . $this->last;

		// see if we should expand to include more classes
		if ($expand) {
			if (! is_array($expand)) $expand = array($expand);
			foreach ($expand as $type) {
				switch ($type) {
					case 'membership':
						$this->membership = new Memberships($this->membershipID);
						break;
					case 'wpData':
						$this->wpData = new WP_data($this->wpID);
						break;
				}
			}
		}
	}

	public function getByEmail($email) {
		// return an array of Members() objects or an empty array
		$membersArray = array();
		$rows = $this->db->select('members', 'memberID', ['email[~]' => $email, 'ORDER' => ['memberID']]);
		foreach ($rows as $memberID) {
			$membersArray[] = new Members($memberID);
		}
		return $membersArray;
	}

	public function dbSave() {
		// save the member info into the database table
		$dataArray = [
			'membershipID' => $this->membershipID,
			'wpID' => $this->wpID,
			'first' => $this->first,
			'm_i' => nully($this->m_i),
			'last' => $this->last,
			'email' => nully($this->email),
			'phone' => nully($this->phone),
			'mobile' => nully($this->mobile),
			'dob' => nully($this->dob)
		];

		if (! is_null($dataArray['m_i'])) {
			$dataArray['m_i'] = preg_replace('/(.*)\.$/', '$1', $dataArray['m_i']); // remove trailing .
		}

		if ($this->memberID) {
			// update existing
			$this->db->update('members', $dataArray, ['memberID' => $this->memberID] );
		} else {
			// create new
			$this->db->insert('members', $dataArray);
			$this->memberID = $this->db->id();
		}
		return $this->memberID;
	}
} // end Members class

class Memberships extends Model {
	public $membershipID;
	public $class;
	public $paperTrails = 0;
	public $expiration;
	public $street;
	public $city;
	public $state;
	public $zip;
	public $sponsors = NULL;
	public $resigned = 0;
	public $resignedDate = NULL;
	public $created;
	public $modified;

	public $expired;
	public $memDescription;
	public $joint;
	public $dues;

	public function __construct($membershipID=0) {
		parent::__construct(); // set up our database
		if ($membershipID) {
			$this->getByID($membershipID);
		}
	}

	public function getByID($membershipID) {
		$row = $this->db->get(
			'memberships',
			["[><]classes" => "class"],
			['membershipID', 'memberships.class','paperTrails', 'expiration', 'street', 'city', 'state', 'zip', 'sponsors',
				'resigned', 'resignedDate', 'created', 'modified', 'description(memDescription)', 'joint', 'dues'],
			['membershipID' => $membershipID]
		);
		if (! $row) return;
		foreach ($row as $key => $value) {
			$this->$key = $value;
		}
		$this->expired = $this->isExpired();
	}

	public function isExpired($expiration=false, $grace=false) {
		// specify the grace period as a DateInterval, eg P10D for 10days, P3W for 3 weeks
		if (! $expiration) $expiration = $this->expiration;
		if (is_null($expiration)) {
			$expired = 1;
		} else {
			$tz = new DateTimeZone('America/Los_Angeles');
			$now = new DateTime("now", $tz);
			$expDate = new DateTime($expiration . ' 23:59:59', $tz); // end of day
			if ($grace) {
				$expDate->add(new DateInterval($grace));
			}
			if ($now > $expDate) {
				$expired = 1;
			} else {
				$expired = 0;
			}
		}
		return($expired);
	}

	public function getJointMembers($membershipID=0) {
		// get all members for a joint membership
		// return an array of member objects
		$family = array();
		if (! $membershipID) {
			$membershipID = $this->membershipID;
		}
		$rows = $this->db->select('members', 'memberID', ['membershipID' => $membershipID, 'ORDER' => ['memberID']]);
		foreach ($rows as $memberID) {
			$family[] = new Members($memberID);
		}
		return $family;
	}

	public function dbSave() {
		// save the membership info into the database table
		$dataArray = [
				'class' => $this->class,
				'paperTrails' => $this->paperTrails,
				'expiration' => $this->expiration,
				'street' => $this->street,
				'city' => $this->city,
				'state' => strtoupper($this->state),
				'zip' => $this->zip,
				'sponsors' => nully($this->sponsors),
				'resigned' => $this->resigned,
				'resignedDate' => nully($this->resignedDate)
		];

		if ($this->membershipID) {
			// update existing membership
			$this->db->update('memberships', $dataArray, ['membershipID' => $this->membershipID] );
		} else {
			// create new membership
			$this->db->insert('memberships', $dataArray);
			$this->membershipID = $this->db->id();
		}
		return $this->membershipID;
	}

	public function delete() {
		// delete the membership if no members are assigned to it.
		$membersArray = $this->getJointMembers();
		if (! $membersArray) {
			$this->db->delete('memberships', ['membershipID' => $this->membershipID]);
		}
	}

	public function delete_all() {
		// Delete a membership and all it's members. This will only
		// succeed if the membership was just created. This is to
		// allow reversal of a membership created by mistake.
		$tz = new DateTimeZone('America/Los_Angeles');
		$cutoff = (new DateTime('-2hours', $tz))->format('Y-m-d H:i:s');
		if ($this->created < $cutoff) {
			return("Sorry, it's too late to delete this membership this way. Contact registrar@californiaalpineclub.org for assistance.");
		}
		$membersArray = $this->getJointMembers();
		foreach ($membersArray as $member) {
			$this->db->delete('members',
				['memberID' => $member->memberID]);
		}
		$this->db->delete('memberships',
			['membershipID' => $this->membershipID]);
		return("Membership $this->membershipID deleted.");
	}
} // end Memberships class

class Classes extends Model {
	public $class;
	public $description;
	public $joint;
	public $dues;
	public $entryFee;
	public $showUsers;
	public $showAdmin;
	public $displayOrder;

	public $usage = NULL;

	public function __construct($class=false) {
		parent::__construct(); // set up our database
		if ($class) {
			$this->getByID($class);
		}
	}

	public function getByID($class) {
		$row = $this->db->get('classes', '*', ['class' => $class]);
		if (! $row) return;
		foreach ($row as $key => $value) {
			$this->$key = $value;
		}
	}

	public function getArray($show=false) {
		$typesArray = array();
		$whereArray = array();
		if ($show == 'admin') {
			$whereArray['showAdmin'] = 1;
		} elseif ($show == 'users') {
			$whereArray['showUsers'] = 1;
		}
		$whereArray['ORDER'] = array('displayOrder');

		$rows = $this->db->select('classes', 'class', $whereArray);
		foreach ($rows as $memberID) {
			$typesArray[] = new Classes($memberID);
		}
		return $typesArray;
	}

	public function dbSave() {
		// save the Membership Type info into the database table
		$dataArray = [
			'class' => $this->class,
			'description' => $this->description,
			'joint' => $this->joint,
			'dues' => $this->dues,
			'entryFee' => $this->entryFee,
			'showUsers' => $this->showUsers,
			'showAdmin' => $this->showAdmin
		];

		// medoo doesn't currently support the MySQL REPLACE INTO statement,
		// which would do the following in one shot:
		$exists = $this->db->get('classes','class', ['class' => $this->class]);
		if ($exists) {
			// update existing
			$this->db->update('classes', $dataArray, ['class' => $this->class] );
		} else {
			// get the current sort order
			$oOrder = new ClassOrder(true);
			//	create new
			$this->db->insert('classes', $dataArray);
			// put new class at the end of the order
			$oOrder->orderArray[] = $this->class;
			$oOrder->dbSave();
		}
	}

	public function dbDelete() {
		// delete the Type if no memberships are assigned to it.
		if (is_null($this->usage)) {
			$this->getUsage();
		}
		if ($this->usage == 0) {
			$this->db->delete('classes', ['class' => $this->class]);
		}
	}

	public function getUsage() {
		// count usage of this type of membership (current and expired)
		$this->usage = $this->db->count('memberships', ['class' => $this->class]);
	}
} // end Classes class

class WP_data {
	public $wpID;
	public $user_login;
	public $wpFirst;
	public $wpLast;
	public $wpEmail;
	public $wpFullname;

	public function __construct($wpID=false) {
		if ($wpID) {
			$this->getByID($wpID);
		}
	}

	public function getById($wpID) {

		$wpUser = get_userdata($wpID);

		if ($wpUser) {
			$this->wpID = $wpID;
			$this->user_login = $wpUser->user_login;
			$this->wpEmail = $wpUser->user_email;

			$wpMeta = get_user_meta($wpID);
			$this->wpFirst = $wpMeta['first_name'][0];
			$this->wpLast = $wpMeta['last_name'][0];

			$this->wpFullname = $this->wpFirst . ' ' . $this->wpLast;
		}
	}
}
