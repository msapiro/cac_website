<?php
//tools specific to this category

function getHelp($subject) {
	switch ($subject) {
		case 'relativeLink':
			$helpText = "<ul>
<li>If you paste the RELATIVE link into the <b>URL</b> field in
the Mailer's html editor <b>Image</b> tool, the image will be embedded in the message.</li>
<li>If you paste the ABSOLUTE link, a link to the image will be included in the message.</li>
</ul>";
			break;

		case 'hide':
			$helpText = "Check the box to omit this image in the Mailer's <b>Browse Server</b>
window. For example, you might hide a high resolution version, but display lower resolution versions
of the same image.";
			break;

		default:
			$helpText = 'Help text not found. Is it defined?';
			break;
	}

	return $helpText;
}

function toolbar($id=0) {
	//create the toolbar for this page ?>

			<!-- Top Menu Bar -->
			<nav class="navbar navbar-expand cac_tool">
				<div class="navbar-nav">
					<a class="nav-link" href="index.php"><?php add_icon('images') ?> Gallery</a>
					<a class="nav-link" href="add.php"><?php add_icon('plus') ?> New Image</a>
<?php if ($id): ?>
					<a class="nav-link" href="edit.php?id=<?= $id ?>"><?php add_icon('edit') ?> Edit</a>
					<a class="nav-link" href="download.php?id=<?= $id ?>"><?php add_icon('download') ?> Download</a>
					<a class="nav-link" href="javascript:confirmDelete()"><?php add_icon('trash') ?> Delete</a>
<?php endif; ?>
				</div>
			</nav>
<?php
}
?>
