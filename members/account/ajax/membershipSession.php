<?php
// create a stripe session for a pending membership transaction
require '../../support/config.php';
require CLASSLOADER;

require AUTH_AJAX; // enforce login

/**
 * from included files:
 * @var string $stripeSecretKey
 * @var string $stripeApiVersion
 */

cleanMoney($_POST['fdn']); // function defined below
cleanMoney($_POST['gf']);
$data = array();
$stripe = new Stripes($_POST['stripeID']);

if (empty($stripe->stripeID)) stopNow('stripeID invalid or not provided');

$memberTrans = new MemberTrans();
$memberTrans->getByStripeID($stripe->stripeID);
if (empty($memberTrans->mTransID)) stopNow('membership transaction not found');

if ($stripe->status <> 'pending') stopNow('stripeID=' . $stripe->stripeID . ' not pending');

// this stripeID might already have a sessionID stored in the chargeID field
// if the stripe payment was canceled, but we don't re-use it because the
// donations values might have been changed before resubmit

$donationFDN = new DonationsFDN();
$donationFDN->getByStripeID($stripe->stripeID);
// in the following line empty(0.00) would be false, so we also test for zero
if (empty($_POST['fdn']) || $_POST['fdn'] == 0) {
	$donationFDN->dbDelete(); // may or may not exist
	$donationFDN->donationID = 0;
	$donationFDN->amount = 0;
} else {
	if (! $donationFDN->donationID) {
		$donationFDN->memberID = $_SESSION['cac_login']['memberID']; // logged in user
		$donationFDN->stripeID = $stripe->stripeID;
	}
	$donationFDN->amount = $_POST['fdn'];
	$donationFDN->dbSave();
}

$donationGF = new DonationsGF();
$donationGF->getByStripeID($stripe->stripeID);
// in the following line empty(0.00) would be false, so we also test for zero
if (empty($_POST['gf']) || $_POST['gf'] == 0) {
	$donationGF->dbDelete(); // may or may not exist
	$donationGF->donationID = 0;
	$donationGF->amount = 0;
} else {
	if (! $donationGF->donationID) {
		$donationGF->memberID = $_SESSION['cac_login']['memberID']; // logged in user
		$donationGF->stripeID = $stripe->stripeID;
	}
	$donationGF->amount = $_POST['gf'];
	$donationGF->dbSave();
}


// create the stripe client
require '/www/cac/data/credentials/stripe.php'; // stripe credentials
$stripeClient = new \Stripe\StripeClient([
		'api_key' => $stripeSecretKey,
		'stripe_version' => $stripeApiVersion
]);

// membership action
$oClass = new Classes($memberTrans->membershipType);

$membership_item = [
	'quantity' => 1,
	'price_data' => [
		'currency' => 'usd',
		'unit_amount' =>$memberTrans->amount * 100,
		'product_data' => [
			'name' => $oClass->description . ' Renewal',
			'description' => 'Membership Dues'
		]
	]
];

$fdn_item = false;
if ($donationFDN->donationID) {
	$fdn_item = [
		'quantity' => 1,
		'price_data' => [
			'currency' => 'usd',
			'unit_amount' => $donationFDN->amount * 100,
			'product_data' => [
				'name' => 'Donation to CAC Foundation',
				'description' => 'tax deductible'
			]
		]
	];
}

$gf_item = false;
if ($donationGF->donationID) {
	$gf_item = [
		'quantity' => 1,
		'price_data' => [
			'currency' => 'usd',
			'unit_amount' => $donationGF->amount * 100,
			'product_data' => [
				'name' => 'Donation to General Fund',
				'description' => 'not deductible'
			]
		]
	];
}

$line_items = array();
$line_items[] = $membership_item;
if ($fdn_item) $line_items[] = $fdn_item;
if ($gf_item) $line_items[] = $gf_item;

// we need access to wordpress home_url() function
require __DIR__ . '/../../../../wp-load.php';

$success_url = home_url() . '/cac/members/payment/success.php?sessionID={CHECKOUT_SESSION_ID}&id='
	. $stripe->stripeID ;

// return to the calling page with cancelID set. include email as a second check.
// user might cancel multiple times, so construct the URL from parts rather than adding on.
$parts = parse_url($_POST['fromPage']);
$cancel_url = $parts['scheme'] . '://' . $parts['host'] . $parts['path'] . '?cancelID='
	. $stripe->stripeID . '&email=' . $memberTrans->email;

// the description attached to the payment intent gets transferred to the payment,
// which is helpful to extract on our Admin financial pages.
$stripeSession = $stripeClient->checkout->sessions->create([
	"client_reference_id" => $stripe->stripeID,
	"customer_email" => $memberTrans->email,
	"success_url" => $success_url,
	"cancel_url" => $cancel_url,
	'mode' => 'payment',
	"line_items" => $line_items,
	"payment_intent_data" => [
		"description" => "Membership Dues",
		"metadata" => [
			"stripeID" => $stripe->stripeID,
			"category" => "dues",
			"transType" => $memberTrans->action,
			"memDescription" => $oClass->description,
			"FDNdonation" => $donationFDN->amount,
			"GFdonation" => $donationGF->amount
		]
	]
]);

// we store the sessionID in the chargeID field temporarily.
// replace it with the chargeID after successful payment.
$stripe->chargeID = $stripeSession->id;
$stripe->amount = $memberTrans->amount + $donationGF->amount + $donationFDN->amount;
$stripe->item =  $membership_item['price_data']['product_data']['name'];
if ($fdn_item) $stripe->item .= ' + CAC Fdn. donation';
if ($gf_item) $stripe->item .= ' + Gen. Fund donation';
$stripe->dbSave();

$data['success'] = 1;
$data['sessionURL'] = $stripeSession->url;
echo json_encode($data);
exit;

function stopNow($msg) {
	$data = array();
	$data['success'] = 0;
	$data['msg'] = $msg;
	echo json_encode($data);
	exit;
}

function cleanMoney(&$text) {
	// take user input and make sure it is valid for money

	$text = trim($text);
	$text = str_replace(['$', ','], '', $text);  //remove $ and , chars

	if (! is_numeric($text)) {
		$text = 0;
	}
}
