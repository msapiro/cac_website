<?php
require '../../support/config.php';
require CLASSLOADER;

require '../includes/config.php'; // contains $allowGroups for this directory
require AUTH_AJAX;                // make sure user is logged in and a group member

// suggest entry fee and dues for an application

$data = array();
inty($_POST['applicantID']);

$applicant = new Applicants($_POST['applicantID']);

if (empty($applicant->applicantID)) {
	stopNow('Did not find an applicant with applicantID = ' . $_POST['applicantID']);
}

if ($applicant->applyType == 'JTadd') {
	$jointMember = new Members($applicant->addToMemberID, 'membership');
	if (empty($jointMember->memberID)) {
		stopNow('Did not find the existing member for the other half of Joint membership.');
	}
}

switch($applicant->applyType) {
	case 'RG':
	case 'JTadd': // fees and dues are same as individual regular member
		$class= new Classes('RG');
		$dues = $class->dues;
		$entryFee = $class->entryFee;
		break;

	case 'JT':
	case 'ST':
		$class= new Classes($applicant->applyType);
		$dues = $class->dues;
		$entryFee = $class->entryFee;
		break;

	case 'RGre':
		$entryFee = 0;
		$class= new Classes('RG');
		$dues = $class->dues;
		break;
	case 'JTre':
		$entryFee = 0;
		$class= new Classes('JT');
		$dues = $class->dues;
		break;
	case 'SRre':
		$entryFee = 0;
		$class= new Classes('SR');
		$dues = $class->dues;
		break;
	case 'JSre':
		$entryFee = 0;
		$class= new Classes('JS');
		$dues = $class->dues;
}

$tz = new DateTimeZone('America/Los_Angeles');
$now = new DateTime('now', $tz);
$month = $now->format('n'); // month without leading zero (1 - 12)
if ($month > 6) $dues = $dues/2;


$data['success'] = 1;
$data['entryFee'] = $entryFee + 0; // to make it numeric
$data['dues'] = $dues + 0;

echo json_encode($data);
exit;


function stopNow($msg) {
	global $data;
	$data['success'] = 0;
	$data['msg'] = $msg;
	echo json_encode($data);
	exit;
}
