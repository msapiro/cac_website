<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';


// Ensure we came from a POST.
requirePost();

inty($_POST['memberID']);

$member = new Members($_POST['memberID'], 'membership');
if (empty($member->memberID)) {
	error('Error', 'Did not find a member with memberID=' . $_POST['memberID']);
	exit;
}

$oClass = new Classes($_POST['membershipType']);
if (! $oClass) {
	error('Error', 'Membership Class ' . $_POST['membershipType'] . ' not found.');
	exit;
}

$donationFDN = false;
if (! empty($_POST['fdn'])) {
	$donationFDN = new DonationsFDN();
	$donationFDN->memberID = $member->memberID;
	$donationFDN->amount = $_POST['fdn'];
}

$donationGF = false;
if (! empty($_POST['gf'])) {
	$donationGF = new DonationsGF();
	$donationGF->memberID = $member->memberID;
	$donationGF->amount = $_POST['gf'];
}

$newExpiration = $_POST['newExpiration'];
if ($_POST['reinstatement']) {
	$item = $oClass->description . ' Reinstatement';
} else {
	$item = $oClass->description . ' Renewal';
}
$amount = $oClass->dues;

if ($donationFDN) {
	$item .=  ' + CAC Fdn. donation';
	$amount += $donationFDN->amount;
}
if ($donationGF) {
	$item .= ' + Gen. Fund donation';
	$amount += $donationGF->amount;
}

// create the payment
$oPmt = new Payments();
$oPmt->amount = $amount;
$oPmt->pmtType =$_POST['pmtType'];
$oPmt->item = $item;
$oPmt->pmtAmount = $_POST['pmtAmount'];
$oPmt->transNote = $_POST['transNote'];
$oPmt->dbSave();

// now we have a pmtID for the donation entries
if ($donationFDN) {
	$donationFDN->pmtID = $oPmt->pmtID;
	$donationFDN->dbSave();
}

if ($donationGF) {
	$donationGF->pmtID = $oPmt->pmtID;
	$donationGF->dbSave();
}


// can we just update the membership or do we need to make a new one?
$updateMembership = true;
if ($oClass->class == $member->membership->class) {
	// no change in membershipType, update
} else {
	if ($oClass->joint == $member->membership->joint) {
		// no change in joint status, update
	} else {
		if ($oClass->joint) {
			// changing from Individual to Joint, update
		} else {
			// changing from Joint to Individual. see if there are other family members
			$jointMembers = $member->membership->getJointMembers();
			if (count($jointMembers) == 1) {
				// change does not affect other members, update
			} else {
				// make a new membership for this user, copying some data from current membership.
				// other joint member will retain the old joint membership.
				$updateMembership = false;

				$newMembership = new Memberships();
				$newMembership->class = $oClass->class;
				$newMembership->expiration = $newExpiration;
				$newMembership->paperTrails = $member->membership->paperTrails;
				$newMembership->street = $member->membership->street;
				$newMembership->city = $member->membership->city;
				$newMembership->state = $member->membership->state;
				$newMembership->zip = $member->membership->zip;
				$newMembership->sponsors = $member->membership->sponsors;
				$newMembershipID = $newMembership->dbSave();

				// assign new membership to the member
				$member->membershipID = $newMembershipID;
				$member->dbSave();
			}
		}
	}
}

if ($updateMembership) {
	$member->membership->class = $oClass->class;
	$member->membership->expiration = $newExpiration;
	if ($member->membership->resigned) {
		$member->membership->resigned = 0;
		$member->membership->resignedDate = null;
	}
	$member->membership->dbSave();
}

// record the member transaction
$oTrans = new  MemberTrans();
$oTrans->adminID =  $_SESSION['cac_login']['memberID'];
$oTrans->memberID = $member->memberID;
$oTrans->membershipID = $member->membershipID;
$oTrans->pmtID = $oPmt->pmtID;
$oTrans->amount = $oClass->dues;
$oTrans->action = 'renew';
$oTrans->membershipType = $oClass->class;
$oTrans->expiration = $newExpiration;
$oTrans->dbSave();

header("Location: detail.php?id=$member->memberID");
