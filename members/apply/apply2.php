<?php
// Login not required for this page
require '../support/config.php';
require CLASSLOADER;

require '../../support/functions.php';
require_once '../../../wp-load.php';
require '../../support/restoreGlobals.php';
restoreGlobals(); //stupid WordPress

requirePost();

if (empty($_POST['key'])) {
	error('Error', 'You must submit a code to identify the application.');
	exit;
}

$applicant = new Applicants();
$applicant->getByKey($_POST['key']);
if (! $applicant->applicantID) {
	error('Error', 'Did not find an application for code ' . $_POST['key']);
	exit;
}

$applicant->applyType = $_POST['rbApplyType'];
if ($applicant->applyType == 'JTadd') {
	$applicant->addToMember = $_POST['addToMember'];
} else {
	$applicant->addToMember = NULL;
}

// omit email from stdFields. it is set when application is created
$stdFields = ['first', 'm_i', 'last', 'phone', 'mobile', 'dob',  'sponsor1', 'sponsor2',
	'event1', 'event2', 'notes'];
$addressFields = ['street', 'city', 'state', 'zip'];
$jointFields = ['first2', 'm_i2', 'last2', 'email2', 'phone2', 'mobile2', 'dob2'];

foreach ($stdFields as $field) $applicant->$field = $_POST[$field];

if ($applicant->applyType == 'JTadd') {
	foreach ($addressFields as $field) $applicant->$field = NULL;
} else {
	foreach ($addressFields as $field) $applicant->$field = $_POST[$field];
}

if ($applicant->applyType == 'JT') {
	foreach ($jointFields as $field) $applicant->$field = $_POST[$field];

	$member2 = new Members();

	if (filter_var($applicant->email2 , FILTER_VALIDATE_EMAIL)) {
		$in_use_cac = $member2->getByEmail($applicant->email2);

		// also make sure it isn't in use by WordPress
		$in_use_wp = get_user_by('email', $applicant->email2);

		if ($in_use_cac) {
			error('Error', '"' . $applicant->email2 . '"' . " is already being used by a CAC member.");
			exit;
		}

		if ($in_use_wp) {
			error('Error', '"' . $applicant->email2 . '"' . " is already being used by a WordPress user.");
			exit;
		}
	}

} else {
	foreach ($jointFields as $field) $applicant->$field = NULL;
}

$applicant->dbSave();

header("Location: applyDetail.php?key=$applicant->applicantID:$applicant->code");
exit;
