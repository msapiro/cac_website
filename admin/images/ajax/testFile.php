<?php
require '../../support/config.php';

require '../includes/config.php'; // contains $allowed_extension

/**
 * @var array  $allowed_extension
 */

$data = array();

$docFilename = $_POST['docFilename'];

$docFilename = str_replace('\\', '/', $docFilename);
$docFilename = basename($docFilename);

// test the filename
$data['badFilename'] = 0;
if (badFilename($docFilename)) {
	 $data['badFilename'] = 1;
}

$data['badExtension'] = 0;
$file_extension = pathinfo($docFilename, PATHINFO_EXTENSION);
if (is_null($file_extension)) {
	$data['badExtension'] = 1;
} elseif (! in_array(strtolower($file_extension), $allowed_extension)) {
	$data['badExtension'] = 1;
}

echo json_encode($data);
exit;

function badFilename($filename) {
	$result = true;
	if (preg_match("`^[- 0-9A-Z_\.]+$`i",$filename)) $result = false;
	if (strlen($filename) > 200) $result = true;
	return $result;
}
