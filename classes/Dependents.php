<?php

class Dependents extends Model {

	public $depID;
	public $membershipID;
	public $first;
	public $last;
	public $dob = NULL;

	public $fullname;
	public $age = NULL;

	public function __construct($depID = 0) {
		parent::__construct(); // set up our database
		if ($depID) {
			$this->getByID($depID);
		}
	}

	public function getByID($depID) {
		// get the values for an existing dependent
		$row = $this->db->get('dependents', '*', ['depID' => $depID]);
		if (! $row) return;
		foreach ($row as $key => $value) {
			$this->$key = $value;
		}
		$this->fullname = $this->first . ' ' . $this->last;

		if (! empty($this->dob)) {
			// calculate age
			$birth = new DateTime($this->dob);
			$now = new DateTime();
			$interval = $now->diff($birth);
			$this->age = $interval->y;
		}
	}

	public function getAllDeps($membershipID) {
		// get all dependents for a family
		// return an array of dependent objects
		$family = array();
		$rows = $this->db->select('dependents', 'depID', ['membershipID' => $membershipID, 'ORDER' => ['dob']]);
		foreach ($rows as $depID) {
			$family[] = new Dependents($depID);
		}
		return $family;
	}

	public function dbSave() {
		// save the dependent info into the database table
		$dataArray = [
			'membershipID' => $this->membershipID,
			'first' => $this->first,
			'last' => $this->last,
			'dob' => nully($this->dob)
		];

		if ($this->depID) {
			// update existing dependent
			$this->db->update('dependents', $dataArray, ['depID' => $this->depID] );
		} else {
			// create new dependent
			$this->db->insert('dependents', $dataArray);
			$this->depID = $this->db->id();
		}
		return $this->depID;
	}

	public function dbDelete() {
		if ($this->depID) {
			$obj = $this->db->delete('dependents', ['depID' => $this->depID]);
			$ret = $obj->rowCount(); // number of affected rows
		} else {
			$ret = 0;
		}
		return $ret;
	}

} // end class
