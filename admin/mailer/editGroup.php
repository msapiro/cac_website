<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';
require '../../support/formFunctions.php';
include 'includes/tools.php';

$groupID = inty($_REQUEST['id']); // might be zero for new group

if ($groupID) {
	$pageTitle = 'Edit Group Query';
	$msgGroup = new MsgGroups($groupID);
	if (empty($msgGroup->groupID)) {
		error('Error', 'Did not find a message group with groupID=' . $groupID);
		exit;
	}
} else {
	$pageTitle = 'Create New Group Query';
	$msgGroup = new MsgGroups(); // blank object
}

start_page('CAC Mailer');
?>
<script>
$(function() {
	// do stuff when DOM is ready

	$(".pop").popover({trigger: 'hover'}); // info popups

});
</script>
<?php
start_content();
toolbar();
?>

<h3 class="pageheader"><?= $pageTitle ?></h3>

<div class="row">
	<div class="col-xl-10 offset-xl-1">

	<form action="editGroup2.php" method="post" id="mailForm">
		<input type="hidden" name="groupID" value="<?= $groupID ?>">

<?php if ($groupID): ?>
		<div class="row align-items-center mb-3">
			<div class="col-lg-5 offset-lg-3">
				<a class="btn btn-secondary" href="detailGroup.php?id=<?= $groupID ?>"><?php add_icon('user-friends') ?> groupID: <?= $groupID ?></a>
			</div>
		</div>
<?php endif;

edit_field('Group Name', 'groupName', $msgGroup->groupName, false, false, false, 'groupName', 'col-lg-3', 'col-lg-8');
edit_textfield('Query String', 'queryString', $msgGroup->queryString, 8, array('MySQL Query String', 'queryString'), 'queryString', 'col-lg-3', 'col-lg-8');
edit_textfield('Note', 'note', $msgGroup->note, 4, false, 'note', 'col-lg-3', 'col-lg-8');
static_submit('Submit')
?>
	</form>

	</div>
</div>

<?php end_page(); ?>
