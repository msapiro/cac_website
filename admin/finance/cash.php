<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';
include 'includes/tools.php';

$weeks = 4; // show 4 weeks worth of payouts
$offset = inty($_GET['offset']);

if ($offset) {
	$string = '-' . $offset * $weeks . ' weeks';
	$periodEnd = strtotime($string);
} else {
	$periodEnd = time();
}
$string = '-' . $weeks . ' weeks';
$periodStart = strtotime($string, $periodEnd);

$periodStart = date ('Y-m-d', $periodStart);
$periodEnd = date ('Y-m-d', $periodEnd);
$periodEnd = $periodEnd . ' 23:59:59'; // created has H:i:s - need end of day

$thisPage = $_SERVER["PHP_SELF"];
$prevURL = false;
$prev = $offset + 1;
$prevURL = $thisPage . '?offset=' . $prev;

$next = $offset - 1;
if ($next > 0) {
	$nextURL = $thisPage . '?offset=' . $next;
} elseif ($next == 0) {
	$nextURL = $thisPage;
} else {
	$nextURL = false;
}

// hold our spreadsheet data in an array named table
// $table = [pmtID, Date, Total, Club Amount, FDN Amount, Note, Item]
$table = array();

//  find all payments with the date range
$model = new Model();
$rows = $model->db->select('payments', 'pmtID', ['created[<>]' => [$periodStart, $periodEnd], 'ORDER' => ['created' => 'DESC']]);

$payment = new Payments();
foreach ($rows as $pmtID) {
	$payment->getByID($pmtID);
	$memberTrans = new MemberTrans;
	$memberTrans->getByPaymentID($pmtID);
	$first = true;

	if ($memberTrans->mTransID) {
		$tRow = array();
		$tRow[] = $pmtID;
		$tRow[] = explode(' ', $payment->created)[0];
		$tRow[] = '$' . $payment->pmtAmount;
		$tRow[] = '$' . $memberTrans->amount;
		$tRow[] = '';
		$tRow[] = $payment->transNote;
		$tRow[] = $memberTrans->membershipType . ' ' . $memberTrans->action;
		$table[] = $tRow;
		$first = false;
	}

	$donationGF = new DonationsGF();
	$donationGF->getByPaymentID($pmtID);
	if ($donationGF->donationID) {
		$tRow = array();
		if ($first) {
			$tRow[0] = $pmtID;
			$tRow[1] = explode(' ', $payment->created)[0];
			$tRow[2] = '$' . $payment->pmtAmount;
			$tRow[5] = $payment->transNote;
		} else {
			$tRow[0] = '';
			$tRow[1] = '';
			$tRow[2] = '';
			$tRow[5] = '';
		}
		$tRow[3] = '$' . $donationGF->amount;
		$tRow[4] = '';
		$tRow[6] = 'General Fund donation';
		$table[] = $tRow;
		$first = false;
	}

	$donationFDN = new DonationsFDN();
	$donationFDN->getByPaymentID($pmtID);
	if ($donationFDN->donationID) {
		$tRow = array();
		if ($first) {
			$tRow[0] = $pmtID;
			$tRow[1] = explode(' ', $payment->created)[0];
			$tRow[2] = $payment->pmtAmount;
			$tRow[5] = $payment->transNote;
		} else {
			$tRow[0] = '';
			$tRow[1] = '';
			$tRow[2] = '';
			$tRow[5] = '';
		}
		$tRow[3] = '';
		$tRow[4] = '$' . $donationFDN->amount;
		$tRow[6] = 'CAC Foundation donation';
		$table[] = $tRow;
	}
}


start_page('CAC Finance');
start_content();
toolbar();
?>

<div class="row justify-content-center">
	<div class="col-xl-11">

<div class="row">
	<div class="col-sm-6 col-md-5 col-lg-4 col-xl-3 mt-2">
		<div class="btn-group">
<?php if ($prevURL): ?>
			<a class="btn btn-secondary" href="<?= $prevURL ?>"><?php add_icon('chevron-left') ?> Previous</a>
<?php else : ?>
			<a class="btn btn-secondary disabled" href="#"><?php add_icon('chevron-left') ?> Previous</a>
<?php endif;
if ($nextURL): ?>
			<a class="btn btn-secondary" href="<?= $nextURL ?>">Next <?php add_icon('chevron-right') ?></a>
			<a class="btn btn-secondary" href="<?= $thisPage ?>"><?php add_icon('step-forward') ?> </a>
<?php else : ?>
			<a class="btn btn-secondary disabled" href="#">Next <?php add_icon('chevron-right') ?></a>
<?php endif; ?>
		</div>
	</div>

	<div class="col-sm-6">
		<h3 class="pageheader">Cash and Check Payments</h3>
	</div>
</div> <!-- end row -->


<table class="table table-sm table-striped">
	<thead>
		<tr><th class="text-center">pmtID</th>
		<th class="text-center">Date</th>
		<th class="text-end">Total</th>
		<th class="text-end">Club Amount</th>
		<th class="text-end">FDN Amount</th>
		<th>Note</th><th>Item</th></tr>
	</thead>
	<tbody>

<?php foreach ($table as $display): ?>
		<tr>
		<td class="text-center"><?= $display[0] ?></td>
		<td><?= $display[1] ?></td>
		<td class="text-end"><?= $display[2] ?></td>
		<td class="text-end"><?= $display[3] ?></td>
		<td class="text-end"><?= $display[4] ?></td>
		<td><?= $display[5] ?></td>
		<td><?= $display[6] ?></td>
		</tr>

<?php endforeach; ?>
	</tbody>
</table>


</div>
</div>

<?php end_page(); ?>
