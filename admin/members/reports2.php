<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
array_push($allowGroups, 'ROmembers', 'trainee');
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';
include 'includes/tools.php';


// Ensure we came from a POST.
requirePOST();

$spreadsheet = new PhpOffice\PhpSpreadsheet\Spreadsheet();

// Report format
if (! empty($_POST['format'])) {
	$format = $_POST['format'];
} else {
	$format = 'none';
}
if ($format == 'xlsx') {
	$writer = new PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
	$ext = '.xlsx';
	$ct = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
} elseif ($format == 'ods') {
	$writer = new PhpOffice\PhpSpreadsheet\Writer\Ods($spreadsheet);
	$ext = '.ods';
	$ct = 'application/vnd.oasis.opendocument.spreadsheet';
} elseif ($format == 'html') {
	$writer = new PhpOffice\PhpSpreadsheet\Writer\Html($spreadsheet);
	$ext = '.html';
	$ct = 'text/html';
} else {
	die("You must select a format for the report.");
}

// Set Column widths auto.
foreach(array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M')
	as $col) {
	$spreadsheet->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
}

// Set header style.
$styleArray = [
	'font' => [
		'bold' => true,
	],
	'alignment' => [
		'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	],
];

// Get the database.
$db = (new Model(false))->db;

// Report type
if (! empty($_POST['report'])) {
	$type = $_POST['report'];
} else {
	$type = 'none';
}

if ($type == 'roster') {
	$fname = '/tmp/roster' . $ext;
	$arrayData = makeRosterArray($db);
	$spreadsheet->getActiveSheet()->getStyle('A1:L1')->applyFromArray($styleArray);
} elseif ($type == 'trails') {
	$fname = '/tmp/trails' . $ext;
	$arrayData = makeTrailsArray($db);
	$spreadsheet->getActiveSheet()->getStyle('A1:G1')->applyFromArray($styleArray);
} elseif ($type == 'since') {
	if (empty($_POST['since_d'])) {
		die("You must select a New Since date for the New Members report.");
	} elseif (! preg_match('/^\d{4}-\d{1,2}-\d{1,2}/', $_POST['since_d'])) {
		die('New Since date must be in form YYYY-MM-DD.');
	} elseif (!empty($_POST['end_d']) and ! preg_match('/^\d{4}-\d{1,2}-\d{1,2}/', $_POST['end_d'])) {
		die('New End date must be in form YYYY-MM-DD.');
	} else {
		$fname = '/tmp/newmembers' . $ext;
		$arrayData = makeNewMembersArray($db, $_POST['since_d'], $_POST['end_d']);
		$spreadsheet->getActiveSheet()->getStyle('A1:E1')->applyFromArray($styleArray);
	}
} elseif ($type == 'host') {
	if (! empty($_POST['since_d']) and ! preg_match('/^\d{4}/', $_POST['since_d'])) {
		die('Since must be empty or a four digit year.');
	} elseif (! empty($_POST['end_d']) and ! preg_match('/^\d{4}/', $_POST['end_d'])) {
		die('End must be empty or a four digit year.');
        } elseif (empty($_POST['lodge'])) {
		die('You must select a lodge for the Host Training report.');
	} else {
		$fname = '/tmp/host_training' . $ext;
		if (! empty($_POST['since_d'])) {
			$since_d = substr($_POST['since_d'], 0, 4);
		} else {
			$since_d = '0000';
		}
		if (! empty($_POST['end_d'])) {
			$end_d = substr($_POST['end_d'], 0, 4);
		} else {
			$end_d = '9999';
		}
		$arrayData = makeHostTrainingArray($db, $since_d, $end_d, $_POST['lodge']);
		$spreadsheet->getActiveSheet()->getStyle('A1:F1')->applyFromArray($styleArray);
	}
} else {
	die("You must select a report.");
}

$spreadsheet->getActiveSheet()->fromArray($arrayData, NULL, 'A1');
$writer->save($fname);
header("Content-Type: $ct");
if ($ext == ".html") {
	header('Content-Disposition: inline');
} else {
	header('Content-Disposition: attachment; filename="' . basename($fname) . '"');
}
header('Content-Transfer-Encoding: binary');
header("Content-Length: " . filesize($fname));
readfile($fname);

function makeRosterArray($db) {
	$cutoff = date('Y-m-d', time() - 3600*24*91); // now - 91 days grace
	$arrayData = [
		['LastName', 'FirstName', 'Email', 'Email', 'Phone H',
		'Phone C', 'Address', 'City', 'State', 'ZipCode',
		'Member Since', 'Skills Volunteered']
		];
	$rows = $db->select("members",
		["[>]memberships"=>"membershipID",
		"[>]volNotes"=>"memberID"],
		['last', 'first', 'email', 'phone', 'mobile',
		'street', 'city', 'state', 'zip',
		'memberships.created(msc)', 'members.created(mc)',
		'notes(skills)'],
		["AND"=>["expiration[>=]"=>$cutoff, "resigned"=>0,
			"class[!]"=>['0', 'OR', 'NM', '']],
		"ORDER"=>["last", "first"]]
		);
	foreach ($rows as $row) {
		if (is_null($row['msc'])) {
			$since = $row['mc'];
		} elseif (is_null($row['mc'])) {
			$since = $row['msc'];
		} else {
			$since = min($row['mc'], $row['msc']);
		}
		if (strlen($since) > 10) {
			$since = substr($since, 0, 10);
		}
		if (preg_match('/^.*_bouncing$/', $row['email'])) {
			$row['email'] = NULL;
		}
		if (is_null($row['email'])) {
			$hlink = NULL;
		} else {
			$hlink = '"mailto:' . $row['email'] . '","' .
				$row['email'] . '"';
			$hlink = "=HYPERLINK($hlink)";
		}
		$arrayData[] = [$row['last'], $row['first'], $row['email'],
			$hlink, format_phone($row['phone']),
			format_phone($row['mobile']),
			$row['street'], $row['city'], $row['state'],
			$row['zip'], $since, $row['skills']
			];
	}
	return $arrayData;
}

function makeTrailsArray($db) {
	$cutoff = date('Y-m-d', time() - 3600*24*91); // now - 91 days grace
	$arrayData = [
		['Name', 'Company', 'Address 1', 'Address 2', 'City', 'State',
                 'Zip']
	];
	$rows = $db->select('memberships',
		['membershipID', 'street', 'city', 'state', 'zip'],
		["AND"=>[
			"OR"=>["class"=>'NM', "expiration[>=]"=>$cutoff],
			"paperTrails"=>1,
			"resigned"=>0
			],
		"ORDER"=>"zip"]
		);
	foreach($rows as $row) {
		$mems = $db->select('members',
			['first', 'last'],
			['membershipID'=>$row['membershipID']]
			);
		if (count($mems) == 1) {
			$names = $mems[0]['first'] . ' ' . $mems[0]['last'];
		} elseif (count($mems) == 2) {
			if ($mems[0]['last'] == $mems[1]['last']) {
				$names = $mems[0]['first'] . ' & ' . $mems[1]['first']
					. ' ' . $mems[0]['last'];
			} else {
				$names = $mems[0]['first'] . ' ' . $mems[0]['last'] .
					' & ' . $mems[1]['first'] . ' ' . $mems[1]['last'];
			}
		} else {
			$names = 'CAC Members';
		}
		$addrs = explode(";", $row['street']);
		if (count($addrs) == 1) {
			$company = '';
			$addr2 = '';
			$addr1 = $addrs[0];
		} elseif (count($addrs) == 2) {
			$company = trim($addrs[0]);
			$addr2 = '';
			$addr1 = trim($addrs[1]);
		} elseif (count($addrs) == 3) {
			$company = trim($addrs[0]);
			$addr1 = trim($addrs[1]);
			$addr2 = trim($addrs[2]);
		} else {    // Bad data
			$company = '';
                        $addr2 = '';
                        $addr1 = $row['street'];
		}
		$arrayData[] = [$names, $company, $addr1, $addr2, $row['city'],
				$row['state'], $row['zip']];
	}
	return $arrayData;
}

function makeNewMembersArray($db, $since, $end) {
	$cutoff = date('Y-m-d', time() - 3600*24*91); // now - 91 days grace
	if (empty($end)) {
		$end = '9999-01-01';
	}
	$arrayData = [
		['LastName', 'FirstName', 'Email', 'Phone H', 'Phone C']
	];
	if ( $_POST['expired'] == 'include' ) {
		$rows = $db->select("members",
			["[>]memberships"=>"membershipID"],
			['last', 'first', 'email', 'phone', 'mobile'],
			["AND"=>[
				"class[!]"=>['0', 'OR', 'NM', ''],
				"OR"=>["members.created[<>]"=>[$since, $end], "memberships.created[<>]"=>[$since, $end]],
				],
			"ORDER"=>["last", "first"]]
			);
	} else {
		$rows = $db->select("members",
			["[>]memberships"=>"membershipID"],
			['last', 'first', 'email', 'phone', 'mobile'],
			["AND"=>[
				"expiration[>=]"=>$cutoff,
				"class[!]"=>['0', 'OR', ''],
				"OR"=>["members.created[<>]"=>[$since, $end], "memberships.created[<>]"=>[$since, $end]],
				],
			"ORDER"=>["last", "first"]]
			);
	}
	foreach ($rows as $row) {
		$arrayData[] = [$row['last'], $row['first'], $row['email'],
			format_phone($row['phone']),
			format_phone($row['mobile'])];
	}
	return $arrayData;
}

function makeHostTrainingArray($db, $since, $end, $lodge) {
	$cutoff = date('Y-m-d', time() - 3600*24*91); // now - 91 days grace
	if ($lodge == 'alpine') {
		$lodge = 'A';
	} elseif ($lodge == 'echo') {
		$lodge = 'E';
	} else {
		die('Trainee report, lodge not alpine or echo.');
	}
	$arrayData = [
		['LastName', 'FirstName', 'Email', 'Phone H', 'Phone C', 'Training Year']
	];
	if ( $_POST['expired'] == 'include' ) {
		$rows = $db->select("trainees",
			["[>]members"=>"memberID", "[>]memberships"=>"membershipID"],
			['last', 'first', 'email', 'phone', 'mobile', 'year'],
			["AND"=>[
				"class[!]"=>['0', 'OR', 'NM', ''],
				"year[<>]"=>[$since, $end],
				"lodge"=>$lodge,
				],
			"ORDER"=>["last", "first", "year"]]
			);
	} else {
		$rows = $db->select("trainees",
			["[>]members"=>"memberID", "[>]memberships"=>"membershipID"],
			['last', 'first', 'email', 'phone', 'mobile', 'year'],
			["AND"=>[
				"expiration[>=]"=>$cutoff,
				"class[!]"=>['0', 'OR', ''],
				"year[<>]"=>[$since, $end],
				"lodge"=>$lodge,
				],
			"ORDER"=>["last", "first", "year"]]
			);
	}
	foreach ($rows as $row) {
		$arrayData[] = [$row['last'], $row['first'], $row['email'],
			format_phone($row['phone']),
			format_phone($row['mobile']),
			$row['year']];
	}
	return $arrayData;
}

function format_phone($phone) {
	if (strlen($phone) == 10) {
		$phone = substr($phone, 0, 3) . '-' . substr($phone, 3, 3) .
			'-' . substr($phone, 6, 4);
	}
	return $phone;
}
?>
