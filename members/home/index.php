<?php
require '../support/config.php';
require CLASSLOADER;

require AUTH_PAGE; // enforce login

// if you want this page to have a custom title, set it here.
// leave it blank to use the default title for the site.
// this is the text that appears on the page tab in most browsers.
$title = '';
$description = 'California Alpine Club Member Services';

require_once '../../support/functions.php';

start_page($title, $description);
start_content();
?>

<div class="row">
	<div class="col-xl-10 offset-xl-1">

	<h2 class="mt-4">Member Services home page</h2>

	<p>Welcome, you are logged in as <b><?= $_SESSION['cac_login']['name'] ?></b>.</p>

	<p>To update or renew your membership, click ACCOUNT in the orange bar above.</p>

	<p>To update your volunteer interests, click VOLUNTEER in the orange bar above.</p>

	<h4>Other Useful Links</h4>

	<p><a href="<?= home_url() ?>" target="_blank">Public CAC website</a> (in a new tab)</p>

	</div>
</div>
<?php end_page(); ?>
