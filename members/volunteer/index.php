<?php
require '../support/config.php';
require CLASSLOADER;

require AUTH_PAGE; // enforce login

$title = 'CAC Member';
$pageDescription = 'Volunteer Interests';

require '../../support/functions.php';

// logged in user
$member = new Members('login', 'membership');
if (empty($member->memberID)) {
	error('Error', 'Did not find the logged-in member.');
	exit;
}

// include the member and the joint member, if any
$memberArray = $member->membership->getJointMembers();

// make an array of opportunities (jobs) assigned to each member
// ie: $jobs(memberID) = array(3,5,9);
$jobs = array();
$vol = new Volunteers();
foreach ($memberArray as $oMember) {
	$jobs[$oMember->memberID] = array();

	$memOpps = $vol->getByMemberID($oMember->memberID);
	foreach ($memOpps as $oOpp) {
		$jobs[$oMember->memberID][] = $oOpp->oppID;
	}
}

$volCat = new VolCats();
$catArray = $volCat->getAll();


start_page($title, $pageDescription);
?>
<script>
"use strict";

$(function() {
    // do stuff when DOM is ready

     $(".pop").popover({trigger: 'hover'}); // info popups

	$("input.cbJob").change(function() {
		var memberID = $(this).data("member");
		var rowID = "#" + $(this).parents("tr").attr("id");
		var oppID = rowID.replace("#opp_", "");
		var check = 0;
		var myCB = $(this);
		if (myCB.prop("checked")) check = 1;
		$.post( "ajax/volEdit.php", {memberID: memberID, oppID: oppID, check: check}, function(data) {
			if (data.success == 0) {
				myCB.prop("checked", ! myCB.prop("checked")); // undo the change
			}
			alert(data.msg);
		}, 'json');
	});

	$("button.btnSave").click(function() {
		var textarea = $(this).siblings("textarea");
		var memberID = textarea.data("member");
		var notes = textarea.val();
		$.post("ajax/saveNotes.php", {memberID: memberID, notes: notes}, function(data) {
			alert(data.msg);
		}, 'json');
		$(this).blur();
	});

	$("button.btnDelete").click(function() {
		var textarea = $(this).siblings("textarea");
		var memberID = textarea.data("member");
		textarea.val("");
		$.post("ajax/saveNotes.php", {memberID: memberID, delete: 1}, function(data) {
			alert(data.msg);
		}, 'json');
		$(this).blur();
	});

});

</script>
<?php
start_content();
?>
<h3 class="pageheader mb-3">Volunteer Opportunities</h3>

<div class="row justify-content-center">
	<div class=" col-md-10 col-lg-8 col-xl-6">

	<div class="alert alert-info">
		<p>As a CAC member, I agree to participate in one or more volunteer opportunities in
		the coming year.</p>

		<p>For example, I may coordinate a Club event, host at Alpine or Echo Lodge, cook or
		clean-up at CAC Club events at Alpine Lodge, edit our TRAILS newsletter, proofread CAC
		documents, fill a leadership position, join a committee, or lead a hike or outing.</p>
	</div>
	<div class="alert alert-success">
		Please enter or update your volunteer skills and interests below. Write in any
		clarifications or comments at the bottom.
	</div>

<?php
$volOpp = new VolOpps();

foreach ($catArray as $oCat):
	$oppArray = $volOpp->getByCategory($oCat->catID);
?>
<h4><?= $oCat->catName ?></h4>

<table class="table table-striped table-sm">
	<tbody>
		<tr>
			<td></td>
			<td style="min-width: 250px"></td>
<?php
foreach ($memberArray as $oMember):
?>
			<td class="text-center"><a href="../account/index.php"><?= $oMember->first ?></a></td>
<?php
endforeach;
?>
		</tr>
<?php
	foreach ($oppArray as $oVolOpp):
?>
		<tr id="opp_<?= $oVolOpp->oppID ?>">
			<td class="text-center" style="min-width: 28px">
<?php
		if ($oVolOpp->description) {
			Show_Info($oVolOpp->oppName, $oVolOpp->description);
		}
?>
			</td>
			<td><?= $oVolOpp->oppName ?></td>
<?php
		foreach ($memberArray as $oMember):
			$checkit = false;
			if (in_array($oVolOpp->oppID, $jobs[$oMember->memberID])) $checkit = true;
?>
			<td class="text-center"><input type="checkbox" class="cbJob"
				data-member="<?= $oMember->memberID ?>"<?php if ($checkit) echo ' checked'?>></td>
<?php
		endforeach;
?>
		</tr>
<?php
	endforeach;
?>
	</tbody>
</table>

<?php
endforeach;

foreach ($memberArray as $oMember):
	$volNotes = new VolNotes($oMember->memberID);
?>

<div class="comments">
	<h4>Other Skills, Interests, and Comments - <?= $oMember->first ?></h4>

	<button type="button" class="btnSave btn btn-secondary btn-sm mb-2">
		Save Comments for <?= $oMember->first ?>
	</button>
	<button type="button" class="btnDelete btn btn-secondary btn-sm mb-2">
		Delete
	</button>

<textarea class="form-control mb-3" rows="4" placeholder="optional" data-member="<?= $oMember->memberID ?>">
<?= $volNotes->notes ?>
</textarea>
</div>
<?php
endforeach; ?>

	</div>
</div>

<?php end_page(); ?>
