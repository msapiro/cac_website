<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';
require '../../support/formFunctions.php';

include 'includes/tools.php';


$model = new Model(false);
$db = $model->db;

$offset = inty($_REQUEST['offset']);
$rowsPerPage = 30;
$thisPage = $_SERVER["PHP_SELF"];

$numEntries = $db->count("authLog");
$numPages = ceil($numEntries / $rowsPerPage);

$prevURL = false;
$prev = $offset + 1;
if ($prev < $numPages) $prevURL = $thisPage . '?offset=' . $prev;

$nextURL = false;
$next = $offset - 1;
if ($next >= 0) $nextURL= $thisPage . '?offset=' . $next;

$startRow = $offset * $rowsPerPage;

$rows = $db->select("authLog", ["[>]members" => "memberID"], [
		"logTime", "memberID", "event", "page",
		"user" => Medoo\Medoo::raw("CONCAT(<first>, ' ', <last>)")
	],[
		"ORDER" => ["logID" => "DESC"],
		"LIMIT" => [$startRow, $rowsPerPage]
	]);

start_page('CAC Authentication');
start_content();
toolbar();
?>

<div class="row">
<div class="col-xl-10 offset-xl-1">

<div class="row">
	<div class="col-sm-6 col-md-5 col-lg-4 col-xl-3 mt-2">
		<div class="btn-group">
<?php if ($prevURL): ?>
			<a class="btn btn-secondary" href="<?= $prevURL ?>"><?php add_icon('chevron-left') ?> Previous</a>
<?php else: ?>
			<a class="btn btn-secondary disabled" href="#"><?php add_icon('chevron-left') ?> Previous</a>
<?php endif;
if ($nextURL): ?>
			<a class="btn btn-secondary" href="<?= $nextURL ?>">Next <?php add_icon('chevron-right') ?></a>
			<a class="btn btn-secondary" href="<?= $thisPage ?>"><?php add_icon('step-forward') ?> </a>
<?php else: ?>
			<a class="btn btn-secondary disabled" href="#">Next <?php add_icon('chevron-right') ?></a>
<?php endif; ?>
		</div>
	</div>

	<div class="col-sm-6">
		<h3 class="pageheader">Authentication Log</h3>
	</div>
</div> <!-- end row -->


<table class="table table-sm table-striped">
<thead>
	<tr><th style="min-width: 175px">Time</th><th>User</th><th>Event</th><th>Request</th></tr>
</thead>
<tbody>
<?php
foreach($rows as $row):
	$user = '';
	if ($row['memberID']) $user = '<a href="../members/detail.php?id=' . $row['memberID'] . '">' . $row['user'] . '</a>';
?>

	<tr>
		<td><?= $row['logTime'] ?></td>
		<td><?= $user ?></td>
		<td><?= $row['event'] ?></td>
		<td><?= $row['page'] ?></td>
	</tr>
<?php  endforeach; ?>
</tbody>
</table>

<div class="row">
	<div class="col-sm-6 col-md-5 col-lg-4 col-xl-3 mt-2">
		<div class="btn-group">
<?php if ($prevURL): ?>
			<a class="btn btn-secondary" href="<?= $prevURL ?>"><?php add_icon('chevron-left') ?> Previous</a>
<?php else: ?>
			<a class="btn btn-secondary disabled" href="#"><?php add_icon('chevron-left') ?> Previous</a>
<?php endif;
if ($nextURL): ?>
			<a class="btn btn-secondary" href="<?= $nextURL ?>">Next <?php add_icon('chevron-right') ?></a>
			<a class="btn btn-secondary" href="<?= $thisPage ?>"><?php add_icon('step-forward') ?> </a>
<?php else: ?>
			<a class="btn btn-secondary disabled" href="#">Next <?php add_icon('chevron-right') ?></a>
<?php endif; ?>
		</div>
	</div>
</div> <!-- end row -->

</div>
</div>
<?php end_page(); ?>
