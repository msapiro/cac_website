<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)

if (isset($allowGroups)) {
	require AUTH_PAGE; // make sure user is logged in and a group member
}

require_once '../../../wp-load.php'; // need the WP user functions
require '../../support/functions.php';


// copy members and memberships info from CAC database into new alpine database

$proceed = booly($_GET['proceed']);

start_page('CAC Migration');
start_content();

if (! $proceed):
?>
<div class="alert alert-danger mt-4">
<p><b>WARNING</b>: This will ERASE the contents of the tables alpine.members and alpine.memberships!<br>
<p>Then it will import member data from CAC.member and CAC.membership.</p>
<p>It will also link Mark Sapiro's CAC account to his WordPress account and set an expiration date so he can log in.<br>
Finally it will set up a temporary CAC account for Mark Nienberg and link it to his WordPress account for testing.</p>
</div>

<a class="btn btn-secondary" href="<?= $_SERVER['PHP_SELF'] . '?proceed=1' ?>">Continue</a>

<?php
	end_page();
	exit;
endif;

// set up a read-only connection to the CAC database
require_once '/www/cac/data/credentials/migrate.php';
$db_m = new Medoo\Medoo($db_migrate);
?>

<p class="mt-4">Truncating tables alpine.members and alpine.memberships</p>

<?php
// clear our new tables in case of previously imported data
$model = new Model();
$db = $model->db;
$db->query("TRUNCATE TABLE <members>");
$db->query("TRUNCATE TABLE <memberships>");

// import members
$rows = $db_m->select("member",
	["membID(memberID)", "membershipID", "first", "m_i", "last", "dob", "email", "phone", "cell(mobile)", "membVoted(created)"],
	["ORDER" => "membID"]
	);

$numMembers = count($rows);

foreach($rows as $row) {

	$row['phone'] = preg_replace('/\D/', '', $row['phone']);  // remove nondigits
	$row['mobile'] = preg_replace('/\D/', '', $row['mobile']);

	$dataArray = [
		'memberID' => $row['memberID'],
		'membershipID' => $row['membershipID'],
		'first' => $row['first'],
		'm_i' => nully( $row['m_i']),
		'last' => $row['last'],
		'email' => nully($row['email']),
		'phone' => nully( $row['phone']),
		'mobile' => nully( $row['mobile']),
		'dob' => nully($row['dob']),
		'created' => nully($row['created'])
	];

	$db->insert('members', $dataArray);
}

// import memberships
$rows = $db_m->select("membership",
	["membershipID", "street", "city", "state", "zip", "cls(class)", "sponsors",
		"entrancePaid(created)", "sendTrails(paperTrails)", "resigned", "resignedDate"],
	["ORDER" => "membershipID"]
	);

$numMemberships = count($rows);

foreach ($rows as $row) {

	if ($row['resignedDate'] == '0000-00-00') $row['resignedDate'] = NULL;

	$dataArray = [
			'membershipID' => $row['membershipID'],
			'class' =>  $row['class'],
			'paperTrails' => $row['paperTrails'],
			'street' => ucwords(strtolower($row['street'])),
			'city' =>  ucwords(strtolower($row['city'])),
			'state' => strtoupper($row['state']),
			'zip' => $row['zip'],
			'sponsors' => nully($row['sponsors']),
			'resigned' => $row['resigned'],
			'resignedDate' => nully($row['resignedDate']),
			'created' => nully($row['created'])
	];

	$db->insert('memberships', $dataArray);
}

// make sure Mark Sapiro can log on
$wpuser = get_user_by('email','mark@msapiro.net');

if ($wpuser) {
	$member = new Members();
	$memberID = $member->db->get('members', 'memberID', ['email' => 'mark@msapiro.net']);

	if ($memberID) {
		$member->getByID($memberID, 'membership');
		if (! empty($member->memberID)) {
			$member->wpID = $wpuser->ID;
			$member->dbSave();
			if ($member->membership->expired) {
				$member->membership->expiration = '2022-03-31';
				$member->membership->dbSave();
			}
			$msgSapiro = 'Set wpID and expiration for Mark Sapiro so he can log on.';

			$isAdmin = $member->db->has("authGroups", ["memberID" => $memberID, "privGroup" => "admin"]);
			if ($isAdmin) {
				$msgSapiro .= '<br>Mark Sapiro already has admin privileges assigned.';
			} else {
				$member->db->insert("authGroups", ["memberID" => $memberID, "privGroup" => "admin"]);
				$msgSapiro .= '<br>Assigned admin privileges for Mark Sapiro.';
			}
		}
	} else {
		$msgSapiro = 'Could not find a CAC account for Mark Sapiro.';
	}
} else {
	$msgSapiro = 'Could not find a WordPress account for Mark Sapiro.';
}


// make a member and membership for Mark Nienberg so he can log on to test
$wpID = false;
$wpuser = get_user_by('email','mark.nienberg@gmail.com');
if($wpuser) $wpID = $wpuser->ID;

if ($wpID) {
	$member = new Members();
	$memberID = $member->db->get('members', 'memberID', ['email' => 'mark.nienberg@gmail.com']);

	if ($memberID) {
		$member->getByID($memberID, 'membership');
		if (! empty($member->memberID)) {
			$member->wpID = $wpID;
			$member->dbSave();
			if ($member->membership->expired) {
				$member->membership->expiration = '2022-03-31';
				$member->membership->dbSave();
			}
			$msgNienberg = 'Set wpID and expiration for member Mark Nienberg so he can log on.';
		}
	} else {
		// make a new membership with membershipID=1, if it doesn't exist
		$membership = new Memberships(1);
		if ($membership->membershipID == 1) {
			$msgNienberg = 'membershipID=1 already exists. Did not create a CAC member for Mark Nienberg';
		} else {

			$dataArray = [
				'memberID' => 1,
				'membershipID' => 1,
				'wpID' => $wpID,
				'first' => 'Mark',
				'last' => 'Nienberg',
				'email' =>'mark.nienberg@gmail.com'
			];

			$db->insert('members', $dataArray);

			$dataArray = [
					'membershipID' => 1,
					'class' =>  'RG',
					'street' =>  '297 Berkeley Park Blvd',
					'city' =>  'Kensington',
					'state' => 'CA',
					'zip' => '94707',
					'expiration' => '2022-03-31'
			];

			$db->insert('memberships', $dataArray);

			$msgNienberg = 'Created memberID=1 and membershipID=1 assigned to Mark Nienberg so he can log on to test.';
		}
	}

	$isAdmin = $member->db->has("authGroups", ["memberID" => 1, "privGroup" => "admin"]);
	if ($isAdmin) {
		$msgNienberg .= '<br>Mark Nienberg already has admin privileges assigned.';
	} else {
		$member->db->insert("authGroups", ["memberID" => 1, "privGroup" => "admin"]);
		$msgNienberg .= '<br>Assigned admin privileges for Mark Nienberg.';
	}
} else {
	$msgNienberg = 'Could not find a WordPress account for Mark Nienberg.';
}
?>

<p>Copied <?= $numMembers ?> members from old CAC database.</p>
<p>Copied <?= $numMemberships ?> memberships from old CAC database.</p>
<p><?= $msgSapiro ?></p>
<p><?= $msgNienberg ?></p>
<p>Done.</p>

<p><a href="index.php">Setup and Migration</a></p>

<?php end_page(); ?>
