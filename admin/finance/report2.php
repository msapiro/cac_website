<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';
include 'includes/tools.php';


// Ensure we came from a POST.
requirePOST();

$spreadsheet = new PhpOffice\PhpSpreadsheet\Spreadsheet();

// Report format
if (! empty($_POST['format'])) {
        $format = $_POST['format'];
} else {
        $format = 'none';
}
if ($format == 'xlsx') {
	$writer = new PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
	$ext = '.xlsx';
	$ct = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
} elseif ($format == 'ods') {
	$writer = new PhpOffice\PhpSpreadsheet\Writer\Ods($spreadsheet);
	$ext = '.ods';
	$ct = 'application/vnd.oasis.opendocument.spreadsheet';
} elseif ($format == 'html') {
	$writer = new PhpOffice\PhpSpreadsheet\Writer\Html($spreadsheet);
	$ext = '.html';
	$ct = 'text/html';
} else {
	die("You must select a format for the report.");
}

// Set Column widths auto.
foreach(array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M')
	as $col) {
	$spreadsheet->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
}
// Set number format for currency columns.
foreach(array('K', 'L', 'M') as $col) {
	$spreadsheet->getActiveSheet()->getStyle($col)->getNumberFormat()
		->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::
		FORMAT_CURRENCY_USD_SIMPLE);
}
// Set header style.
$styleArray = [
	'font' => [
		'bold' => true,
	],
	'alignment' => [
		'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	],
];

// Get the database.
$db = (new Model(false))->db;

if (empty($_POST['rpt_year'])) {
	die("You must select a year for the report.");
} elseif (! preg_match('/^\d{2,4}$/', $_POST['rpt_year'])) {
	die('Year must be 2 or 4 digits.');
} else {
	$fname = '/tmp/cacfreport' . $ext;
	$arrayData = makeCACFReport($db, $_POST['rpt_year']);
	$spreadsheet->getActiveSheet()->getStyle('A1:M1')->applyFromArray($styleArray);
}

$spreadsheet->getActiveSheet()->fromArray($arrayData, NULL, 'A1');
$writer->save($fname);
header("Content-Type: $ct");
if ($ext == ".html") {
	header('Content-Disposition: inline');
} else {
	header('Content-Disposition: attachment; filename="' . basename($fname) . '"');
}
header('Content-Transfer-Encoding: binary');
header("Content-Length: " . filesize($fname));
readfile($fname);

function makeCACFReport($db, $year) {
	if (strlen($year) == 2) {
		$year = '20' . $year;
	} elseif (strlen($year) == 3) {
		$year = '2' . $year;
	}
	$ymin = $year . '-01-01';
        $ymax = $year . '-12-31';
	$arrayData = [
		['LastName', 'FirstName', 'Email', 'Phone H',
		'Phone C', 'Address', 'City', 'State', 'ZipCode',
		'Date', 'Donation', 'Stripe Fee', 'Net']
		];
	$rows = $db->select("donations",
		["[>]members"=>"memberID", "[>]memberships"=>"membershipID"],
		['last', 'first', 'email', 'phone', 'mobile',
		'street', 'city', 'state', 'zip', 'amount', 'stripeID',
		'pmtID', 'class', 'memberID', 'membershipID'],
		["type"=>"fdn", "ORDER"=>"donationID"]
		);
	$myarray = array();
	$dtot = $ftot = $ntot = 0.0;
	foreach ($rows as $row) {
		if ($row['stripeID'] != 0) {
			$stripe = $db->select("stripe",
				['created'],
				['AND'=>['created[<>]'=>[$ymin, $ymax],
					'status'=>'ok',
					'stripeID'=>$row['stripeID']]],
				);
			if ($stripe and $row['amount'] != 0) {
				if (in_array($row['class'], array('JT', 'JL', 'JS'))) {
					$data = other_joint($db,
							$row['membershipID'],
							$row['memberID']);
					if ($data) {
						$myarray[] = $data;
					}
				}
				$myarray[] = [$row['last'], $row['first'],
					$row['email'],
					format_phone($row['phone']),
					format_phone($row['mobile']),
					$row['street'], $row['city'],
					$row['state'], $row['zip'],
					get_date($stripe[0]['created']),
					$row['amount'],
					round($row['amount'] * 0.029, 2),
					$row['amount'] - round($row['amount'] * 0.029, 2)];
				$dtot += $row['amount'];
				$ftot += round($row['amount'] * 0.029, 2);
				$ntot += $row['amount'] - round($row['amount'] * 0.029, 2);
			}
		} elseif ($row['pmtID'] != 0) {
			$payment = $db->select("payments",
				['created'],
				['AND'=>['created[<>]'=>[$ymin, $ymax],
					'pmtID'=>$row['pmtID']]],
				);
			if ($payment and $row['amount'] != 0) {
				if (in_array($row['class'], array('JT', 'JL', 'JS'))) {
					$data = other_joint($db,
							$row['membershipID'],
							$row['memberID']);
					if ($data) {
						$myarray[] = $data;
					}
				}
				$myarray[] = [$row['last'], $row['first'],
					$row['email'],
					format_phone($row['phone']),
					format_phone($row['mobile']),
					$row['street'], $row['city'],
					$row['state'], $row['zip'],
					get_date($payment[0]['created']),
					$row['amount'], '0.00',
					$row['amount']];
				$dtot += $row['amount'];
				$ntot += $row['amount'];
			}
		}
	}
        $myarray[] = ['', '', '', '', '', '', '', '', '', 'Totals', $dtot,
			$ftot, $ntot];
	return array_merge($arrayData, $myarray);
}

function format_phone($phone) {
	if (strlen($phone) == 10) {
		$phone = substr($phone, 0, 3) . '-' . substr($phone, 3, 3) .
			'-' . substr($phone, 6, 4);
	}
	return $phone;
}

function get_date($date) {
	return preg_replace('/ \d{2}:\d{2}:\d{2}/', '', $date);
}

function other_joint($db, $membershipID, $memberID) {
	// Returns data for the other member of a joint membership.
	$rows = $db->select("members",
		['last', 'first', 'email', 'phone', 'mobile'],
		["AND"=>["membershipID"=>$membershipID,
			"memberID[!]"=>$memberID]]);
	if (count($rows) == 1) {
		return [$rows[0]['last'], $rows[0]['first'], $rows[0]['email'],
			format_phone($rows[0]['phone']),
			format_phone($rows[0]['mobile']),
			"(joint w/following)"];
	} else {
	// If there's not just one row, ignore it.
		return false;
	}
}
?>
