<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';
require '../../support/formFunctions.php';

include 'includes/tools.php';


$img = new Images();
$imageArray = $img->getAll();

start_page('CAC Images');
add_script('jqueryui');
?>
<script>

$(function() {
    // do stuff when DOM is ready

	$('.tip').tooltip({placement: 'bottom'});

    $("#sortList").sortable({
        handle: '.handle'
	});
	$("#sortList").disableSelection();

    var copySupported = document.queryCommandSupported("copy");
    if (! copySupported) { $(".btnCopy").hide(); }

});

function toClipboard(text) {
	$("#clipper").val(text).show().select();
	document.execCommand("cut"); // only works on input element
	$("#clipper").blur().hide();
	alert("Copied path to clipboard:\n\n " + text);
}

</script>
<style>

#sortList {
	list-style-type: none;
	margin: 0px;
	padding: 0px;
	}

#sortList li {
	float: left;
	display: block;
	padding: 2px;
	background-color: rgb(248,248,248);
	border-style: solid;
	border-color: black;
	border-width: 1px;
	}

#sortList li img {
	cursor: move;
	max-width: 250px;
}
</style>
<?php
start_content();
toolbar();
?>

<h3 class="pageheader">Image Gallery</h3>

<?php
if (count($imageArray) == 0): ?>
	<p>No images uploaded yet.</p>
<?php
else: ?>

<ul id="sortList" class="mt-3">
<?php
	foreach ($imageArray as $image):
		$URLdetail = 'detail.php?id=' . $image->imageID;
		$URLedit = 'edit.php?id=' . $image->imageID;
		$file = $image->imageID . '.' . $image->ext;
?>
	<li>
		<a class="tip" style="margin-left: 2px; margin-right: 2px" title="Detail" href="<?= $URLdetail ?>"><?php add_icon('list-alt') ?></a>
		<a class="tip" style="margin-right: 2px" title="Edit" href="<?= $URLedit ?>"><?php add_icon('edit')?></a>
		<a class="btnCopy tip" href="#" style="margin-right: 2px" title="Copy relative path to clipboard" onclick="toClipboard('<?= $image->fileURL ?>')"><?php add_icon('copy') ?></a>
		&nbsp;<?= $file ?> [<?= $image->width ?>w x <?= $image->height ?>h]
		<br><img class="handle" title="<?= $image->name ?>" src="<?= $image->thumbURL ?>">
	</li>
<?php
	endforeach; ?>
</ul>
<?php
endif; ?>

<!-- used for clipboard copy -->
<input type="text" id="clipper" style="display: none">

<?php end_page(); ?>
