<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';

// Ensure we came from a POST.
requirePOST();

$spreadsheet = new PhpOffice\PhpSpreadsheet\Spreadsheet();

// Report format
if (! empty($_POST['format'])) {
        $format = $_POST['format'];
} else {
        $format = 'none';
}
if ($format == 'xlsx') {
	$writer = new PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
	$ext = '.xlsx';
	$ct = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
} elseif ($format == 'ods') {
	$writer = new PhpOffice\PhpSpreadsheet\Writer\Ods($spreadsheet);
	$ext = '.ods';
	$ct = 'application/vnd.oasis.opendocument.spreadsheet';
} elseif ($format == 'html') {
	$writer = new PhpOffice\PhpSpreadsheet\Writer\Html($spreadsheet);
	$ext = '.html';
	$ct = 'text/html';
} else {
	die("You must select a format for the report.");
}

// Set Column widths auto.
foreach(array('A', 'B', 'C', 'D', 'E')
	as $col) {
	$spreadsheet->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
}
// Set header style.
$styleArray = [
	'font' => [
		'bold' => true,
	],
	'alignment' => [
		'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	],
];

if (empty($_POST['rpt_start'])) {
	die("You must select a start date for the report.");
} elseif (! preg_match('/^\d{4}-\d{2}-\d{2}$/', $_POST['rpt_start'])) {
	die('Date must be YYYY-MM-DD.');
} elseif (empty($_POST['rpt_end'])) {
        die("You must select a end date for the report.");
} elseif (! preg_match('/^\d{4}-\d{2}-\d{2}$/', $_POST['rpt_end'])) {
        die('Date must be YYYY-MM-DD.');

} else {
	$fname = '/tmp/eventreport' . $ext;
	$arrayData = makeEventReport($_POST['rpt_start'], $_POST['rpt_end']);
	$spreadsheet->getActiveSheet()->getStyle('A1:E1')->applyFromArray($styleArray);
}

$spreadsheet->getActiveSheet()->fromArray($arrayData, NULL, 'A1');
$writer->save($fname);
header("Content-Type: $ct");
if ($ext == ".html") {
	header('Content-Disposition: inline');
} else {
	header('Content-Disposition: attachment; filename="' . basename($fname) . '"');
}
header('Content-Transfer-Encoding: binary');
header("Content-Length: " . filesize($fname));
readfile($fname);

function makeEventReport($start, $end) {
	$arrayData = [
		['StartDate', 'EndDate', 'Title', 'Host', 'Email']
		];
	$query = tribe_events();
	if (! empty($_POST['category'])) {
		if ($_POST['category'] == 'alpine') {
			$query->where('category', 'alpine-lodge-events');
		} elseif ($_POST['category'] == 'echo') {
			$query->where('category', 'echo-summit-lodge-events');
		} elseif ($_POST['category'] == 'club') {
			$query->where('category', 'official-club-events');
		} elseif ($_POST['category'] == 'sun') {
			$query->where('category', 'sundays-at-alpine-lodge');
		} elseif ($_POST['category'] == 'fam') {
			$query->where('category', 'youth-and-families');
		} elseif ($_POST['category'] == 'cacf') {
			$query->where('category', 'cac-foundation-events');
		} elseif ($_POST['category'] == 'out') {
			$query->where('category', 'club-outings');
		}
	}
	$rows = $query
		->where( 'starts_after', $start . ' 00:00:00' )
		->where( 'ends_before', $end . ' 23:59:59' )
		->order_by( 'event_date', 'ASC' )
		->per_page( 999 )
		->all();
	$myarray = array();
	foreach ($rows as $row) {
		$myarray[] = array($row->start_date,
				$row->end_date,
				$row->post_title,
				tribe_get_organizer($row->ID),
				tribe_get_organizer_email($row->ID, false),
				);
	}
	return array_merge($arrayData, $myarray);
}
