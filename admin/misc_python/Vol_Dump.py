#! /usr/bin/python3
import pymysql
db=pymysql.connect(user='CAC_query', passwd='CAC_dbqry_1913', db='alpine')
c = db.cursor()
c.execute("""SELECT * FROM `volMembers` JOIN `volOpps` USING (oppID)
             JOIN members USING (memberID)""")
with open('Vol_Dump.csv', 'w') as fp:
    fp.write('memberID\toppID\tvolID\tcatID\toppName\tdescription\tfirst\t'
             'last\temail\tphone\tmobile\n')
    while True:
        rec = c.fetchone()
        if not rec:
            break
        memberID, oppID, volID, catID, oppName, description, sortorder, \
        membershipID, wpID, first, m_i, last, email, phone, mobile, dob, \
        created, modified = rec
        fp.write(f'{memberID}\t{oppID}\t{volID}\t{catID}\t{oppName}\t'
                 f'{description}\t{first}\t{last}\t{email}\t{phone}\t'
                 f'{mobile}\n')
c.close()
db.close()
