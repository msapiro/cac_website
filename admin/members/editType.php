<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

$title = 'CAC Membership';
$pageDescription = 'Edit Membership Type';

require '../../support/functions.php';
require '../../support/formFunctions.php';
include 'includes/tools.php';


$membershipType = nully($_GET['id']);

if ($membershipType) {
	// editing an existing type
	$mType = new Classes($membershipType);
	if (! $mType->class) {
		error('Error', "Did not find a Membership Type with ID=$membershipType");
		exit;
	}
	$pageTitle = 'Edit Membership Type';
} else {
	//creating a new type
	$mType = new Classes(); // blank object
	$pageTitle = 'New Membership Type';
}

start_page($title, $pageDescription);
?>
<script>
$(function() {
	// make nice tooltips for the buttons
	$('.tip').tooltip({container: 'body', placement: 'bottom'});

	$('#btnCancel').click(function() {
		window.open('types.php', '_self');
	});

});
</script>
<?php
start_content();
toolbar();
?>

<h2 class="pageheader"><?= $pageTitle ?></h2>

<div class="row">
	<div class="col-lg-10 offset-lg-1">

		<form method="post" action="editType2.php">

<?php
if ($membershipType) {
	echo ' <input type="hidden" name="class" value="' . $membershipType . '"> ';
	static_field('Type ID', $membershipType, false, false, 'col-2 col-md-1 col-lg-3', 'col-5');
} else {
	edit_field('Type ID', 'class', NULL, 2, 'Two characters', false, false, 'col-lg-3', 'col-7 col-md-6 col-lg-5');
}

edit_field('Description', 'description', $mType->description, 40, false, false, false, 'col-lg-3', 'col-7 col-md-6 col-lg-5');
?>
			<div class="row mb-3">
				<label class="text-lg-end col-lg-3"><b>Joint</b></label>
				<div class="col-lg-5">
					<div class="form-check">
					<input class="form-check-input" type="checkbox" value="1" name="joint"
						id="joint"<?php if ($mType->joint) echo ' checked'?>>
					<label class="form-check-label" for="joint">
						Two Members Allowed
					</label>
					</div>
				</div>
			</div>

<?php
edit_field_prepend('Dues', 'dues', '$', $mType->dues, 7, false, false, 'col-2 col-md-1 col-lg-3', 'col-5');
if ($mType->entryFee > 0) {
	$displayEntry = $mType->entryFee;
} else {
	$displayEntry = '';
}
edit_field_prepend('Entry Fee', 'entryFee', '$', $displayEntry, 7, false, false, 'col-2 col-md-1 col-lg-3', 'col-5');

?>

			<div class="row mb-3">
				<label class="text-lg-end col-lg-3"><b>Show Members</b></label>
				<div class="col-lg-5">
					<div class="form-check">
					<input class="form-check-input" type="checkbox" value="1" name="showUsers"
						id="showUsers"<?php if ($mType->showUsers) echo ' checked'?>>
					<label class="form-check-label" for="showUsers">
						Members can see and select
					</label>
					</div>
				</div>
			</div>
			<div class="row mb-3">
				<label class="text-lg-end col-lg-3"><b>Show Admins</b></label>
				<div class="col-lg-5">
					<div class="form-check">
					<input class="form-check-input" type="checkbox" value="1" name="showAdmin"
						id="showAdmin"<?php if ($mType->showAdmin) echo ' checked'?>>
					<label class="form-check-label" for="showAdmin">
						Administrators can see and select
					</label>
					</div>
				</div>
			</div>

			<div class="row">
				<label class="col-lg-3"></label>
				<div class="col-lg-5">
					<div class="btn-group">
						<button type="submit" class="btn btn-primary">Submit</button>
						<button type="reset" class="btn btn-secondary">Reset</button>
						<button id="btnCancel" type="button" class="btn btn-secondary">Cancel</button>
					</div>
				</div>
			</div>
		</form>

</div>
</div>
<?php end_page();
