<?php
// update the foundArray after a table re-sort

session_name('CACSESSID');
session_start();

$foundarray = $_POST['arrayname'];

// the post will be a comma separated string
$items = explode(',',$_POST['order']);

unset($_SESSION[$foundarray]);

foreach($items as $val) {
	$_SESSION[$foundarray][] = $val;
}
