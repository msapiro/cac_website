<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';

include 'includes/tools.php';

start_page('CACF Report');

start_content();
toolbar();
?>

<h3 class="pageheader">CACF Report</h3>

<div class="card well mt-3">
	<div class="card-body">

	<h4>Select Year and Format</h4>
	<form action="report2.php" method="post" id="reportForm">
		<input type="hidden" name="reporttype" value="standard">

		<div class="row align-items-center mb-3">
			<label class="text-md-end col-md-2 col-lg-3">
				<b>Report Year</b>
			</label>
			<div class="col-6 col-md-4">
				<input class="form-control" type="text" name="rpt_year" maxlength="4" placeholder="YYYY">
			</div>
		</div>

		<div class="row align-items-center mb-3">
			<label class="text-md-end col-md-2 col-lg-3">
				<b>Format</b>
			</label>
			<div class="col-sm-8">
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="format" id="rb_xlsx" value="xlsx">
					<label class="form-check-label" for="rb_xlsx">Excel</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="format" id="rb_ods" value="ods">
					<label class="form-check-label" for="rb_ods">Open Document</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="format" id="rb_html" value="html">
					<label class="form-check-label" for="rb_html">HTML</label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-5 offset-md-2 offset-lg-3">
				<button type="submit" class="btn btn-primary">Get Report</button>
			</div>
		</div>

	</form>
	</div>
</div>

<?php end_page(); ?>
