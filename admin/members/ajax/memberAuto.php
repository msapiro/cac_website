<?php
require '../../support/config.php';
require CLASSLOADER;

require '../includes/config.php'; // contains $allowGroups for this directory
array_push($allowGroups, 'ROmembers', 'trainee');
require AUTH_AJAX;                // make sure user is logged in and a group member

// creates an autocomplete list of members (includes expired) plus email,
// with the memberID returned as a third variable.


$db = (new Model(false))->db;

$lookFor = $_GET['term'];
$lookFor= '%' . preg_replace('/\s+/', '%', $lookFor) . '%';

// WHERE clause is too complicated for Medoo, so use Medoo as a wrapper for PDO
$query = "SELECT memberID, first, last, email FROM members WHERE CONCAT(first, ' ', last) LIKE :look OR email LIKE :look";
$stmt = $db->pdo->prepare($query);
$stmt->bindParam(':look', $lookFor, PDO::PARAM_STR);
$stmt->execute();
$rows = $stmt->fetchAll();

$return_arr = array();
foreach ($rows as $row) {
	$text = $row['first'] . ' ' . $row['last'] . ' <' . $row['email'] . '>';

	$row_array = array();
	$row_array['label'] = $text;
	$row_array['value'] = $text;
	$row_array['memberID'] = $row['memberID'];

	array_push($return_arr, $row_array);
}

echo json_encode($return_arr);
