<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
//array_push($allowGroups, 'ROmembers');
require AUTH_PAGE; // make sure user is logged in and a group member

$title = 'CAC Membership';
$description = 'Membership Application';

require_once '../../support/functions.php';
require_once '../../support/formFunctions.php';
include 'includes/tools.php';

$applicantID = inty($_GET['id']);

if (!$applicantID) {
	error('Error', 'You must submit an applicantID to identify the application.');
	exit();
}

$applicant = new Applicants($applicantID);

if (!$applicant->applicantID) {
	error('Error', 'Did not find an application with applicantID ' . $applicantID . '.');
	exit();
}

if (strpos($applicant->applyType, 're')) {
	$reinstate = true;
	$member = new Members($applicant->addToMemberID, 'membership');
} else {
	$reinstate = false;
}

if ($applicant->addToMemberID && !$reinstate) {
	$memberAddTo = new Members($applicant->addToMemberID, 'membership');
	$quick = $memberAddTo->first . ' ' . $memberAddTo->last . ' <' . $memberAddTo->email . '>';
} else {
	$quick = '';
}

if ($applicant->entryFee == 0.00) $applicant->entryFee = ''; // display blank
if ($applicant->dues == 0.00) $applicant->dues = '';

start_page($title, $description);
add_script('jqueryUI'); // for auto-complete
?>
<script>
"use strict";

var applyType = "<?= $applicant->applyType ?>";
var applicantID = "<?= $applicant->applicantID ?>";

$(function() {
    // do stuff when DOM is ready

    $(".pop").popover({trigger: 'hover'}); // info popups

	$("input[name='rbApplyType']").click(function() {
		var appType = $(this).val();
		if (appType == 'JT' || appType == 'JTre' || appType == 'JSre') {
			$("#jtApplicant").show();
		} else {
			$("#jtApplicant").hide();
		}
		if (appType == 'JTadd') {
			$("#jtMember").show();
			$("#divAddress").hide();
			$("#divNoAddress").show();
		} else {
			$("#jtMember").hide();
			$("#divAddress").show();
			$("#divNoAddress").hide();
		}
	});

	$("#" + applyType).click();
	$('#addToMemberID').prop('readonly', true);

	// prevent the enter key from submitting the form
	$('#formApply').on("keydown", ":input:not(textarea)", function(event) {
		if (event.key == "Enter") {
			event.preventDefault();
		}
	});

    $("#memberSearch").autocomplete({
		source: "ajax/memberAuto.php",
		minLength: 3,
		select: function(event, ui) {
			var memberID = ui.item.memberID;
			$("#addToMemberID").val(memberID);
			$("#addToMemberID").change();
        }
    });

    $("#addToMemberID").change(function() {
        var memberID = $(this).val();
    	$.post('ajax/memberJoint.php', {memberID: memberID, applicantID: applicantID}, function(data) {
    		if (data.success) {
    			$("#divError").hide();
    			$("#divSuccess").show();
    		} else {
    			$("#errMsg").html(data.msg);
    			$("#divSuccess").hide();
				$("#divError").show();
    		}
    	}, "json");
    });

    $("#btnClear").click(function() {
		$("#addToMemberID").val('');
		$("#memberSearch").val('').focus();
		$.post('ajax/memberJoint.php', {memberID: 0, applicantID: applicantID});
		$("#divError").hide();
		$("#divSuccess").show();
	});

});
</script>
<?php
start_content();
toolbar();
?>

<div class="row">
	<div class="col-xl-10 offset-xl-1">

		<h2 class="mt-4">
			Membership Application &nbsp;<a href="applyDetail.php?id=<?= $applicant->applicantID ?>">
			<small>applicantID=<?= $applicant->applicantID ?></small></a>
		</h2>

		<form id="formApply" action="applyEdit2.php" method="post" autocomplete="off">
			<input type="hidden" name="applicantID" value="<?= $applicant->applicantID ?>">


			<div class="row align-items-center mb-3">
				<label class="text-lg-end col-lg-3"> <b>Applying for</b></label>

				<div class="col-lg-5">
<?php
if (!$reinstate):
?>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="rbApplyType" value="RG" id="RG">
						<label class="form-check-label" for="RG"> Individual Membership </label>
					</div>

					<div class="form-check">
						<input class="form-check-input" type="radio" name="rbApplyType" value="JT" id="JT">
						<label class="form-check-label" for="JT"> Joint Membership (two new members) </label>
					</div>

					<div class="form-check">
						<input class="form-check-input" type="radio" name="rbApplyType" value="JTadd" id="JTadd">
						<label class="form-check-label" for="JTadd"> Joint Membership (add to existing member) </label>
					</div>

					<div class="form-check">
						<input class="form-check-input" type="radio" name="rbApplyType" value="ST" id="ST">
						<label class="form-check-label" for="ST"> Student Membership (age 18 to 26, on April 1) </label>
					</div>
<?php
else :
?>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="rbApplyType" value="RGre" id="RGre">
						<label class="form-check-label" for="RGre"> Reinstate Individual Membership </label>
					</div>
<?php
	if ($member->membership->joint) :
		// Joint and Joint Senior can reinstate as Joint JT ?>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="rbApplyType" value="JTre" id="JTre">
						<label class="form-check-label" for="JTre"> Reinstate Joint Membership </label>
					</div>
<?php
	endif;

	if ($member->membership->class == 'SR' || $member->membership->class == 'JS') :
		// allow senior member to reinstate as senior
		// also allow joint senior to reinstate as individual senior ?>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="rbApplyType" value="SRre" id="SRre">
						<label class="form-check-label" for="SRre"> Reinstate Senior Membership </label>
					</div>
<?php
	endif;

	if ($member->membership->class == 'JS') :
		// only Joint Senior can reinstate as Joint Senior JS ?>

					<div class="form-check">
						<input class="form-check-input" type="radio" name="rbApplyType" value="JSre" id="JSre">
						<label class="form-check-label" for="JSre"> Reinstate Senior Joint Membership </label>
					</div>
<?php
	endif;

	if ($member->membership->class == 'ST') :
		// only Student can reinstate as Student ?>

					<div class="form-check">
						<input class="form-check-input" type="radio" name="rbApplyType" value="STre" id="STre">
						<label class="form-check-label" for="STre"> Reinstate Student Membership (age 18 to 26,
							on April 1) </label>
					</div>
<?php
	endif;

endif;
?>
	</div>
			</div>

			<div id="jtMember">
<?php edit_field('Existing Member Name', 'addToMember', $applicant->addToMember, 100); ?>

	<div class="card mb-3">
					<h4 class="card-header bg-warning">
						Administrator - please match entry above to a current member
					</h4>
					<div class="card-body">

						<div class="row mb-3">
							<label class="text-lg-end col-lg-3">
								<b>Quick-Find Member</b>&nbsp;<?php Show_Info('Quick Find', getHelp('memberAuto')) ?>
							</label>
							<div class="col-lg-8">
								<div class="input-group">
									<button class="btn btn-secondary" type="button" id="btnClear">
										Clear
									</button>
									<input type="text" class="form-control" name="memberSearch" id="memberSearch"
										placeholder="Start typing name or email" value="<?= $quick ?>">
								</div>
							</div>
						</div>

<?php
edit_field('memberID', 'addToMemberID', $applicant->addToMemberID, 6, false, false, 'addToMemberID',
	'col-lg-3', 'col-3 col-md-2');
?>
		</div>
					<!-- end card body -->
				</div>
				<!-- end card -->

				<div id="divError" class="alert alert-danger" style="display: none">
					<p>
						<b>There is a problem.</b> Please resolve this before continuing with
						application review.
					</p>
					<p id="errMsg">
						<!-- ajax will fill -->
					</p>
				</div>
				<div id="divSuccess" class="alert alert-success" style="display: none">
					<p>
						<b>Success.</b>
					<p>

					<p>
						Your change to the current member has been saved.<br> You do not need to
						submit this form unless you have other changes.
					</p>
				</div>

			</div>
			<!-- end jtMember -->

			<h4>Applicant</h4>
<?php
edit_field('First Name', 'first', $applicant->first, 30);
edit_field('Middle Initial', 'm_i', $applicant->m_i, 20);
edit_field('Last Name', 'last', $applicant->last, 30);
static_field('Email', $applicant->email);
edit_field('Phone', 'phone', phone_number($applicant->phone), 20, 'no need to format');
edit_field('Mobile Phone', 'mobile', phone_number($applicant->mobile), 20, 'no need to format');
edit_field('Date of Birth', 'dob', $applicant->dob, 10, 'YYYY-MM-DD');
?>

<div id="jtApplicant">
				<h4>Joint Applicant</h4>
<?php
edit_field('First Name', 'first2', $applicant->first2, 30);
edit_field('Middle Initial', 'm_i2', $applicant->m_i2, 20);
edit_field('Last Name', 'last2', $applicant->last2, 30);
edit_field('Email', 'email2', $applicant->email2, 100);
edit_field('Phone', 'phone2', phone_number($applicant->phone2), 20, 'no need to format');
edit_field('Mobile Phone', 'mobile2', phone_number($applicant->mobile2), 20, 'no need to format');
edit_field('Date of Birth', 'dob2', $applicant->dob2, 10, 'YYYY-MM-DD');
?>
</div>

			<h4>Address</h4>
			<div id="divNoAddress" class="row">
				<div class="col-lg-8">
					<p>Address will be taken from the existing member in the joint membership.</p>
				</div>
			</div>
			<div id="divAddress">
<?php
edit_field('Street Address', 'street', $applicant->street, 120);
edit_field('City', 'city', $applicant->city, 50);
edit_field('State', 'state', $applicant->state, 2);
edit_field('Zip', 'zip', $applicant->zip, 10);
?>
</div>

			<h4>Sponsors</h4>
			<div class="row">
				<div class="col-lg-8">
					<p>List two current Alpine Club members who have agreed to sponsor your
						application. They may not be two parts of the same joint membership.</p>
				</div>
			</div>
<?php
edit_field('Sponsor #1', 'sponsor1', $applicant->sponsor1, 100);
edit_field('Sponsor #2', 'sponsor2', $applicant->sponsor2, 100);

if (! $reinstate): ?>
<h4>Work and Social Activities</h4>
			<div class="row">
				<div class="col-lg-8">
					<p>List two Alpine Club events (with dates) that you have attended. One event
						must be a &quot;work&quot; activity.</p>
				</div>
			</div>
<?php
	edit_field('Event #1', 'event1', $applicant->event1, 100);
	edit_field('Event #2', 'event2', $applicant->event2, 100);
endif;
?>

<h4>Additional Comments</h4>
			<div class="row">
				<div class="col-lg-8">
					<p>If you have paid fees as an associate member (probably as a guest at Echo
						Lodge), you can submit your receipts for a credit toward your entry fee
						(but not your dues). Indicate that here. Also provide any other information
						that might be helpful in processing your application.</p>
				</div>
			</div>
<?php
edit_textfield('Notes', 'notes', $applicant->notes, 4);
?>

			<div class="row mb-3">
				<div class="col-lg-5 offset-lg-3">
					<div class="btn-group">
						<button type="submit" class="btn btn-primary">Save</button>
						<a class="btn btn-secondary" href="applyDetail.php?id=<?= $applicantID ?>">
							Cancel
						</a>
					</div>
				</div>
			</div>

		</form>

	</div>
</div>
<?php end_page(); ?>
