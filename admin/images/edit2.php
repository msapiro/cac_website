<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';

// Ensure we came from a POST.
requirePOST();

$image = new Images($_POST['imageID']);

if (empty($image->imageID)) {
	error('ERROR', 'Did not find an image with imageID=' . $_POST['imageID']);
	exit;
}

$image->name = $_POST['name'];
$image->credit = $_POST['credit'];
$image->hide = booly($_POST['hide']); // checkbox
$image->notes = $_POST['notes'];

$image->dbSave();

header("Location: detail.php?id=$image->imageID");
