<?php

// Return some Global variables to their clean state after WordPress messes with them.
// https://developer.wordpress.org/reference/functions/add_magic_quotes/

// WordPress emulates the deprecated magic_quotes feature of PHP.
// If we don't do this then our sanitizing functions don't work properly.

function restoreGlobals() {
	$_GET    = strip_magic_quotes( $_GET );
	$_POST   = strip_magic_quotes( $_POST );
	$_COOKIE = strip_magic_quotes( $_COOKIE );
	$_SERVER = strip_magic_quotes( $_SERVER );
	$_REQUEST = array_merge( $_GET, $_POST );
}


function strip_magic_quotes( $array ) {
	foreach ( (array) $array as $k => $v ) {
		if ( is_array( $v ) ) {
			$array[ $k ] = strip_magic_quotes( $v );
		} elseif ( is_string( $v ) ) {
			$array[ $k ] = stripslashes( $v );
		} else {
			continue;
		}
	}
	return $array;
}
