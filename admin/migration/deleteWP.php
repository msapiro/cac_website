<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)

if (isset($allowGroups)) {
	require AUTH_PAGE; // make sure user is logged in and a group member
}

require_once '../../../wp-load.php';
require '../../support/functions.php';
// WP admin functions for user management
require ABSPATH . 'wp-admin/includes/user.php';

// Delete and unlink a WordPress user for all members with a wpID and with
// expiration before January 1 of the current year.

$proceed = booly($_GET['proceed']);

start_page('CAC Migration');
start_content();

if (! $proceed): ?>

<div class="alert alert-danger mt-4">
	<p><b>WARNING</b>: This will delete the wordpress user and the member's
	wpID for all members whose membership expired prior to the current year.
	</p>
	<p>You can run this script multiple times, but generally just once
	after the first of the year to delete those expired in the prior year.</p>
</div>

<a class="btn btn-secondary" href="<?= $_SERVER['PHP_SELF'] . '?proceed=1' ?>">Continue</a>

<?php
	end_page();
	exit;
endif;

$model = new Model();
$db = $model->db;

// Get webmaster's user id
$userdata = get_user_by("login", "webmaster");
if (empty($userdata)) {
	exit("Can't find wpID for webmaster.");
}
// Process expired users with wpID != 0
$cutoff = date('Y-m-d', mktime(0,0,0,1,1));
$rows = $db->select("members", ["[>]memberships"=>"membershipID"],
	['memberID', 'wpID', 'first', 'last'],
	["AND"=>["expiration[<]"=>$cutoff, "wpID[!]"=>0],
	"ORDER"=>"email"]
	);
echo '<pre>';
foreach ($rows as $row) {
	$member = new Members($row['memberID']);
	$deleteID = $row['wpID'];
	$deleteName = $row['first'] . " " . $row['last'];
	// Delete the WP user and assign posts =, etc to webmaster
	wp_delete_user($deleteID, $userdata->ID);
	$member->wpID = 0;
	$member->dbSave();
	echo "Deleted wpID $deleteID for $deleteName, ";
	echo "memberID $member->memberID\n";
}
echo '</pre>';
?>

<p><a href="index.php">Setup and Migration</a></p>

<?php end_page(); ?>
