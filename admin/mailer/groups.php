<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';
include 'includes/tools.php';

// get an array of the groups
$groupsArray = (new MsgGroups())->getArray();

start_page('CAC Mailer');
?>
<script>
$(function() {
	// do stuff when DOM is ready

	// make nice tooltips for the buttons
	$('.tip').tooltip({container: 'body', placement: 'bottom'});

	$(".pop").popover({trigger: 'hover'}); // info popups

});

function confirmDelete(groupID) {
	var agree = confirm("Are you sure you want to delete this Group?");
	if (agree) {
		$("#row-" + groupID + " button.tip").tooltip("dispose");
		$("#row-" + groupID).remove();
		$.post('ajax/deleteGroup.php', {groupID: groupID});
	} else {
		$("button.tip").blur();
	}
}
</script>
<?php
start_content();
toolbar();
?>

<h3 class="pageheader">Groups List</h3>

<div class="row">
	<div class="col-xl-10 offset-xl-1">

	<table class="table table-sm table-striped">
	<thead>
		<tr><th class="text-center">Action</th><th class="text-center">groupID</th><th>Description</th></tr>
	</thead>
	<tbody>
<?php
foreach ($groupsArray as $msgGroup):
	$URLedit = 'editGroup.php?id=' . $msgGroup->groupID;
	$URLdetail = 'detailGroup.php?id=' . $msgGroup->groupID;
?>
		<tr id="row-<?= $msgGroup->groupID ?>">
		<td class="text-center">
			<div class="btn-group btn-group-sm">
				<button type="button" class="btn btn-secondary tip" onclick="window.open('<?= $URLdetail ?>','_self')" title="Detail">
					<?php add_icon('list-alt') ?>
				</button>
				<button type="button" class="btn btn-secondary tip" onclick="window.open('<?= $URLedit ?>','_self')" title="Edit">
					<?php add_icon('edit') ?>
				</button>
				<button type="button" class="btn btn-secondary tip" onclick="confirmDelete(<?= $msgGroup->groupID ?>)" title="Delete">
					<?php add_icon('times') ?>
				</button>

			</div>
		</td>
		<td class="text-center"><?= $msgGroup->groupID ?></td>
		<td><?= $msgGroup->groupName ?></td>
		</tr>
<?php endforeach; ?>
	</tbody>
	</table>

	</div>
</div>
<?php end_page(); ?>
