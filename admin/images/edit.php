<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member


require '../../support/functions.php';
require '../../support/formFunctions.php';

include 'includes/tools.php';

inty($_GET['id']);
$image = new Images($_GET['id']);
if (empty($image->imageID)) {
	error('ERROR', 'Did not find an image with imageID=' . $_GET['id']);
	exit;
}

$member = new Members($image->memberID);
if (empty($member->memberID)) $member->first = 'unknown';

start_page('CAC Images');
?>
<script>
var imageID = <?= $image->imageID ?>;

$(function() {
    // do stuff when DOM is ready

    $('.tip').tooltip({placement: 'bottom', container: 'body'});
    $(".pop").popover({trigger: 'hover'}); // info popups

});

function confirmDelete() {
    var agree=confirm("Deletion cannot be undone.\n\nAre you sure you want to delete this image?");
    if (agree) {
    	$.post('ajax/delete.php', {imageID: imageID}, function() {
			location.href = "index.php";
		});
    }
}
</script>
<?php
start_content();
toolbar($image->imageID);
?>

<h3 class="pageheader">Edit Image Details &nbsp; <small>imageID=<?= $image->imageID ?></small></h3>

<div class="row">
	<div class="col-xl-10 offset-xl-1">

<?php if (! empty($_GET['firstload'])): ?>
		<div class="alert alert-danger">
			<p><b>Important - Your file was uploaded!</b></p>
			<p>Please carefully check the Preview, Dimensions and File Size below. If they are correct then
				your upload was successful, and you can complete the process by filling in the editable
				fields below.</p>
		</div>

<?php endif; ?>
		<div class="row mb-3">
			<div class="col-lg-6 offset-lg-3">
				<img class="img-thumbnail" src="<?= $image->fileURL ?>" alt="<?= $image->name ?>">
			</div>
		</div>

<form class="well form-horizontal" action="edit2.php" method="post" id="editform">
	<input type="hidden" name="imageID" value="<?= $image->imageID ?>">

<?php
edit_field('Image Name', 'name', $image->name, 120, false, false, false, 'col-md-3', 'col-md-8 col-lg-6');
static_field('Dimensions', $image->width. 'w x ' . $image->height . 'h', false, false, 'col-3', 'col-9');
static_field('File Size', byteSize($image->size), false, false, 'col-3', 'col-5');
static_field('Original Filename', $image->origFilename, false, false, 'col-sm-3', 'col-sm-5');
static_field('Uploaded', $image->created . ' by ' . $member->fullname, false, false, 'col-sm-3', 'col-sm-9');
edit_field('Credit', 'credit', $image->credit, 60, false, false, false, 'col-md-3', 'col-md-8 col-lg-6');
?>

	<div class="row align-items-center mb-3">
		<label class="col-sm-3 text-lg-end">
			<b>Hide in Mailer</b>&nbsp;<?php Show_Info('Hide in Mailer', getHelp('hide')) ?>
		</label>
		<div class="col-sm-5">
			<div class="form-check">
				<input class="form-check-input" type="checkbox" name="hide" id="hide" value="1">
			</div>
		</div>
	</div>

<?php
edit_textfield('Notes', 'notes', $image->notes, 4, false, false, 'col-md-3', 'col-md-8 col-lg-6');
static_submit();
?>
</form>
	</div>
</div>

<?php end_page(); ?>
