<?php

//tools specific to this category

function toolbar() {
	//create the toolbar for this page ?>

			<!-- Top Menu Bar -->
			<nav class="navbar navbar-expand cac_tool">
				<div class="navbar-nav">
					<a class="nav-link" href="index.php"><?php add_icon('cc-stripe', 'fab') ?> Stripe</a>
					<a class="nav-link" href="cash.php"><?php add_icon('money-bill-alt', 'far') ?> Cash/Check</a>
					<a class="nav-link" href="report.php"><?php add_icon('file') ?> CACF Report</a>
				</div>
			</nav>
<?php
}
?>
