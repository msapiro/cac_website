<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';
require '../../support/formFunctions.php';
include 'includes/tools.php';


$mailID = inty($_REQUEST['id']);

if ($mailID) {
	$pageTitle = 'Edit Message';

	$msg = new Msgs($mailID);
	if (empty($msg->mailID)) {
		error('Error', 'Did not find a message with mailID=' . $mailID);
		exit;
	}

} else {
	$pageTitle = 'Create New Message';
	$msg = new Msgs(); // blank object
	$msg->envelopeSender = 'returns@californiaalpineclub.org';
	$msg->html = 0;
}

// get a list of available templates
$templates =  $msg->getTemplates();
$templates = array('0' => 'None') + $templates;


// set some paths for javascript
$URLs = array();
$thisDir =  pathinfo($_SERVER['PHP_SELF'])['dirname'];
$URLs['config'] = $thisDir . '/ckeditorConfig.js';
$URLs['upload'] = $thisDir . '/../images/upload.php';
$URLs['list']   = $thisDir . '/../images/imagesMailer.php';

start_page('CAC Mailer');
add_script('ckeditor');
?>
<script>
var mailID = "<?= $mailID ?>";
var URLs = <?= json_encode($URLs) ?>;

$(function() {
	// do stuff when DOM is ready

	// make nice tooltips for the buttons
	$('.tip').tooltip({container: 'body', placement: 'bottom'});

	$(".pop").popover({trigger: 'hover'}); // info popups

	// the fullPage option adds html, head, and body tags.
	// without it, these elements are removed.
	CKEDITOR.replace( 'htmlBody', {
		height: 400,
		allowedContent: true,
		fullPage: true,
		customConfig: URLs.config,
		filebrowserUploadUrl: URLs.upload,
		extraPlugins: "imagebrowser",
		imageBrowser_listUrl: URLs.list
	});

	// remove empty <div> tags
	CKEDITOR.dtd.$removeEmpty['div'] = 1;

	$("input[name='html']").change(function() {
		rbHTML();
	});

	rbHTML(); // trigger at load

	$("#isTemplate").change(function() {
		if ( $(this).prop("checked") ) {
			$("#useTemplate").val("0");
			$("#divAddresses, #divTextBody").hide();
		} else {
			$("#divAddresses, #divTextBody").show();
		}
	});

	$("#useTemplate").change(function() {
		if ( $(this).val() ) {
			$("#isTemplate").prop("checked", false);
			$("#divAddresses, #divTextBody").show();
		}
	});

});

function rbHTML() {
	if( $("#rb_html").prop("checked") ) {
		$("#divHTMLbody, #divIsTemplate, #divUseTemplate, #divImagesHelp").show();
	} else {
		$("#divHTMLbody, #divIsTemplate, #divUseTemplate, #divImagesHelp").hide();
	}
}
</script>
<style>
	#textBody {font-family: monospace; font-size: 0.8rem}
</style>
<?php
start_content();
toolbar();
?>

<h3 class="pageheader"><?= $pageTitle ?></h3>

<div class="row">
	<div class="col-xl-10 offset-xl-1">

	<form action="edit2.php" method="post" id="mailForm">
		<input type="hidden" name="mailID" value="<?= $mailID ?>">

<?php if ($mailID): ?>
		<div class="row align-items-center mb-3">
			<div class="col-lg-5 offset-lg-3">
				<a class="btn btn-secondary" href="detail.php?id=<?= $mailID ?>">
					<?php add_icon('envelope') ?> mailID: <?= $mailID ?>
				</a>
			</div>
		</div>
<?php endif;

edit_field('Subject', 'subject', $msg->subject, 250, false, false, 'subject',
	'col-lg-3', 'col-lg-8');

echo '<div id="divAddresses">';

edit_field('From Name', 'fromName', $msg->fromName, false, 'optional', false, 'fromName',
	'col-lg-3', 'col-lg-8');
edit_field('From Address', 'fromAddress', $msg->fromAddress, false, 'required',
	array('From Address', 'from'), 'fromAddress', 'col-lg-3', 'col-lg-8');
edit_field('Reply To', 'replyTo', $msg->replyTo, false, 'optional',
	array('Reply To Address', 'replyTo'), 'replyto', 'col-lg-3', 'col-lg-8');
edit_field('Envelope Sender', 'envelopeSender', $msg->envelopeSender, false, 'optional',
	array('Envelope Sender Address', 'envelopeSender'), 'envelopeSender', 'col-lg-3', 'col-lg-8');

echo '</div>';
?>

		<div class="row align-items-center mb-3">
			<label class="text-lg-end col-md-2 col-lg-3">
				<b>Msg Type</b>
			</label>
			<div class="col-sm-6 col-md-4 col-lg-3">
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="html" id="rb_text"
						value="0"<?php if (! $msg->html) echo ' checked' ?>>
					<label class="form-check-label" for="rb_text">Plain Text</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="html" id="rb_html"
						value="1"<?php if ($msg->html) echo ' checked' ?>>
					<label class="form-check-label" for="rb_html">HTML</label>
				</div>
			</div>

			<div id="divIsTemplate" class="col-sm-6 col-md-4">
				<div class="form-check">
					<input class="form-check-input" type="checkbox" value="1" name="isTemplate"
						id="isTemplate"<?php if ($msg->isTemplate) echo ' checked' ?>>
					<label class="form-check-label" for="isTemplate">
						HTML Template&nbsp;<?php Show_Info('HTML Template', getHelp('isTemplate'), 'left') ?>
					</label>
				</div>
			</div>
		</div>

		<div id="divUseTemplate">
<?php
edit_select_field('Use Template', 'useTemplate', $templates, $msg->useTemplate,
	array('Use a Template', 'useTemplate'), 'useTemplate');
?>
		</div>

		<div id="divHTMLbody">
			<div class="row mb-3">
				<label class="text-lg-end col-lg-3">
					<b>HTML Body</b>&nbsp;<?php Show_Info('HTML Body', getHelp('htmlBody')) ?>
				</label>
			</div>

			<div class="row mb-3">
				<div class="col-lg-11 offset-lg-1">
					<textarea class="form-control" name="htmlBody" id="htmlBody"
						rows="12"><?= $msg->htmlBody ?></textarea>
				</div>
			</div>
		</div>

		<div id="divTextBody">
			<div class="row mb-3">
				<label class="text-lg-end col-lg-3">
					<b>Plain Text Body</b>&nbsp;<?php Show_Info('Plain Text Body',
						getHelp('textBody')) ?>
				</label>
			</div>

			<div class="col-lg-11 offset-lg-1 mb-3">
				<textarea class="form-control" name="textBody" id="textBody"
					rows="8"><?= $msg->textBody ?></textarea>
			</div>
		</div>

<?php static_submit('Submit') ?>
	</form>


<div id="divImagesHelp" class="alert alert-info mt-3">
	<h4>Images in HTML messages</h4>
	<ul>
		<li>Images from our library are available for insertion into any html message. Place your
			cursor where you want the image to appear and then click the <b>Image</b> button in
			the html editor. Click on <b>Browse Server</b> to see thumbnails of the available
			images, then click on the image you want.</li>
		<li>Images may be uploaded to our library from this page using the <b>Upload</b> tab found
			under the <b>Image</b> button in the html editor. Images uploaded from this page will
			be immediately inserted into the html body.</li>
		<li>Images may also be uploaded to our library using the <b>Images</b> category in the main
			toolbar above.</li>
		<li>Library images will be embedded in the email message (rather than external links).</li>
		<li>You can also insert an image as an external link by specifying a complete URL in the
			<b>Images</b> dialog rather than using the <b>Browse Server</b> button, but be aware
			that some email programs will suppress the display of external images.</li>
		<li>To make an image responsive, click Source in the HTML editor to view the HTML source.
			Find the &lt;img ... style="width: ...px; height: ...px;"&gt; tag and insert
			'maxwidth: 100%; height: auto;' in the style</li>
	</ul>
</div>

	</div>
</div>

<?php end_page(); ?>
