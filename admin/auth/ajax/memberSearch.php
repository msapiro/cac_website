<?php
require '../../support/config.php';
require CLASSLOADER;

require '../includes/config.php'; // contains $allowGroups for this directory
//array_push($allowGroups, 'ROmembers');
require AUTH_AJAX;                // make sure user is logged in and a group member


$whereArray = array();

if ($_POST['first']) {
	$whereArray = ['first[~]' => wild($_POST['first'])];
}
if ($_POST['last']) {
	$whereArray = array_merge($whereArray, ['last[~]' => wild($_POST['last'])]);
}
if ($_POST['email']) {
	$whereArray = array_merge($whereArray, ['email[~]' => wild($_POST['email'])]);
}

$db = (new Model(false))->db;
$rows = $db->select("members", [
	"memberID",
	"email",
	"fullname" => Medoo\Medoo::raw("CONCAT(<first>, ' ', <last>)")
	],
	array_merge($whereArray, ['ORDER' => ['first', 'last']])
	);

if (count($rows) == 0): ?>
<div class="row">
	<div class="col-lg-4 offset-lg-3">
		<h4>Nothing Found!</h4>
	</div>
</div>

<?php else: ?>
<table class="table table-sm">
<?php foreach($rows as $row): ?>

	<tr>
	<td><a href="#" style="width: 180px" class="btn btn-sm btn-secondary" onclick="addMember(<?= $row['memberID'] ?>)"><?= $row['fullname'] ?></a></td>
	<td><?= $row['email'] ?></td>
	</tr>
<?php endforeach; ?>
</table>

<div class="alert alert-info">
	Click on a member name to select it.
</div>
<?php endif;
exit;

function wild($string) {
	// add leading and trailing mysql wildcards and replace spaces with wildcards
	return '%' . preg_replace('/\s+/', '%', $string) . '%';
}

?>