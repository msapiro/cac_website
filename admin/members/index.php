<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
array_push($allowGroups, 'ROmembers', 'trainee');
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';
require '../../support/formFunctions.php';
include 'includes/tools.php';

$fieldClass = 'col-sm-9 col-md-7 col-lg-5';

// get a list of membershipTypes
$membershipTypesSelect[0] = 'Any Type';
$classArray = (new Classes())->getArray();
foreach ($classArray as $oClass) {
	$membershipTypesSelect[$oClass->class] = $oClass->description;
}

$expiredSelect['-1'] = 'Current and Expired Members';
$expiredSelect['0'] = 'Current Member';
$expiredSelect['1'] = 'Expired Member';

start_page('CAC Members');
add_script('jqueryUI');  // for auto-complete
?>
<script>

$(function() {
    // do stuff when DOM is ready

    $("#memberSearch").focus();

    $(".pop").popover({trigger: 'hover'}); // info popups

	$("#memberSearch").autocomplete({
		source: "ajax/memberAuto.php",
		minLength: 3,
		select: function(event, ui) {
			memberID = ui.item.memberID;
			if (memberID > 0) {
				window.location = "detail.php?id=" + memberID;
			}
        }
    });

});

</script>
<?php
start_content();
toolbar();
?>

<h3 class="pageheader">CAC Search Members</h3>

<div class="card well">
	<div class="card-body">
		<h4>Quick-Find One Member</h4>
<?php
	edit_field('Member', 'memberSearch', false, false, 'Start typing name or email',
	array('Quick Search', 'memberAuto'), 'memberSearch') ?>
	</div>
</div>

<div class="card well mt-3">
	<div class="card-body">

	<h4>Advanced Search</h4>
	<form action="searchResults.php" method="post" id="searchForm">
		<input type="hidden" name="searchtype" value="standard">

<?php
edit_field('memberID', 'memberID', false, 11, false, false, false, 'col-sm-3', 'col-sm-2');
edit_field('membershipID', 'membershipID', false, 11, false, false, false,
	'col-sm-3', 'col-sm-2');
edit_field(' WordPress ID', 'wpID', false, 11, false, false, false, 'col-sm-3', 'col-sm-2');
edit_field('First', 'first', false, 30, false, false, false, 'col-sm-3', $fieldClass);
edit_field('Last', 'last', false, 30, false, false, false, 'col-sm-3', $fieldClass);
edit_field('Email', 'email', false, 100, false, false, false, 'col-sm-3', $fieldClass);
edit_select_field('Membership Type', 'class', $membershipTypesSelect, false, false, false,
	'col-sm-3', $fieldClass);
edit_select_field('Expired', 'expired', $expiredSelect, false, false, false,
	'col-sm-3', $fieldClass);
?>

		<div class="row">
			<div class="col-md-5 offset-md-3">
				<div class="btn-group">
					<button type="submit" class="btn btn-primary">Find</button>
					<button type="reset" class="btn btn-secondary">Reset</button>
				</div>
			</div>
		</div>

	</form>
	</div>
</div>

<div class="alert alert-info mt-3">
	<h4>Search Tips</h4>
	<ul>
		<li>Results will contain only members meeting <b>All</b> of your criteria.</li>
		<li>If you provide a <b>memberID</b> or a <b>membershipID</b> all other criteria will
			be ignored.</li>
		<li>Use a zero for <b>WordPress ID</b> to find members that are not linked to a WordPress
			account.</li>
		<li>For text fields you may use partial words. They must be in the correct order.
			Example: "avi mit" will find <b>David Smith</b>.</li>
	</ul>
</div>

<?php end_page(); ?>
