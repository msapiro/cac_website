#! /usr/bin/env php
<?php
require '../support/config.php';
require CLASSLOADER;

require_once 'Mail.php';

$usage = "Usage: $argv[0] print|mail test|prod|mID [final] [ReallyDoIt]\n";
if (in_array('print', $argv)) {
    $printit = true;
} elseif (in_array('mail', $argv)) {
    $printit = false;
} else {
    die($usage);
}
if (in_array('test', $argv)) {
    $testit = true;
} elseif (in_array('prod', $argv)) {
    $testit = false;
} elseif (in_array('mID', $argv)) {
    $testit = readline('Enter a single membershipID: ');
    if (! is_numeric($testit)) {
        die("MembershipID must be numeric\n");
    }
} else {
    die($usage);
}
if (in_array('final', $argv)) {
   $final = true;
} else {
   $final = false;
}
if (! $printit and ! $testit and ! in_array('ReallyDoIt', $argv)) {
    die($usage . "mail and prod require ReallyDoIt\n");
}
$seq = 0;
$pres_msg = file_get_contents('./pm.txt');
$extra_txt = file_get_contents('./extra.txt');
$extra2_txt = file_get_contents('./extra2.txt');
$vol_txt = file_get_contents('./vol.txt');
$vol1_txt = file_get_contents('./vol1.txt');
$email_txt = file_get_contents('./email.txt');
$FY = sprintf('%4d/%4d', intval(date('Y')), intval(date('Y'))+1);
$email_txt = preg_replace('/yyyy.yyyy/', $FY, $email_txt);

function merge_names($names) {
    if (count($names) == 2) {
        return("$names[0] $names[1]");
    } else if (count($names) == 4) {
        if ($names[1] == $names[3]) {
            return("$names[0] & $names[2] $names[3]");
        } else {
            return("$names[0] $names[1] & $names[2] $names[3]");
        }
    } else {
        print "Unable to merge names\n";
        print_r($names);
        return("Bad Names");
    }
}

function format_phone($phone) {
    if(strlen($phone) == 10) {
        $phone = substr($phone, 0, 3) . '-' . substr($phone, 3, 3) .
                '-' . substr($phone, 6, 4);
    }
    return $phone;
}

function make_pdf($names, $street, $city_st_zip, $duestable, $desc, $dues, $phones, $recips, $deps, $total, $final) {
   global $printit, $seq, $pres_msg, $extra_txt, $extra2_txt, $vol_txt, $vol1_txt, $FY;

   $AUTHOR = 'CAC Registrar';
   $pdf = new TCPDF('P', 'in', array(8.5, 11.0));
   $pdf->SetCompression(false);
   $pdf->setViewerPreferences(array('PrintScaling'=>'None'));
   $pdf->SetAuthor($AUTHOR);
   $pdf->SetTitle("CAC $FY Annual Dues Letter");
   $pdf->SetMargins(0.5,0.5,0.5,true);  //sets lft, top, right and keeps as default
   $pdf->setPrintHeader(false);    //suppress default header (horiz line)
   $pdf->setPrintFooter(false);
   $pdf->setCellPaddings('0', '0', '0','0');       //to get text printed with MultiCell to align with margins.
   $pdf->SetAutoPageBreak(false);
   $pdf->AddPage();
   $pdf->setFont('Helvetica', '', 18);
   $pdf->SetX(1.0);
   $pdf->write(20/72, "California Alpine Club\n");
   $pdf->SetX(1.3);
   $pdf->setFont('Helvetica', '', 12);
   $pdf->write(20/72, $FY . " Annual Dues\n");
   if ($final) {
      $pdf->SetX(1.0);
      $pdf->setTextColorArray(array(255, 0, 0));
      $pdf->setFont('Helvetica', 'B', 12);
      $pdf->write(20/72, 'FINAL NOTICE - RENEW NOW');
      $pdf->setTextColorArray(array(0, 0, 0));
   }
   $pdf->SetX(1.0);
   $pdf->setFont('Helvetica', 'B', 10);
   $pdf->MultiCell(3.4, 1,   // width, height
      merge_names($names) . "\n$street\n$city_st_zip",
      0, 'L', false, 1, 1.0, 2.0); // border, align, fill, ending pos, x, y
   $pdf->Image('../../support/graphics/cac-seal.png', 5.4, 0.25, 1.5);
   $pdf->setFont('Helvetica', '', 10);
   $pdf->Text(4.5, 2, "President's Message...");
   $pdf->setTextColorArray(array(255, 0, 0));
   $pdf->setFont('Helvetica', '', 9);
   $pdf->MultiCell(3.5, 2, $pres_msg, 0, 'J', false, 1, 4.5, 2+12/72);
   $pdf->setTextColorArray(array(0, 0, 0));
   $pdf->MultiCell(3.5, 2, $extra_txt, 0, 'C', false, 1, 4.5, 4.1);
   $pdf->setFont('Helvetica', 'B', 10);
   $pdf->MultiCell(3.5, .5, "Many Thanks from your Club Leaders!", 0, 'C', false, 1, 4.5, 5.3);
   $pdf->setXY(0.5, 3);
   $pdf->Cell(3, 12/72, "Class of Membership                                Dues",
      0, 1, 'L');
$rgdues = sprintf('%2.0f', $duestable['RG']);
$jrdues = sprintf('%2.0f', $duestable['JT']);
$srdues = sprintf('%2.0f', $duestable['SR']);
$jsdues = sprintf('%2.0f', $duestable['JS']);
$stdues = sprintf('%2.0f', $duestable['ST']);
   $pdf->MultiCell(3, 1,
      "Regular                                                        $$rgdues
Joint Regular                                                $jrdues
Senior (over 70)                                              $srdues
Joint Senior (Aggregate Age Over 140)     $jsdues
Student (under 26)                                         $stdues
Dependent Children (under 18)      No Charge",
      1, "L");
   $pdf->setFont('Helvetica', '', 9);
   $pdf->MultiCell(3, 1, $extra2_txt, 0, 'J', false, 1, 0.5, 4.4);
   $pdf->MultiCell(1.4, .5,
   'Make checks payable to
"California Alpine Club"
mail to:',
   0, 'C', false, 1, 0.5, 5.5);
   $pdf->MultiCell(1.6, .5,
   'California Alpine Club
P.O.Box 2180
Mill Valley, CA 94942-2180',
   1, 'L', false, 1, 2, 5.5);
   $pdf->setXY(0.5, 6.1);
   $pdf->Write(10/72, "Questions? Contact Registrar Mark Sapiro @415 515 8120 / ");
   $pdf->Write(10/72, "registrar.cac@gmail.com", "mailto:registrar.cac@gmail.com");
   $pdf->ln();
   $pdf->Write(10/72, "Make any necessary additions or corrections and mail with your check.");
   $pdf->ln(.3);
   $pdf->setFont('Helvetica', 'B', 10);
   $pdf->Write(12/72, "$names[0] $names[1]");
   $pdf->setY($pdf->getY() - 0.2);
   $pdf->setX(5.7);
   $pdf->Write(12/72, $desc);
   $pdf->setX(7);
   $pdf->Write(12/72, "Dues \$ $dues");
   $pdf->setFont('Helvetica', 'B', 9);
   $pdf->ln(.25);
   $depsY = $pdf->getY();
   $pdf->setY($pdf->getY() + 0.2);
   $pdf->Write(10/72, "Home Phone");
   $pdf->setX(1.5);
   $pdf->setFont('Helvetica', '', 9);
   $pdf->Cell(1.5, 10/72, $phones[0], 'B', 0);
   $pdf->setFont('Helvetica', 'B', 9);
   $pdf->setX(3.1);
   $pdf->Write(10/72, "Cell Phone");
   $pdf->setX(3.8);
   $pdf->setFont('Helvetica', '', 9);
   $pdf->Cell(1.5, 10/72, $phones[1], 'B', 1);
   $pdf->setFont('Helvetica', 'B', 9);
   $pdf->ln(.1);
   $pdf->Write(10/72, "Email");
   $pdf->setX(1.5);
   $pdf->setFont('Helvetica', '', 9);
   $pdf->Cell(1.5, 10/72, $recips[0], 'B', 1);
   $pdf->ln(.1);
   $pdf->setFont('Helvetica', 'B', 9);
   $pdf->Write(10/72, "All members are asked to agree to the following: ");
   $pdf->setFont('Helvetica', '', 9);
   $pdf->Write(10/72, "I Agree [ ]");
   $pdf->ln(.2);
   $pdf->setFont('Helvetica', '', 9);
   $pdf->MultiCell(0, 1, $vol_txt, 0, 'L', false, 1);
   $pdf->setFont('Helvetica', 'B', 9);
   $pdf->setY($pdf->getY() -0.6);
   $pdf->setTextColorArray(array(255, 0, 0));
   $pdf->MultiCell(0, 1, $vol1_txt, 0, 'L', false, 1);
   $pdf->setFont('Helvetica', '', 9);
   $pdf->setTextColorArray(array(0, 0, 0));

   if (count($names) > 2) {
      $pdf->setY(8.7);
      $pdf->setFont('Helvetica', 'B', 10);
      $pdf->Write(12/72, "$names[2] $names[3]");
      $pdf->setFont('Helvetica', 'B', 9);
      $pdf->ln(.2);
      $pdf->Write(10/72, "Home Phone");
      $pdf->setX(1.5);
      $pdf->setFont('Helvetica', '', 9);
      $pdf->Cell(1.5, 10/72, $phones[2], 'B', 0);
      $pdf->setFont('Helvetica', 'B', 9);
      $pdf->setX(3.1);
      $pdf->Write(10/72, "Cell Phone");
      $pdf->setX(3.9);
      $pdf->setFont('Helvetica', '', 9);
      $pdf->Cell(1.5, 10/72, $phones[3], 'B', 1);
      $pdf->setFont('Helvetica', 'B', 9);
      $pdf->ln(.1);
      $pdf->Write(10/72, "Email");
      $pdf->setX(1.5);
      $pdf->setFont('Helvetica', '', 9);
      $pdf->Cell(1.5, 10/72, $recips[1], 'B', 1);
      $pdf->ln(.1);
      $pdf->setFont('Helvetica', 'B', 9);
      $pdf->Write(10/72, "All members are asked to agree to the following: ");
      $pdf->setFont('Helvetica', '', 9);
      $pdf->Write(10/72, "I agree [ ]");
      $pdf->ln(.2);
      $pdf->setFont('Helvetica', '', 9);
      $pdf->MultiCell(0, 1, $vol_txt, 0, 'L', false, 1);
      $pdf->setFont('Helvetica', 'B', 9);
      $pdf->setY($pdf->getY() -0.6);
      $pdf->setTextColorArray(array(255, 0, 0));
      $pdf->MultiCell(0, 1, $vol1_txt, 0, 'L', false, 1);
      $pdf->setFont('Helvetica', '', 9);
      $pdf->setTextColorArray(array(0, 0, 0));
   }
   if (count($deps) > 0) {
      $pdf->setXY(5.4, $depsY);
      $pdf->setFont('Helvetica', 'B', 10);
      $pdf->Write(10/72, "Dependents:");
      $pdf->ln();
      $pdf->setX(5.4);
      $pdf->Write(10/72, "Status");
      $pdf->setX(6.2);
      $pdf->Write(10/72, "Name");
      $pdf->setX(7.5);
      $pdf->Write(10/72, "Dues");
      $pdf->setFont('Helvetica', '', 10);
      foreach ($deps as $x) {
         $pdf->ln();
         $pdf->setX(5.4);
         $pdf->Write(10/72, "$x[0]");
         $pdf->setX(6.2);
         $pdf->Write(10/72, "$x[1]");
         $pdf->setX(7.5);
         $pdf->Write(10/72, "\$ $x[2]");
         }
   }
   $pdf->setXY(6, 8.5);
   $pdf->setFont('Helvetica', 'B', 10);
   $pdf->Cell(1.4, 10/72, 'Total Dues $', 0, 0, 'R');
   $pdf->setFont('Helvetica', '', 10);
   $pdf->setX(7.5);
   $pdf->Cell(.5, 10/72, sprintf('%1.2f', $total), 'B', 1);
   $pdf->ln();
   $pdf->setX(6);
   $pdf->setFont('Helvetica', 'B', 10);
   $pdf->Cell(1.4, 10/72, 'General Fund Contribution $', 0, 0, 'R');
   $pdf->setFont('Helvetica', '', 10);
   $pdf->setX(7.5);
   $pdf->Cell(.5, 10/72, '', 'B', 1);
   $pdf->ln();
   $pdf->setX(6);
   $pdf->setFont('Helvetica', 'B', 10);
   $pdf->Cell(1.4, 10/72, 'CAC Foundation Contribution $', 0, 0, 'R');
   $pdf->setFont('Helvetica', '', 10);
   $pdf->setX(7.5);
   $pdf->Cell(.5, 10/72, '', 'B', 1);
   $pdf->ln();
   $pdf->setX(6);
   $pdf->setFont('Helvetica', 'B', 10);
   $pdf->Cell(1.4, 10/72, 'Total Tendered $', 0, 0, 'R');
   $pdf->setFont('Helvetica', '', 10);
   $pdf->setX(7.5);
   $pdf->Cell(.5, 10/72, '', 'B', 1);
   $pdf->ln();

   if ($printit) {
      file_put_contents(sprintf('x%03d.pdf', $seq), $pdf->Output('dues_letter.pdf', 'S'));
      $seq += 1;
   } else {
      return($pdf->Output('dues_letter.pdf', 'E'));
   }
}

function make_hdrs($names, $recips) {
   global $FY;
   $hdrs = array();
   $hdrs['Return-Path'] = '<returns@californiaalpineclub.org>';
   $hdrs['Subject'] = "California Alpine Club $FY Dues";
   if (count($recips) == 1) {
       $hdrs['To'] = "\"$names[0] $names[1]\" <$recips[0]>";
   } else {
       if ($recips[0] != '' && $recips[1] != '') {
          $hdrs['To'] = "\"$names[0] $names[1]\" <$recips[0]>,
 \"$names[2] $names[3]\" <$recips[1]>";
       } else if ($recips[0] != '') {
          $hdrs['To'] = "\"$names[0] $names[1]\" <$recips[0]>";
       } else if ($recips[1] != '') {
          $hdrs['To'] = "\"$names[2] $names[3]\" <$recips[1]>";
       } else {
          print "Unable to make To:\n";
          print_r($names);
       }
   }
   $hdrs['From'] = "CAC Registrar <registrar@californiaalpineclub.org>";
   $hdrs['MIME-Version'] = '1.0';
   $hdrs['Content-Type'] = "multipart/mixed;\n " . 'boundary="--Part_Separator"';
   return($hdrs);
}

function make_msg($att) {
   global $email_txt;
   $msg = "----Part_Separator\nContent-Type: text/plain\n\n";
   $msg .= $email_txt;
   $msg .= "----Part_Separator\n";
   $msg .= $att;
   $msg .= "\n----Part_Separator--\n";
   return($msg);
}

$db = (new Model(false))->db;
// The current year's expiration date.
$exdate = date("Y-m-d", mktime(0,0,0,3,31));
// Get the dues amounts.
$clsres = $db->select("classes",
        ['class', 'dues']
        );
$duestable = array();
foreach ($clsres as $cls) {
    $duestable[$cls['class']] = $cls['dues'];
}

if (is_numeric($testit)) {
$mships = $db->select("memberships",
        ['membershipID', 'street', 'city', 'state', 'zip', 'class'],
        ["AND"=>["resigned"=>0, "membershipID"=>$testit]]
        );
} elseif ($testit) {
// For testing:
// 2283 is Westreich
// 1916 is Mellett - many dependents
// 1941 is Gustafson and Cosgrove
// 1944 is Sapiro
$mships = $db->select("memberships",
        ['membershipID', 'street', 'city', 'state', 'zip', 'class'],
	["membershipID"=>[2283, 1916, 1941, 1944]]
        );
} else {
$mships = $db->select("memberships",
	['membershipID', 'street', 'city', 'state', 'zip', 'class'],
	["AND"=>["expiration"=>$exdate,	"resigned"=>0,
		"class[!]"=>["0", "", "OR", "NM"]]]
	);
}
if (!$printit) {
   $mailer = Mail::factory('smtp', array('persist'=>true,
                                         'host'=>'localhost',
                                         'port'=>25,));
}
foreach ($mships as $mship) {
    $street = $mship['street'];
    $city_st_zip = $mship['city'] . ", " . $mship['state'] . " " . $mship['zip'];
    $clsres = $db->select("classes",
	    ['description', 'dues'],
	    ["class"=>$mship['class']]
	    );
    if (count($clsres) != 1) {
        print $mship['class'] . " not in class table: " . $mship['membershipID'] . "\n";
        break;
    } else {
        $clsrow = $clsres[0];
        $desc = $clsrow['description'];
        $dues = $clsrow['dues'];
    }
    $total = $dues;
    $deps = array();
    $depres = $db->select("dependents",
	    ['first', 'last'],
	    ["membershipID"=>$mship['membershipID']]
    );
    foreach ($depres as $dep) {
        $deps[] = array('Dependent',
                        join(' ', array($dep['first'], $dep['last'])),
                        0.00);
    }
    $memres = $db->select("members",
	   ['first', 'last', 'email', 'phone', 'mobile'],
           ["membershipID"=>$mship['membershipID']]
    );
    // Check for anomalous membership
    if (count($memres) == 0) {
        print 'Membership ' . $mship['membershipID'] . ' has no members.';
        continue;
    }
    if (count($memres) > 2) {
        print 'Membership ' . $mship['membershipID'] . ' has ' . count($memres)
              . ' members: ';
        print_r($memres);
        continue;
}
    $first = true;
    foreach ($memres as $memrow) {
	    if ($first) {
		    $names = array($memrow['first'], $memrow['last']);
		    $recips = array($memrow['email']);
		    $phones = array(format_phone($memrow['phone']),
	                    format_phone($memrow['mobile']));
		    $first = false;
	    } else {

	        $names[] = $memrow['first'];
	        $names[] = $memrow['last'];
	        $recips[] = $memrow['email'];
	        $phones[] = format_phone($memrow['phone']);
	        $phones[] = format_phone($memrow['mobile']);
	    }
    }

   if ($printit) {
      make_pdf($names, $street, $city_st_zip, $duestable, $desc, $dues, $phones, $recips, $deps, $total, $final);
   } else {
      $att = make_pdf($names, $street, $city_st_zip, $duestable, $desc, $dues, $phones, $recips, $deps, $total, $final);
      $msg = make_msg($att);
      $hdrs = make_hdrs($names, $recips);
      $new_recips = array();
      foreach ($recips as $x) {
         if ($x != '' and !preg_match('/^.*_bouncing$/', $x)) {
            $new_recips[] = $x;
         }
      }
      if (count($new_recips) == 0) {
         $new_recips = array('mark@msapiro.net');
         print("No emails:\n");
         print_r($names);
      }
   if ($testit and ! is_numeric($testit)) {
      // For testing.
      $new_recips = array('mark@ms2.msapiro.net');
   }
   $mailer->send($new_recips, $hdrs, $msg);
   $seq += 1;
   }
}
print("Total = $seq\n");
?>
