<?php
// clear our login session variables, log out of WordPress, redirect to CAC public website

// we need access to some wordpress functions
require __DIR__ . '/../wp-load.php';

if (session_status() !== PHP_SESSION_ACTIVE) {
	session_name('CACSESSID');
	session_start();
}

// log out of WordPress
wp_logout();

if (! empty($_SESSION['cac_login']['memberID'])) {
	require 'members/support/config.php';
	require CLASSLOADER;

	$model = new Model();
	$memberID = $_SESSION['cac_login']['memberID'];
	$event = 'logged out';

	$model->db->insert("authLog", ["memberID" => $memberID, "event" => $event]);
}

// clear session login data
$_SESSION['cac_login'] = array();

$location = home_url();
header('Location: ' . $location);
