<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// array_push($allowGroups, 'ROmembers');
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';

// Ensure we came from a POST.
requirePOST();

$newOpp = false;
if (empty ($_POST['oppID'])) $newOpp = true;

$volOpp = new VolOpps();

$volOpp->oppID = $_POST['oppID'];
$volOpp->catID = $_POST['catID'];
$volOpp->oppName = $_POST['oppName'];
$volOpp->description = $_POST['description'];

$volOpp->dbSave();

if ($newOpp) {
	header("Location: index.php");
	exit;
} else {
	header("Location: detail.php?id=$volOpp->oppID");
	exit;
}
