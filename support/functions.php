<?php
function start_page($title = false, $description = false) {
	// starts the html5 page output but does not complete the <head>
	global $basedir, $defTitle, $defDescription;
	if (empty($title)) $title = $defTitle;
	if (empty($description)) $description = $defDescription;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?= $description ?>">

	<link rel="icon" type="image/jpg" href="../../support/graphics/cac-seal-32.jpg" sizes="32x32">

	<script src="https://code.jquery.com/jquery-3.5.1.min.js"
		integrity="sha384-ZvpUoO/+PpLXR1lu4jmpXWu80pZlYUAfxl5NsBMWOEPSjUn/6Z/hRTt8+pR6L4N2"
		crossorigin="anonymous"></script>

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css"
		integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65"
		rel="stylesheet" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"
		integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4"
		crossorigin="anonymous"></script>

	<script src="//kit.fontawesome.com/c6491eaecf.js" crossorigin="anonymous"></script>

	<script src="<?= fileVersion('../../support/cac.js') ?>"></script>
	<title><?= $title ?></title>
<?php
}


function add_script($scriptname){
	// add links to javascript files inside the header
	switch (strtolower($scriptname)):

	case 'datatables':
		// Bootstrap 4 styling with added PDF button feature
		// choose any combination of features at https://datatables.net/download/
		// if using the PDF feature remember to add the pdfmake script too
?>
		<link rel="stylesheet" type="text/css"
			href="https://cdn.datatables.net/v/bs4/dt-1.10.18/b-1.5.2/b-html5-1.5.2/datatables.min.css"/>
		<script src="https://cdn.datatables.net/v/bs4/dt-1.10.18/b-1.5.2/b-html5-1.5.2/datatables.min.js">
			</script>
<?php
		add_script('datatables_config');
		break;
	case 'jqueryui': ?>

		<link rel="stylesheet" type="text/css" href="../../support/jquery-ui-1.12.1/jquery-ui.min.css">
		<script src="../../support/jquery-ui-1.12.1/jquery-ui.min.js"></script>
		<script src="../../support/jquery.ui.touch-punch.js"></script>
<?php	break;
	case 'ckeditor': ?>

		<script src="<?= fileVersion('../../support/ckeditor/ckeditor.js') ?>"></script>
<?php	break;
	case 'pdfmake': ?>

		<script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.37/pdfmake.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.37/vfs_fonts.js"></script>
<?php	break;
	case 'cookies':
	case 'cookie': ?>

		<script src="../../support/js.cookie-2.2.1.min.js"></script>
<?php	break;
	default: ?>

	<script src="<?= fileVersion('../../support/' . $scriptname . '.js') ?>"></script>
<?php 	endswitch;
}

function add_local_script($scriptname = 'functions') { ?>
	<script src="<?= fileVersion('includes/' . $scriptname . '.js') ?>"></script>
<?php
}

function start_content() {
	// insert our custom css last so it overrides anything defined before it.
	// then close the head and create the top navbar. then the content.
	global $menuShow, $menuCategory;

	// work out the directory name of the page that called this function
	$pageDir = $_SERVER['REQUEST_URI'];
	if (!preg_match("/\/$/", $pageDir)) {
		$pageDir = dirname($pageDir);  //cut off the page name
	}
	$pageDir = basename($pageDir);  //eg "home" or "contacts"
?>
	<link rel="stylesheet" type="text/css" href="<?= fileVersion('../../support/cac.css') ?>">
</head>

<body>
<div class="container">

	<!-- Banner content just above the main menu bar -->
	<div class="container-fluid" id="topBanner">
		<a href="../home/" title="Go to Home Page">
			<!-- visible on xs, sm, md -->
			<img class="img-fluid d-lg-none" src="../../support/graphics/cac-logo.png" alt="CAC Banner">

			<!-- visible on lg, xl - set width to 730 so there is room for the seal -->
			<img class="d-none d-lg-block" src="../../support/graphics/cac-logo.png" style="width: 730px"
				alt="CAC Banner">
		</a>
	</div>

	<!--  Navbar collapses when going from md to sm -->
	<nav id="topMenu" class="navbar navbar-expand-md">
		<div class="container-fluid">

			<!-- these items always visible. force them to remain horizontal (row) -->
			<ul class="navbar-nav" style="flex-direction: row">
<?php
// the menuShow array is specified in the file "support/config.php"
foreach ($menuShow as $dir => $text): ?>
				<li class="nav-item">
					<a class="nav-link<?php if ($dir == $pageDir) echo ' active' ?>" href="../<?= $dir ?>/"><?= $text ?></a>
				</li>
<?php endforeach; ?>
			</ul>

			<!-- hamburger -->
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse"
				data-bs-target="#navbarContentCollapse">
				<span class="navbar-toggler-icon"></span>
			</button>

			<!-- menu items in the next div will collapse into the hamburger -->
			<div id="navbarContentCollapse" class="collapse navbar-collapse">
				<ul class="navbar-nav me-auto">
<?php
// the menuCategory array is specified in the file "support/config.php"
foreach ($menuCategory as $dir => $text): ?>
					<li class="nav-item">
						<a class="nav-link<?php if ($dir == $pageDir) echo ' active' ?>"
							href="../<?= $dir ?>/"><?= $text ?></a>
					</li>
<?php endforeach; ?>
					<li class="nav-item">
						<a class="nav-link" href="../../logout.php">LOGOUT</a>
					</li>
				</ul>

			</div><!-- /.navbar-collapse -->
		</div><!-- / .container-fluid -->
	</nav> <!-- end topMenu -->

	<div class="d-none d-lg-block"
		style="position:relative; top:-150px; float:right; height:0 !important; margin-right:15px; z-index:2;">
		<img src="../../support/graphics/cac-seal.png">
	</div>

	<div class="mainarea">

<?php
}


function tabs($select=false) {
	// create the tabs for this category
	// size is the display size at which vertical layout becomes horizontal

	global $tabItem;
?>

	<!-- Top Tabs for md, lg, xl displays-->
	<ul id="tabs" class="nav nav-tabs d-none d-md-flex">
<?php
if (! $select) {
	// find the name of the page we are on (without the extension)
	$select = basename ($_SERVER['PHP_SELF'], '.php');
}

foreach ($tabItem as $item => $text):
		$link = $item . '.php';
?>
		<li class="nav-item">
			<a class="nav-link<?php if ($item == $select) echo ' active'; ?>" href="<?= $link ?>">
				<?= $text ?>
			</a>
		</li>
<?php
endforeach; ?>
	</ul>

	<!-- Top Tabs for xs, sm displays -->
	<nav id="tabsSm" class="nav nav-pills flex-column d-md-none">
<?php
foreach ($tabItem as $item => $text):
		$link = $item . '.php'; ?>
		<a class="nav-link<?php if ($item == $select) echo ' active'; ?>" href="<?= $link ?>">
			<?= $text ?>
		</a>
<?php
endforeach; ?>
	</nav>

<?php
}

function PrevNext($id, $arrayname="foundArray", $mylist=0){
	// make a block of html to display the Previous and Next buttons
	// Each type of search (members, routes, locations, etc) should
	// use a different arrayname to avoid stepping on each other
	// if we know the list page, we display a button for that too

	isset($_SESSION[$arrayname]) ? $numfound = count($_SESSION[$arrayname]) : $numfound = false;
	if (! $numfound) return;

	$numthis = array_search($id, $_SESSION[$arrayname]) + 1;

	$numprevious = $numthis - 1;
	$numnext = $numthis + 1;

	$thisPage = $_SERVER["PHP_SELF"];

	$prevURL = false;
	if ($numthis > 1){
		$prev = $_SESSION[$arrayname][$numprevious - 1];
		$prevURL = $thisPage . '?set=1&amp;id=' . $prev;
	}
	$nextURL = false;
	if ($numthis < $numfound){
		$next = $_SESSION[$arrayname][$numnext - 1];
		$nextURL = $thisPage . '?set=1&amp;id=' . $next;
	}

	if ($mylist) {
		$addstring = 'fromID=' . $id;
		if (strpos($mylist,"?")) {
			$mylist .= '&amp;' . $addstring;
		} else {
			$mylist .= '?' . $addstring;
		}
		$displaystring = '#' . $numthis . ' of ' . $numfound ;
	} else {
		$mylist = '#';
		$displaystring = '';
	}
	?>

		<!-- Previous and Next Buttons -->
		<div class="btn-group prevnext mt-3" role="group">
<?php if ($prevURL): ?>
			<a class="btn btn-secondary" href="<?= $prevURL ?>"><?php add_icon('chevron-left') ?> Previous</a>
<?php else : ?>
			<a class="btn btn-secondary disabled" href="#"><?php add_icon('chevron-left') ?> Previous</a>
<?php endif; ?>
			<a class="btn btn-secondary" href="<?= $mylist ?>"><?php add_icon('list-alt') ?> <?= $displaystring ?></a>
<?php if ($nextURL): ?>
			<a class="btn btn-secondary" href="<?= $nextURL ?>">Next <?php add_icon('chevron-right') ?></a>
<?php else : ?>
			<a class="btn btn-secondary disabled" href="#">Next <?php add_icon('chevron-right') ?></a>
<?php endif; ?>
		</div>
<?php
}


function add_icon($name, $style='fas', $noprint = false) {
	// add icons from font awesome
	echo '<span class="' . $style . ' fa-' . $name;
	if ($noprint) echo ' noprint';
	echo '"></span>';
}

function add_icon_text($name, $style=false, $tip = false) {
	// like above, except return the html instead of echoing it
	// also allow tooltips as titles
	if (empty($style)) $style = 'fas'; //allows us to use false as well as unset
	$ret = '<span class="' . $style . ' fa-' . $name;
	if ($tip) $ret .= ' tip';
	$ret .= '"';
	if ($tip) $ret .= ' title="' . $tip . '"';
	$ret .= '></span>';
	return($ret);
}

function end_page() {
	?>
	</div><!-- end of mainarea -->
</div><!-- end of container -->

</body>
</html>
<?php
}


// format some phone numbers
function  phone_number($inPhone) {
	if (is_null($inPhone)) {
		return '';
	}

	$sPhone = preg_replace('/\D/', '', $inPhone);  //remove nondigits

	if ($sPhone == 0) {
		return '';
	}

	// format 11, 10, or 7 digit numbers, just return others
	if(strlen($sPhone)  ==  10) {
		$sArea  =  substr($sPhone,0,3);
		$sPrefix  =  substr($sPhone,3,3);
		$sNumber  =  substr($sPhone,6,4);
		$sPhone  =  "(".$sArea.") ".$sPrefix."-".$sNumber;
		return($sPhone);
	} elseif (strlen($sPhone) == 7) {
		$sPrefix  =  substr($sPhone,0,3);
		$sNumber  =  substr($sPhone,3,4);
		$sPhone  =  $sPrefix."-".$sNumber;
		return($sPhone);
	} elseif (strlen($sPhone) == 11) {
		if (substr($sPhone, 0, 1) == 1) {
			// it starts with 1
			$sArea  =  substr($sPhone,1,3);
			$sPrefix  =  substr($sPhone,4,3);
			$sNumber  =  substr($sPhone,7,4);
			$sPhone  =  "(".$sArea.") ".$sPrefix."-".$sNumber;
			return($sPhone);
		}
	}
	return($inPhone);
}

function byteSize($bytes) {
	//return number of bytes in human readable format
	$size = $bytes / 1024;
	if ($size < 1024) {
		$size = number_format($size, 0);
		$size .= ' Kb';
	} else if ($size / 1024 < 1024) {
		$size = number_format($size / 1024, 1);
		$size .= ' Mb';
	} else {
		$size = number_format($size / 1024 / 1024, 1);
		$size .= ' Gb';
	}
	return $size;
}

function htmlchars($text) {
	// replace html special characters & < > ' "
	// do not use this on text that contains html tags.
	return htmlspecialchars($text, ENT_QUOTES);
}


function Show_Info($title, $content, $placement='right') {
	//for popups at the bottom of the page, use the top placement and viceversa ?>
<a class="pop"
data-bs-title="<?= $title ?>"
data-bs-html="true"
data-bs-placement="<?= $placement ?>"
data-bs-content="<?= $content ?>"><?php add_icon('info-circle')?></a>
<?php
}


function makeRow($display, $value, $class=false, $help=false) {
	// make a standard row in a two-column table
	if ($help) {
		$helpTitle = $help[0];
		$helpID = $help[1];
		isset($help[2]) ? $helpLoc = $help[2] : $helpLoc='right';
	}
	?>
<tr>
<th<?php if ($class) echo ' class="' . $class . '"'; ?>><?= $display ?><?php if ($help):
echo '&nbsp;';
Show_Info($helpTitle, getHelp($helpID), $helpLoc);
endif; ?>
</th>
<td><?php if($value) echo nl2br($value) ?></td>
</tr>
<?php }

function isApple() {
	// see if this request is from an Apple iOS, or MacOS system.
	// note that Safari on iPadOS identifies as MacOS if so configured.
	// Script Kiddies and command line PHP don't set user agent.
	if ( isset($_SERVER['HTTP_USER_AGENT']) ) {
		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		$regex = '/macintosh|mac os x|iphone|ipad|ipod/i';

		if ( preg_match($regex, $user_agent ) ) {
			return true;
		}
	}
	return false;
}


function fileVersion($filename) {
	// add a version timestamp to a link to prevent overzealous browser caching.
	// mostly for pdf, css, and js files.

	$ret = $filename; // default if we cannot find it

	if (strpos($filename, '/') === 0) {
		// the filename starts with '/' (relative to document root)
		$fullpath = $_SERVER['DOCUMENT_ROOT'] . $filename;
		if (file_exists($fullpath)) {
			$ret = $filename . '?v=' . filemtime($fullpath);
		}
	} else {
		// relative path
		if (file_exists($filename)) {
			$ret = $filename . '?v=' . filemtime($filename);
		}
	}

	return $ret;
}

// Builds up an error screen
function error($box_title, $content) {
	// note that error is sometimes called from ajax or some script not in the usual directory depth
	// so we call the CDN version of css and don't worry about our customizations.
	?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
		integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
	<title>Error Page</title>
</head>
<body>
<div class="container">
	<h3 class="mt-3"><?= $box_title ?></h3>
	<div class="alert alert-danger">
		<?= $content ?>
	</div>
</div>
</body>
</html>

<?php
exit;   //do not return as that would be futile
}

// Function to call at the beginning of pages that expect POST data to
// avoid PHP Notices and possible bad effect when bots and others GET them.
function requirePost() {
    if (empty($_POST)) error('Invalid',
        '<p>This page expects data from a form. None was present.</p>' .
        '<p>If you think this is a problem with our web site, please report ' .
        'it to <a href="mailto:webmaster@californiaalpineclub.org">the webmaster</a>.</p>'
    	);
}

function wpAddUser() {
	// add a new user to WordPress
	global $displayResult;

	if (empty($_POST['user_login'])) {
		$displayResult .= "did not create new WordPress user - no Username provided";
		return false;
	}

	// WP needs the following fields too
	$_POST['role'] = 'subscriber';
	$_POST['pass1'] = wp_generate_password();
	$_POST['pass2'] = $_POST['pass1'];

	// The default action run by add_user() is to notify both new user and admin or just admin.
	// We want to notify only user, so remove the default action and send notification directly
	// later in this script (if creating new user succeeds).
	remove_action( 'edit_user_created_user', 'wp_send_new_user_notifications', 10 );

	$_POST = add_magic_quotes( $_POST ); // we removed WordPress quoting in the auth.php file.
	$returnID = add_user(); // a WordPress function. It uses the $_POST data, which it expects to be quoted.

	if (is_wp_error($returnID)) {
		$displayResult .= "there was a problem creating the WordPress user \n";
		$wpErrs = $returnID->get_error_messages();
		foreach ($wpErrs as $err) {
			$displayResult .= "  $err \n";
		}
		return false;
	} else {
		$displayResult .= "created a new WordPress user with user_id = $returnID \n";
		add_user_meta($returnID, 'active', 1); // Ensure the new user is activated

		// This filter is needed only for testing from a "localhost" development site.
		// By default WP uses 'wordpress@localhost; as the from: address and
		// phpMailer considers that invalid, so mail will not be sent.

		if (strpos(network_home_url(), 'localhost')) {
			add_filter('wp_mail_from', function() { return 'members@example.com'; } );
		}

		// send password reset email only to the new user (not admin)
		wp_send_new_user_notifications($returnID, 'user');
		$displayResult .= "sent a password reset email to the new WP user \n";

		return $returnID;
	}
}


function wpEditUser($wpID) {
	// edit a WordPress user
	global $displayResult;

	// edit_user() function expects a WP nickname.
	// look it up so we can feed it back unchanged.
	$nicknameArray = get_user_meta($wpID, 'nickname');
	$_POST['nickname'] = $nicknameArray[0];

	// edit_user() function expects wpEmail to be named email
	$_POST['email'] = $_POST['wpEmail'];

	$_POST = add_magic_quotes( $_POST ); // we removed WordPress quoting in the auth.php file.
	$returnID = edit_user($wpID); // a WordPress function. It uses the $_POST data, which it expects to be quoted.

	$wpErrs = array();
	if (is_wp_error($returnID)) {
		$displayResult .= "there was a problem editing the WordPress user \n";

		$wpErrs = $returnID->get_error_messages();
		foreach ($wpErrs as $err) {
			$displayResult .= "  $err \n";
		}
		return false;
	} else {
		// success
		$displayResult .= "edited the WordPress user with user_id = $wpID \n";
		return true;
	}
}

function wpAssign($oMember, $wpAssign, $wpDetach = false) {
	// assign a member to an already existing WP account
	global $displayResult;

	if ($wpAssign) {
		// make sure the WP account exists
		$user = get_userdata($wpAssign);
		if ($user === false) {
			$displayResult .= "did not assign to WordPress user_id = $wpAssign because that WP user does not exist. \n";
			return false;
		} else {
			$oMember->wpID = $wpAssign;
			$oMember->dbSave();
			$displayResult .= "assigned to WordPress user_id = $wpAssign \n";
			return true;
		}
	} else {
		if ($wpDetach) {
			$oMember->wpID = 0;
			$oMember->dbSave();
			$displayResult .= "detached member from WordPress account \n";
			return true;
		} else {
			$displayResult .= "did not revise WordPress account status \n";
		}
	}
}
?>
