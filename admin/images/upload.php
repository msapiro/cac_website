<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

// upload a file from ckeditor image tool or our add.php page into the images/uploads folder

if (empty($_GET['CKEditor'])) {
	// this upload is being called from our own pages
	$ckUpload = false;
	$funcNum = NULL;
	require '../../support/functions.php'; // need the error() function
} else {
	// this upload is being called from the CKeditor image uploader
	$ckUpload = true;
	$funcNum = $_GET['CKEditorFuncNum'];
}

/**
 * from includes/config.php
 * @var string  $allowed_extension
 * @var int $thumbWidth
 * @var int $thumbHeight
 * @var float $thumbRatio
 * */

if (empty($_FILES['upload'])) {
	// no info about the error
	sendError('There was a problem with the upload.');
}

$doc = basename($_FILES['upload']['name']);
if (badFilename($doc)) {
	$errMsg = 'Upload failed - filename can only contain letters, numbers, spaces,
		- (dash) _ (underscore) and . (dot) and must be less than 200 characters.';
	sendError($errMsg);
}

$file_extension = pathinfo($_FILES['upload']['name'], PATHINFO_EXTENSION);
if (is_null($file_extension)) {
	$errMsg = 'Upload failed - a file extension is required.';
	sendError($errMsg);
} else {
	$file_extension = strtolower($file_extension);
}
if (! in_array($file_extension, $allowed_extension)) {
	$errMsg = 'Upload failed - ' . $file_extension . ' is not an allowed file extension.';
	sendError($errMsg);
}

if ($_FILES['upload']['error'] <> 0 ) {
	$phpFileUploadErrors = array(
			0 => 'There is no error, the file uploaded with success',
			1 => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
			2 => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
			3 => 'The uploaded file was only partially uploaded',
			4 => 'No file was uploaded',
			6 => 'Missing a temporary folder',
			7 => 'Failed to write file to disk.',
			8 => 'A PHP extension stopped the file upload.',
	);
	$errMsg = $phpFileUploadErrors[$_FILES['upload']['error']];
	sendError($errMsg);
}

if ($_FILES['upload']['size'] == 0) sendError("The uploaded file was empty so it was not copied.");

// rotate the image according to the orientation data in exif,
// then set the orientation to normal, so we can use it anywhere
// without relying on the viewing software to rotate the image
$iMag = new Imagick($_FILES['upload']['tmp_name']);
autoRotateImage($iMag);


$image = new Images(); // a blank object

// test the file to see if it is already in the database.
// this is a SHA-256 hash of just the image pixels, not including any metadata. Good!
$image->signature = $iMag->getimagesignature();

$id = $image->isDuplicate();
if ($id) {
	// do not complete the upload
	if ($ckUpload) {
		// return info for existing image
		$image = new Images($id);
		$url = $image->fileURL;
		echo '<script>window.parent.CKEDITOR.tools.callFunction(' . $funcNum . ', "' . $url . '", "")</script>';
		exit;
	} else {
		// go to the page for the existing image
		header("Location: detail.php?duplicate=1&id=$id");
		exit;
	}
}

// not a duplicate, so continue with upload
$image->ext = $file_extension;
$image->memberID = $_SESSION['cac_login']['memberID'];
$image->origFilename = $_FILES['upload']['name'];
$image->size = $_FILES['upload']['size'];

$image->width = $iMag->getImageWidth();
$image->height = $iMag->getImageHeight();

$image->dbSave(); // need to get the imageID to use in moving the file

umask(0002); //this fixes the problem where group write is not set

if (move_uploaded_file($_FILES['upload']['tmp_name'], $image->filePath)) {

	// make a square thumbnail jpeg for use in display gallery
	if ( $image->width > $thumbWidth || $image->height > $thumbHeight) {
		// the image is too big, resize it
		$ratio = $image->width / $image->height;
		if ($ratio > $thumbRatio) {
			$iMag->thumbnailImage($thumbWidth, null);
		} else {
			$iMag->thumbnailImage(null, $thumbHeight);
		}
	}

	// see what we ended up with
	$imgwidth = $iMag->getImageWidth();
	$imgheight = $iMag->getImageHeight();


	if ($imgwidth < $thumbWidth -2 || $imgheight < $thumbHeight -2) {
		//put our image on a grey background
		$canvas = new Imagick();
		$canvas->newImage($thumbWidth, $thumbHeight, new ImagickPixel("rgb(220,220,220)"));
		$canvas->setImageFormat("jpeg");

		$yoffset = floor(($thumbHeight - $imgheight) / 2);
		$xoffset = floor(($thumbWidth - $imgwidth) / 2);
		$canvas->compositeImage($iMag, imagick :: COMPOSITE_OVER, $xoffset, $yoffset);
		$canvas->writeImage($image->thumbPath);
		$canvas->destroy();
	} else {
		//img is close enough, no need for background
		$iMag->setImageFormat('jpeg');
		$iMag->writeImage($image->thumbPath);
		$iMag->destroy();
	}


	if ($ckUpload) {
		$url = $image->fileURL;
		echo '<script>window.parent.CKEDITOR.tools.callFunction(' . $funcNum . ', "' . $url . '", "")</script>';
	} else {
		header("Location: edit.php?firstload=1&id=$image->imageID");
	}
} else {
	$image->dbDelete(); // clean up
	sendError("There was a problem moving the temporary file to the uploads directory.");
}

exit;

function badFilename($filename) {
	$result = true;
	if (preg_match("`^[- 0-9A-Z_\.]+$`i", $filename)) $result = false;
	if (strlen($filename) > 200) $result = true;
	return $result;
}

function sendError($errMmsg) {
	global $ckUpload, $funcNum;
	if ($ckUpload) {
		echo '<script>window.parent.CKEDITOR.tools.callFunction(' . $funcNum . ', "", "' . $errMmsg . '")</script>';
	} else {
		error('Upload Error', $errMmsg);
	}
	exit;
}

// Note: $oImage is an Imagick object
function autoRotateImage(&$oImage) {
	$orientation = $oImage->getImageOrientation();

	switch($orientation) {
		case imagick::ORIENTATION_TOPLEFT:
			// nothing to do
			return;

		case imagick::ORIENTATION_BOTTOMRIGHT:
			$oImage->rotateimage("#000", 180); // rotate 180 degrees
			break;

		case imagick::ORIENTATION_RIGHTTOP:
			$oImage->rotateimage("#000", 90); // rotate 90 degrees CW
			break;

		case imagick::ORIENTATION_LEFTBOTTOM:
			$oImage->rotateimage("#000", -90); // rotate 90 degrees CCW
			break;
	}

	// Now that it's auto-rotated, mset EXIF data in case the EXIF gets saved with the image
	$oImage->setImageOrientation(imagick::ORIENTATION_TOPLEFT);
	$oImage->writeImage(); // overwrites the file
}
