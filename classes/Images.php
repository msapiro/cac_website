<?php

class Images extends Model {

	public $imageID;
	public $ext;
	public $name = NULL;
	public $signature;
	public $memberID;
	public $origFilename;
	public $width = 0;
	public $height = 0;
	public $size = 0;
	public $credit = NULL;
	public $hide = 0;
	public $notes= NULL;
	public $created;

	public $filePath;
	public $fileURL;
	public $thumbPath;
	public $thumbURL;
	public $absoluteURL;

	public function __construct($imageID = 0) {
		parent::__construct(); // set up our database
		if ($imageID) {
			$this->getByID($imageID);
		}
	}

	public function getByID($imageID) {
		// get the values for an existing image
		$row = $this->db->get('images', '*', ['imageID' => $imageID]);
		if (! $row) return;
		foreach ($row as $key => $value) {
			$this->$key = $value;
		}
		$this->setPaths();
	}

	public function setPaths() {
		$this->filePath = realpath(__DIR__ . '/../uploads') . '/' . $this->imageID . '.' . $this->ext;
		$this->thumbPath = realpath(__DIR__ . '/../uploads/thumb') . '/' . $this->imageID . '.jpg';

		// we want fileURL to be a relative path, not full url
		$this->fileURL = '/cac/uploads/' . $this->imageID . '.' . $this->ext;
		$this->thumbURL = '/cac/uploads/thumb/' . $this->imageID . '.jpg';

		// devel installations often have an apache alias
		if ($_SERVER['CONTEXT_PREFIX'] != '/') {
			$this->fileURL = $_SERVER['CONTEXT_PREFIX'] . $this->fileURL;
			$this->thumbURL = $_SERVER['CONTEXT_PREFIX'] . $this->thumbURL;
		}

		$this->absoluteURL = $_SERVER['SERVER_NAME'] . $this->fileURL;
		if (! empty($_SERVER['HTTPS'])) {
			$this->absoluteURL = 'https://' . $this->absoluteURL;
		} else {
			$this->absoluteURL = 'http://' . $this->absoluteURL;
		}
	}

	public function getAll($mailer = false) {
		// return an array of image objects
		$imageArray = array();
		if ($mailer) {
			$where = [ "hide" => 0, "ORDER" => ["imageID" => "DESC"] ];
		} else {
			$where = ["ORDER" => ["imageID" => "DESC"]];
		}
		$imageList = $this->db->select("images", "imageID", $where);
		foreach ($imageList as $imageID) {
			$imageArray[] = new Images($imageID);
		}
		return $imageArray;
	}

	public function isDuplicate() {
		// return the imageID of first other image to match signature
		// or return false if no other image matches
		$ids = $this->db->select("images", "imageID", ["signature" => $this->signature]);
		foreach ($ids as $imageID) {
			if ($imageID != $this->imageID) return $imageID;
		}
		return false;
	}

	public function dbSave() {
		// save the image info into the database table
		$dataFields = array('ext', 'name', 'signature', 'memberID', 'origFilename',
			'width', 'height', 'size', 'credit', 'hide', 'notes');

		nully($this->name);
		nully($this->credit);
		nully($this->notes);

		$dataArray = array();
		foreach ($dataFields as $field) {
			$dataArray[$field] = $this->$field;
		}

		if ($this->imageID) {
			// update existing image
			$this->db->update('images', $dataArray, ['imageID' => $this->imageID] );
		} else {
			// create new image. note that the uploaded file hasn't been moved yet
			$this->db->insert('images', $dataArray);
			$this->imageID = $this->db->id();
			$this->setPaths();
		}

		return $this->imageID;
	}

	public function dbDelete() {
		if ($this->imageID) {
			// remove the image files
			if (file_exists($this->filePath)) unlink($this->filePath);
			if (file_exists($this->thumbPath)) unlink($this->thumbPath);
			$obj = $this->db->delete('images', ['imageID' => $this->imageID]);
			$ret = $obj->rowCount(); // number of affected rows
		} else {
			$ret = 0;
		}
		return $ret;
	}

} // end Images class
