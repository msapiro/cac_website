<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
array_push($allowGroups, 'ROmembers');
require AUTH_PAGE; // make sure user is logged in and a group member

$title = 'CAC Membership';
$pageDescription = 'Membership Applications';

require '../../support/functions.php';
require '../../support/formFunctions.php';
include 'includes/tools.php';

$limit = 10; // number of applications to show on one page
$offset = inty($_GET['offset']);

$thisPage = $_SERVER["PHP_SELF"];
$prevURL = false;
$prev = $offset + 1;
$prevURL = $thisPage . '?offset=' . $prev;

$next = $offset - 1;
if ($next > 0) {
	$nextURL = $thisPage . '?offset=' . $next;
} elseif ($next == 0) {
	$nextURL = $thisPage;
} else {
	$nextURL = false;
}

if ($offset) {
	$limit = [$offset * $limit, $limit];
}

$oApplicant = new Applicants();
$applyArray = $oApplicant->getAll($limit);

start_page($title, $pageDescription);
?>
<script>
$(function() {
	// do stuff when DOM is ready

	// make nice tooltips for the buttons
	$('.tip').tooltip({container: 'body', placement: 'bottom'});
});
</script>
<?php
start_content();
toolbar();
?>
<div class="row justify-content-center">
	<div class="col-xl-10">

	<div class="row">
		<div class="col-sm-5 col-md-4 col-lg-3 mt-3">
			<div class="btn-group">
<?php if ($prevURL): ?>
			<a class="btn btn-secondary" href="<?= $prevURL ?>">
				<?php add_icon('chevron-left') ?> Previous
			</a>
<?php else : ?>
			<a class="btn btn-secondary disabled" href="#">
				<?php add_icon('chevron-left') ?> Previous
			</a>
<?php endif;
if ($nextURL): ?>
			<a class="btn btn-secondary" href="<?= $nextURL ?>">
				Next <?php add_icon('chevron-right') ?>
			</a>
			<a class="btn btn-secondary" href="<?= $thisPage ?>">
				<?php add_icon('step-forward') ?>
			</a>
<?php else : ?>
			<a class="btn btn-secondary disabled" href="#">
				Next <?php add_icon('chevron-right') ?>
			</a>
<?php endif; ?>
			</div>
		</div>

		<div class="col-sm-6 col-md-7">
			<h2 class="pageheader">Membership Applications</h2>
		</div>
	</div> <!-- end row -->

	<table class="table table-striped">
	<thead>
		<tr>
			<th class="text-center">Details</th>
			<th>Status</th>
			<th class="text-center">Type</th>
			<th>Name(s)</th><th>Sponsors</th>
		</tr>
	</thead>
	<tbody>

<?php
foreach ($applyArray as $applicant):
	$URLdetail = 'applyDetail.php?id=' . $applicant->applicantID;

	$name = $applicant->fullname;
	if (empty(trim($name))) $name = $applicant->email;

	if ($applicant->applyType == 'JT') {
		$name .= '<br>' . $applicant->fullname2;
	} elseif ($applicant->applyType == 'JTadd') {
		$name .= '<br>[add to ' . $applicant->addToMember . ']';
	}

	$sponsors = $applicant->sponsor1;
	if ($applicant->sponsor2) $sponsors .= '<br>' . $applicant->sponsor2;

	$shade = false;
	if ($applicant->complete) {
		$status = 'complete<br>' . $applicant->complete;
	} elseif ($applicant->approved) {
		$status = 'approved pending payment<br>' . $applicant->approved;
		$shade = 'bg-danger';
	} elseif($applicant->submitted) {
		$status = 'submitted for approval<br>' . $applicant->submitted;
		$shade = 'bg-warning';
	} else {
		$status = 'created, still editing<br>' . $applicant->created;
		$shade = 'bg-success';
	}
?>
	<tr data-id="<?=  $applicant->applicantID ?>">
	<td class="text-center">
		<a href="<?= $URLdetail ?>" class="btn btn-sm btn-secondary tip" title="Application Details">
			<?php add_icon('list-alt') ?>
		</a>
	</td>
	<td<?php if ($shade) echo ' class="' . $shade . '"'?>><?= $status ?></td>
	<td class="text-center"><?= $applicant->applyType ?></td>
	<td><?= $name ?></td>
	<td><?= $sponsors ?></td>
	</tr>

<?php endforeach; ?>
	</tbody>
	</table>

	</div> <!-- end col -->
</div> <!-- end row -->

<?php end_page(); ?>
