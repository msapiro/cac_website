<?php
// edit configuration options here

// some common paths
define('CAC_DIR', realpath(__DIR__ . '/../../') . '/');
define('CLASSLOADER', CAC_DIR . 'classes/classLoader.php');
define('AUTH_PAGE', CAC_DIR . 'auth.php');
define('AUTH_AJAX', CAC_DIR . 'auth_ajax.php');

// This title will be used by default unless a different
// one is specified in the individual page.
$defTitle = 'California Alpine Club';

// This description will be put in a meta tag on each page unless a different
// one is specified in the individual page.
$defDescription = 'California Alpine club';


// Define the categories for the top navigation menu.
// These top menu items will always be visible.
// They won't collapse into the hamburger for small screens
// Give directory name and text to be used on the button.
$menuShow = array (
	'home' => 'HOME',
	'members' => 'MEMBERS'
);

// These top menu items will collapse into the hamburger for small screens.
$menuCategory = array(
	'volunteers' => 'VOLUNTEERS',
	'mailer' => 'MAILER',
	'images' => 'IMAGES',
	'finance' => 'FINANCE',
	'events' => 'EVENTS',
	'auth' => 'AUTH'
);

// groups that have special privileges in addition to members privileges
// these appear in form on auth/groups.php
$privGroups = array('admin', 'members', 'ROmembers', 'mailer', 'finance',
	'events', 'volunteer', 'trainee');

// explicity set the Stripe API version
$stripeApiVersion = "2022-11-15";
