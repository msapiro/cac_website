<?php

class ClassOrder extends Model {
	public $orderArray = array();

	public function __construct($get = false) {
		parent::__construct(); // set up our database
		if ($get) {
			$this->getOrder();
		}
	}

	public function getOrder() {
		$this->orderArray = array();
		$rows = $this->db->select('classes', 'class', ['ORDER' => 'displayOrder']);
		if (count($rows)) {
			foreach ($rows as $class) {
				$this->orderArray[] = $class;
			}
		}
	}

	public function dbSave() {
		foreach ($this->orderArray as $order => $class) {
			$this->db->update('classes', ['displayOrder' => $order], ['class' => $class]);
		}
	}
} // end class ClassOrder
