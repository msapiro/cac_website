#!/usr/bin/python3

import re
import sys
import pymysql

from datetime import date, timedelta

BUCKET_SIZE = 5
now = date.today()
ranges = dict()
for x in range(int(200/BUCKET_SIZE)):
    years = x * BUCKET_SIZE
    days = years * 365.25
    ranges[x] = (timedelta(days), years)

def sort_key(x):
    if x[0].startswith('Missing'):
        return 999
    mo = re.search('^Between (\d+)', x[0])
    if mo:
        return int(mo.group(1))
    return 0
    
def get_cat(dob):
    if not dob or isinstance(dob, str):
        return 'Missing'
    for x in range(int(200/BUCKET_SIZE)):
        if dob > now - ranges[x][0]:
            return f'Between {ranges[x][1] - BUCKET_SIZE} and {ranges[x][1]}'
    print(f'Strange dob: {dob}', file = sys.stderr)

def main():
    db = pymysql.connect(user='CAC_query',
                         passwd='CAC_dbqry_1913',
                         db='alpine',
                         charset='utf8')
    c = db.cursor()
    c.execute("""SELECT dob FROM memberships JOIN members USING (membershipID)
              WHERE class NOT IN ('', '0', 'OR', 'NM') AND expiration > %s AND resigned = 0""",
              (now - timedelta(91)))
    data = dict()
    total = 0
    while True:
        rec = c.fetchone()
        if not rec:
            break
        cat = get_cat(rec[0])
        data[cat] = data.setdefault(cat, 0) + 1
    items_list = list(data.items())
    items_list.sort(key=sort_key)
    for cat, count in items_list:
        print(f'{cat}: {count}')
        total += count
    print(f'Total members: {total}')
    c.close()
    db.close()

if __name__ == '__main__':
    main()
