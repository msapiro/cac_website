<?php
require '../../support/config.php';
require CLASSLOADER;

require '../includes/config.php'; // contains $allowGroups for this directory
//array_push($allowGroups, 'ROmembers');
require AUTH_AJAX;                // make sure user is logged in and a group member

inty($_POST['memberID']);
$data = array();

$member = new Members($_POST['memberID']);
if (empty($member->memberID)) {
	$data['memberID'] = 0;
	$data['text'] = 'unknown user';
} else {
	$data['memberID'] = $member->memberID;
	$data['text'] = $member->fullname . ' - ' . $member->email;
}

echo json_encode($data);
