<?php
// Including this file at the top of a php script enables PHP to find and load
// classes without our having to explicity "require" the class file. It works
// best for classes whose name match the filename, so we should do that going
// forward. However, there is a workaround below that allows it to also find
// specific classes inside other files, for backward compatibility. Also be
// aware that the match is case-sensistive on filesystems that are case-sensitive.

// PHP uses this loader as a last resort. If the class is found by some other
// means first, then this is not called, so there is no conflict if we also
// require the class file directly.

// This file is not a class itself, even though it is located in the classes dir.

// Most of the classes require database access and we want the extra functions
// inty(), booly(), etc, so just load the Model class now. Also load the
// Composer vendor/autoload.php, since it is needed by the Medoo package in Model.

require_once 'vendor/autoload.php'; // for Medoo, Stripe, and other packages
require_once __DIR__ . '/Model.php';

spl_autoload_register('ourAutoLoader');


function ourAutoLoader($className) {
	$fullPath = __DIR__ . '/' . $className . '.php';

	if (file_exists($fullPath)) {
		// good, the standard method worked
		require_once $fullPath;

	} elseif (in_array($className,
		['Memberships', 'Classes', 'WP_data'])) {
		$fullPath = __DIR__ . '/Members.php';
		require_once $fullPath;

	} elseif (in_array($className,
		['MsgGroups'])) {
			$fullPath = __DIR__ . '/Msgs.php';
			require_once $fullPath;

	} elseif (in_array($className,
		['MemberTrans', 'Donations', 'DonationsGF', 'DonationsFDN', 'Stripes'])) {
			$fullPath = __DIR__ . '/Payments.php';
			require_once $fullPath;

	} elseif (in_array($className,
		['VolOpps', 'OppsOrder', 'VolCats', 'CatsOrder', 'VolNotes'])) {
			$fullPath = __DIR__ . '/Volunteers.php';
			require_once $fullPath;
	}
}


// The following functions are often used with database columns,
// but do not need to be part of the Model class. We put them here
// just for convenience.

// Important: If an undefined variable is called by reference,
// the variable is created and defined as NULL, and a warning is not generated.

// checkboxes usually represent boolean db columns but an
// unchecked box on a form does not create a $_POST variable.
// Set the original (referenced) parameter and also return the boolean.
function booly(&$post) {
	if (empty($post)) {
		$post = 0;
	} else {
		$post = 1;
	}
	return $post;
}

// sometimes we want blank fields and fields consisting of whitespace to be set to NULL.
// Set the original (referenced) parameter and also return the value.
function nully(&$post) {
	if (empty($post)) {
		$post = NULL;
	} else {
		// php 8 trim() does not accept null arg. We know it is not.
		$post = trim($post);
		if (empty($post)) {
			$post = NULL;
		}
	}
	return $post;
}

// Sometimes we want to force an undefinded var to be an integer.
// Set the original (referenced) parameter and also return the integer.
function inty(&$post) {
	if (empty($post)) {
		$post = 0;
	} else {
		$post = intval($post);
	}
	return $post;
}
