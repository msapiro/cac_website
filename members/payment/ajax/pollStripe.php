<?php
// ajax call to poll Stripe for recent checkout.session.completed events - no login required
// this confirms payment before releasing the pending changes
require '../../support/config.php';
require CLASSLOADER;

require '../../../support/functions.php';
include '../includes/functions.php';


requirePost();

$data = array();
$data['success'] = false;
$data['btnText'] = false;

$stripe = new Stripes($_POST['stripeID']);

if (empty($stripe->stripeID)) {
	$data['success'] = 0;
	$data['msg'] = 'Could not find a transaction for stripeID=' . $_POST['stripeID'];
	echo json_encode($data);
	exit;
}
/**
 * from config file
 * @var string $stripeSecretKey
 * @var string $stripeApiVersion
 * */

if ($stripe->status == 'pending') {
	// we need to call Stripe
	require '/www/cac/data/credentials/stripe.php';
	$stripeClient = new \Stripe\StripeClient([
			'api_key' => $stripeSecretKey,
			'stripe_version' => $stripeApiVersion
	]);

	try {
		$session = $stripeClient->checkout->sessions->retrieve(
			$_POST['sessionID'], ['expand' => ['payment_intent']]
		);
	} catch (\Stripe\Exception\ApiErrorException $e) {
		$data['msg'] = "Stripe returned the following error while retrieving"
			. " the checkout session.<br>$e<br>Please contact membership@californiaalpineclub.org";
			echo json_encode($data);
			exit;
	}

	if (empty($session)) {
		$data['msg'] = 'Could not find a checkout session for sessionID=' . $_POST['sessionID'] . '.';
		echo json_encode($data);
		exit;
	}

	if ($session->client_reference_id <> $stripe->stripeID) {
		$data['msg'] = 'Could not find a checkout for stripeID=' . $stripe->stripeID . '.';
		echo json_encode($data);
		exit;
	}

	$paymentIntent = $session['payment_intent']; // could be NULL if user never submitted pmt info

	if (is_null($paymentIntent) || $paymentIntent['status'] != 'succeeded') {
		$data['msg'] = 'Payment not completed for stripeID=' . $stripe->stripeID;
		echo json_encode($data);
		exit;
	}

	// new in api version 2022-11-15
	$chargeID = $paymentIntent->latest_charge;
	$charge = $stripeClient->charges->retrieve($chargeID, ['expand' => ['balance_transaction']]);

	$stripe->status = 'ok';
	$stripe->chargeID = $charge->id;
	$stripe->stripeAmount =  $charge->amount / 100; // maybe test this against $amount
	$stripe->stripeFee =  $charge->balance_transaction->fee / 100;
	$stripe->amountNet = $charge->balance_transaction->net / 100;
	$stripe->dbSave();

} elseif ($stripe->status == 'ok') {
	// already done with this item
	// don't set $data['btnText'] so we don't show the login button
	$data['success'] = true;
	$data['msg'] = '<p>' . add_icon_text('check-circle') . ' Verified your payment of $'
		. $stripe->amount . ' for <b>' . $stripe->item . '</b>.</p>';
	echo json_encode($data);
	exit;
} else {
	// something else, maybe in the future
	$data['msg'] = 'That charge was already processed with status = ' . $stripe->status;
	echo json_encode($data);
	exit;
}


// successful charge. release the pending changes
$data['success'] = true;
$data['btnText'] = 'Log In';
$msg = '<p>' . add_icon_text('check-circle') . ' Verified your payment of $' . $stripe->amount
	. ' for <b>' . $stripe->item . '</b>.</p>';
$msg .= '<p>A receipt will be sent from Stripe (our payment processor) to <b>'
	.  $session->customer_email . '</b>.</p>';

// donations are part of the renewal
switch ($paymentIntent->metadata->category) {
	case 'dues':
		switch ($paymentIntent->metadata->transType) {
			case 'new':
				// we only have renewals at the moment
				break;
			case 'renew':
				releaseRenewal($stripe->stripeID);
				$data['btnText'] = 'Continue';
				break;
		}
		break;

	case 'application':
		releaseApplication($stripe->stripeID);
		$msg .= '<p>Welcome! Your membership has been activated. You will receive an email with'
			. ' instructions for setting a login password.</p>';
		break;

	case 'reinstate':
		releaseReinstate($stripe->stripeID);
		$msg .= '<p>Welcome! Your membership has been reinstated. Unless you already have one, you'
			. ' will receive an email with instructions for setting a login password.</p>';
		break;
}

$data['msg'] = $msg;
echo json_encode($data);
