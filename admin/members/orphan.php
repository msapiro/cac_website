<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';


inty($_POST['memberID']);
inty($_POST['membershipID']);

$member = new Members($_POST['memberID']);
if (empty($member->memberID)) {
	error('Error', 'Did not find a member with memberID=' . $_POST['memberID']);
	exit;
}

$orphanMembership = new Memberships($member->membershipID);

if ($orphanMembership->class != 'OR') {
	error('Error', "Member with memberID=$member->memberID is not an orphan.");
	exit;
}

$membership = new Memberships($_POST['membershipID']);
if (empty($membership->membershipID)) {
	error('Error', 'Did not find a membership with membershipID=' . $_POST['membershipID']);
	exit;
}

if (! $membership->joint) {
	error('Error', "Membership with membershipID= $membership->membershipID is not a Joint membership.");
	exit;
}

if (count($membership->getJointMembers()) >= 2) {
	error('Error', "Membership with membershipID= $membership->membershipID already has 2 members.");
	exit;
}

// OK to proceed
$member->membershipID = $membership->membershipID;
$member->dbSave();
$orphanMembership->delete();

header("Location: detail.php?id=$member->memberID");
