<?php

//tools specific to this category

function getHelp($subject) {
	switch ($subject) {
		case 'memberAuto':
			$helpText = "<ul>
<li>Start typing a member name or email address.</li>
<li>You can use partial words.</li>
<li>Select a member from the matches, or keep typing to reduce the set.</li>
<li>You can use your arrow keys to scroll and the Return key to select.</li>
</ul>";
			break;

		case 'appCreated':
			$helpText = "When the applicant saved the first draft of the application.";
			break;

		case 'appSubmitted':
			$helpText = "When the applicant finished the application and submitted it.";
			break;

		case 'appApproved':
			$helpText = "When the administrator finished reviewing the application, approved it, and sent an invoice.";
			break;

		case 'appComplete':
			$helpText = "When the applicant paid the invoice online or the administrator received a physical check.";
			break;

		case 'suggestFees':
			$helpText = "Click the button to have the program suggest fees. You may have to correct the suggested amounts
				if the applicant has paid associate fees or there are other circumstances.";
			break;

		default:
			$helpText = 'Help text not found. Is it defined?';
			break;
	}

	return $helpText;
}

function toolbar($id=0){
	//create the toolbar for this page ?>

			<!-- Top Menu Bar -->
			<nav class="navbar navbar-expand cac_tool">
				<div class="navbar-nav">
					<a class="nav-link" href="index.php"><?php add_icon('search') ?> Search</a>
					<a class="nav-link" href="log.php"><?php add_icon('book') ?> Log</a>
					<a class="nav-link" href="applications.php"><?php add_icon('user-check') ?> Applications</a>
					<a class="nav-link" href="edit.php"><?php add_icon('plus') ?> New Member</a>
<?php if ($id): ?>
					<a class="nav-link" href="edit.php?id=<?= $id ?>"><?php add_icon('edit') ?> Edit</a>
<?php endif; ?>
					<a class="nav-link" href="types.php"><?php add_icon('user-cog') ?> Types</a>
					<a class="nav-link" href="reports.php"><?php add_icon('file') ?> Reports</a>

				</div>
			</nav>
<?php
}
?>
