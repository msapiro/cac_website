<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
array_push($allowGroups, 'ROmembers', 'trainee');
require AUTH_PAGE; // make sure user is logged in and a group member


$title = 'CAC Membership';
$pageDescription = 'Member Details';

require '../../support/functions.php';
require '../../support/formFunctions.php';

include 'includes/tools.php';

inty($_GET['id']);

$member = new Members($_GET['id'], ['membership', 'wpData']);
if (empty($member->memberID)) {
	error('Error', 'Did not find a member with memberID=' . $_GET['id']);
	exit;
}

$trailsFormat = 'Electronic only';
if ($member->membership->paperTrails) $trailsFormat = 'Electronic plus mailed paper';

$wpIDdisplay = 'none';
$wpError = false;
if ($member->wpID) {
	$wpIDdisplay = $member->wpID;
	if (! $member->wpData->wpID) {
		$wpError = true;
	}
}

$vol = new Volunteers();
$memOppsArray = $vol->getByMemberID($member->memberID);
$volNote = new VolNotes($member->memberID);

$traineeClass = new Trainees();
$trainings = $traineeClass->getAllTrainings($member->memberID);

if ($member->membership->joint) {
	// get the other member
	$jointMembers = $member->membership->getJointMembers();
	$jointMember = NULL;
	foreach ($jointMembers as $thisMember) {
		if ($thisMember->memberID <> $member->memberID) {
			$jointMember = $thisMember;

			$jointOppsArray = $vol->getByMemberID($jointMember->memberID);
			$jointTrainings = $traineeClass->getAllTrainings($jointMember->memberID);
			$jointNote = new VolNotes($jointMember->memberID);
			$wpIDdisplayJoint = 'none';
			$wpErrorJoint = false;
			if ($jointMember->wpID) {
				$wpIDdisplayJoint = $jointMember->wpID;
				$jointMember->wpData = new WP_data($jointMember->wpID);
				if (! $jointMember->wpData->wpID) {
					$wpErrorJoint = true;
				}
			}
		}
	}
}

$depClass = new Dependents();
$deps = $depClass->getAllDeps($member->membershipID);

start_page($title, $pageDescription);
?>
<script>
$(function() {
	// do stuff when DOM is ready

	// make nice tooltips for the buttons
	$('.tip').tooltip({container: 'body', placement: 'bottom'});

	$("#btnAddDep").click(function() {
		editDependent(0);
	});

	// focus on the first name input when adding a new dependent
	$("#depModal").on('shown.bs.modal', function() {
		if ($("#depID").val() == '0') {
        	$('#first').focus();
		}
    });

	$("#first").prop('required',true);

});

function removeMember(memberID) {
	var agree = confirm("Are you sure you want to remove this member from the Joint membership?\n\nThis will orphan (not delete) the member.");
	if (agree) {
		$.post('ajax/removeJointMember.php', {memberID: memberID}, function(data) {
			if (data.success) {
				location.reload();
			} else {
				alert("There was a problem removing the member from the joint membership.");
			}
		}, "json");
	}
}

function deleteDependent(elmt, depID) {
	var agree = confirm("Are you sure you want to delete this Dependent?");
	if (agree) {
		$.post('ajax/deleteDep.php', {depID: depID}, function() {
			$(elmt).closest("tr").remove();
		});
	}
	elmt.blur();
}

function editDependent(depID) {
	resetDependent(depID);
	$('#depModal').modal('show');
}

function resetDependent(depID) {
	if (depID == 0) {
		$("h4.modal-title").html("Add Dependent");
		$("#depID").val('0');
		$("#first").val('');
		$("#last").val('');
		$("#dob").val('');
	} else {
		$("h4.modal-title").html("Edit Dependent");
		// get the dependent data from database
		$.post('ajax/getDep.php', {depID: depID}, function(data) {
			if (data.success == 1) {
				$("#depID").val(depID);
				$("#first").val(data.first);
				$("#last").val(data.last);
				$("#dob").val(data.dob);
			}
		},'json');
	}
}
</script>
<?php
start_content();
toolbar($member->memberID);
?>

<h2 class="pageheader">Member Details</h2>

<div class="row">
	<div class="col-xl-10 offset-xl-1">

<?php if (isset($_GET['set']) && isset($_SESSION['listPageMembers'])): ?>
	<div style="margin-bottom: 10px">
<?php PrevNext($member->memberID, 'foundArrayMembers', $_SESSION['listPageMembers']) ?>
	</div>
<?php endif; ?>

		<div class="card well">
		<div class="card-body">
			<div class="row">
				<div class="col-lg">
					<table class="table table-sm table-striped">
					<thead>
						<tr class="table-dark">
							<th style="width: 180px">Personal Data</th><th style="font-weight: normal;">memberID: <?= $member->memberID ?></th>
						</tr>
					</thead>
					<tbody>
						<tr><td>Name</td><td><?= $member->fullname ?></td></tr>
						<tr><td>Email</td><td><a href="mailto:<?= $member->email ?>"><?= $member->email ?></a></td></tr>
						<tr><td>Main Phone</td><td><?= phone_number($member->phone) ?></td></tr>
						<tr><td>Mobile Phone</td><td><?= phone_number($member->mobile) ?></td></tr>
						<tr><td>Date of Birth</td><td><?= $member->dob ?></td></tr>
						<tr><td>Created</td><td><?= $member->created ?></td></tr>
					</tbody>
					</table>

					<table class="table table-sm table-striped">
					<thead>
						<tr class="table-dark">
							<th style="width: 180px">WordPress Data</th>
							<th style="font-weight: normal;">wordpressID: <?= $wpIDdisplay ?></th>
						</tr>
					</thead>
					<tbody>
<?php
if ($member->wpID):
	if ($wpError): ?>
						<tr><td colspan="2"><span style="color: red;">ERROR:</span> wordpressID does not exist</td></tr>
<?php
	else: ?>
						<tr><td>WP Username</td><td><?= $member->wpData->user_login ?></td></tr>
						<tr><td>WP Name</td><td><?= $member->wpData->wpFullname ?></td></tr>
						<tr><td>WP Email</td><td><a href="mailto:<?= $member->wpData->wpEmail ?>"><?= $member->wpData->wpEmail ?></a></td></tr>
<?php
	endif;
endif; ?>
					</tbody>
					</table>

					<table class="table table-sm table-striped">
					<thead>
						<tr class="table-dark">
							<th>Volunteer Skills and Interests</th>
							<th class="text-end"><a class="btn btn-sm btn-secondary" href="volunteer.php?id=<?= $member->memberID ?>">Edit</a></th>
						</tr>
					</thead>
					<tbody>
						<tr>
						<td colspan="2">
<?php foreach ($memOppsArray as $oVolOpp) {
	echo $oVolOpp->oppName . '<br>';
} ?>
						</td>
						</tr>
<?php if (! empty($volNote->notes)): ?>
						<tr>
						<td colspan="2">
							<b>Other Skills, Interests, and Comments:</b><br><?= $volNote->notes ?>
						</td>
						</tr>
<?php endif; ?>
					</tbody>
					</table>

					<table class="table table-sm table-striped">
					<thead>
						<tr class="table-dark">
						<th>Host Trainings<br>Lodge, Year</th>
						<th class="text-end"><a class="btn btn-sm btn-secondary" href="traineeEdit.php?id=<?= $member->memberID ?>">Edit</a></th>
						</tr>
					</thead>
					<tbody>
						<tr>
						<td colspan="2">
<?php foreach ($trainings as $training) {
	if ($training->lodge == 'A') {
		$lodge = 'Alpine, ';
	} elseif ($training->lodge == 'E') {
		$lodge = 'Echo, ';
	} else {
		$lodge = 'Unknown, ';
	}
	echo $lodge . $training->year . '<br>';
} ?>
						</td>
						</tr>
					</tbody>
					</table>

				</div>

				<div class="col-lg">
					<table class="table table-sm table-striped">
					<thead>
						<tr class="table-dark">
							<th style="width: 180px">Membership Data</th><th style="font-weight: normal;">membershipID: <?= $member->membershipID ?></th>
						</tr>
					</thead>
					<tbody>
						<tr><td>Membership Type</td><td><?= $member->membership->memDescription ?></td></tr>
						<tr>
							<td>Expiration</td>
							<td><?php echo $member->membership->expiration;
							if ($member->membership->resigned) echo ' <span style="color: red">RESIGNED</span>';
							else if ($member->membership->expired) echo ' <span style="color: red">EXPIRED</span>'; ?></td>
						</tr>
						<tr><td>Street Address</td><td><?= $member->membership->street ?></td></tr>
						<tr><td>City</td><td><?= $member->membership->city ?></td></tr>
						<tr><td>State</td><td><?= $member->membership->state ?></td></tr>
						<tr><td>Zip</td><td><?= $member->membership->zip ?></td></tr>
						<tr><td>Trails Newsletter</td><td><?= $trailsFormat?></td></tr>
						<tr><td>Sponsors</td><td><?= $member->membership->sponsors ?></td></tr>
						<tr><td>Created</td><td><?= $member->membership->created ?></td></tr>
					</tbody>
					</table>
					<a href="renew.php?id=<?= $member->memberID ?>" class="btn btn-secondary mb-3"><?php add_icon('redo') ?>&nbsp;Renew</a>
<?php
$tz = new DateTimeZone('America/Los_Angeles');
$cutoff = (new DateTime('-2hours', $tz))->format('Y-m-d H:i:s');
if ($member->membership->created >= $cutoff): ?>
					<a href="reverse.php?id=<?= $member->membershipID ?>" class="btn btn-danger float-end mb-3"><?php add_icon('undo') ?>&nbsp;Cancel</a>
<?php endif; ?>
<?php
$curyear = date('Y');
$exyear = explode('-', $member->membership->expiration)[0];
if ($exyear < $curyear and $exyear > $curyear - 6): ?>
					<a href="renew.php?id=<?= $member->memberID ?>&reinstate=yes" class="btn btn-secondary mb-3"><?php add_icon('redo') ?>&nbsp;Reinstate</a>
<?php endif; ?>
				</div>

			</div> <!-- end row -->
		</div> <!--  end card body -->
		</div> <!-- end card -->

<?php if ($member->membership->joint): ?>
		<h4 class="pageheader">Joint Member</h4>
		<div class="card well mt-4">
		<div class="card-body">

<?php
	if (is_null($jointMember)): ?>
			<p><a class="btn btn-secondary" href="edit.php?id=<?= $member->memberID ?>&add=1"><?php add_icon('plus') ?> Add Joint Member</a></p>
<?php
	else:
		$familyEmails = array();
		if ($member->email) $familyEmails[] = strtolower($member->email);

		$URLedit = 'edit.php?id=' . $jointMember->memberID;

		if (! empty($jointMember->email)) {
			if (! in_array(strtolower($jointMember->email), $familyEmails)) {
			$familyEmails[] = strtolower($jointMember->email);
			}
		}
?>
			<div class="row">
			<div class="col-lg-6"> <!-- start left column -->

			<div class="btn-group">
				<a href="<?= $URLedit ?>" class="btn btn-secondary"><?php add_icon('edit') ?> Edit <?= $jointMember->first ?></a>
				<button type="button" class="btn btn-secondary" onclick="removeMember(<?= $jointMember->memberID ?>)"><?php add_icon('times')?> Remove</button>
			</div>

			<table class="table table-sm table-striped mt-3">
				<thead>
					<tr class="table-dark">
						<th style="width: 180px">Personal Data</th><th style="font-weight: normal">memberID: <?= $jointMember->memberID ?></th>
					</tr>
				</thead>
				<tbody>
					<tr><td>Name</td><td><a href="detail.php?id=<?= $jointMember->memberID ?>"><?= $jointMember->fullname  ?></a></td></tr>
					<tr><td>Email</td><td><a href="mailto:<?= $jointMember->email ?>"><?= $jointMember->email ?></a></td></tr>
					<tr><td>Main Phone</td><td><?= phone_number($jointMember->phone) ?></td></tr>
					<tr><td>Mobile Phone</td><td><?= phone_number($jointMember->mobile) ?></td></tr>
					<tr><td>Date of Birth</td><td><?= $jointMember->dob ?></td></tr>
					<tr><td>Created</td><td><?= $jointMember->created ?></td></tr>
				</tbody>
			</table>


			<table class="table table-sm table-striped">
				<thead>
					<tr class="table-dark">
						<th style="width: 180px">WordPress Data</th>
						<th style="font-weight: normal;">wordpressID: <?= $wpIDdisplayJoint ?></th>
					</tr>
				</thead>
				<tbody>
<?php
if ($jointMember->wpID):
	if ($wpErrorJoint): ?>
						<tr><td colspan="2"><span style="color: red;">ERROR:</span> wordpressID does not exist</td></tr>
<?php
	else: ?>
						<tr><td>WP Username</td><td><?= $jointMember->wpData->user_login ?></td></tr>
						<tr><td>WP Name</td><td><?= $jointMember->wpData->wpFullname ?></td></tr>
						<tr><td>WP Email</td><td><a href="mailto:<?= $jointMember->wpData->wpEmail ?>"><?= $jointMember->wpData->wpEmail ?></a></td></tr>
<?php
	endif;
endif; ?>
				</tbody>
			</table>

			<table class="table table-sm table-striped">
				<thead>
					<tr class="table-dark">
						<th>Volunteer Skills and Interests</th>
						<th class="text-end"><a class="btn btn-sm btn-secondary" href="volunteer.php?id=<?= $jointMember->memberID ?>">Edit</a></th>
					</tr>
				</thead>
				<tbody>
					<tr>
					<td colspan="2">
<?php foreach ($jointOppsArray as $oVolOpp) {
	echo $oVolOpp->oppName . '<br>';
} ?>
					</td>
					</tr>

<?php if (! empty($jointNote->notes)): ?>
					<tr>
					<td colspan="2">
						<b>Other Skills, Interests, and Comments:</b><br><?= $jointNote->notes ?>
					</td>
					</tr>
<?php endif; ?>

				</tbody>
				</table>

					<table class="table table-sm table-striped">
					<thead>
						<tr class="table-dark">
						<th>Host Trainings<br>Lodge, Year</th>
						<th class="text-end"><a class="btn btn-sm btn-secondary" href="traineeEdit.php?id=<?= $jointMember->memberID ?>">Edit</a></th>
						</tr>
					</thead>
					<tbody>
						<tr>
						<td colspan="2">
<?php foreach ($jointTrainings as $training) {
	if ($training->lodge == 'A') {
		$lodge = 'Alpine, ';
	} elseif ($training->lodge == 'E') {
		$lodge = 'Echo, ';
	} else {
		$lodge = 'Unknown, ';
	}
	echo $lodge . $training->year . '<br>';
} ?>
						</td>
						</tr>
					</tbody>
					</table>

			</div> <!-- end left column -->
<?php
	if (count($familyEmails) <> 2):
?>
			<div class="col-lg-6"> <!-- right column -->
				<div class="alert alert-danger">
					<p>Every CAC member should have a valid and unique email address in order to be able to log in and be properly identified.</p>
					<p>This joint membership does not meet the goal, but that is not unusual for legacy joint memberships.</p>
				</div>
			</div> <!--  end right column -->
<?php
	endif;
?>
			</div>  <!-- end row -->

<?php
endif;
?>

		</div> <!-- end card body -->
		</div> <!-- end card -->
<?php
endif; // end if family
?>

		<h4 class="pageheader">Dependents</h4>
		<div class="card well mt-4">
		<div class="card-body">

			<p><button id="btnAddDep" class="btn btn-secondary" type="button"><?php add_icon('plus') ?>&nbsp;Add Dependent</button></p>
<?php if ($deps): ?>
			<table class="table table-striped table-sm col-lg-10">
				<thead>
					<tr><th class="text-center">Action</th><th>depID</th><th>Name</th><th>DOB</th><th>Age</th></tr>
				</thead>
				<tbody>
<?php 	foreach ($deps as $thisDep): ?>

					<tr>
						<td class="text-center">
							<div class="btn-group btn-group-sm">
								<button type="button" class="btn btn-secondary tip" onclick="editDependent(<?= $thisDep->depID ?>)" title="Edit">
									<?php add_icon('edit') ?>
								</button>
								<button type="button" class="btn btn-secondary tip" onclick="deleteDependent(this,<?= $thisDep->depID ?>)" title="Delete">
									<?php add_icon('times') ?>
								</button>
							</div>
						</td>
						<td><?= $thisDep->depID ?></td>
						<td><?= $thisDep->fullname ?></td>
						<td><?= $thisDep->dob ?></td>
						<td><?= $thisDep->age ?></td>
					</tr>
<?php 	endforeach; ?>

				</tbody>
			</table>

<?php endif; ?>
		</div> <!-- end card-body -->
		</div> <!-- end card -->

<?php
if ($member->membership->memDescription == 'Orphan'): ?>
		<div class="card well mt-3">
		<div class="card-body">
			<h4>Orphan member</h4>

			<form id="orphanForm" method="post" action="orphan.php">
				<input type="hidden" name="memberID" value="<?= $member->memberID ?>">
<?php
edit_field('membershipID', 'membershipID', false, 11,'membership to join' );
static_submit('Join to membership');
?>
			</form>

			<div class="alert alert-info">
				<ul>
				<li>This is an orphan member (member not linked to a real membership).</li>
				<li>The <b>Renew</b> button will allow you to convert (renew) this membership as either Regular or Joint.</li>
				<li>You can also join this member to an existing Joint membership by entering the Joint membershipID above.</li>
				</ul>
			</div>

		</div> <!-- end card-body -->
		</div> <!-- end card -->
<?php endif; // end if orphan ?>

	</div> <!-- end col -->
</div> <!-- end row -->

<!-- Modal for dependent add or edit -->
<div class="modal fade" id="depModal">
	<div class="modal-dialog">
		<div class="modal-content" id="depModalContent">
			<div class="modal-header">
				<h4 class="modal-title">Dependent</h4>
				<button type="button" class="btn-close" data-bs-dismiss="modal"></button>
			</div>

			<form id="depForm" method="post" action="editDep2.php">
				<input type="hidden" id="depID" name="depID" value="0">
				<input type="hidden" name="memberID" value="<?= $member->memberID ?>">
				<input type="hidden" name="membershipID" value="<?= $member->membershipID ?>">

			<div class="modal-body">

<?php
edit_field('First Name', 'first', false, 30, 'required', false, 'first');
edit_field('Last Name', 'last', false, 30, false, false, 'last');
edit_field('DOB', 'dob', false, 10, 'YYYY-MM-DD', false, 'dob');
?>

			</div> <!-- end modal-body -->

			<div class="modal-footer">
				<div class="btn-group">
					<button type="submit" class="btn btn-primary">Submit</button>
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
				</div>
			</div>
			</form>

        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal -->

<?php end_page(); ?>
