<?php
/* this script holds functions to create blocks of html related to editable forms
 * and is configured to use bootstrap 4
 */


function edit_field($label, $name, $value=false, $maxlength=false, $placeholder=false,
	$help=false, $inputID=false, $labelClass='col-lg-3', $inputClass='col-lg-5') {
	if ($help) {
		$helpTitle = $help[0];
		$helpID = $help[1];
		isset($help[2]) ? $helpLoc = $help[2] : $helpLoc='right';
	}
	?>

<div class="row align-items-center mb-3">
	<label class="text-lg-end <?= $labelClass ?>">
		<b><?= $label ?></b><?php if ($help):
echo '&nbsp;';
Show_Info($helpTitle, getHelp($helpID), $helpLoc);
endif; ?>
	</label>
	<div class="<?= $inputClass ?>">
		<input type="text" class="form-control" name="<?= $name ?>"
		<?php
		if ($inputID) echo ' id="' . $inputID . '"';
		if ($maxlength) echo ' maxlength="' . $maxlength . '"';
		if ($value) echo ' value="' . htmlentities($value) . '"';
		if ($placeholder) echo ' placeholder="' . $placeholder . '"'; ?>>
	</div>
</div>
<?php
}


function edit_field_append($label, $name, $append, $value=false, $maxlength=false, $help=false,
	$inputID=false, $labelClass='col-lg-3', $inputClass='col-lg-5') {
	if ($help) {
		$helpTitle = $help[0];
		$helpID = $help[1];
		isset($help[2]) ? $helpLoc = $help[2] : $helpLoc='right';
	}
?>

<div class="row align-items-center mb-3">
	<label class="text-lg-end <?= $labelClass ?>">
		<?= $label ?><?php if ($help):
echo '&nbsp;';
Show_Info($helpTitle, getHelp($helpID), $helpLoc);
endif; ?>
	</label>
	<div class="<?= $inputClass ?>">
		<div class="input-group">
			<input type="text" class="form-control" name="<?= $name ?>"
<?php
	if ($inputID) echo ' id="' . $inputID . '"';
	if ($maxlength) echo ' maxlength="' . $maxlength . '"';
	if ($value) echo ' value="' . htmlentities($value) . '"'; ?>>
			<span class="input-group-text"><?= $append ?></span>
		</div>
	</div>
</div>
<?php
}


function edit_field_prepend($label, $name, $prepend, $value=false, $maxlength=false, $help=false,
	$inputID=false, $labelClass='col-lg-3', $inputClass='col-lg-5') {
	if ($help) {
		$helpTitle = $help[0];
		$helpID = $help[1];
		isset($help[2]) ? $helpLoc = $help[2] : $helpLoc='right';
	}
?>

<div class="row align-items-center mb-3">
	<label class="text-lg-end <?= $labelClass ?>">
		<b><?= $label ?></b><?php if ($help):
echo '&nbsp;';
Show_Info($helpTitle, getHelp($helpID), $helpLoc);
endif; ?>
	</label>
	<div class="<?= $inputClass ?>">
		<div class="input-group">
			<span class="input-group-text"><?= $prepend ?></span>
			<input type="text" class="form-control" name="<?= $name ?>"
<?php
	if ($inputID) echo ' id="' . $inputID . '"';
	if ($maxlength) echo ' maxlength="' . $maxlength . '"';
	if ($value) echo ' value="' . htmlentities($value) . '"'; ?>>
		</div>
	</div>
</div>
<?php
}


function edit_select_field($label, $name, $options, $myValue=false, $help=false, $inputID=false,
	$labelClass='col-lg-3', $inputClass='col-lg-5') {
	if ($help) {
		$helpTitle = $help[0];
		$helpID = $help[1];
		isset($help[2]) ? $helpLoc = $help[2] : $helpLoc='right';
	}
?>

<div class="row align-items-center mb-3">
	<label class="text-lg-end <?= $labelClass ?>">
		<b><?= $label ?></b><?php if ($help):
echo '&nbsp;';
Show_Info($helpTitle, getHelp($helpID), $helpLoc);
endif; ?>
	</label>
	<div class="<?= $inputClass ?>">
		<select name="<?= $name ?>" class="form-select"<?php if ($inputID) echo ' id="' . $inputID . '"'; ?>>
<?php foreach ($options as $val => $opt): ?>
			<option value="<?= htmlentities($val) ?>"<?php if ($myValue !== false && $myValue == $val) echo ' selected'; ?>>
			<?= $opt ?></option>
<?php endforeach; ?>
		</select>
	</div>
</div>
<?php
}


function edit_textfield($label, $name, $value=false, $rows=false, $help=false, $inputID=false,
	$labelClass='col-lg-3', $inputClass='col-lg-5') {
	if ($help) {
		$helpTitle = $help[0];
		$helpID = $help[1];
		isset($help[2]) ? $helpLoc = $help[2] : $helpLoc='right';
	}
	$textRight = 'text-lg-end';
	$marginBottom = 'mb-3';
	if (strpos($labelClass,'col-md-') !== false) {
		$textRight = 'text-md-end';
		$marginBottom = 'mb-3';
	}
?>

<div class="row align-items-center <?= $marginBottom ?>">
	<label class="<?= $textRight ?> <?= $labelClass ?>">
		<b><?= $label ?></b><?php if ($help):
echo '&nbsp;';
Show_Info($helpTitle, getHelp($helpID), $helpLoc);
endif; ?>
	</label>
	<div class="<?= $inputClass ?>">
		<textarea class="form-control" name="<?= $name ?>"
<?php
if ($inputID) echo ' id="' . $inputID . '"';
if ($rows) echo ' rows="' . $rows . '"'; ?>><?php if ($value) echo htmlentities($value) ?></textarea>
	</div>
</div>
<?php
}


function static_field($label, $value, $help=false, $inputID=false, $labelClass='col-lg-3', $inputClass='col-lg-5') {
	if ($help) {
		$helpTitle = $help[0];
		$helpID = $help[1];
		isset($help[2]) ? $helpLoc = $help[2] : $helpLoc='right';
	}
	$textRight = 'text-lg-end';
	$marginBottom = 'mb-1 mb-lg-3';
	if (strpos($labelClass,'col-md-') !== false) {
		$textRight = 'text-md-end';
		$marginBottom = 'mb-1 mb-md-3';
	}
?>

<div class="row align-items-center <?= $marginBottom ?>">
	<label class="<?= $textRight ?> <?= $labelClass ?>">
		<b><?= $label ?></b><?php if ($help):
echo '&nbsp;';
Show_Info($helpTitle, getHelp($helpID), $helpLoc);
endif; ?>
	</label>
	<div class="<?= $inputClass ?>">
		<div<?php if ($inputID) echo ' id="' . $inputID . '"'; ?>>
<?php if ($value) echo htmlentities($value); ?>
		</div>
	</div>
</div>
<?php
}


function static_submit($text='Submit', $labelClass='col-lg-3', $inputClass='col-lg-5') {
	$marginBottom = 'mb-1 mb-lg-3';
	$display = 'd-lg-block';
	if (strpos($labelClass,'col-md-') !== false) {
		$marginBottom = 'mb-1 mb-md-3';
		$display = 'd-md-block';
	}
	?>
<div class="row <?= $marginBottom ?>">
	<label class="d-none <?= $display ?> <?= $labelClass ?>"></label>
	<div class="<?= $inputClass ?>">
		<button type="submit" class="btn btn-primary"><?= $text ?></button>
	</div>
</div>
<?php
}
