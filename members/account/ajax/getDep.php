<?php
require '../../support/config.php';
require CLASSLOADER;

require AUTH_AJAX; // enforce login


$data = array();
$dep = new Dependents($_POST['depID']);

if ($dep->depID) {
	// security check  - make sure this is a dependent in the user's membership
	$user = new Members('login');
	if ($user->membershipID != $dep->membershipID) {
		// do not allow user to retrieve dep info
		$data['success'] = 0;
	} else {
		$data['depID'] = $dep->depID;
		$data['first'] = $dep->first;
		$data['last'] = $dep->last;
		$data['dob'] = $dep->dob;
		$data['success'] = 1;
	}
} else {
	$data['success'] = 0;
}

echo json_encode($data);
