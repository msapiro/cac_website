<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';
include 'includes/tools.php';

use PHPMailer\PHPMailer\PHPMailer;

$msg = new Msgs($_POST['mailID']);
if (empty($msg->mailID)) {
	error('Error', 'Did not find a message with mailID=' . $_POST['mailID']);
	exit;
}

$toName = $_POST['toName'];
$toAddress = trim($_POST['toAddress']);
$prepend = inty($_POST['prepend']);
$record = inty($_POST['record']);

if ($msg->html) {
	$type = 'HTML';
	$body = $msg->htmlBody;
	if (is_null($body)) {
		$body = ''; // make it an empty string
	}

	if ($msg->useTemplate) {
		// if our message has html, head, or body tags, remove them. these should be only in the template.
		$body = preg_replace('/<html.*<body.*>/siU', '', $body);  // from opening <html> to <body attrib="something">
		$body = preg_replace('/<\/body.*html>/si', '', $body);    // from closing </body> to closing </html>

		// perform the merge
		$template = new Msgs($msg->useTemplate);
		$body = str_replace('[BODY]', $body, $template->htmlBody);
	}

	// where to find image files
	$prependDir = realpath(__DIR__ . '/../../uploads');

	$relativeDir = '/cac/uploads/';
	if ($_SERVER['CONTEXT_PREFIX'] != '/') $relativeDir = $_SERVER['CONTEXT_PREFIX'] . $relativeDir;

	// remove the relativeDir from relative image tags
	$body = preg_replace_callback ('/<img.* src="(.*)".*>/isU',
		function ($matches) use($relativeDir) {
			if (strpos($matches[1], '://') !== false) {
				// contains a protocol, do not modify
				return $matches[0];
			}
			// else remove the relative path
			return str_replace($relativeDir, '', $matches[0]);
		}, $body);

} else {
	$type = 'Plain Text';
	$body = $msg->textBody;
}

$subjectDisplay = $msg->subject;
if ($prepend) $subjectDisplay = '[TEST] ' . $subjectDisplay;

$mail = new PHPMailer();
$mail->isSendmail();  // use system MTA
$mail->CharSet = 'utf-8';
$mail->XMailer = 'CAC Mail'; // default is phpMailer version
$mail->AddCustomHeader("List-Id: CAC Mailer <mailer.californiaalpineclub.org>");  // for Gmail users to filter
$mail->AddCustomHeader("X-List-Id: CAC Mailer <mailer@californiaalpineclub.org>");  // for opendkim signing

start_page('CAC Mailer');
start_content();
toolbar();
?>
<h3 class="pageheader">Test Message Results</h3>

<a class="btn btn-secondary mb-3" href="detail.php?id=<?= $msg->mailID ?>"><?php add_icon('envelope') ?> mail ID: <?= $msg->mailID ?></a>

<p>
Subject: <?= $subjectDisplay ?> <br>
Type: <?= $type ?><?php if ($msg->useTemplate) echo ' using a template'?>
</p>

<?php
if ($mail->validateAddress($msg->envelopeSender)) {
	$mail->Sender = $msg->envelopeSender;
}

if ($mail->validateAddress($toAddress)) {
	$mail->addAddress($toAddress, $toName);
} else {
	show_error('invalid <b>To</b> address: ' . $toAddress);
}

if ($mail->validateAddress($msg->fromAddress)) {
	$mail->setFrom($msg->fromAddress, $msg->fromName);
} else {
	show_error('invalid <b>From</b> address: ' . $msg->fromAddress);
}

if (! empty($msg->replyTo)) {
	if ($mail->validateAddress($msg->replyTo)) {
		$mail->addReplyTo($msg->replyTo); // default is to use fromAddress
	} else {
		show_error('invalid <b>Reply To</b> address: ' . $msg->replyTo);
	}
}

if (! empty($msg->subject)) {
	$mail->Subject = $subjectDisplay;
} else {
	show_error('Subject field is empty');
}

if (empty($body)) show_error('message body is empty');

if ($msg->html) {
	$mail->msgHTML($body,$prependDir);
	if (! empty($msg->textBody)) $mail->AltBody = $msg->textBody;
} else {
	$mail->Body = $msg->textBody;
}

// finally, try to send the message
if ($mail->send()) {
	echo 'Message sent to ' . $toAddress;
	if ($record) {
		$msg->db->insert("mailSent", ["mailID" => $msg->mailID, "email" => $toAddress]);
		echo '<br>recorded email address as <b>Sent</b>';
	}
} else {
	echo 'Mailer Error: ', $mail->ErrorInfo;
}

end_page();

function show_error($text) {
	echo '<p>ERROR: ' . $text . '</p>';
	echo '<p><b>Message not sent!</b></p>';
	end_page();
	exit;
}
?>
