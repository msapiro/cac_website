<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';
// Ensure we came from a POST.
requirePOST();

$msgGroup = new MsgGroups();

$msgGroup->groupID = $_POST['groupID'];
$msgGroup->groupName = $_POST['groupName'];
$msgGroup->queryString = $_POST['queryString'];
$msgGroup->note = $_POST['note'];

$msgGroup->dbSave();

header("Location: detailGroup.php?id=$msgGroup->groupID");
