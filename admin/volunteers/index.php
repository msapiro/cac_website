<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
array_push($allowGroups, 'ROmembers');
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';
include 'includes/tools.php';


$volCat = new VolCats();
$catArray = $volCat->getAll();

$vol = new VolOpps();
$oppArray = array();

foreach ($catArray as $oCat) {
	$oppArray[$oCat->catID] = $vol->getByCategory($oCat->catID);
}

start_page('CAC Volunteers');
add_script('jqueryui');  // for sortable
?>
<script>
$(function() {
	// do stuff when DOM is ready

	 $("tbody.opps").sortable( {
			handle: '.handle',
			axis: "y",
			cursor: "ns-resize",
			opacity: .50,
			update: function() {
				// $(this) is the tbody
		        var order = $(this).sortable('serialize');
				$.post("ajax/oppSorter.php", {input: order});
		    }
		});


});
</script>
<?php
start_content();
toolbar();
?>

<h3 class="pageheader">Volunteer Opportunities</h3>

<div class="row justify-content-center">
	<div class=" col-md-8 col-lg-7 col-xl-6">

	<div class="alert alert-info">Expired members included until dues become delinquent on June 30.</div>


<?php
foreach ($catArray as $oCat):
?>
	<h4 class="mt-4"><?= $oCat->catName ?></h4>
	<table id="catTable" class="table table-striped table-sm">
		<thead>
			<tr><th class="text-center">Order</th><th>Opportunity</th><th class="text-center">Members</th></tr>
		</thead>
		<tbody class="opps">
<?php
	foreach ($oppArray[$oCat->catID] as $oVolOpp):
?>
			<tr id="row_<?= $oVolOpp->oppID ?>" data-id="<?= $oVolOpp->oppID ?>">
				<td class="handle"><?php add_icon('arrows-alt-v') ?></td>
				<td><a href="detail.php?id=<?= $oVolOpp->oppID ?>"><?= $oVolOpp->oppName ?></a></td>
				<td class="text-center"><span class="badge bg-primary bg-pill"><?= $oVolOpp->getCount() ?></span></td>
			</tr>
<?php
	endforeach;
?>
	</tbody>
	</table>

<?php
endforeach;
?>

	</div>
</div>
