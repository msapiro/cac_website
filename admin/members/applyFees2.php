<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
//array_push($allowGroups, 'ROmembers');
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';

requirePost();

inty($_POST['applicantID']);

$applicant = new Applicants($_POST['applicantID']);
if (! $applicant->applicantID) {
	error('Error', 'Did not find an application with applicantID ' . $_POST['applicantID'] . '.');
	exit;
}

$feeFields = ['entryFee', 'dues'];

foreach ($feeFields as $field) $applicant->$field = $_POST[$field];

$applicant->dbSave();

header("Location: applyDetail.php?id=$applicant->applicantID");
exit;
