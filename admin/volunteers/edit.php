<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// array_push($allowGroups, 'ROmembers');
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';
require '../../support/formFunctions.php';
include 'includes/tools.php';


$oppID = inty($_REQUEST['id']);

if ($oppID) {
	$pageTitle = 'Edit Volunteer Opportunity';

	$volOpp = new VolOpps($oppID);
	if (empty($volOpp->oppID)) {
		error('Error', 'Did not find an opportunity with oppID=' . $oppID);
		exit;
	}
} else {
	$pageTitle = 'New Volunteer Opportunity';
	$volOpp = new VolOpps(); // blank object
}


$volCat = new VolCats();
$catArray = $volCat->getAll();

$catSelect = array();
$catSelect[0] = 'Select';
foreach ($catArray as $oCat) {
	$catSelect[$oCat->catID] = $oCat->catName;
}

start_page('CAC Volunteers');
?>
<script>
var oppID = <?= $oppID ?>;

function confirmDelete() {
    var agree=confirm("Deletion cannot be undone.\n\nAre you sure you want to delete this Volunteer Opportunity?");
    if (agree) {
    	$.post('ajax/delete.php', {oppID: oppID}, function() {
			location.href = "index.php";
		});
    }
}
</script>
<?php
start_content();
toolbar($volOpp->oppID);
?>

<div class="row justify-content-center">
	<div class="col-xl-10">

	<div class="row">
		<div class="col-lg-5 offset-lg-3 mt-3"><h3><?= $pageTitle ?></h3></div>
	</div>

	<form action="edit2.php" method="post" id="oppForm">
		<input type="hidden" name="oppID" value="<?= $oppID ?>">
<?php
edit_select_field('Category', 'catID', $catSelect, $volOpp->catID, false, 'catID');
edit_field('Opp. Name', 'oppName', $volOpp->oppName, 50, 'required');
edit_textfield('Description', 'description', $volOpp->description, 4);
static_submit();
?>

	</form>
	</div>
</div>

<?php end_page(); ?>