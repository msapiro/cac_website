<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)
require AUTH_PAGE; // make sure user is logged in and a group member

$title = 'CAC Membership';
$pageDescription = 'New/Edit member';

require '../../support/functions.php';
require '../../support/formFunctions.php';
include 'includes/tools.php';


inty($_GET['id']);
$addJointMember = booly($_REQUEST['add']);
$id = 0;

if (! empty($_GET['id'])) {
	// might be editing member or adding joint member
	$member = new Members($_GET['id'], ['membership', 'wpData']);
	if (empty($member->memberID)) {
		error('Error', 'Did not find a member with memberID=' . $_GET['id']);
		exit;
	}
	$id = $member->memberID; // pass this in our form
}

if ($addJointMember) {
	$pageTitle = "Add a Joint Member";
	$member = new Members(); // overwrite the passed member with a blank
	$classArray = array();

} else if (! empty($_GET['id'])) {
	$pageTitle = "Edit a Member";
	$classArray = (new Classes())->getArray('admin');

} else {
	$pageTitle = "Create a New Member";
	$member = new Members();
	$member->membership = new Memberships();
	$member->membership->class = 0;
	$member->membership->paperTrails = 0;
	$classArray = (new Classes())->getArray('users');
}

// get a list of membershipTypes
$membershipTypesSelect[0] = 'Select';
foreach ($classArray as $oClass) {
	$membershipTypesSelect[$oClass->class] = $oClass->description;
}

start_page($title, $pageDescription);
?>
<script>
var wpID = parseInt("<?= $member->wpID ?>");

$(function() {
    // do stuff when DOM is ready

	$(".pop").popover({trigger: 'hover'}); // info popups

	if (! wpID) {
		$("#btnSuggest").click(function() {
			if ($("#wpUsername").val()) {
				$("#wpUsername").val("")
			} else {
				// construct a suggested wpUsername based on the first and last names
				var suggest = ( $("#first").val() + "_" + $("#last").val() ).toLowerCase();
				suggest = suggest.replace(/\W/g, ''); // remove non-alphanumeric characters
				$("#wpUsername").val(suggest);
			}
		});
	}

    $("#paperTrails").click(function() {
		if ( $(this).prop("checked") ) {
			var msg = "Every member with a valid email receives an electronic version of Trails."
				+ "\nPrinting and mailing the newsletter is costly. Don't send paper Trails"
				+ " unless the member requested it!"
				+ "\n\n(Uncheck the box to cancel paper delivery)";
			alert(msg);
		}
    });

});

</script>
<?php
start_content();
toolbar($member->memberID);
?>

<h2 class="pageheader"><?= $pageTitle ?></h2>

<div class="row">
	<div class="col-lg-10 offset-lg-1">

	<form id="memberForm" method="post" action="edit2.php">
		<input type="hidden" name="memberID" value="<?= $id ?>">
		<input type="hidden" name="addJointMember" value="<?= $addJointMember ?>">
<?php
if ($member->memberID && ! $addJointMember) static_field('CAC memberID', $member->memberID);

edit_field('First Name', 'first', $member->first, 30, false, false, 'first');
edit_field('Middle Initial', 'm_i', $member->m_i, 20);
edit_field('Last Name', 'last', $member->last, 30, false, false, 'last');
edit_field('Email', 'email', $member->email, 100, false, false, 'email');
edit_field('Phone', 'phone', phone_number($member->phone), 20);
edit_field('Mobile Phone', 'mobile', phone_number($member->mobile), 20);
edit_field('Date of Birth', 'dob', $member->dob, 10, 'YYYY-MM-DD');

if ($member->memberID && ! $addJointMember) {
	static_field('CAC membershipID', $member->membershipID);
}

if (! $addJointMember):
	edit_select_field('Membership Type', 'membershipType', $membershipTypesSelect,
		$member->membership->class, false, false);
	edit_field('Street Address', 'street', $member->membership->street, 120);
	edit_field('City', 'city', $member->membership->city, 50);
	edit_field('State', 'state', $member->membership->state, 2);
	edit_field('Zip', 'zip', $member->membership->zip, 10);
	edit_field('Sponsors', 'sponsors', $member->membership->sponsors, 100);
?>

	<div class="row align-items-center">
		<label class="col-lg-3 text-lg-end"><b>Trails Newsletter</b></label>
		<div class="col-lg-7 mt-2">
			<div class="form-check">
				<input class="form-check-input" type="checkbox" name="paperTrails" id="paperTrails"
					value="1"<?php if ($member->membership->paperTrails) echo ' checked' ?>>
				<label class="form-check-label" for="paperTrails">
					Needs a mailed paper copy of Trails
				</label>
			</div>
		</div>
	</div>
	<div class="row align-items-center">
		<label class="col-lg-3 text-lg-end"><b>Resignation</b></label>
		<div class="col-lg-7 mt-2">
			<div class="form-check">
				<input class="form-check-input" type="checkbox" name="resigned" id="resigned"
					value="1"<?php if ($member->membership->resigned) echo ' checked' ?>>
				<label class="form-check-label" for="resigned">
					Member has indicated non-renewal/resignation
				</label>
			</div>
		</div>
	</div>
<?php endif; ?>

<hr class="cac">

<?php
if ($member->wpID):
	// already have a WP acount
	static_field('WP user_id', $member->wpID);
	static_field('WP Username', $member->wpData->user_login);
	edit_field('WP First Name', 'first_name', $member->wpData->wpFirst, 30);
	edit_field('WP Last Name', 'last_name', $member->wpData->wpLast, 30);
	edit_field('WP Email', 'wpEmail', $member->wpData->wpEmail, 100, false, false, 'wpEmail');
	edit_field('Assign to WP user_id', 'wpAssign', false, 10,
		'Leave blank to edit existing WP user');
else:
	// do not have a WP account
?>

<div class="row mb-3">
	<label class="text-lg-end col-lg-3">
		<b>WP Username</b>
	</label>
	<div class="col-lg-5">
		<div class="input-group">
			<button class="btn btn-secondary" type="button" id="btnSuggest">Suggest</button>
			<input type="text" class="form-control" name="user_login" id="wpUsername"
				maxlength="60">
		</div>
	</div>
</div>

<?php
	edit_field('Assign to WP user_id', 'wpAssign', false, 10, 'Leave blank to create new WP user');
endif;

static_submit();
?>
	</form>

<div class="alert alert-info">
<?php if ($member->memberID): ?>

<b>Editing Members</b>
	<ul>
<?php if ($member->wpID): ?>
		<li>If you provide an integer in <b>Assign to WP user_id</b> then the member will
		be linked to that WordPress account.</li>
		<li>If you put the word <b>clear</b> in <b>Assign to WP user_id</b> then the member will be
		unlinked from WordPress.</li>
		<li>In either case, the currently linked WP account will not be deleted from WordPress.</li>
<?php else: ?>
		<li>The <b>WP Username</b> will be used for login. Once set, it cannot be changed.</li>
		<li>Leave <b>WP Username</b> blank if you do not want to create a new WordPress account.</li>
		<li>The program can suggest a valid WP Username based on the First and Last names, but
		you can change it.</li>
		<li>If you provide an integer in <b>Assign to WP user_id</b> then the new member will
		be associated with that WordPress account, and <b>WP Username</b> will be ignored.</li>
<?php endif; ?>

	</ul>
<?php else: ?>

	<b>Creating New Members</b>
	<ul>
		<li>The <b>WP Username</b> will be used for login. Once set, it cannot be changed.</li>
		<li>Leave <b>WP Username</b> blank if you do not want to create a new WordPress account.</li>
		<li>The program can suggest a valid WP Username based on the First and Last names, but
		you can change it.</li>
		<li>If you provide an integer in <b>Assign to WP user_id</b> then the new member will
		be associated with that WordPress account, and <b>WP Username</b> will be ignored.</li>
		<li>First Name, Last Name, and Email will be set in both WordPress and CAC.</li>
	</ul>

<?php endif; ?>
</div>

	</div>
</div>

<?php end_page(); ?>
