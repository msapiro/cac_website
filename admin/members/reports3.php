<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
//array_push($allowGroups, 'ROmembers', 'trainee');
require AUTH_PAGE; // make sure user is logged in and a group member

require '../../support/functions.php';


// Get the database.
$db = (new Model(false))->db;

// Get classes.
$rows = $db->select("classes", ['class']);
$cutoff = date('Y-m-d', time() - 3600*24*91); // now - 91 days grace
$count = array();
foreach ($rows as $row) {
	$clsCount = $db->count("memberships",
		["AND"=>["expiration[>=]"=>$cutoff, "resigned"=>0,
			"class"=>[$row['class']]]
		]);
	$count[$row['class']] = $clsCount;
}
$total_1 = 0;
foreach (['ST', 'RG', 'JT', 'SR', 'JS', 'LM', 'JL'] as $x) {
	$total_1 += $count[$x];
}
$total_2 = $total_1;
foreach (['JT', 'JS', 'JL'] as $x) {
	$total_2 += $count[$x];
}
echo "<pre>";
echo "\nStudent Memberships:      " . sprintf('%3d', $count['ST']);
echo "\nRegular Memberships:      " . sprintf('%3d', $count['RG']);
echo "\nJoint Memberships:        " . sprintf('%3d', $count['JT']);
echo "\nSenior Memberships:       " . sprintf('%3d', $count['SR']);
echo "\nJoint Senior Memberships: " . sprintf('%3d', $count['JS']);
echo "\nLife Memberships:         " . sprintf('%3d', $count['LM']);
echo "\nJoint Life Memberships:   " . sprintf('%3d', $count['JL']);
echo "\n-----------------------------";
echo "\nTotal Memberships:        " . sprintf('%3d', $total_1);
echo "\nTotal Members:            " . sprintf('%3d', $total_2);
echo "\n</pre>";

?>
