<?php
require '../support/config.php';
require CLASSLOADER;

require 'includes/config.php'; // contains $allowGroups for this directory
// $allowGroups = array('some','groups', 'here'); // override config for this page (optional)

if (isset($allowGroups)) {
	require AUTH_PAGE; // make sure user is logged in and a group member
}

require '../../support/functions.php';

// set membership expiration from CAC database into new alpine database

$proceed = booly($_GET['proceed']);

start_page('CAC Migration');
start_content();

if (! $proceed): ?>

<div class="alert alert-danger mt-4">
<p><b>WARNING</b>:<br>
This will import membership expiration dates from <b>CAC.membership</b> into <b>alpine.memberships</b>.<br>
Historical membership years will also be imported into <b>alpine.history</b>, after truncating the table.</p>
</div>

<a class="btn btn-secondary" href="<?= $_SERVER['PHP_SELF'] . '?proceed=1' ?>">Continue</a>

<?php
end_page();
exit;
endif;

// set up a read-only connection to the CAC database
require_once '/www/cac/data/credentials/migrate.php';
$db_m = new Medoo\Medoo($db_migrate);

// and a read-write connection to the alpine database
$model = new Model();
$db = $model->db;
$db->query("TRUNCATE TABLE <history>");

$rows = $db_m->select("membership",
	[
	"membershipID",
	"cls",
	"memb11_12",
	"memb12_13",
	"memb13_14",
	"memb14_15",
	"memb15_16",
	"memb16_17",
	"memb17_18",
	"memb18_19",
	"memb19_20",
	"memb20_21",
	"memb21_22"
	],
	["ORDER" => "membershipID"]
	);

$numMemberships = count($rows);
$numUpdated = 0;

// expiration date will be based on the latest of the years membership was active
// so cycle through starting at earliest year for which  we have record
foreach($rows as $row) {
	extract($row);

	$memberYears = array();
	$expiration = false;

	if ($memb11_12) {
		$memberYears[] = 2011;
		$expiration = '2012-03-31';
	}
	if ($memb12_13) {
		$memberYears[] = 2012;
		$expiration = '2013-03-31';
	}
	if ($memb13_14) {
		$memberYears[] = 2013;
		$expiration = '2014-03-31';
	}
	if ($memb14_15) {
		$memberYears[] = 2014;
		$expiration = '2015-03-31';
	}
	if ($memb15_16) {
		$memberYears[] = 2015;
		$expiration = '2016-03-31';
	}
	if ($memb16_17) {
		$memberYears[] = 2016;
		$expiration = '2017-03-31';
	}
	if ($memb17_18) {
		$memberYears[] = 2017;
		$expiration = '2018-03-31';
	}
	if ($memb18_19) {
		$memberYears[] = 2018;
		$expiration = '2019-03-31';
	}
	if ($memb19_20) {
		$memberYears[] = 2019;
		$expiration = '2020-03-31';
	}
	if ($memb20_21) {
		$memberYears[] = 2020;
		$expiration = '2021-03-31';
	}
	if ($memb21_22) {
		$memberYears[] = 2021;
		$expiration = '2022-03-31';
	}
	if (in_array(strtolower($cls), array('lm', 'jl'))) {
		$expiration = '2099-12-31';
	}

	if ($expiration) {
		$db->update("memberships", ["expiration" => $expiration], ["membershipID" => $membershipID]);
		$numUpdated++;
	}

	if ($memberYears) {
		foreach ($memberYears as $thisYear) {
			$db->insert("history", ["membershipID" => $membershipID, "year" => $thisYear]);
		}
	}
}
?>

<p>Processed <?= $numMemberships ?> memberships from CAC database.</p>
<p>Updated expiration date and history for <?= $numUpdated ?> memberships.
<p>Done.</p>
<p><a href="index.php">Setup and Migration</a></p>

<?php end_page(); ?>
