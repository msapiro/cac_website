<?php
// auth (login) is not required for this page
require '../support/config.php';
require CLASSLOADER;

// after making a payment on the Stripe site, user is re-directed to this page
// be careful because malicious user could simply load this page without making payment

$title = 'CAC Payment';
$pageDescription = 'California Alpine Club Payment Result';


require '../../support/functions.php';

$stripe = new Stripes($_GET['id']);

if (empty($stripe->stripeID)) {
	error('Error', 'Could not find your Stripe transaction.');
	exit;
}

isset($_GET['sessionID']) ? $sessionID = $_GET['sessionID'] : $sessionID = 0;

$pollStripe = true;
if ($stripe->status == 'ok') $pollStripe = false; // already processed, no need to poll Stripe

start_page($title, $pageDescription);
?>
<script>
"use strict";
var stripeID = "<?= $stripe->stripeID ?>";
var sessionID = "<?= $sessionID ?>";
var pollStripe = <?= json_encode($pollStripe) ?>; // convert php boolean to javascript boolean

$(function() {
    // do stuff when DOM is ready

	if (pollStripe) {
		// verify that the Stripe transaction succeeded
		$.post('ajax/pollStripe.php', {stripeID: stripeID, sessionID: sessionID}, function(data) {
			if (data.success) {
				$("#successText").html(data.msg).show();
				if (data.btnText) {
					$("#btnNext").append(data.btnText).show();
				}
			} else {
				$("#errorText").html(data.msg).show();
			}
		}, "json");
	}

});
</script>
<?php
start_content();
?>

<h2 class="pageheader">Payment</h2>

<div class="row">
	<div class="col-xl-10 offset-xl-1">

<?php if ($pollStripe): ?>
		<div class="alert alert-success">
			<p><?php add_icon('hourglass') ?> Your charge succeeded. Attempting to verify payment. Please be patient&nbsp;...</p>
		</div>

		<div id="successText" class="alert alert-success mt-3" style="display:none">
			<!-- ajax will fill -->
		</div>
<?php else: ?>
		<div class="alert alert-success">
			<?php add_icon('check-circle') ?> Your payment of $<?= $stripe->stripeAmount ?> succeeded at <?= $stripe->modified ?>.
		</div>
<?php endif; ?>

		<a id="btnNext" href="../account/index.php" class="btn btn-secondary" style="display:none">
			<?php add_icon("user") ?>&nbsp;
		</a>

		<div id="errorText" class="alert alert-danger mt-3" style="display:none">
			<!-- ajax will fill -->
		</div>
	</div>
</div>

<?php end_page(); ?>